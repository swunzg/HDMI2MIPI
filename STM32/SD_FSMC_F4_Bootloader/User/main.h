#ifndef _MAIN_H_
#define _MAIN_H_

#include "stm32fxxx.h"

#include "sdio_sd.h"
#include "string.h"
#include "diskio.h"
#include "ff.h"
#include "Pic_Process.h"
#include "IOConfig.h"
#include "InternalFlash.h"
#include "timer.h"
#include "ics307.h"
#include "delay.h"
#include "uart.h"
#include "sys.h"
#include <stdio.h> 
#include <stdlib.h> 

#include <ctype.h>                      /* Character functions                */
#include "cmsis_os.h"                   /* CMSIS RTOS definitions             */
#include "rl_fs.h"                      /* FileSystem definitions             */
//#include "Terminal.h"

#include "nand.h"

#define SD_Disc			0
#define Nand_Disc		1
#define SD_Path			"0:"
#define Nand_Path		"N0:"
#endif

