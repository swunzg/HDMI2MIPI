/*********************************************************************************************************
*
* File                : main.c
* Hardware Environment: 
* Build Environment   : RealView MDK-ARM  Version: 4.20
* Version             : V1.0
* By                  : 
*
*                                  (c) Copyright 2005-2011, WaveShare
*                                       http://www.waveshare.net
*                                          All Rights Reserved
*
*********************************************************************************************************/

/* Includes ------------------------------------------------------------------*/
#include "main.h"

FILE *fout;
FRESULT res = FR_NOT_READY;
FATFS SD_fatfs;
FIL sd_file;
u32 br;
u8 buffer[512];
u32 WriteAddress = 0x08020000; 
u8 err_code=0;

typedef  void (*iapfun)(void);				//定义一个函数类型的参数.
iapfun jump2app; 
//跳转到应用程序段
//appxaddr:用户代码起始地址.
void iap_load_app(u32 appxaddr)
{
	if(((*(vu32*)appxaddr)&0x2FFE0000)==0x20000000)	//检查栈顶地址是否合法.
	{ 
		jump2app=(iapfun)*(vu32*)(appxaddr+4);		//用户代码区第二个字为程序开始地址(复位地址)	
		MSR_MSP(*(vu32*) appxaddr);					//初始化APP堆栈指针(用户代码区的第一个字用于存放栈顶地址)
		jump2app();									//跳转到APP.
	}
}

int main(void)
{   
    u32 i = 0;
	u32 bin_buffer[128];
	HardWare_Init();  
    if((Nand_FS_Init(Nand_Path) == fsOK))
    {
        fout = fopen ("N0:F4Updata.bin","r");
        if(fout != NULL)
        {
            FLASH_Unlock();
            if(FLASH_EraseSector(FLASH_Sector_5, VoltageRange_3) ==FLASH_COMPLETE)
            {
                for(;;)
                {
                    br = fread(buffer,sizeof(uint8_t), sizeof(buffer), fout); 
                    if(br==0)
                        break;
                    for(i=0;i<(br/4);i++)
                    {
                            bin_buffer[i]=((uint32_t)buffer[(i*4)+3]<<24)+((uint32_t)buffer[(i*4)+2]<<16)+((uint32_t)buffer[(i*4)+1]<<8)+((uint32_t)buffer[i*4]);
                    }

                    if (FLASH_If_Write(&WriteAddress, bin_buffer, br/4) != 0)
                    {
                            err_code=1;
                    }	
                    GPIO_WriteBit( LEDPeriph_GPIO,  LED1,  (BitAction)(1 -  GPIO_ReadOutputDataBit( LEDPeriph_GPIO, LED1 )));
                    delay_ms(20);     
                }
            }
            fclose(fout);
            
            fdelete("N0:F4Updata.bin","1");
            FLASH_Lock();          
        }
        else
        {
            if (disk_initialize(SD_Disc))		//SD卡初始化失败
            {
         //       printf(" SD卡初始化失败，请检查...\r\n");
            }
            else
            {
                res = f_mount(&SD_fatfs,SD_Path,1);		//挂载SD卡 
                if(res == FR_OK)
                {
                    res=f_open(&sd_file,"0:/code/STM32F4XX_Updata.bin",FA_OPEN_EXISTING|FA_READ);
                    if(res==FR_OK)
                    {
                        FLASH_Unlock();
                        if(FLASH_EraseSector(FLASH_Sector_5, VoltageRange_3) ==FLASH_COMPLETE)
                        {
                            for(;;)
                            {
                                res=f_read(&sd_file,buffer,sizeof(buffer),&br);
                                if(res||br==0)
                                    break;
                                for(i=0;i<(br/4);i++)
                                {
                                        bin_buffer[i]=((uint32_t)buffer[(i*4)+3]<<24)+((uint32_t)buffer[(i*4)+2]<<16)+((uint32_t)buffer[(i*4)+1]<<8)+((uint32_t)buffer[i*4]);
                                }

                                if (FLASH_If_Write(&WriteAddress, bin_buffer, br/4) != 0)
                                {
                                        err_code=1;
                                }	
                                GPIO_WriteBit( LEDPeriph_GPIO,  LED1,  (BitAction)(1 -  GPIO_ReadOutputDataBit( LEDPeriph_GPIO, LED1 )));
                                delay_ms(20);
                                    
                            }
                        }
                        FLASH_Lock();           
                    }

                    f_close(&sd_file);
                    
                }
            }
                
        }
        if(err_code==0)
            iap_load_app(0x08020000);   


        fout = fopen ("N0:F4Updata_Bak.bin","r");
        if(fout != NULL)
        {
            FLASH_Unlock();
            if(FLASH_EraseSector(FLASH_Sector_5, VoltageRange_3) ==FLASH_COMPLETE)
            {
                for(;;)
                {
                    br = fread(buffer,sizeof(uint8_t), sizeof(buffer), fout); 
                    if(br==0)
                        break;
                    for(i=0;i<(br/4);i++)
                    {
                            bin_buffer[i]=((uint32_t)buffer[(i*4)+3]<<24)+((uint32_t)buffer[(i*4)+2]<<16)+((uint32_t)buffer[(i*4)+1]<<8)+((uint32_t)buffer[i*4]);
                    }

                    if (FLASH_If_Write(&WriteAddress, bin_buffer, br/4) != 0)
                    {
                            err_code=1;
                    }	
                    GPIO_WriteBit( LEDPeriph_GPIO,  LED1,  (BitAction)(1 -  GPIO_ReadOutputDataBit( LEDPeriph_GPIO, LED1 )));
                    delay_ms(20);     
                }
            }
            fclose(fout);
            
            FLASH_Lock();          
        }        
    }

    if(err_code==0)
		iap_load_app(0x08020000); 
    
    while (1)
    {    
        GPIO_SetBits(LEDPeriph_GPIO, LED2);
        delay_ms(500);
        GPIO_ResetBits(LEDPeriph_GPIO, LED2);
        delay_ms(500);
    }
}


/******************* (C) COPYRIGHT 2008 STMicroelectronics *****END OF FILE****/
