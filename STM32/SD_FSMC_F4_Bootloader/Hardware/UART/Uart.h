#ifndef USART_H
#define	USART_H
#include <stm32fxxx.h>
#include "main.h"

#define Debug_Mode 		1
#define Normal_Mode 	0

extern u16 crc16_data;
extern u8  Working_Mode;

u16 crc16(u8 *addr, u16 num, u16 crc);
void USART1_config(void);
void USART2_config(void);
void stdout_putchar(u8 ch);
void stdout_putchar2(u8 ch);
void Mode_Display(void);

#endif

