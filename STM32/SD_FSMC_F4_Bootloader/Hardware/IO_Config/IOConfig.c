#include "IOConfig.h"

void LED_IO_init()
{
  GPIO_InitTypeDef GPIO_InitStructure;  
  RCC_AHB1PeriphClockCmd(RCC_LEDPeriph_GPIO , ENABLE);  
    
  GPIO_InitStructure.GPIO_Pin = LED1|LED2;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL ;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_Init(LEDPeriph_GPIO,&GPIO_InitStructure);
}


void HardWare_Init(void)
{   

	SysTick_Init();
	NVIC_Configuration();
	USART1_config();
    LED_IO_init();			//LED���ų�ʼ��
    NAND_Init();
}
