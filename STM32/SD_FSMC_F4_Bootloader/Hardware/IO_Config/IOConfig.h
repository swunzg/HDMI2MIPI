#ifndef _IOCONFIG_H
#define _IOCONFIG_H

#include "stm32fxxx.h"
#include "main.h"

#define RCC_LEDPeriph_GPIO				RCC_AHB1Periph_GPIOC
#define LED1							GPIO_Pin_13
#define LED2							GPIO_Pin_14
#define LEDPeriph_GPIO					GPIOC

#define LED_HIGH						GPIO_SetBits	( LEDPeriph_GPIO,  LED1)
#define LED_LOW							GPIO_ResetBits( LEDPeriph_GPIO,  LED1)

#define LED2_HIGH						GPIO_SetBits	( LEDPeriph_GPIO,  LED2)
#define LED2_LOW							GPIO_ResetBits( LEDPeriph_GPIO,  LED2)


void LED_IO_init(void);
void HardWare_Init(void);

#endif

