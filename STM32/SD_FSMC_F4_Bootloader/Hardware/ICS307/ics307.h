#ifndef _ICS307_H
#define _ICS307_H
#include "stm32fxxx.h"
#include "delay.h"

#define RCC_ICS307_CLK_Periph_GPIO			RCC_AHB1Periph_GPIOB
#define ICS307_CLK											GPIO_Pin_6
#define ICS307_CLK_Periph_GPIO					GPIOB

#define RCC_ICS307_DATA_Periph_GPIO			RCC_AHB1Periph_GPIOB
#define ICS307_DATA											GPIO_Pin_7
#define ICS307_DATA_Periph_GPIO					GPIOB

#define RCC_ICS307_STROBE_Periph_GPIO		RCC_AHB1Periph_GPIOB
#define ICS307_STROBE										GPIO_Pin_8
#define ICS307_STROBE_Periph_GPIO				GPIOB

void ICS307_IO_init(void);
u32 calculate_idt307_config(float output_clk);
void ICS307_HighLow_Set(float pclk,u32 ICS307_Data);
void ISC307_config(u32 config);
void ICS307_ValidClk_Set(u32 ICS307_Data);
#endif

