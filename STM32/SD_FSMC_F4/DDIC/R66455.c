#include "debug.h"
#include "../DDIC/AllDDIC.h"

/********************************************************************************************
********************************************************************************************
    R66455  写寄存器 读寄存器  写Gamma 读Gamma OTP烧录
********************************************************************************************
********************************************************************************************/

void SSD2828_W_Array_Demura			(u8 SigMode ,u8 channel,u8 para[],u8 offerset);
void R66455_Write_Register		 	(USB_OTG_CORE_HANDLE *pdev,uint8_t  ep_addr,u8 SigMode ,u8 channel,u8 buffer[],u16 LP_B7_Data,u16 HS_B7_Data);
void R66455_Write_Register_Demura	(USB_OTG_CORE_HANDLE *pdev,uint8_t  ep_addr,u8 SigMode ,u8 channel,u8* buffer,u16 LP_B7_Data,u16 HS_B7_Data);
void R66455_Read_Register		 	(USB_OTG_CORE_HANDLE *pdev,uint8_t  ep_addr,u8 SigMode ,u8 channel,u8* buffer,u16 LP_B7_Data,u16 HS_B7_Data);
void R66455_Write_Gamma			 	(USB_OTG_CORE_HANDLE *pdev,uint8_t  ep_addr,u8 SigMode ,u8 channel,u8* buffer,u16 LP_B7_Data,u16 HS_B7_Data);
void R66455_Gamma_OTP_Start		 	(USB_OTG_CORE_HANDLE *pdev,uint8_t  ep_addr,u8 SigMode ,u8 channel,u8* buffer,u16 LP_B7_Data,u16 HS_B7_Data);
void R66455_DLL_Calculation		 	(USB_OTG_CORE_HANDLE *pdev,uint8_t  ep_addr,u8 SigMode ,u8 channel,u8* buffer,u16 LP_B7_Data,u16 HS_B7_Data);	

u32 DemuraAddHL=0xf00-256;
void DemuraFlagStart(void)
{
	DemuraAddHL=0xf00-256;
}
void SSD2828_W_Array_Demura(u8 SigMode ,u8 channel,u8 para[],u8 offerset)
{
	if(SigMode == Mipi_Mode)
		{   DemuraAddHL +=256; 
			SSD2828_W_Reg(SigMode ,channel,0xBC, 2);
			SSD2828_W_Reg(SigMode ,channel,0xBD, 0x0000);
			SSD2828_W_Cmd(SigMode ,channel,0xBF);	
			SSD2828_W_Data(SigMode ,channel,0xb0);
			SSD2828_W_Data(SigMode ,channel,0x04);
			//delay_ms(10);
			
			SSD2828_W_Reg(SigMode ,channel,0xBC, 6);
			SSD2828_W_Reg(SigMode ,channel,0xBD, 0x0000);
			SSD2828_W_Cmd(SigMode ,channel,0xBF);
			SSD2828_W_Data(SigMode ,channel,0xf3);
			SSD2828_W_Data(SigMode ,channel,0x6);
			SSD2828_W_Data(SigMode ,channel,0x0);
			SSD2828_W_Data(SigMode ,channel,0x0);
			SSD2828_W_Data(SigMode ,channel,0x0);
			SSD2828_W_Data(SigMode ,channel,0x0);
			//delay_ms(10);
			
			SSD2828_W_Reg(SigMode ,channel,0xBC, 2);
			SSD2828_W_Reg(SigMode ,channel,0xBD, 0x0000);
			SSD2828_W_Cmd(SigMode ,channel,0xBF);
			SSD2828_W_Data(SigMode ,channel,0xf2);
			SSD2828_W_Data(SigMode ,channel,0x11);
			//delay_ms(10);
			
			SSD2828_W_Reg(SigMode ,channel,0xBC, 6);
			SSD2828_W_Reg(SigMode ,channel,0xBD, 0x0000);
			SSD2828_W_Cmd(SigMode ,channel,0xBF);
			SSD2828_W_Data(SigMode ,channel,0xf3);
			SSD2828_W_Data(SigMode ,channel,0x2);
			SSD2828_W_Data(SigMode ,channel,(DemuraAddHL)&0xff);
			SSD2828_W_Data(SigMode ,channel,(DemuraAddHL>>8)&0xff);
			SSD2828_W_Data(SigMode ,channel,(DemuraAddHL>>16)&0xff);
			SSD2828_W_Data(SigMode ,channel,0xFF);
			
			//delay_ms(10);
			SSD2828_W_Reg(SigMode ,channel,0xBC, 1+256);
			SSD2828_W_Reg(SigMode ,channel,0xBD, 0x0000);
			SSD2828_W_Cmd(SigMode ,channel,0xBF);
			SSD2828_W_Data(SigMode ,channel,0xf4);		
			for(i = 1; i <= 256; i++)
			{
				SSD2828_W_Data(SigMode ,channel,para[i+1+offerset]);
			}
			//delay_ms(10);
			SSD2828_W_Reg(SigMode ,channel,0xBC, 2);
			SSD2828_W_Reg(SigMode ,channel,0xBD, 0x0000);
			SSD2828_W_Cmd(SigMode ,channel,0xBF);	
			SSD2828_W_Data(SigMode ,channel,0xf2);
			SSD2828_W_Data(SigMode ,channel,0x1b);
			delay_ms(12);
		}	
}

void R66455_Write_Register_Demura(USB_OTG_CORE_HANDLE *pdev,uint8_t  ep_addr,u8 SigMode ,u8 channel,u8* buffer,u16 LP_B7_Data,u16 HS_B7_Data)
{
    //SSD2828_W_Reg(SigMode,channel,0xB7,(LP_B7_Data&0xFBFF)|0x0400); 
		SSD2828_W_Reg(SigMode,channel,0xB7,(HS_B7_Data|0x0410)&0XFFFD);		
    delay_ms(1);
    SSD2828_W_Array_Demura(SigMode,channel,buffer,5);// 6个首地址

    SSD2828_W_Reg(SigMode,channel,0xB7,HS_B7_Data);
    delay_ms(1);
    buffer[4] = Uart_Error_None;
    STM2PC_RM671xx(pdev,CDC_IN_EP,buffer,buffer[2]+3);  //待修改         //返回 写寄存器状态  ：ok
}
void R66455_Write_Register(USB_OTG_CORE_HANDLE *pdev,uint8_t  ep_addr,u8 SigMode ,u8 channel,u8* buffer,u16 LP_B7_Data,u16 HS_B7_Data)
{
    //SSD2828_W_Reg(SigMode,channel,0xB7,(LP_B7_Data&0xFBFF)|0x0400); 
		SSD2828_W_Reg(SigMode,channel,0xB7,(HS_B7_Data|0x0410)&0XFFFD);		
    delay_ms(5);
	
		if( (buffer[1]&0xc0)>0)
		SSD2828_W_Array_Demura(SigMode,channel,buffer,2);
    else
		SSD2828_W_Array(SigMode,channel,buffer,2);

    SSD2828_W_Reg(SigMode,channel,0xB7,HS_B7_Data);
    delay_ms(5);
    buffer[4] = Uart_Error_None;
    STM2PC_RM671xx(pdev,CDC_IN_EP,buffer,buffer[2]+3);           //返回 写寄存器状态  ：ok
}
void R66455_Read_Register(USB_OTG_CORE_HANDLE *pdev,uint8_t  ep_addr,u8 SigMode ,u8 channel,u8* buffer,u16 LP_B7_Data,u16 HS_B7_Data)
{
    u16 tmp;
		u8 buffer1[3];
    SSD2828_W_Reg(SigMode,channel,0xB7,LP_B7_Data|0x0080);                               
    SSD2828_W_Reg(SigMode,channel,0xC1,buffer[2]-1); //return package size                                
    SSD2828_W_Reg(SigMode,channel,0xC0,0x0001); //reset
	
    buffer1[0] = 0x01;
    buffer1[1] = buffer[3]; 	//download register 
    SSD2828_W_Array(SigMode,channel,buffer1,0);
    delay_ms(5);  
    SSD2828_W_Reg(SigMode,channel,0xD4,0x00FA);
    SSD2828_W_Cmd(SigMode,channel,0xFF);
    for(i=0;i<buffer[2]-1;i++)  //data read out length
    {
        SSD2828_W_Cmd(SigMode,channel,0xFA);		
        tmp=SPI3_Read_u16_Data(channel);
        buffer[4+i]=tmp>>8;
        buffer[5+i]=tmp;
        delay_ms(5);
        i++;
    }
    SSD2828_W_Reg(SigMode,channel,0xB7,HS_B7_Data);
    STM2PC_RM671xx(pdev,CDC_IN_EP,buffer,buffer[2]+3);           //返回 读取的 寄存器数据  ：ok 
}
void R66455_Write_Gamma(USB_OTG_CORE_HANDLE *pdev,uint8_t  ep_addr,u8 SigMode ,u8 channel,u8* buffer,u16 LP_B7_Data,u16 HS_B7_Data)
{
		u8 buffer1[3];
		//red V1023=5.3750 ,v0
		//green v1023=4.7822 ,v0
		//blue v1023=5.3982 v6.124
		u8 r66455_cxh[]={0x91,0xCC,0x00,0x00,0x00,0x00,0x00,0x00,0x03,0xFF,0x0F,0xFC,0x03,0xFF,0x0F,0xFC,0x03,0xFF,0x0F,0xFC,0x03,0xFF,0x0F,0xFC,0x03,0xFF
										,0x0F,0xFC,0x03,0xFF,0x0F,0xFC,0x03,0xFF,0x0F,0xFC,0x03,0xFF,0x0F,0xFC,0x03,0xFF,0x0F,0xFC,0x03,0xFF,0x0F,0xFC,0x0F,0xFC,0x00,0x00
										,0x00,0x00,0x00,0x00,0x03,0xFF,0x0F,0xFC,0x03,0xFF,0x0F,0xFC,0x03,0xFF,0x0F,0xFC,0x03,0xFF,0x0F,0xFC,0x03,0xFF,0x0F,0xFC,0x03,0xFF
										,0x0F,0xFC,0x03,0xFF,0x0F,0xFC,0x03,0xFF,0x0F,0xFC,0x03,0xFF,0x0F,0xFC,0x03,0xFF,0x0F,0xFC,0x0F,0xFC,0x00,0x00,0x00,0x00,0x00,0x00
										,0x03,0xFF,0x0F,0xFC,0x03,0xFF,0x0F,0xFC,0x03,0xFF,0x0F,0xFC,0x03,0xFF,0x0F,0xFC,0x03,0xFF,0x0F,0xFC,0x03,0xFF,0x0F,0xFC,0x03,0xFF
										,0x0F,0xFC,0x03,0xFF,0x0F,0xFC,0x03,0xFF,0x0F,0xFC,0x03,0xFF,0x0F,0xFC,0x0F,0xFC
										};  //145 data
		//RED=P7-P10     : X2_R[9:8],X2_R[7:0],Y2_R[11:8],Y2_R[7:0]
		//GREEN=P55-P58  : X2_G[9:8],X2_G[7:0],Y2_G[11:8],Y2_G[7:0]
		//BLUE=P103-P106 : X2_B[9:8],X2_B[7:0],Y2_B[11:8],Y2_B[7:0]
		SSD2828_W_Reg(SigMode,channel,0xB7,(HS_B7_Data|0x0410)&0XFFFD);														
		//SSD2828_W_Reg(SigMode,channel,0xB7,(LP_B7_Data&0xFBFF)|0x0400); 
		delay_ms(5);	
		if(buffer[1]&0x80)
		{
			r66455_cxh[1]=0xCC; //HBM
		}
		else
		{			
			r66455_cxh[1]=0xCB;	//normal
		}			
//RED		
		r66455_cxh[8]= buffer[3];//X2_LV >> 8
		r66455_cxh[9]= buffer[4];//X2_LV
		r66455_cxh[10]= buffer[5];//Y2_R >> 8
		r66455_cxh[11]= buffer[6];//Y2_R		
//GREEN
		r66455_cxh[56]= buffer[3];//X2_LV >> 8
		r66455_cxh[57]= buffer[4];//X2_LV
		r66455_cxh[58]= buffer[7];//Y2_B >> 8
		r66455_cxh[59]= buffer[8];//Y2_B		
//BLUE
		r66455_cxh[104]=buffer[3];//X2_LV >> 8
		r66455_cxh[105]=buffer[4];//X2_LV
		r66455_cxh[106]=buffer[9];//Y2_G >> 8
		r66455_cxh[107]=buffer[10];//Y2_G		
	
    buffer1[0] = 0x02;//group0 inter
    buffer1[1] = 0xB0; 
    buffer1[2] = 0x00;
    SSD2828_W_Array(SigMode,channel,buffer1,0); 
		SSD2828_W_Array(SigMode,channel,r66455_cxh,0);
		delay_ms(2);
    SSD2828_W_Reg(SigMode,channel,0xB7,HS_B7_Data);
    delay_ms(5);
    buffer[4] = Uart_Error_None;
    STM2PC_RM671xx(pdev,CDC_IN_EP,buffer,buffer[2]+3);           //返回 写Gamma状态  ：ok  		
		
}
void R66455_Gamma_OTP_Start(USB_OTG_CORE_HANDLE *pdev,uint8_t  ep_addr,u8 SigMode ,u8 channel,u8* buffer,u16 LP_B7_Data,u16 HS_B7_Data)
{
	u8 buffer1[6];
	SSD2828_W_Reg(SigMode,channel,0xB7,(LP_B7_Data&0xFBFF)|0x0400); 	
	delay_ms(15);
    buffer1[0] = 0x02;
    buffer1[1] = 0xB0; 
    buffer1[2] = 0x04;
    SSD2828_W_Array(SigMode,channel,buffer1,0); 	//# Unlock MCP
    buffer1[0] = 0x01;
    buffer1[1] = 0x28; 
    SSD2828_W_Array(SigMode,channel,buffer1,0); 	//#  Display off
    buffer1[0] = 0x01;
    buffer1[1] = 0x10; 
    SSD2828_W_Array(SigMode,channel,buffer1,0);   //#  Enter_sleep_mode
		delay_ms(234);
    buffer1[0] = 0x02;
    buffer1[1] = 0xB0; 
    buffer1[2] = 0x84;
    SSD2828_W_Array(SigMode,channel,buffer1,0); 	//# Bank 1
    buffer1[0] = 0x02;
    buffer1[1] = 0xE6; 
    buffer1[2] = 0x81;
    SSD2828_W_Array(SigMode,channel,buffer1,0);	  //# NVM load on after exit_Sleep mode
    buffer1[0] = 0x02;
    buffer1[1] = 0xB0; 
    buffer1[2] = 0x04;
    SSD2828_W_Array(SigMode,channel,buffer1,0); 	//# Bank 0		
    buffer1[0] = 0x02;
    buffer1[1] = 0xE3; 
    buffer1[2] = 0xFF;
    SSD2828_W_Array(SigMode,channel,buffer1,0); 	//# NVM load enable			
    buffer1[0] = 0x05;
    buffer1[1] = 0xE0; 
    buffer1[2] = 0x61;
    buffer1[3] = 0x00; 
    buffer1[4] = 0x00;		
    buffer1[5] = 0x01;			
    SSD2828_W_Array(SigMode,channel,buffer1,0); //# NVM write Start
		delay_ms(1234);	
    buffer1[0] = 0x02;
    buffer1[1] = 0xE0; 
    buffer1[2] = 0x11;
    SSD2828_W_Array(SigMode,channel,buffer1,0); 	
		delay_ms(1234);	
    SSD2828_W_Reg(SigMode,channel,0xB7,HS_B7_Data);
    delay_ms(5);
    buffer[4] = Uart_Error_None;
    STM2PC_RM671xx(pdev,CDC_IN_EP,buffer,buffer[2]+3);           //返回 写Gamma状态  ：ok 		
}

void R66455_DLL_Calculation(USB_OTG_CORE_HANDLE *pdev,uint8_t  ep_addr,u8 SigMode ,u8 channel,u8* buffer,u16 LP_B7_Data,u16 HS_B7_Data)
{
	u8 buffer1[3];
	SSD2828_W_Reg(SigMode,channel,0xB7,(HS_B7_Data|0x0410)&0XFFFD);														
	delay_ms(5);		
	
    buffer1[0] = 0x02;//group0 inter
    buffer1[1] = 0xB0; 
    buffer1[2] = 0x00;
    SSD2828_W_Array(SigMode,channel,buffer1,0); 	
	delay_ms(5);
	SSD2828_W_Array(SigMode,channel,buffer,2);
	delay_ms(5);

	SSD2828_W_Reg(SigMode,channel,0xB7,HS_B7_Data);
    delay_ms(5);
    buffer[4] = Uart_Error_None;
    STM2PC_RM671xx(pdev,CDC_IN_EP,buffer,buffer[2]+3);           //返回 写Gamma状态  ：ok  	;
}

void ProcessForIc30(  USB_OTG_CORE_HANDLE *pdev,uint8_t  ep_addr,u8 SigMode ,u8 channel,u8 buffer[],u16 LP_B7_Data,u16 HS_B7_Data)
{	
	//void *pdev;
	switch(buffer[1]&0x0f)
	{
		case 0x01:                                      //写寄存器
				R66455_Write_Register(pdev,ep_addr,SigMode,channel,buffer,LP_B7_Data,HS_B7_Data);
				break;
		case 0x02:                                      //读寄存器
				R66455_Read_Register(pdev,ep_addr,SigMode,channel,buffer,LP_B7_Data,HS_B7_Data);
				break;
		case 0x03:                                      //写Gamma
				R66455_Write_Gamma(pdev,ep_addr,SigMode,channel,buffer,LP_B7_Data,HS_B7_Data);
				break;
		case 0x04:                                      //读Gamma
				R66455_Gamma_OTP_Start(pdev,ep_addr,SigMode,channel,buffer,LP_B7_Data,HS_B7_Data); 
				break;
		case 0x05: 
				R66455_DLL_Calculation		(pdev,ep_addr,SigMode,channel,buffer,LP_B7_Data,HS_B7_Data); 
				break;
		case 0x0f:
//				R66455_Write_Register_Demura(pdev,ep_addr,SigMode,channel,buffer,LP_B7_Data,HS_B7_Data);
//				//Demura: 0x30 0x0f add3 add2 add1 add0 //0xf4(spiflashbufferaddr)
				DemuraFlagStart();	
				break;
		default:break;
	}			
}
	




/*************************************************************************************

*/
