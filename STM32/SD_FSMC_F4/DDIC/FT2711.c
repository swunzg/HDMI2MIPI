#include "debug.h"
#include "AllDDIC.h"

/********************************************************************************************
********************************************************************************************
    FT2711  写寄存器 读寄存器  写Gamma 读Gamma OTP烧录
********************************************************************************************
********************************************************************************************/

void FT2711_Write_Register(USB_OTG_CORE_HANDLE *pdev,uint8_t  ep_addr,u8 SigMode ,u8 channel,u8* buffer,u16 LP_B7_Data,u16 HS_B7_Data);
void FT2711_Write51_Register(u8 SigMode ,u8 channel,u8* buffer,u16 LP_B7_Data,u16 HS_B7_Data);
void FT2711_Read_Register(USB_OTG_CORE_HANDLE *pdev,uint8_t  ep_addr,u8 SigMode ,u8 channel,u8* buffer,u16 LP_B7_Data,u16 HS_B7_Data);
void FT2711_Read_Gamma(USB_OTG_CORE_HANDLE *pdev,uint8_t  ep_addr,u8 SigMode ,u8 channel,u8* buffer,u16 LP_B7_Data,u16 HS_B7_Data);
void FT2711_Write_Gamma(USB_OTG_CORE_HANDLE *pdev,uint8_t  ep_addr,u8 SigMode ,u8 channel,u8* buffer,u16 LP_B7_Data,u16 HS_B7_Data);
void FT2711_Gamma_OTP_Start(USB_OTG_CORE_HANDLE *pdev,uint8_t  ep_addr,u8 SigMode ,u8 channel,u8* buffer,u16 LP_B7_Data,u16 HS_B7_Data);


void FT2711_Write_Register(USB_OTG_CORE_HANDLE *pdev,uint8_t  ep_addr,u8 SigMode ,u8 channel,u8* buffer,u16 LP_B7_Data,u16 HS_B7_Data)
{  
		SSD2828_W_Reg(SigMode,channel,0xC0,0x0100);
		SSD2828_W_Reg(SigMode,channel,0xB7,LP_B7_Data);//0x0700
	
    SSD2828_W_Array(SigMode,channel,buffer,2);
		delay_ms(5);
    SSD2828_W_Reg(SigMode,channel,0xB7,HS_B7_Data);//0x74B
    delay_ms(5);
    SSD2828_W_Reg(SigMode,channel,0xBF,0xAAFB); 
		delay_ms(5);
    STM2PC_RM671xx(pdev,CDC_IN_EP,buffer,buffer[2]+3);           //返回 写寄存器状态  ：ok
		
}

void FT2711_Write51_Register(u8 SigMode ,u8 channel,u8* buffer,u16 LP_B7_Data,u16 HS_B7_Data)
{
    if(SigMode != CMD_Mode)
        SSD2828_W_Reg(SigMode,channel,0xB7,(HS_B7_Data|0x0010)&0XFFFD);

    delay_ms(5);
    SSD2828_W_Array(SigMode,channel,buffer,2);
    SSD2828_W_Reg(SigMode,channel,0xB7,HS_B7_Data);
    delay_ms(5);
 
    SSD2828_W_Reg(SigMode,channel,0xBF,0xAAFB); 
}


void FT2711_Read_Register(USB_OTG_CORE_HANDLE *pdev,uint8_t  ep_addr,u8 SigMode ,u8 channel,u8* buffer,u16 LP_B7_Data,u16 HS_B7_Data)
{
		u16 tmp;
    u8 buffer1[2];
		
    if(SigMode == Mipi_Mode)  //RGB+SPI= MIPI  
				SSD2828_W_Reg(SigMode,channel,0xB7,0x290);
    else
        SSD2828_W_Reg(SigMode,channel,0xB7,LP_B7_Data|0x0080); 

    delay_ms(5);
	  
		SSD2828_W_Reg(SigMode,channel,0xC1,buffer[2]);
		SSD2828_W_Reg(SigMode,channel,0xC0,0x0001);
		
    buffer1[0] = 0x01;
    buffer1[1] = buffer[3];//addr which wanna to get 
    SSD2828_W_Array(SigMode,channel,buffer1,0); 
    delay_ms(5);
		SSD2828_W_Reg(SigMode,channel,0xD4,0x00FA);
		delay_ms(5);
    SSD2828_W_Cmd(SigMode,channel,0xFF);
		delay_ms(5);
		
    for(i=0;i<buffer[2]-1;i++){
        SSD2828_W_Cmd(SigMode,channel,0xFA);
				delay_ms(5);		
        tmp=SPI3_Read_u16_Data(channel);
        buffer[4+i]=tmp>>8;
        buffer[5+i]=tmp;
        delay_ms(5);
        i++;
    }
		
		buffer[0]=0x4B;	
		buffer[1]=0x02;
   	SSD2828_W_Reg(SigMode,channel,0xB7,HS_B7_Data);
    STM2PC_RM671xx(pdev,CDC_IN_EP,buffer,buffer[2]+3);           //返回 读取的 寄存器数据  ：ok  
}


void FT2711_Write_Gamma(USB_OTG_CORE_HANDLE *pdev,uint8_t  ep_addr,u8 SigMode ,u8 channel,u8* buffer,u16 LP_B7_Data,u16 HS_B7_Data)
{
		u8 buf_cmd[3];
		
	 SSD2828_W_Reg(SigMode,channel,0xB6,0x00F7); 
	 SSD2828_W_Reg(SigMode,channel,0xB7,0x0019); 

	 //write(0x00,0x00) 切页
   buf_cmd[0] = 0x02;
   buf_cmd[1] = 0x00;
   buf_cmd[2] = 0x00;                            
   SSD2828_W_Array(SigMode,channel,buf_cmd,0);
	 delay_ms(5); 
	 SSD2828_W_Array(SigMode,channel,buffer,2);
	 delay_ms(5);
	
   STM2PC_RM671xx(pdev,CDC_IN_EP,buffer,buffer[2]+3); 
}

void FT2711_Read_Gamma(USB_OTG_CORE_HANDLE *pdev,uint8_t  ep_addr,u8 SigMode ,u8 channel,u8* buffer,u16 LP_B7_Data,u16 HS_B7_Data)
{
    u8 buffer1[5];
    u16 tmp;
    u8 i,j,k=0;
	
		SSD2828_W_Reg(SigMode,channel,0xB7,0x0259);
    delay_ms(5);
	  //write(0x00,0x00) 切页
    buffer1[0] = 0x02;
    buffer1[1] = 0x00;
    buffer1[2] = 0x00;                            
    SSD2828_W_Array(SigMode,channel,buffer1,0); 
	  delay_ms(5);
	
	  
		SSD2828_W_Reg(SigMode,channel,0xB7,0x290); //read
		SSD2828_W_Reg(SigMode,channel,0xC1,0x002A); //返回个数设置
		SSD2828_W_Reg(SigMode,channel,0xC0,0x0001); 
		delay_ms(10);		
		
		buffer1[0] = 0x01; 
		buffer1[1] = buffer[3];
		SSD2828_W_Array(SigMode,channel,buffer1,0);		 
	  delay_ms(5);
	
		SSD2828_W_Reg(SigMode,channel,0xD4,0x00FA);
		SSD2828_W_Cmd(SigMode,channel,0xFF);    
		delay_ms(5);
				
		for(i = 0; i < 42; i++){
			SSD2828_W_Cmd(SigMode,channel,0xFA);	//SPI RESET	
			tmp=SPI3_Read_u16_Data(channel);
			buffer[4+i]=tmp>>8;
			buffer[5+i]=tmp;
			delay_ms(5);
			i++;			
		}
		
    buffer[0]=0x4B;	//FT2711
    buffer[1]=0x09;
		delay_ms(15);
    STM2PC_RM671xx(pdev,CDC_IN_EP,buffer,buffer[2]+3);          //返回 读取的 Gamma数据  ：ok  		

    SSD2828_W_Reg(SigMode,channel,0xB7,HS_B7_Data);
}

void FT2711_Gamma_OTP_Start(USB_OTG_CORE_HANDLE *pdev,uint8_t  ep_addr,u8 SigMode ,u8 channel,u8* buffer,u16 LP_B7_Data,u16 HS_B7_Data)
{
		u8 buf_cmd[5];
	  u8 mtp_flag;
	  SSD2828_W_Reg(SigMode,channel,0xC0,0x0100);
		SSD2828_W_Reg(SigMode,channel,0xB7,LP_B7_Data);//0x0700

	  if(0x0A == buffer[1]){
			
			//CMD2 ENABLE
			buf_cmd[0] = 0x02;
			buf_cmd[1] = 0x00;
			buf_cmd[2] = 0x00;
			SSD2828_W_Array(SigMode,channel,buf_cmd,0);
			buf_cmd[0] = 0x04;
			buf_cmd[1] = 0xFF;
			buf_cmd[2] = 0x27;
			buf_cmd[3] = 0x11;
			buf_cmd[4] = 0x01;
			SSD2828_W_Array(SigMode,channel,buf_cmd,0);
				
			buf_cmd[0] = 0x02;
			buf_cmd[1] = 0x00;
			buf_cmd[2] = 0x80;
			SSD2828_W_Array(SigMode,channel,buf_cmd,0);
			buf_cmd[0] = 0x03;
			buf_cmd[1] = 0xFF;
			buf_cmd[2] = 0x27;
			buf_cmd[3] = 0x11;
			SSD2828_W_Array(SigMode,channel,buf_cmd,0);
			
			
			//OTP program external
			buf_cmd[0] = 0x02;
			buf_cmd[1] = 0x00;
			buf_cmd[2] = 0x90;
			SSD2828_W_Array(SigMode,channel,buf_cmd,0);
			
			buf_cmd[0] = 0x02;
			buf_cmd[1] = 0xB6;
			buf_cmd[2] = 0x14;
			SSD2828_W_Array(SigMode,channel,buf_cmd,0);

		  //Enter_sleep_mode	
			buf_cmd[0] = 0x01;
			buf_cmd[1] = 0x10; 
			SSD2828_W_Array(SigMode,channel,buf_cmd,0);

			//Start NVM Program
			buf_cmd[0] = 0x02;
			buf_cmd[1] = 0x00;
			buf_cmd[2] = 0x00;
			SSD2828_W_Array(SigMode,channel,buf_cmd,0);
			buf_cmd[0] = 0x02;
			buf_cmd[1] = 0xEB;
			buf_cmd[2] = 0x01;
			SSD2828_W_Array(SigMode,channel,buf_cmd,0);
			delay_ms(3000);
			
			//End NVM Program
			buf_cmd[0] = 0x02;
			buf_cmd[1] = 0x00;
			buf_cmd[2] = 0x00;
			SSD2828_W_Array(SigMode,channel,buf_cmd,0);
			buf_cmd[0] = 0x02;
			buf_cmd[1] = 0xEB;
			buf_cmd[2] = 0x00;
			SSD2828_W_Array(SigMode,channel,buf_cmd,0);
			delay_ms(15);
			
		}	
		
		delay_ms(100);
    buffer[0] = 0x4B;		
	  buffer[1] = 0x0A;
  
		buffer[4] = Uart_Error_None;                        //返回 OTP Gamma数据  ：OK
		STM2PC_RM671xx(pdev,CDC_IN_EP,buffer,buffer[2]+3);         
}


void ProcessForIc4B( USB_OTG_CORE_HANDLE *pdev,uint8_t  ep_addr,u8 SigMode ,u8 channel,u8 buffer[],u16 LP_B7_Data,u16 HS_B7_Data)
{
	switch(buffer[1]&0x0f)
	{
		case 0x01:                                      
				//写寄存器
				FT2711_Write_Register(pdev,CDC_IN_EP,OLED.SigMode ,channel,buffer,LP_B7_Data,HS_B7_Data);
				break;
		case 0x02:                                      
				//读寄存器
				FT2711_Read_Register(pdev,CDC_IN_EP,OLED.SigMode ,channel,buffer,LP_B7_Data,HS_B7_Data);
				break;
		case 0x03:
			  //写51寄存器
				FT2711_Write51_Register(OLED.SigMode ,channel,buffer,LP_B7_Data,HS_B7_Data);		
				break;
		case 0x09:
				//读Gamma
				FT2711_Read_Gamma(pdev,CDC_IN_EP,OLED.SigMode ,channel,buffer,LP_B7_Data,HS_B7_Data);		 
				break;
		case 0x08: 
				FT2711_Write_Gamma(pdev,CDC_IN_EP,OLED.SigMode ,channel,buffer,LP_B7_Data,HS_B7_Data);
				break;
		case 0x0A://gamma otp
		case 0x0D://OTP HBM
    case 0x0E://OTP GOA
    case 0x0F://OTP ID 	
				FT2711_Gamma_OTP_Start(pdev,CDC_IN_EP,OLED.SigMode ,channel,buffer,LP_B7_Data,HS_B7_Data);
				break;
		default:
				break;
	}			
}

