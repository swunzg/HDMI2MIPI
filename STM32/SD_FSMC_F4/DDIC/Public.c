#include "usbd_cdc_core.h"


__weak void ProcessForIc30( USB_OTG_CORE_HANDLE *pdev,uint8_t  ep_addr,u8 SigMode ,u8 channel,u8 buffer[],u16 LP_B7_Data,u16 HS_B7_Data);
__weak void ProcessForIc3E( USB_OTG_CORE_HANDLE *pdev,uint8_t  ep_addr,u8 SigMode ,u8 channel,u8 buffer[],u16 LP_B7_Data,u16 HS_B7_Data){}
__weak void ProcessForIc3F( USB_OTG_CORE_HANDLE *pdev,uint8_t  ep_addr,u8 SigMode ,u8 channel,u8 buffer[],u16 LP_B7_Data,u16 HS_B7_Data){}
__weak void ProcessForIc40( USB_OTG_CORE_HANDLE *pdev,uint8_t  ep_addr,u8 SigMode ,u8 channel,u8 buffer[],u16 LP_B7_Data,u16 HS_B7_Data){}
__weak void ProcessForIc41( USB_OTG_CORE_HANDLE *pdev,uint8_t  ep_addr,u8 SigMode ,u8 channel,u8 buffer[],u16 LP_B7_Data,u16 HS_B7_Data)	{}
__weak void ProcessForIc42( USB_OTG_CORE_HANDLE *pdev,uint8_t  ep_addr,u8 SigMode ,u8 channel,u8 buffer[],u16 LP_B7_Data,u16 HS_B7_Data)	{}
__weak void ProcessForIc43( USB_OTG_CORE_HANDLE *pdev,uint8_t  ep_addr,u8 SigMode ,u8 channel,u8 buffer[],u16 LP_B7_Data,u16 HS_B7_Data)	{}
__weak void ProcessForIc44( USB_OTG_CORE_HANDLE *pdev,uint8_t  ep_addr,u8 SigMode ,u8 channel,u8 buffer[],u16 LP_B7_Data,u16 HS_B7_Data)	{}
__weak void ProcessForIc45( USB_OTG_CORE_HANDLE *pdev,uint8_t  ep_addr,u8 SigMode ,u8 channel,u8 buffer[],u16 LP_B7_Data,u16 HS_B7_Data)	{}
__weak void ProcessForIc46( USB_OTG_CORE_HANDLE *pdev,uint8_t  ep_addr,u8 SigMode ,u8 channel,u8 buffer[],u16 LP_B7_Data,u16 HS_B7_Data)	{}
__weak void ProcessForIc47( USB_OTG_CORE_HANDLE *pdev,uint8_t  ep_addr,u8 SigMode ,u8 channel,u8 buffer[],u16 LP_B7_Data,u16 HS_B7_Data)	{}
__weak void ProcessForIc48( USB_OTG_CORE_HANDLE *pdev,uint8_t  ep_addr,u8 SigMode ,u8 channel,u8 buffer[],u16 LP_B7_Data,u16 HS_B7_Data)	{}
__weak void ProcessForIc49( USB_OTG_CORE_HANDLE *pdev,uint8_t  ep_addr,u8 SigMode ,u8 channel,u8 buffer[],u16 LP_B7_Data,u16 HS_B7_Data)	{}
//void ProcessForIc4B( USB_OTG_CORE_HANDLE *pdev,uint8_t  ep_addr,u8 SigMode ,u8 channel,u8 buffer[],u16 LP_B7_Data,u16 HS_B7_Data)	{}
__weak void ProcessForIc4D( USB_OTG_CORE_HANDLE *pdev,uint8_t  ep_addr,u8 SigMode ,u8 channel,u8 buffer[],u16 LP_B7_Data,u16 HS_B7_Data)	{}
__weak void ProcessForIc4E( USB_OTG_CORE_HANDLE *pdev,uint8_t  ep_addr,u8 SigMode ,u8 channel,u8 buffer[],u16 LP_B7_Data,u16 HS_B7_Data)	{}
__weak void ProcessForIc4F( USB_OTG_CORE_HANDLE *pdev,uint8_t  ep_addr,u8 SigMode ,u8 channel,u8 buffer[],u16 LP_B7_Data,u16 HS_B7_Data)	{}
__weak void ProcessForIc50( USB_OTG_CORE_HANDLE *pdev,uint8_t  ep_addr,u8 SigMode ,u8 channel,u8 buffer[],u16 LP_B7_Data,u16 HS_B7_Data)	{}

