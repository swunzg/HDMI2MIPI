#ifndef _INTERNALFLASH_H
#define _INTERNALFLASH_H

#include "stm32fxxx.h"

uint32_t FLASH_If_Write(uint32_t* FlashAddress, uint32_t* Data,uint16_t DataLength);
void FLASH_If_Read(uint32_t* FlashAddress, uint32_t* Data, u8 start_add ,uint16_t DataLength);
#endif

