#ifndef __SPI_INIT_H
#define __SPI_INIT_H

#include "stm32fxxx.h"
#include "delay.h"

#define RCC_SPI1_SCK_Periph_GPIO		RCC_AHB1Periph_GPIOB				//CLK	时钟
#define SPI1_SCK										GPIO_Pin_3
#define SPI1_SCK_Periph_GPIO				GPIOB

#define RCC_SPI1_MISO_Periph_GPIO		RCC_AHB1Periph_GPIOA				//MISO 主进从出
#define SPI1_MISO										GPIO_Pin_6
#define SPI1_MISO_Periph_GPIO				GPIOA

#define RCC_SPI1_MOSI_Periph_GPIO		RCC_AHB1Periph_GPIOA				//MOSI 主出从进
#define SPI1_MOSI										GPIO_Pin_7
#define SPI1_MOSIPeriph_GPIO				GPIOA

#define RCC_ARM_OE_Periph_GPIO		RCC_AHB1Periph_GPIOF				
#define ARM_OE1										GPIO_Pin_10
#define ARM_OE2										GPIO_Pin_11
#define ARM_OE_Periph_GPIO				GPIOF

#define RCC_Mipi2828_CS_Periph_GPIO		RCC_AHB1Periph_GPIOB				//	CS的SPI片选）
#define Mipi2828_CS1										GPIO_Pin_14
#define Mipi2828_CS2										GPIO_Pin_15
#define Mipi2828_CS_Periph_GPIO					GPIOB


#define RCC_M25P64_CS_Periph_GPIO			RCC_AHB1Periph_GPIOA				//  M25P64片选
#define M25P64_CS0										GPIO_Pin_4
#define M25P64_CS_Periph_GPIO					GPIOA

#define CS1 	0X01
#define CS2		0x02
#define CS1_2	0x03

void SPI1_IO_init(void);
void SPI_CS_Select(u8 channel,u8 Enable);
void SPI_Write_u8_Cmd(u8 SPI_Mode,u8 channel,u8 cmd);
void SPI_Write_u8_Cmd_NoCS(u8 SPI_Mode,u8 cmd);
void SPI_Write_u8_Data(u8 SPI_Mode,u8 channel,u8 data);
void SPI_Write_u8_Data_NoCS(u8 SPI_Mode,u8 data);
void SPI_Write_2P3T_Data_NoCS(u16 data);
u8   SPI3_Read_u8_Data(u8 channel);
u8   SPI3_Read_u8_Data_NoCS(void);
u16  SPI3_Read_u16_Data(u8 channel);
void SPI_Write_code(u8 SPI_Mode,u8 channel ,u8 cmd,u8 data);
void SPI_Write_u8_Array(u8 SPI_Mode,u8 channel ,u8* para);
void SPI_Set_Window(u8 SPI_Mode,u8 channel ,u16 x0,u16 x1,u16 y0,u16 y1);
void SPI_WriteRAM_buffer_part(u8 SPI_Mode,u8 channel ,u8* buffer ,u16 length,u8  multiple ,u8 Flag);
void SPI3_WriteRAM(u8 SPI_Mode,u8 channel ,u16 H_OLED,u16 V_OLED,u8 R,u8 G, u8 B);

#endif


