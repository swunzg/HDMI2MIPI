#include "SPI_INIT.h"  
#include "uart.h"
void SPI1_IO_init(void)
{

	GPIO_InitTypeDef GPIO_InitStructure;

	RCC_AHB1PeriphClockCmd(RCC_SPI1_SCK_Periph_GPIO|RCC_SPI1_MISO_Periph_GPIO|RCC_SPI1_MOSI_Periph_GPIO|RCC_Mipi2828_CS_Periph_GPIO|RCC_M25P64_CS_Periph_GPIO|RCC_ARM_OE_Periph_GPIO,ENABLE);

	
	GPIO_InitStructure.GPIO_Mode=GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_Speed=GPIO_Speed_100MHz;
	GPIO_InitStructure.GPIO_OType=GPIO_OType_PP;
	GPIO_InitStructure.GPIO_PuPd=GPIO_PuPd_DOWN;
	
			
	GPIO_InitStructure.GPIO_Pin=SPI1_SCK;						//	CLK
	GPIO_InitStructure.GPIO_Mode=GPIO_Mode_OUT;
	GPIO_Init(SPI1_SCK_Periph_GPIO,&GPIO_InitStructure);
	
	GPIO_InitStructure.GPIO_Pin=SPI1_MOSI;					//	MOSI
	GPIO_InitStructure.GPIO_Mode=GPIO_Mode_OUT;
	GPIO_Init(SPI1_MOSIPeriph_GPIO,&GPIO_InitStructure);
	
	GPIO_InitStructure.GPIO_Pin=Mipi2828_CS1|Mipi2828_CS2;//	CS1,CS2
	GPIO_InitStructure.GPIO_Mode=GPIO_Mode_OUT;
	GPIO_Init(Mipi2828_CS_Periph_GPIO,&GPIO_InitStructure);

	GPIO_InitStructure.GPIO_Pin=ARM_OE1|ARM_OE2;//	OE1,OE2
	GPIO_InitStructure.GPIO_Mode=GPIO_Mode_OUT;
	GPIO_Init(ARM_OE_Periph_GPIO,&GPIO_InitStructure);

	GPIO_InitStructure.GPIO_Pin=M25P64_CS0;//	CS0
	GPIO_InitStructure.GPIO_Mode=GPIO_Mode_OUT;
	GPIO_Init(M25P64_CS_Periph_GPIO,&GPIO_InitStructure);	
	
	GPIO_InitStructure.GPIO_Mode=GPIO_Mode_IN;
	GPIO_InitStructure.GPIO_Pin=SPI1_MISO;					//	MISO
	GPIO_Init(SPI1_MISO_Periph_GPIO,&GPIO_InitStructure);
	
}


void SPI_CS_Select(u8 channel,u8 enable)
{
    if(enable == 0)
    {
        if(channel == CS1)
        {
            GPIO_SetBits		( Mipi2828_CS_Periph_GPIO,  Mipi2828_CS2);	//CS2_SET
            GPIO_ResetBits( Mipi2828_CS_Periph_GPIO,  Mipi2828_CS1);	//CS1_CLR
        }
        else if(channel == CS2)
        {
            GPIO_SetBits		( Mipi2828_CS_Periph_GPIO,  Mipi2828_CS1);	//CS1_SET
            GPIO_ResetBits( Mipi2828_CS_Periph_GPIO,  Mipi2828_CS2);	//CS2_CLR
        }
        else if(channel == CS1_2)
        {
            GPIO_ResetBits( Mipi2828_CS_Periph_GPIO,  Mipi2828_CS1);	//CS1_CLR
            GPIO_ResetBits( Mipi2828_CS_Periph_GPIO,  Mipi2828_CS2);	//CS2_CLR
        }
    }
    else if(enable == 1)
    {
        if(channel == CS1)
        {
            GPIO_SetBits		( Mipi2828_CS_Periph_GPIO,  Mipi2828_CS1);	//CS1_SET
        }
        else if(channel == CS2)	
        {
            GPIO_SetBits		( Mipi2828_CS_Periph_GPIO,  Mipi2828_CS2);	//CS2_SET
        }
        else if(channel == CS1_2)
        {
            GPIO_SetBits		( Mipi2828_CS_Periph_GPIO,  Mipi2828_CS1);	//CS1_SET
            GPIO_SetBits		( Mipi2828_CS_Periph_GPIO,  Mipi2828_CS2);	//CS2_SET
        }        
    }
}

void SPI_4L_DCX_Select(u8 channel,u8 enable)
{
    if(enable == 0)
    {
        if(channel == CS1)
            GPIO_ResetBits      ( ARM_OE_Periph_GPIO,  ARM_OE1);	//LCD_DCX1_CLR;
        else if(channel == CS2)
            GPIO_ResetBits      ( ARM_OE_Periph_GPIO,  ARM_OE2);	//LCD_DCX2_CLR;
        else if(channel == CS1_2)
        {
            GPIO_ResetBits      ( ARM_OE_Periph_GPIO,  ARM_OE1);	//LCD_DCX1_CLR;
            GPIO_ResetBits      ( ARM_OE_Periph_GPIO,  ARM_OE2);	//LCD_DCX2_CLR;
        }
    }
    else if(enable == 1)
    {
        if(channel == CS1)
            GPIO_SetBits		( ARM_OE_Periph_GPIO,  ARM_OE1);	//LCD_DCX1_SET;
        else if(channel == CS2)	
            GPIO_SetBits		( ARM_OE_Periph_GPIO,  ARM_OE2);	//LCD_DCX2_SET;
        else if(channel == CS1_2)
        {
            GPIO_SetBits		( ARM_OE_Periph_GPIO,  ARM_OE1);	//LCD_DCX1_SET;
            GPIO_SetBits		( ARM_OE_Periph_GPIO,  ARM_OE2);	//LCD_DCX2_SET;
        }        
    }
}

/*****************************************************
    三线8bit
    写命令
*******************************************************/

void SPI_Write_u8_Cmd(u8 SPI_Mode,u8 channel,u8 cmd)
{
	u8 i;
	SPI_CS_Select(channel,0);
    
    if(SPI_Mode == SPI3_Mode)
    {
        GPIO_ResetBits( SPI1_MOSIPeriph_GPIO,  SPI1_MOSI);	//LCD_SDA_CLR;	
        GPIO_ResetBits( SPI1_SCK_Periph_GPIO,  SPI1_SCK);	//LCD_SCL_CLR;	
        GPIO_SetBits	( SPI1_SCK_Periph_GPIO,  SPI1_SCK);	//LCD_SCL_SET;
    }  
    else if(SPI_Mode == SPI4_Mode)
    {
        SPI_4L_DCX_Select(channel,0);       //DCX = 0 表示 命令
    }
	for (i = 0; i < 8; ++i)
	{
		if (cmd & (1 << 7)) 	
			GPIO_SetBits	( SPI1_MOSIPeriph_GPIO,  SPI1_MOSI);	//LCD_SDA_SET;
		else					
			GPIO_ResetBits( SPI1_MOSIPeriph_GPIO,  SPI1_MOSI);	//LCD_SDA_CLR;
		cmd <<= 1;
		
		GPIO_ResetBits( SPI1_SCK_Periph_GPIO,  SPI1_SCK);	//LCD_SCL_CLR;
		
		GPIO_SetBits	( SPI1_SCK_Periph_GPIO,  SPI1_SCK);	//LCD_SCL_SET;
		
	}
	SPI_CS_Select(channel,1);
}

void SPI_Write_u8_Cmd_NoCS(u8 SPI_Mode,u8 cmd)
{
	u8 i;

     if(SPI_Mode == SPI3_Mode)
    {
        GPIO_ResetBits( SPI1_MOSIPeriph_GPIO,  SPI1_MOSI);	//LCD_SDA_CLR;	
        GPIO_ResetBits( SPI1_SCK_Periph_GPIO,  SPI1_SCK);	//LCD_SCL_CLR;	
        GPIO_SetBits	( SPI1_SCK_Periph_GPIO,  SPI1_SCK);	//LCD_SCL_SET;
    }  
    else if(SPI_Mode == SPI4_Mode)
    {
        SPI_4L_DCX_Select(CS1_2,0);       //DCX = 0 表示 命令
    }   
	for (i = 0; i < 8; ++i)
	{
		if (cmd & (1 << 7)) 	
			GPIO_SetBits	( SPI1_MOSIPeriph_GPIO,  SPI1_MOSI);	//LCD_SDA_SET;
		else					
			GPIO_ResetBits( SPI1_MOSIPeriph_GPIO,  SPI1_MOSI);	//LCD_SDA_CLR;
		cmd <<= 1;
		
		GPIO_ResetBits( SPI1_SCK_Periph_GPIO,  SPI1_SCK);	//LCD_SCL_CLR;
		
		GPIO_SetBits	( SPI1_SCK_Periph_GPIO,  SPI1_SCK);	//LCD_SCL_SET;
		
	}
}


/*****************************************************
    三线8bit
    写数据
*******************************************************/
void SPI_Write_u8_Data(u8 SPI_Mode,u8 channel,u8 data)
{
	u8 i;
    u8 dat = 0;
    dat = data;
	SPI_CS_Select(channel,0);
    if(SPI_Mode == SPI3_Mode)
    {
        GPIO_SetBits    ( SPI1_MOSIPeriph_GPIO,  SPI1_MOSI);	//LCD_SDA_SET;	
        GPIO_ResetBits  ( SPI1_SCK_Periph_GPIO,  SPI1_SCK);	    //LCD_SCL_CLR;	
        GPIO_SetBits	( SPI1_SCK_Periph_GPIO,  SPI1_SCK);	    //LCD_SCL_SET;
    }  
    else if(SPI_Mode == SPI4_Mode)
    {
        SPI_4L_DCX_Select(channel,1);       //DCX = 0 表示 命令
    } 			
	for (i = 0; i < 8; ++i)
	{
		if (dat & (1 << 7)) 	
			GPIO_SetBits	( SPI1_MOSIPeriph_GPIO,  SPI1_MOSI);	//LCD_SDA_SET;
		else					
			GPIO_ResetBits( SPI1_MOSIPeriph_GPIO,  SPI1_MOSI);	//LCD_SDA_CLR;
		dat <<= 1;
		
		GPIO_ResetBits( SPI1_SCK_Periph_GPIO,  SPI1_SCK);	//LCD_SCL_CLR;
		
		GPIO_SetBits	( SPI1_SCK_Periph_GPIO,  SPI1_SCK);	//LCD_SCL_SET;
		
	}
	SPI_CS_Select(channel,1);
}

void SPI_Write_u8_Data_NoCS(u8 SPI_Mode,u8 data)
{
	u8 i;
    if(SPI_Mode == SPI3_Mode)
    {
        GPIO_SetBits    ( SPI1_MOSIPeriph_GPIO,  SPI1_MOSI);	//LCD_SDA_SET;	
        GPIO_ResetBits  ( SPI1_SCK_Periph_GPIO,  SPI1_SCK);	    //LCD_SCL_CLR;	
        GPIO_SetBits	( SPI1_SCK_Periph_GPIO,  SPI1_SCK);	    //LCD_SCL_SET;
    }  
    else if(SPI_Mode == SPI4_Mode)
    {
        SPI_4L_DCX_Select(CS1_2,1);       //DCX = 0 表示 命令
    }		
	for (i = 0; i < 8; ++i)
	{
		if (data & (1 << 7)) 	
			GPIO_SetBits	( SPI1_MOSIPeriph_GPIO,  SPI1_MOSI);	//LCD_SDA_SET;
		else					
			GPIO_ResetBits( SPI1_MOSIPeriph_GPIO,  SPI1_MOSI);	//LCD_SDA_CLR;
		data <<= 1;		
		GPIO_ResetBits( SPI1_SCK_Periph_GPIO,  SPI1_SCK);	//LCD_SCL_CLR;		
		GPIO_SetBits	( SPI1_SCK_Periph_GPIO,  SPI1_SCK);	//LCD_SCL_SET;
		
	}
}

// SPI3-SP3T 2Wire
void SPI_Write_2P3T_Data_NoCS(u16 data)
{
	u8 i;
    u8 data1,data2;
	GPIO_SetBits	( SPI1_MOSIPeriph_GPIO,  SPI1_MOSI);	//LCD_SDA_SET;
    GPIO_SetBits	( ARM_OE_Periph_GPIO,  ARM_OE1);	    //LCD_DCX1_SET;
    GPIO_SetBits	( ARM_OE_Periph_GPIO,  ARM_OE2);	    //LCD_DCX2_SET;	
	GPIO_ResetBits  ( SPI1_SCK_Periph_GPIO,  SPI1_SCK);	    //LCD_SCL_CLR;	
	GPIO_SetBits	( SPI1_SCK_Periph_GPIO,  SPI1_SCK);	    //LCD_SCL_SET;	
	data1 = (u8)(data&0xFF);
    data2 = (u8)(data>>8);    
	for (i = 0; i < 8; ++i)
	{
		if (data2 & (1 << 7)) 	
			GPIO_SetBits	( SPI1_MOSIPeriph_GPIO,  SPI1_MOSI);	//LCD_SDA_SET;
		else					
			GPIO_ResetBits  ( SPI1_MOSIPeriph_GPIO,  SPI1_MOSI);	//LCD_SDA_CLR;
        
 		if (data1 & (1 << 7)) 
        {            
			GPIO_SetBits	( ARM_OE_Periph_GPIO,  ARM_OE1);	//LCD_DCX_SET;
            GPIO_SetBits	( ARM_OE_Periph_GPIO,  ARM_OE2);	//LCD_DCX2_SET;
        }
		else					
        {
			GPIO_ResetBits	( ARM_OE_Periph_GPIO,  ARM_OE1);	//LCD_DCX_CLR;
            GPIO_ResetBits	( ARM_OE_Periph_GPIO,  ARM_OE2);	//LCD_DCX2_CLR;
        }
        
		data1 <<= 1;
		data2 <<= 1;
		GPIO_ResetBits( SPI1_SCK_Periph_GPIO,  SPI1_SCK);	//LCD_SCL_CLR;		
		GPIO_SetBits	( SPI1_SCK_Periph_GPIO,  SPI1_SCK);	//LCD_SCL_SET;		
	}

}


/*****************************************************
    三线8bit
   读8bit数据
*******************************************************/

u8 SPI3_Read_u8_Data(u8 channel)
{
	u8 j,value = 0;
	SPI_CS_Select(channel,0);
	for(j=0;j<8;j++) // 8 Data
	{
		GPIO_ResetBits( SPI1_SCK_Periph_GPIO,  SPI1_SCK);	//LCD_SCL_CLR;
		GPIO_SetBits	( SPI1_SCK_Periph_GPIO,  SPI1_SCK);	//LCD_SCL_SET;
		value<<= 1;
		if(GPIO_ReadInputDataBit(SPI1_MISO_Periph_GPIO,SPI1_MISO))
			value |= 0x01;	
	}
	SPI_CS_Select(channel,1);	
	return value; 
	    
}

u8 SPI3_Read_u8_Data_NoCS(void)
{
	u8 j,value = 0;
	for(j=0;j<8;j++) // 8 Data
	{
		GPIO_ResetBits( SPI1_SCK_Periph_GPIO,  SPI1_SCK);	//LCD_SCL_CLR;
		GPIO_SetBits	( SPI1_SCK_Periph_GPIO,  SPI1_SCK);	//LCD_SCL_SET;
		value<<= 1;
		if(GPIO_ReadInputDataBit(SPI1_MISO_Periph_GPIO,SPI1_MISO))
			value |= 0x01;	
	}
	return value; 
	    
}
/*****************************************************
    三线8bit
   读16bit数据
*******************************************************/
u16 SPI3_Read_u16_Data(u8 channel)
{
	uint8_t j;
	uint16_t value=0;
	SPI_CS_Select(channel,0);
	for(j=0;j<16;j++) // 16 Data
	{
		GPIO_ResetBits( SPI1_SCK_Periph_GPIO,  SPI1_SCK);	//LCD_SCL_CLR;
		
		GPIO_SetBits	( SPI1_SCK_Periph_GPIO,  SPI1_SCK);	//LCD_SCL_SET;
		
		value<<= 1;
		if(GPIO_ReadInputDataBit(SPI1_MISO_Periph_GPIO,SPI1_MISO))
			value |= 0x01;	
	}
	SPI_CS_Select(channel,1);		
	return value;      
}


void SPI_Write_code(u8 SPI_Mode,u8 channel ,u8 cmd,u8 data)
{  
	GPIO_ResetBits( SPI1_SCK_Periph_GPIO,  SPI1_SCK);	//LCD_SCL_CLR;
    SPI_Write_u8_Cmd(SPI_Mode,channel,cmd);
    SPI_Write_u8_Data(SPI_Mode,channel,data);
}



void SPI_Write_u8_Array(u8 SPI_Mode,u8 channel ,u8* para)
{
	u8 i;
	for(i = 1; i < para[0]; i++)
	{
        GPIO_ResetBits( SPI1_SCK_Periph_GPIO,  SPI1_SCK);	//LCD_SCL_CLR;

        SPI_Write_u8_Cmd(SPI_Mode,channel,para[1]);
        SPI_Write_u8_Data(SPI_Mode,channel,para[1+i]);
	}    
}


void SPI_Set_Window(u8 SPI_Mode,u8 channel ,u16 x0,u16 x1,u16 y0,u16 y1)
{
	SPI_Write_code(SPI_Mode,channel ,0xFE,0x00);
	SPI_Write_u8_Cmd(SPI_Mode,channel ,0x2A);
	SPI_Write_u8_Data(SPI_Mode,channel ,(x0>>8));
	SPI_Write_u8_Data(SPI_Mode,channel ,x0&0xFF);
	SPI_Write_u8_Data(SPI_Mode,channel ,(x1>>8));
	SPI_Write_u8_Data(SPI_Mode,channel ,x1&0xFF);

	SPI_Write_u8_Cmd(SPI_Mode,channel ,0x2B);
	SPI_Write_u8_Data(SPI_Mode,channel ,(y0>>8));
	SPI_Write_u8_Data(SPI_Mode,channel ,y0&0xFF);
	SPI_Write_u8_Data(SPI_Mode,channel ,(y1>>8));
	SPI_Write_u8_Data(SPI_Mode,channel ,y1&0xFF); 
}

void SPI_WriteRAM_buffer_part(u8 SPI_Mode,u8 channel ,u8* buffer ,u16 length,u8  multiple ,u8 Flag)
{
  	uint32_t i,k=0;
	for(i=0;i<length*multiple;i++)
	{
        SPI_CS_Select(channel,0);  
        if((Flag == 1)&&(k == 0))
        {
            k = 1;
            SPI_Write_u8_Cmd_NoCS(SPI_Mode,0x2c); 
        }
        else
        {
            SPI_Write_u8_Cmd_NoCS(SPI_Mode,0x3c); 
        }
        SPI_Write_2P3T_Data_NoCS((u16)((buffer[i*3 +2])<<8)+buffer[i*3 +1]);   

        SPI_CS_Select(channel,1);
        SPI_CS_Select(channel,0);
        
        SPI_Write_2P3T_Data_NoCS((u16)((buffer[i*3])<<8)+buffer[(i+1)*3 +2]);
        
        SPI_CS_Select(channel,1);
        SPI_CS_Select(channel,0);
        
        SPI_Write_2P3T_Data_NoCS((u16)((buffer[(i+1)*3 +1])<<8)+buffer[(i+1)*3]);
        
        SPI_CS_Select(channel,1);
        i++;        
	}        
}


void SPI3_WriteRAM(u8 SPI_Mode,u8 channel ,u16 H_OLED,u16 V_OLED,u8 R,u8 G, u8 B)
{
  	uint32_t i,k=0;

	SPI_Write_code(SPI_Mode,channel ,0xFE,0x01);
	SPI_Write_code(SPI_Mode,channel ,0x04,0x80);
	SPI_Write_code(SPI_Mode,channel ,0xFE,0x00);
	SPI_Write_code(SPI_Mode,channel ,0xC4,0x31);        // SP3T 2Wire


	for(i=0;i<H_OLED*V_OLED;i++)
	{
        SPI_CS_Select(channel,0);                     
        if(k==0)
        {    
            k=1;
            SPI_Write_u8_Cmd_NoCS(SPI_Mode,0x2c); 
        }  
        else
            SPI_Write_u8_Cmd_NoCS(SPI_Mode,0x3c);                                                
        SPI_Write_2P3T_Data_NoCS((u16)(R<<8)+G);   

        SPI_CS_Select(channel,1);
        SPI_CS_Select(channel,0);

        SPI_Write_2P3T_Data_NoCS((u16)(B<<8)+R);

        SPI_CS_Select(channel,1);
        SPI_CS_Select(channel,0);

        SPI_Write_2P3T_Data_NoCS((u16)(G<<8)+B);

        SPI_CS_Select(channel,1); 
        i++;   
	}        
}




