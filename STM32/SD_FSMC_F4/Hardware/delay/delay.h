#ifndef __DELAY_H__
#define __DELAY_H__
#include "stm32fxxx.h"
void SysTick_Init(void);
void delay_ms(u16 nms);
void delay_us(u32 nus);
void delay_6ns(unsigned long n);
#endif
