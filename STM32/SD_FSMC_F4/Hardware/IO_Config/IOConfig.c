#include "IOConfig.h"

void LED_IO_init()
{
  GPIO_InitTypeDef GPIO_InitStructure;  
  RCC_AHB1PeriphClockCmd(RCC_LEDPeriph_GPIO , ENABLE);  
    
  GPIO_InitStructure.GPIO_Pin = LED1|LED2;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL ;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_Init(LEDPeriph_GPIO,&GPIO_InitStructure);
}


void IF_Sel_init()
{
  GPIO_InitTypeDef GPIO_InitStructure;  
  RCC_AHB1PeriphClockCmd(RCC_IF_Sel_GPIO , ENABLE);  
    
  GPIO_InitStructure.GPIO_Pin = IF_Sel1|IF_Sel2;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL ;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_Init(IF_SelPeriph_GPIO,&GPIO_InitStructure);	
    IF_Sel1_Video;
    IF_Sel2_Video;
}
void FPGA_Pro_init()
{
  GPIO_InitTypeDef GPIO_InitStructure;  
  RCC_AHB1PeriphClockCmd(RCC_FPGA_ProPeriph_GPIO , ENABLE);  
    
  GPIO_InitStructure.GPIO_Pin = FPGA_Pro;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_DOWN ;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_Init(FPGA_ProPeriph_GPIO,&GPIO_InitStructure);	
}

void BUTTON_IO_init()
{
  GPIO_InitTypeDef GPIO_InitStructure;  
  RCC_AHB1PeriphClockCmd(RCC_KEYPeriph_GPIO|RCC_KEY3Periph_GPIO|RCC_KEY4Periph_GPIO , ENABLE);  

	GPIO_InitStructure.GPIO_Pin = KEY1|KEY2;                //UP    PAUSE
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP ;
	GPIO_Init(KEYPeriph_GPIO,&GPIO_InitStructure);	
    
    GPIO_InitStructure.GPIO_Pin = KEY3;                     //DOWN
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP ;
	GPIO_Init(KEY3Periph_GPIO,&GPIO_InitStructure);	
    
    GPIO_InitStructure.GPIO_Pin = KEY4;                     //拨码开关
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP ;
	GPIO_Init(KEY4Periph_GPIO,&GPIO_InitStructure);	
}

void EXTIInit(void)
{
  EXTI_InitTypeDef   EXTI_InitStructure;
  GPIO_InitTypeDef   GPIO_InitStructure;
  

  RCC_AHB1PeriphClockCmd(RCC_KEYPeriph_GPIO|RCC_KEY3Periph_GPIO, ENABLE);								  
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_SYSCFG, ENABLE);
  

  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;	//选择输入模式
	
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;	//选择上拉
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;		//100M	
  GPIO_InitStructure.GPIO_Pin	 = KEY1|KEY2;		//选择io  
  GPIO_Init(KEYPeriph_GPIO, &GPIO_InitStructure);
	
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;	//选择输入模式
	
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;	//选择上拉
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;		//100M	
  GPIO_InitStructure.GPIO_Pin	 = KEY3;		//选择io  
  GPIO_Init(KEY3Periph_GPIO, &GPIO_InitStructure);
  
  /* Enable and set EXTI Line0 Interrupt to the lowest priority */


  SYSCFG_EXTILineConfig(EXTI_PortSourceGPIOF, EXTI_PinSource8);  //如果是GPIOD4管脚，必须选择EXTI_Line4通道
	SYSCFG_EXTILineConfig(EXTI_PortSourceGPIOF, EXTI_PinSource9);  //如果是GPIOD13管脚，必须选择EXTI_Line13通道
	SYSCFG_EXTILineConfig(EXTI_PortSourceGPIOE, EXTI_PinSource2);  //如果是GPIOD12管脚，必须选择EXTI_Line12通道

  EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;	   //选择中断模式
  EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Falling;  //选择下降沿触发
  EXTI_InitStructure.EXTI_LineCmd = ENABLE;				   //使能中断

  EXTI_InitStructure.EXTI_Line = EXTI_Line2;			   //选择路径4
  EXTI_Init(&EXTI_InitStructure);

  EXTI_InitStructure.EXTI_Line = EXTI_Line8;			   //选择路径13
  EXTI_Init(&EXTI_InitStructure);
	
  EXTI_InitStructure.EXTI_Line = EXTI_Line9;			   //选择路径12
  EXTI_Init(&EXTI_InitStructure);	
	
  EXTI_ClearITPendingBit(EXTI_Line2);
	EXTI_ClearITPendingBit(EXTI_Line8);
	EXTI_ClearITPendingBit(EXTI_Line9);
}

void EXIT_Enable(u8 status)
{
    
  NVIC_InitTypeDef   NVIC_InitStructure;  
  NVIC_InitStructure.NVIC_IRQChannel = EXTI9_5_IRQn;
  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0x0f;
  NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0x0f;
  if(status == Enable)  
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
  else
    NVIC_InitStructure.NVIC_IRQChannelCmd = DISABLE;  
  NVIC_Init(&NVIC_InitStructure);	
	
  NVIC_InitStructure.NVIC_IRQChannel = EXTI2_IRQn;
  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0x0f;
  NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0x0f;
    if(status == Enable)  
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
  else
    NVIC_InitStructure.NVIC_IRQChannelCmd = DISABLE;
  NVIC_Init(&NVIC_InitStructure);
}

void Other_IO_init(void)
{
  GPIO_InitTypeDef GPIO_InitStructure;  
  RCC_AHB1PeriphClockCmd(RCC_USB_RESETPeriph_GPIO|RCC_FPGA_ResetPeriph_GPIO|RCC_SSD_RESETPeriph_GPIO|RCC_Load_donePeriph_GPIO , ENABLE);  

  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
   
	
  GPIO_InitStructure.GPIO_Pin = Load_done;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_DOWN ;
  GPIO_Init(Load_donePeriph_GPIO,&GPIO_InitStructure);	

  GPIO_InitStructure.GPIO_Pin = FPGA_Reset;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_DOWN ;
  GPIO_Init(FPGA_ResetPeriph_GPIO,&GPIO_InitStructure);

  GPIO_InitStructure.GPIO_Pin = SSD_RESET;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;	
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL ;
  GPIO_Init(SSD_RESETPeriph_GPIO,&GPIO_InitStructure);
	
  GPIO_InitStructure.GPIO_Pin = USB_RESET;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL ;
  GPIO_Init(USB_RESETPeriph_GPIO,&GPIO_InitStructure);	
	
}
void FPGA_ResetProcess(void)
{
    FPGA_RESET_LOW;			//FPGA复位
    USB_RESET_HIGH;
    delay_ms(30);
    FPGA_RESET_HIGH;
    USB_RESET_LOW;
    delay_ms(100);	
}
void HardWare_Init(void)
{   

	SysTick_Init();
    ICS307_IO_init();		//ICS307引脚初始化
    ICS307_Valid_Data = calculate_idt307_config(80);	
    ICS307_ValidClk_Set(ICS307_Valid_Data);   
//#ifdef STM32F2XX
//	delay_ms(900);
//#else
//	delay_ms(1200);
//#endif  

	NVIC_Configuration();
	Other_IO_init();		//FPGA复位引脚，图片载入完成标志引脚，模组复位引脚(ssd2828复位)引脚 初始化
	
	SPI1_IO_init();			//SPI1接口的引脚初始化	
	BUTTON_IO_init();		//按键引脚初始化
	IF_Sel_init();
	USART1_config();
    LED_IO_init();			//LED引脚初始化
    FSMC_IO_init();
    NAND_Init();
#ifdef STM32F2XX
	TIM3_Int_Init(10-1,6500-1);	//定时器时钟60M，分频系数6000，所以60M/6000=10Khz的计数频率，计数10次为1ms	
#else
	TIM3_Int_Init(10-1,9400-1);	//定时器时钟60M，分频系数6000，所以60M/6000=10Khz的计数频率，计数10次为1ms	
#endif
}
