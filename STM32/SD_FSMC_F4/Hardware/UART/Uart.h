#ifndef USART_H
#define	USART_H
#include <stm32fxxx.h>
#include "main.h"

#define Debug_Mode 		1
#define Normal_Mode 	0

extern u16 crc16_data;
extern u8  Working_Mode;

u16 crc16(u8 *addr, u16 num, u16 crc);
void USART1_config(void);
void USART2_config(void);
void stdout_putchar(u8 ch);
void stdout_putchar2(u8 ch);
void STM2PC_ERROR(USB_OTG_CORE_HANDLE *pdev,uint8_t  ep_addr,u8 State,u8 CMD,u8 ERROR_CODE,u8 Other_data);
void STM2PC_RM671xx(USB_OTG_CORE_HANDLE *pdev,uint8_t  ep_addr, u8 table[],u8 length);
void STM2PC_LCDConfig(USB_OTG_CORE_HANDLE *pdev,uint8_t  ep_addr,u8 CMD,u8 table[],u8 ERROR_CODE);
void STM2PC_SW_Version(USB_OTG_CORE_HANDLE *pdev,uint8_t  ep_addr,u8 State,u8 CMD,u8 ERROR_CODE,u8 Version,u8 SubVersion,u8 ID);
void STM2PC_Pattern(USB_OTG_CORE_HANDLE *pdev,uint8_t  ep_addr,u8 CMD,u8 R,u8 G,u8 B,u8 ERROR_CODE);
void STM2PC_FilConfig(USB_OTG_CORE_HANDLE *pdev,uint8_t  ep_addr,u8 State,u8 CMD,u8 table[],u16 num ,u8 ERROR_CODE);
void Mode_Display(void);

#endif

