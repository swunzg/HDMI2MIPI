#ifndef __FSMC_H__
#define __FSMC_H__	
#include "stm32fxxx.h"
#include "main.h"
void write_cmd(unsigned short cmd);
void Write_DATA(unsigned short data_code );
void MCU_2828_write_CMD(u8 channel,u16 cmd);
void MCU_2828_write_CMD_NOCS(u16 cmd);
void MCU_2828_Write_Data(u8 channel,u16 data_code );
u16 MCU_2828_Read_Data(u8 channel);

void FSMC_Pure_colour(u32 Color);
void FSMC_BMP_NUM(u8 BMP_NUM);
void FSMC_OLED_Parameter(u32* buf,u8 SigMode);
void FSMC_Open(u16 Init_Mode);
void FSMC_IO_init(void);
#endif
