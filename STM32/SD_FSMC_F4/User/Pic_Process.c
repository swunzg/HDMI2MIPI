#include "Pic_Process.h"
#include "main.h"

char FileNames [File_NUM_MAX][File_NAME_MAX];
u8 PicCnt;



FILE *fout;
u32 br;
int read_picture_type(char *pic_name)
{
	char *pic_tail;
	int name_len = strlen(pic_name);
	pic_tail = pic_name + name_len-1;
	while(pic_name != pic_tail)
	{
		if(*pic_tail == '.')
		{
			if(strcmp(pic_tail,".bmp") == 0 || strcmp(pic_tail,".BMP") == 0)
			{
				return 1;
			}
		}
		pic_tail--;
	}
	return 0;
}


FRESULT scan_files (u8 Disc_type,u8 status)        /* Start node to be scanned (also used as work area) */
{
    FRESULT res;
    FILINFO fno;
    DIR dir;
    fsFileInfo info;
    int j;
    char *fn;   /* This function assumes non-Unicode configuration */
    if(Disc_type == Nand_Disc)
    {
        info.fileID = 0;                /* 每次使用ffind函数前，info.fileID必须初始化为0 */
        while(ffind ("N0:*.*", &info) == 0) 
        {
            fn = info.name;
            if (!(info.attrib & FS_FAT_ATTR_DIRECTORY) )    //非目录
            {
                if(read_picture_type(fn))
                {
                    strcpy(FileNames[PicCnt],fn);
                    PicCnt ++;
                }
            }                  
        }
    }
    else if(Disc_type == All_Disc)
    {  
        if(status == 1)
        {
            res = f_opendir(&dir, SD_Path);                       /* Open the directory */
            if (res == FR_OK) 
            {
                for (;;) 
                {
                    res = f_readdir(&dir, &fno);                   /* Read a directory item */
                    if (res != FR_OK || fno.fname[0] == 0) break;  /* Break on error or end of dir */
                    if (fno.fname[0] == '.') continue;             /* Ignore dot entry */

                    fn = fno.fname;
                    if (!(fno.fattrib & AM_DIR)) 							
                    {                                       //非目录
                        if(read_picture_type(fn))
                        {
                            strcpy(FileNames[PicCnt],fn);                                              
                            PicCnt++;
                        }
                    }
                }
            }
            else
            {
            	printf(" 存储设备枚举BMP图片失败\r\n");
                return FR_DISK_ERR;
            }
        }
        else
        {
            info.fileID = 0;                /* 每次使用ffind函数前，info.fileID必须初始化为0 */
            while(ffind ("N0:*.*", &info) == 0) 
            {
                fn = info.name;
                for(j=0;j<32;j++)
                    FileNames[0][j]=0;
                if (!(info.attrib & FS_FAT_ATTR_DIRECTORY) )    //非目录
                {
                    if(read_picture_type(fn))
                    {
                        if(fdelete(fn,"1")!=fsOK)
                            return FR_DISK_ERR;
                        else
                            printf(" 删除画面%s\r\n",fn);
                    }
                }                     
            }           
        }
    }
	if(status == 1)	
    	printf(" 存储设备枚举BMP图片成功\r\n"); 

            
    return FR_OK;
}


