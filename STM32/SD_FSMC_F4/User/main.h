#ifndef _MAIN_H_
#define _MAIN_H_

#include "stm32fxxx.h"
#include  "usbd_cdc_core.h"
#include  "usbd_usr.h"
#include "usbd_desc.h"
#include "usbd_cdc_vcp.h"
#include "usbd_cdc_core.h"

#include "sdio_sd.h"
#include "string.h"
#include "diskio.h"
#include "ff.h"
#include "Pic_Process.h"
#include "IOConfig.h"
#include "fsmc.h"
#include "InternalFlash.h"
#include "timer.h"
#include "SPI_INIT.h" 
#include "SSD2828.h"
#include "ics307.h"
#include "delay.h"
#include "uart.h"
#include "sys.h"
#include <stdio.h> 
#include <stdlib.h> 

#include <ctype.h>                      /* Character functions                */
#include "cmsis_os.h"                   /* CMSIS RTOS definitions             */
#include "rl_fs.h"                      /* FileSystem definitions             */
//#include "Terminal.h"

#include "nand.h"

#include "debug.h"
#define FW_Version      0x00
#define FW_SubVersion   0x10

#define BMP_GROWTH      1           //从图片的 最顶端 开始读取数据  1:从上向下读取   0：从下向上读取

#define Enable          1
#define Disable         0

#define Enable          1
#define Disable         0

#define NO_Disc			5
#define SD_Disc			0
#define Nand_Disc		1
#define All_Disc		3

#define Mipi_Mode 				0x10
#define CMD_Mode			    0x00
#define SPI3_Mode				0x20
#define SPI4_Mode				0x30

#define SD_Path							"0:"
#define SD_Conf_File					"0:/PGConfig1.cfg"
#define SD_Conf_File1					"0:/PGConfig2.cfg"
#define SD_Conf_File2					"0:/PGConfig3.cfg"
#define SD_Conf_File3					"0:/PGConfig4.cfg"
#define SD_Conf_File4					"0:/PGConfig5.cfg"
#define SD_Conf_File5					"0:/PGConfig6.cfg"
#define SD_Conf_File_New			    "0:/PGConNew.cfg"

#define Nand_Path						"N0:"
#define Nand_Conf_File					"N0:PGConfig1.cfg"
#define Nand_Conf_File1					"N0:PGConfig2.cfg"
#define Nand_Conf_File2					"N0:PGConfig3.cfg"
#define Nand_Conf_File3					"N0:PGConfig4.cfg"
#define Nand_Conf_File4					"N0:PGConfig5.cfg"
#define Nand_Conf_File5					"N0:PGConfig6.cfg"
#define Nand_Conf_File_New			    "N0:PGConfig_New.cfg"
#ifdef STM32F2XX
#define Updata_file                     "0:/code/STM32F2XX_Updata.bin"
#else
#define Updata_file                     "0:/code/STM32F4XX_Updata.bin"
#endif
#define None_Err						0x00		//没有错误
#define Internal_Flash_W_Error	        0x01		//内部Flash写出现错误
#define File_Illegal_Error			    0x02		//文件非法错误
#define File_Open_Error					0x03		//文件打开错误
#define File_Read_Error					0x04		//文件读取错误
#define File_Creat_Error				0x05		//文件创建错误

#define Timing_Data_Max 				64
#define Timing_table_num				Fil_Timing_length/4
#define Inter_Flash_Addr				0x080D0000
#define Device_Flash_Addr				0x080E0000
#define Device_Flash_Sect				FLASH_Sector_11
#define Inter_Flash_Sect				FLASH_Sector_10


#define LED_Flick_Fast				    50
#define LED_Flick_Slow				    300



#define RED                             0xFF0000
#define GREEN                           0x00FF00
#define BLUE                            0x0000FF
#define YELLOW                          0xFFFF00
#define PINK                            0xFF00FF
#define CYAN                            0x00FFFF
#define WHITE                           0xFFFFFF
#define BLACK                           0x000000

#endif

