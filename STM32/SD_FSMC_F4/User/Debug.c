#include "debug.h"
#include "../DDIC/AllDDIC.h"
//----------------------------------------------------------------------------------
/*RM67195 Gamma�Ĵ���λַ*/
u8 RM195_Gamma_r[62]=
{       
    0x00,0x01,0x02,0x03,0x04,0x05,0x06,0x07,0x08,0x09,0x0A,0x0B,0x0C,0x0D,0x0E,0x0F,
    0x10,0x11,0x12,0x13,0x14,0x15,0x16,0x17,0x18,0x19,0x1A,0x1B,0x1C,0x1D,0x1E,0x1F,
    0x20,0x21,0x22,0x23,0x24,0x25,0x26,0x27,0x28,0x29,0x2A,0x2B,0x2D,0x2F,0x30,0x31,
    0x32,0x33,0x34,0x35,0x36,0x37,0x38,0x39,0x3A,0x3B,0x3D,0x3F,0x40,0x41
};
u8 RM195_Gamma_g[62]=
{
    0x42,0x43,0x44,0x45,0x46,0x47,0x48,0x49,0x4A,0x4B,0x4C,0x4D,0x4E,0x4F,0x50,0x51,
    0x52,0x53,0x54,0x55,0x56,0x58,0x59,0x5A,0x5B,0x5C,0x5D,0x5E,0x5F,0x60,0x61,0x62,
    0x63,0x64,0x65,0x66,0x67,0x68,0x69,0x6A,0x6B,0x6C,0x6D,0x6E,0x6F,0x70,0x71,0x72,
    0x73,0x74,0x75,0x76,0x77,0x78,0x79,0x7A,0x7B,0x7C,0x7D,0x7E,0x7F,0x80
};
u8 RM195_Gamma_b[62]=
{
    0x81,0x82,0x83,0x84,0x85,0x86,0x87,0x88,0x89,0x8A,0x8B,0x8C,0x8D,0x8E,0x8F,0x90,
    0x91,0x92,0x93,0x94,0x95,0x96,0x97,0x98,0x99,0x9A,0x9B,0x9C,0x9D,0x9E,0x9F,0xA0,
    0xA2,0xA3,0xA4,0xA5,0xA6,0xA7,0xA9,0xAA,0xAB,0xAC,0xAD,0xAE,0xAF,0xB0,0xB1,0xB2,
    0xB3,0xB4,0xB5,0xB6,0xB7,0xB8,0xB9,0xBA,0xBB,0xBC,0xBD,0xBE,0xBF,0xC0
};

//----------------------------------------------------------------------------------
/*RM67295 Gamma�Ĵ���λַ*/
u8 RM295_Gamma_r[62]=
{       
    0x00,0x01,0x02,0x03,0x04,0x05,0x06,0x07,0x08,0x09,0x0A,0x0B,0x0C,0x0D,0x0E,0x0F,
    0x10,0x11,0x12,0x13,0x14,0x15,0x16,0x17,0x18,0x19,0x1A,0x1B,0x1C,0x1D,0x1E,0x1F,
    0x20,0x21,0x22,0x23,0x24,0x25,0x26,0x27,0x28,0x29,0x2A,0x2B,0x30,0x31,0x32,0x33,
	0x34,0x35,0x36,0x37,0x38,0x39,0x3A,0x3B,0x40,0x41,0x42,0x43,0x44,0x45
};
u8 RM295_Gamma_g[62]=
{
    0x46,0x47,0x48,0x49,0x4A,0x4B,0x4C,0x4D,0x4E,0x4F,0x50,0x51,0x52,0x53,0x58,0x59,
    0x5A,0x5B,0x5C,0x5D,0x5E,0x5F,0x60,0x61,0x62,0x63,0x64,0x65,0x66,0x67,0x68,0x69,
    0x6A,0x6B,0x6C,0x6D,0x6E,0x6F,0x70,0x71,0x72,0x73,0x74,0x75,0x76,0x77,0x78,0x79,
    0x7A,0x7B,0x7C,0x7D,0x7E,0x7F,0x80,0x81,0x82,0x83,0x84,0x85,0x86,0x87
};
u8 RM295_Gamma_b[62]=
{
    0x88,0x89,0x8A,0x8B,0x8C,0x8D,0x8E,0x8F,0x90,0x91,0x92,0x93,0x94,0x95,0x96,0x97,
    0x98,0x99,0x9A,0x9B,0x9C,0x9D,0x9E,0x9F,0xA4,0xA5,0xA6,0xA7,0xAC,0xAD,0xAE,0xAF,
    0xB0,0xB1,0xB2,0xB3,0xB4,0xB5,0xB6,0xB7,0xB8,0xB9,0xBA,0xBB,0xBC,0xBD,0xBE,0xBF,
    0xC0,0xC1,0xC2,0xC3,0xC4,0xC5,0xc6,0xC7,0xC8,0xC9,0xCA,0xCB,0xCC,0xCD
};
//----------------------------------------------------------------------------------
/*RM67198 Gamma�Ĵ���λַ*/
u8 RM198_Gamma33pt_r[66]=
{       
    0x00,0x01,0xCE,0xCF,0xD0,0xD1,0x02,0x03,0x04,0x05,0x06,0x07,0x08,0x09,0x0A,0x0B,
    0x0C,0x0D,0x0E,0x0F,0x10,0x11,0x12,0x13,0x14,0x15,0x16,0x17,0x18,0x19,0x1A,0x1B,
    0x1C,0x1D,0x1E,0x1F,0x20,0x21,0x22,0x23,0x24,0x25,0x26,0x27,0x28,0x29,0x2A,0x2B,
    0x30,0x31,0x32,0x33,0x34,0x35,0x36,0x37,0x38,0x39,0x3A,0x3B,0x40,0x41,0x42,0x43,
    0x44,0x45
};
u8 RM198_Gamma33pt_g[66]=
{
    0x46,0x47,0xD2,0xD3,0xD4,0xD5,0x48,0x49,0x4A,0x4B,0x4C,0x4D,0x4E,0x4F,0x50,0x51,
    0x52,0x53,0x58,0x59,0x5A,0x5B,0x5C,0x5D,0x5E,0x5F,0x60,0x61,0x62,0x63,0x64,0x65,
    0x66,0x67,0x68,0x69,0x6A,0x6B,0x6C,0x6D,0x6E,0x6F,0x70,0x71,0x72,0x73,0x74,0x75,
    0x76,0x77,0x78,0x79,0x7A,0x7B,0x7C,0x7D,0x7E,0x7F,0x80,0x81,0x82,0x83,0x84,0x85,
    0x86,0x87
};
u8 RM198_Gamma33pt_b[66]=
{
    0x88,0x89,0xD6,0xD7,0xD8,0xD9,0x8A,0x8B,0x8C,0x8D,0x8E,0x8F,0x90,0x91,0x92,0x93,
    0x94,0x95,0x96,0x97,0x98,0x99,0x9A,0x9B,0x9C,0x9D,0x9E,0x9F,0xA4,0xA5,0xA6,0xA7,
    0xAC,0xAD,0xAE,0xAF,0xB0,0xB1,0xB2,0xB3,0xB4,0xB5,0xB6,0xB7,0xB8,0xB9,0xBA,0xBB,
    0xBC,0xBD,0xBE,0xBF,0xC0,0xC1,0xC2,0xC3,0xC4,0xC5,0xc6,0xC7,0xC8,0xC9,0xCA,0xCB,
    0xCC,0xCD
};
//----------------------------------------------------------------------------------
/*RM67198 Gamma�Ĵ���λַ*/
u8 RM198_Gamma19pt_r[38]=
{       
    0x00,0x01,0xD0,0xD1,0x04,0x05,0x06,0x07,0x08,0x09,0x0A,0x0B,0x0C,0x0D,
    0x10,0x11,0x14,0x15,0x16,0x17,0x1A,0x1B,0x1E,0x1F,0x22,0x23,0x28,0x29,
    0x34,0x35,0x38,0x39,0x40,0x41,0x42,0x43,0x44,0x45
};
u8 RM198_Gamma19pt_g[38]=
{
    0x46,0x47,0xD4,0xD5,0x4A,0x4B,0x4C,0x4D,0x4E,0x4F,0x50,0x51,0x52,0x53,
    0x5A,0x5B,0x5E,0x5F,0x60,0x61,0x64,0x65,0x68,0x69,0x6C,0x6D,0x72,0x73,
    0x7A,0x7B,0x7E,0x7F,0x82,0x83,0x84,0x85,0x86,0x87
};
u8 RM198_Gamma19pt_b[38]=
{
    0x88,0x89,0xD8,0xD9,0x8C,0x8D,0x8E,0x8F,0x90,0x91,0x92,0x93,0x94,0x95,
    0x98,0x99,0x9C,0x9D,0x9E,0x9F,0xA6,0xA7,0xAE,0xAF,0xB2,0xB3,0xB8,0xB9,
    0xC0,0xC1,0xC4,0xC5,0xC8,0xC9,0xCA,0xCB,0xCC,0xCD,
};

/*TC1100 Gamma�Ĵ���λַ*///(0->255)(PAGE+OFFSET)
u8 TC1100_Gamma_r[48]={0x0F,0x68,0x0F,0x69,0x0F,0x6B,0x0F,0x6C,0x0F,0x6D,0x0F,0x6E,0x0F,0x6F,0x0F,0x70,0x0F,0x71,0x0F,0x72,0x0F,0x73,0x0F,0x74,0x0F,0x75,0x0F,0x76,0x0F,0x77,0x0F,0x78,0x0f,0x79,0x0f,0x7a,0x0f,0x7b,0x0f,0x7c,0x0f,0x7d,0x0f,0x7e,0x0f,0x7f,0x0b,0xff};
u8 TC1100_Gamma_g[48]={0x10,0xe9,0x10,0xea,0x10,0xeb,0x10,0xec,0x10,0xed,0x10,0xee,0x10,0xef,0x10,0xf0,0x10,0xf1,0x10,0xf2,0x10,0xf3,0x10,0xf4,0x10,0xf5,0x10,0xf6,0x10,0xf7,0x10,0xf8,0x10,0xf9,0x10,0xfa,0x10,0xfb,0x10,0xfc,0x10,0xfd,0x10,0xfe,0x10,0xff,0x0c,0xff};
u8 TC1100_Gamma_b[48]={0x12,0x69,0x12,0x6a,0x12,0x6b,0x12,0x6c,0x12,0x6d,0x12,0x6e,0x12,0x6f,0x12,0x70,0x12,0x71,0x12,0x72,0x12,0x73,0x12,0x74,0x12,0x75,0x12,0x76,0x12,0x77,0x12,0x78,0x12,0x79,0x12,0x7a,0x12,0x7b,0x12,0x7c,0x12,0x7d,0x12,0x7e,0x12,0x7f,0x0d,0xff};

/*RM67160 Gamma�Ĵ���λַ*/
u8 RM160_Gamma_r[22]={0x00,0x01,0x02,0x03,0x04,0x05,0x06,0x07,0x0A,0x0B,0x10,0x11,0x14,0x15,0x18,0x19,0x1C,0x1D,0x20,0x21,0x32,0x33};
u8 RM160_Gamma_g[22]={0x34,0x35,0x36,0x37,0x38,0x39,0x3A,0x3B,0x40,0x41,0x46,0x47,0x4A,0x4B,0x4E,0x4F,0x52,0x53,0x56,0x58,0x67,0x68};
u8 RM160_Gamma_b[22]={0x69,0x6A,0x6B,0x6C,0x6D,0x6E,0x6F,0x70,0x73,0x74,0x79,0x7A,0x7D,0x7E,0x81,0x82,0x85,0x86,0x89,0x8A,0x99,0x9A};

/*RM67162 Gamma�Ĵ���λַ*/
u8 RM162_Gamma_r[50]={0x00,0x01,0x02,0x03,0x04,0x05,0x06,0x07,0x08,0x09,0x0A,0x0B,0x0c,0x0D,0x0e,0x0f,0x10,0x11,0x12,0x13,0x14,0x15,
                        0x16,0x17,0x18,0x19,0x1a,0x1b,0x1C,0x1D,0x1e,0x1f,0x20,0x21,0x22,0x23,0x24,0x25,0x26,0x27,0x28,0x29,0x2a,0x2b,0x2d,0x2f,0x30,0x31,0x32,0x33};
u8 RM162_Gamma_g[50]={0x34,0x35,0x36,0x37,0x38,0x39,0x3A,0x3B,0x3d,0x3f,0x40,0x41,0x42,0x43,0x44,0x45,0x46,0x47,0x48,0x49,0x4A,0x4B,
                        0x4c,0x4d,0x4E,0x4F,0x50,0x51,0x52,0x53,0x54,0x55,0x56,0x58,0x59,0x5a,0x5b,0x5c,0x5d,0x5e,0x5f,0x60,0x61,0x62,0x63,0x64,0x65,0x66,0x67,0x68};
u8 RM162_Gamma_b[50]={0x69,0x6A,0x6B,0x6C,0x6D,0x6E,0x6F,0x70,0x71,0x72,0x73,0x74,0x75,0x76,0x77,0x78,0x79,0x7A,0x7b,0x7c,0x7D,0x7E,
                        0x7f,0x80,0x81,0x82,0x83,0x84,0x85,0x86,0x87,0x88,0x89,0x8A,0x8b,0x8c,0x8d,0x8e,0x8f,0x90,0x91,0x92,0x93,0x94,0x95,0x96,0x97,0x98,0x99,0x9A};

/*RM9300 Gamma�Ĵ���λַ*/
u8 RM300_Gamma_r[56]=
{
    0x00,0x01,  //0
    0x02,0x03,  //1
    0x04,0x05,  //2
    0x06,0x07,  //3
    0x08,0x09,  //7
    0x0A,0x0B,  //1
    0x0c,0x0D,  //15
    0x0e,0x0f,  //19
    0x10,0x11,  //23
    0x12,0x13,  //27
    0x14,0x15,  //31
    0x16,0x17,  //35
    0x18,0x19,  //39
    0x1a,0x1b,  //47
    0x1C,0x1D,  //55
    0x1e,0x1f,  //63
    0x20,0x21,  //79
    0x22,0x23,  //95
    0x24,0x25,  //111
    0x26,0x27,  //127
    0x28,0x29,  //143
    0x2a,0x2b,  //159
    0x30,0x31,  //175
    0x32,0x33,  //191
    0x34,0x35,  //207
    0x36,0x37,  //223
    0x38,0x39,  //239
    0x3A,0x3B   //255
};
u8 RM300_Gamma_g[56]=
{
    0x40,0x41,
    0x42,0x43,
    0x44,0x45,
    0x46,0x47,
    0x48,0x49,
    0x4A,0x4B,
    0x4c,0x4d,
    0x4E,0x4F,
    0x50,0x51,
    0x52,0x53,
    0x58,0x59,
    0x5a,0x5b,
    0x5c,0x5d,
    0x5e,0x5f,
    0x60,0x61,
    0x62,0x63,
    0x64,0x65,
    0x66,0x67,
    0x68,0x69,
    0x6A,0x6B,
    0x6C,0x6D,
    0x6E,0x6F,
    0x70,0x71,
    0x72,0x73,
    0x74,0x75,
    0x76,0x77,
    0x78,0x79,
    0x7A,0x7b
};
u8 RM300_Gamma_b[56]={
    0x7c,0x7D,
    0x7E,0x7f,
    0x80,0x81,
    0x82,0x83,
    0x84,0x85,
    0x86,0x87,
    0x88,0x89,
    0x8A,0x8b,
    0x8c,0x8d,
    0x8e,0x8f,
    0x90,0x91,
    0x92,0x93,
    0x94,0x95,
    0x96,0x97,
    0x98,0x99,
    0x9A,0x9b,
    0x9c,0x9d,
    0x9e,0x9f,
    0xa4,0xa5,
    0xa6,0xa7,
    0xac,0xad,
    0xae,0xaf,
    0xb0,0xb1,
    0xb2,0xb3,
    0xb4,0xb5,
    0xb6,0xb7,
    0xb8,0xb9,
    0xbA,0xbb    
};

/*RM9350 Gamma�Ĵ���λַ*/
u8 RM350_Gamma_r[56]=
{
    0x00,0x01,  //r0
    0x02,0x03,  //r1
    0x04,0x05,  //r2
    0x06,0x07,  //r3
    0x08,0x09,  //r7
    0x0A,0x0B,  //r11
    0x0c,0x0D,  //r15
    0x0e,0x0f,  //r19
    0x10,0x11,  //r23
    0x12,0x13,  //r27
    0x14,0x15,  //r31
    0x16,0x17,  //r35
    0x18,0x19,  //r39
    0x1a,0x1b,  //r47
    0x1C,0x1D,  //r55
    0x1e,0x1f,  //r63
    0x20,0x21,  //r79
    0x22,0x23,  //r95
    0x24,0x25,  //r111
    0x26,0x27,  //r127
    0x28,0x29,  //r143
    0x2a,0x2b,  //r159
    0x30,0x31,  //r175
    0x32,0x33,  //r191
    0x34,0x35,  //r207
    0x36,0x37,  //r223
    0x38,0x39,  //r239
    0x3A,0x3B   //r255
};
u8 RM350_Gamma_g[56]=
{
    0x40,0x41,  //g0
    0x42,0x43,  //g1
    0x44,0x45,  //g2
    0x46,0x47,  //g3
    0x48,0x49,  //g7
    0x4A,0x4B,  //g11
    0x4c,0x4d,  //g15
    0x4E,0x4F,  //g19
    0x50,0x51,  //g23
    0x52,0x53,  //g27
    0x58,0x59,  //g31
    0x5a,0x5b,  //g35
    0x5c,0x5d,  //g39
    0x5e,0x5f,  //g47
    0x60,0x61,  //g55
    0x62,0x63,  //g63
    0x64,0x65,  //g79
    0x66,0x67,  //g95
    0x68,0x69,  //g111
    0x6A,0x6B,  //g127
    0x6C,0x6D,  //g143
    0x6E,0x6F,  //g159
    0x70,0x71,  //g175
    0x72,0x73,  //g191
    0x74,0x75,  //g207
    0x76,0x77,  //g223
    0x78,0x79,  //g239
    0x7A,0x7b   //g255
};
u8 RM350_Gamma_b[56]={
    0x7c,0x7D,  //b0
    0x7E,0x7f,  //b1
    0x80,0x81,  //b2
    0x82,0x83,  //b3
    0x84,0x85,  //b7
    0x86,0x87,  //b11
    0x88,0x89,  //b15
    0x8A,0x8b,  //b19
    0x8c,0x8d,  //b23
    0x8e,0x8f,  //b27
    0x90,0x91,  //b31
    0x92,0x93,  //b35
    0x94,0x95,  //b39
    0x96,0x97,  //b47
    0x98,0x99,  //b55
    0x9A,0x9b,  //b63
    0x9c,0x9d,  //b79
    0x9e,0x9f,  //b95
    0xa4,0xa5,  //b111
    0xa6,0xa7,  //b127
    0xac,0xad,  //b143
    0xae,0xaf,  //b159
    0xb0,0xb1,  //b175
    0xb2,0xb3,  //b191
    0xb4,0xb5,  //b207
    0xb6,0xb7,  //b223
    0xb8,0xb9,  //b239
    0xbA,0xbb   //b255
};

u8 RM010_Gamma_r[50]={0x00,0x01,   //r0
	                    0x02,0x03,   //r7
	                    0x04,0x05,   //r11
	                    0x06,0x07,   //r15
	                    0x08,0x09,   //r23
	                    0x0A,0x0B,   //r31
	                    0x0C,0x0D,   //r39
	                    0x0E,0x0F,   //r47
                      0x10,0x11,   //r63
                      0x12,0x13,   //r79
                      0x14,0x15,   //r95
                      0x16,0x17,   //r111
                      0x18,0x19,   //r127
                      0x1A,0x1B,   //r143
                      0x1C,0x1D,   //r159
                      0x1E,0x1F,   //r175
                      0x20,0x21,   //r191
	                    0x22,0x23,   //r207
	                    0x24,0x25,   //r215
	                    0x26,0x27,   //r223
	                    0x28,0x29,   //r231
	                    0x2A,0x2B,   //r239
											0x2D,0x2F,   //r243
											0x30,0x31,   //r247
                      0x32,0x33};  //r255

u8 RM010_Gamma_g[50]={0x34,0x35,   //g0
                      0x36,0x37,   //g7
                      0x38,0x39,   //g11
                      0x3A,0x3B,   //g15
	                    0x3D,0x3F,   //g23
                      0x40,0x41,   //g31
	                    0x42,0x43,   //g39
	                    0x44,0x45,   //g47
                      0x46,0x47,   //g63
	                    0x48,0x49,   //g79
                      0x4A,0x4B,   //g95
	                    0x4C,0x4D,   //g111
                      0x4E,0x4F,   //g127
	                    0x50,0x51,   //g143
                      0x52,0x53,   //g159
	                    0x54,0x55,   //g175
                      0x56,0x58,   //g191
	                    0x59,0x5A,   //g207
	                    0x5B,0x5C,   //g215
	                    0x5D,0x5E,   //g223
	                    0x5F,0x60,   //g231
	                    0x61,0x62,   //g239
											0x63,0x64,   //g243
											0x65,0x66,   //g247
                      0x67,0x68};  //g255

u8 RM010_Gamma_b[50]={0x69,0x6A,   //b0
                      0x6B,0x6C,   //b7
                      0x6D,0x6E,   //b11
                      0x6F,0x70,   //b15
	                    0x71,0x72,   //b23
                      0x73,0x74,   //b31
	                    0x75,0x76,   //b39
	                    0x77,0x78,   //b47
                      0x79,0x7A,   //b63
	                    0x7B,0x7C,   //b79
                      0x7D,0x7E,   //b95
	                    0x7F,0x80,   //b111
                      0x81,0x82,   //b127
	                    0x83,0x84,   //b143
                      0x85,0x86,   //b159
	                    0x87,0x88,   //b175
                      0x89,0x8A,   //b191
	                    0x8B,0x8C,   //b207
	                    0x8D,0x8E,   //b215
	                    0x8F,0x90,   //b223
	                    0x91,0x92,   //b231
	                    0x93,0x94,   //b239
											0x95,0x96,   //b243
											0x97,0x98,   //b247
                      0x99,0x9A};  //b255

/********************************************************************************************
********************************************************************************************
    ICN9608  д�Ĵ��� ���Ĵ���  дGamma ��Gamma OTP��¼
    ICN9608  д�Ĵ��� ���Ĵ���  дGamma ��Gamma OTP��¼
    ICN9608  д�Ĵ��� ���Ĵ���  дGamma ��Gamma OTP��¼
    ICN9608  д�Ĵ��� ���Ĵ���  дGamma ��Gamma OTP��¼
    ICN9608  д�Ĵ��� ���Ĵ���  дGamma ��Gamma OTP��¼
********************************************************************************************
********************************************************************************************/
void ICN9608_Write_Register(USB_OTG_CORE_HANDLE *pdev,uint8_t  ep_addr,u8 SigMode ,u8 channel,u8* buffer,u16 LP_B7_Data,u16 HS_B7_Data)
{
		if(USB_Rx_Buffer[1]&0x80)
			SSD2828_W_Reg(SigMode,channel,0xB7,(HS_B7_Data|0x0010)&0XFFFD);
		else
			SSD2828_W_Reg(SigMode,channel,0xB7,LP_B7_Data);
    SSD2828_W_Array(SigMode,channel,buffer,2);                               

    delay_ms(5);
    SSD2828_W_Reg(SigMode,channel,0xB7,HS_B7_Data);
    buffer[4] = Uart_Error_None;
    buffer[5] = 0;
    STM2PC_RM671xx(pdev,CDC_IN_EP,buffer,buffer[2]+3);           //���� д�Ĵ���״̬  ��ok  
}
void ICN9608_Write_51Register(USB_OTG_CORE_HANDLE *pdev,uint8_t  ep_addr,u8 SigMode ,u8 channel,u8* buffer,u16 LP_B7_Data,u16 HS_B7_Data)
{
    SSD2828_W_Reg(SigMode,channel,0xB7,0x0019);
	  //SSD2828_W_Reg(SigMode,channel,0xB7,LP_B7_Data);
		delay_ms(5);
    SSD2828_W_Array(SigMode,channel,buffer,2);                               

    delay_ms(5);
    SSD2828_W_Reg(SigMode,channel,0xB7,HS_B7_Data);
    buffer[4] = Uart_Error_None;
    buffer[5] = 0;
    STM2PC_RM671xx(pdev,CDC_IN_EP,buffer,buffer[2]+3);           //���� д�Ĵ���״̬  ��ok  
}
void ICN9608_Read_Register(USB_OTG_CORE_HANDLE *pdev,uint8_t  ep_addr,u8 SigMode ,u8 channel,u8* buffer,u16 LP_B7_Data,u16 HS_B7_Data)
{
    u16 tmp;
    buffer[1]=0x03;
    SSD2828_W_Reg(SigMode,channel,0xB7,LP_B7_Data|0x0080);                               
    SSD2828_W_Reg(SigMode,channel,0xC1,buffer[2]); //return package size                                
    SSD2828_W_Reg(SigMode,channel,0xC0,0x0001);
    buffer[0] = 0x01;
    buffer[1] = buffer[3];                        
    SSD2828_W_Array(SigMode,channel,buffer,0);
    delay_ms(5);  
   SSD2828_W_Reg(SigMode,channel,0xD4,0x00FA);
    SSD2828_W_Cmd(SigMode,channel,0xFF);
    for(i=0;i<buffer[2]-1;i++)
    {
        SSD2828_W_Cmd(SigMode,channel,0xFA);		
        tmp=SPI3_Read_u16_Data(channel);
        buffer[4+i]=tmp>>8;
        buffer[5+i]=tmp;
        delay_ms(5);
        i++;
    }
		buffer[0]=0x2D;	
		buffer[1]=0x02;
    SSD2828_W_Reg(SigMode,channel,0xB7,HS_B7_Data);
    STM2PC_RM671xx(pdev,CDC_IN_EP,buffer,buffer[2]+3);           //���� ��ȡ�� �Ĵ�������  ��ok 
}

void ICN9608_Write_Gamma(USB_OTG_CORE_HANDLE *pdev,uint8_t  ep_addr,u8 SigMode ,u8 channel,u8* buffer,u16 LP_B7_Data,u16 HS_B7_Data)
{
    u8 buffer1[7];
    u8 i,j,m,k=0,temp;
	
		SSD2828_W_Reg(SigMode,channel,0xB7,0x0019);
		delay_ms(5);
	
    buffer1[0] = 0x03;        
    buffer1[1] = 0xF0;
    buffer1[2] = 0x5A;
    buffer1[3] = 0x5A;            //   Manufacture command set key      
    SSD2828_W_Array(SigMode,channel,buffer1,0);		 
	  delay_ms(5);  

		SSD2828_W_Reg(SigMode,channel,0xBC,30); 
		SSD2828_W_Cmd(SigMode,channel,0xBF);	
		SSD2828_W_Data(SigMode,channel,0xBB); //GAMMA REG=>0XBB
		for( i = 0;i<29;i++)   //10�����
		{	                                                                                  
			SSD2828_W_Data(SigMode,channel,buffer[i+4]);	
			delay_ms(5);  
		}
	  buffer1[0] = 0x02;        
    buffer1[1] = 0xBE;
    buffer1[2] = 0x01;    
    SSD2828_W_Array(SigMode,channel,buffer1,0);		 
	  delay_ms(5);  	
		
		buffer[4] = Uart_Error_None;
		buffer[1] =  0x08;
		STM2PC_RM671xx(pdev,CDC_IN_EP,buffer,buffer[2]+3);           //���� дGamma״̬  ��ok 		
		
		SSD2828_W_Reg(SigMode,channel,0xB7,HS_B7_Data);
}

void ICN9608_Read_Gamma(USB_OTG_CORE_HANDLE *pdev,uint8_t  ep_addr,u8 SigMode ,u8 channel,u8* buffer,u16 LP_B7_Data,u16 HS_B7_Data)
{
    u8 buffer1[7];
    u16 tmp;
    u8 i,j,k=0;
    //SSD2828_W_Reg(SigMode,channel,0xB7,(HS_B7_Data|0x0010)&0XFFFD);
		SSD2828_W_Reg(SigMode,channel,0xB7,LP_B7_Data);
		delay_ms(10);
    buffer1[0] = 0x03;
    buffer1[1] = 0xF0;
    buffer1[2] = 0x5A;
    buffer1[3] = 0x5A;                       // Page 1
    SSD2828_W_Array(SigMode,channel,buffer1,0); 
    delay_ms(5);
    //SSD2828_W_Reg(SigMode,channel,0xB7,((HS_B7_Data|0x0010)&0XFFFD)|0x0080);
		SSD2828_W_Reg(SigMode,channel,0xB7,(LP_B7_Data|0x0080));
    delay_ms(5);
				
		SSD2828_W_Reg(SigMode,channel,0xC1,0x001D); //���ظ�������
		buffer1[0] = 0x01;
		buffer1[1] = 0xBB;     		
		SSD2828_W_Array(SigMode,channel,buffer1,0);              
		delay_ms(5);  
		SSD2828_W_Reg(SigMode,channel,0xD4,0x00FA);
		SSD2828_W_Cmd(SigMode,channel,0xFF);
		delay_ms(5);
		for(i=0;i<29;i++)
		{
				SSD2828_W_Cmd(SigMode,channel,0xFA);		
				tmp=SPI3_Read_u16_Data(channel);
				buffer[4+i]=tmp>>8;
				buffer[5+i]=tmp;
				i++;
				delay_ms(5);
		}
    buffer[0]=0x2D;	//ICN9608
    buffer[1]=0x09;
		delay_ms(15);
    STM2PC_RM671xx(pdev,CDC_IN_EP,buffer,buffer[2]+3);          //���� ��ȡ�� Gamma����  ��ok  		

    SSD2828_W_Reg(SigMode,channel,0xB7,HS_B7_Data);
}

void ICN9608_Gamma_OTP_Start(USB_OTG_CORE_HANDLE *pdev,uint8_t  ep_addr,u8 SigMode ,u8 channel,u8* buffer,u16 LP_B7_Data,u16 HS_B7_Data)
{
    u8 buffer1[10];
	  u8 mtp_flag;
	  SSD2828_W_Reg(SigMode,channel,0xB7,LP_B7_Data);
		delay_ms(15); 
    buffer1[0] = 0x03;
    buffer1[1] = 0xF1;
    buffer1[2] = 0x5A;
    buffer1[3] = 0x5A;                       //OTP_Register_Access_Enable
    SSD2828_W_Array(SigMode,channel,buffer1,0); 
		delay_ms(10); 
    buffer1[0] = 0x02;
    buffer1[1] = 0xd2;
    buffer1[2] = USB_Rx_Buffer[4];  //OTP��¼��
		SSD2828_W_Array(SigMode,channel,buffer1,0); 
		delay_ms(10); 

    buffer1[0] = 0x02;
    buffer1[1] = 0xD0;
    buffer1[2] = 0x04;    //OTP_MODE_Enable
		SSD2828_W_Array(SigMode,channel,buffer1,0);	
		delay_ms(10); 	
	
    buffer1[0] = 0x02;
    buffer1[1] = 0xD0;
    buffer1[2] = 0x05;    //OTP_MODE_Enable
		SSD2828_W_Array(SigMode,channel,buffer1,0);	
		delay_ms(120); 	

    buffer1[0] = 0x02;
    buffer1[1] = 0xD0;
    buffer1[2] = 0x04;    //OTP_Program_Enable
		SSD2828_W_Array(SigMode,channel,buffer1,0);	
		delay_ms(120); 	

    buffer1[0] = 0x02;
    buffer1[1] = 0xD0;
    buffer1[2] = 0x00;    //OTP_Program_End
		SSD2828_W_Array(SigMode,channel,buffer1,0);	
		delay_ms(10); 	
	
    buffer1[0] = 0x03;
    buffer1[1] = 0xF1;
    buffer1[2] = 0xA5;
    buffer1[3] = 0xA5;                       //OTP_Register_Access_Disable
    SSD2828_W_Array(SigMode,channel,buffer1,0); 	
		delay_ms(10);  	
		
		buffer[1] = 0x0A;
		buffer[2] = 0x02;
		buffer[3] = Uart_Error_None;   //���� OTP Gamma����  ��OK
		buffer[4] = 0;
		STM2PC_RM671xx(pdev,CDC_IN_EP,buffer,buffer[2]+3);         
}


void ICN9608_Read_OTP_Times(USB_OTG_CORE_HANDLE *pdev,uint8_t  ep_addr,u8 SigMode ,u8 channel,u8* buffer,u16 LP_B7_Data,u16 HS_B7_Data)
{
    u16 tmp;
    buffer[1]=0x03;
    SSD2828_W_Reg(SigMode,channel,0xB7,LP_B7_Data|0x0080);                               
    SSD2828_W_Reg(SigMode,channel,0xC1,0x0001); //return package size                                
    SSD2828_W_Reg(SigMode,channel,0xC0,0x0001);
    buffer[0] = 0x01;
    buffer[1] = 0xD8;                        
    SSD2828_W_Array(SigMode,channel,buffer,0);
    delay_ms(5);  
		SSD2828_W_Reg(SigMode,channel,0xD4,0x00FA);
    SSD2828_W_Cmd(SigMode,channel,0xFF);

		SSD2828_W_Cmd(SigMode,channel,0xFA);		
		buffer[4]=SPI3_Read_u8_Data(channel);
		delay_ms(5);

		buffer[0]=0x2D;	
		buffer[1]=0x0B;
    SSD2828_W_Reg(SigMode,channel,0xB7,HS_B7_Data);
	
    STM2PC_RM671xx(pdev,CDC_IN_EP,buffer,buffer[2]+3);           //���� ��ȡ�� �Ĵ�������  ��ok 
}
/********************************************************************************************/

/********************************************************************************************
********************************************************************************************
    CH13721  д�Ĵ��� ���Ĵ���  дGamma ��Gamma OTP��¼
    CH13721  д�Ĵ��� ���Ĵ���  дGamma ��Gamma OTP��¼
    CH13721  д�Ĵ��� ���Ĵ���  дGamma ��Gamma OTP��¼
    CH13721  д�Ĵ��� ���Ĵ���  дGamma ��Gamma OTP��¼
    CH13721  д�Ĵ��� ���Ĵ���  дGamma ��Gamma OTP��¼
********************************************************************************************
********************************************************************************************/
void CH13721_Write_Register(USB_OTG_CORE_HANDLE *pdev,uint8_t  ep_addr,u8 SigMode ,u8 channel,u8* buffer,u16 LP_B7_Data,u16 HS_B7_Data)
{
		if(USB_Rx_Buffer[1]&0x80)
			SSD2828_W_Reg(SigMode,channel,0xB7,(HS_B7_Data|0x0010)&0XFFFD);
		else
			SSD2828_W_Reg(SigMode,channel,0xB7,LP_B7_Data);
    SSD2828_W_Array(SigMode,channel,buffer,2);                               

    delay_ms(5);
    SSD2828_W_Reg(SigMode,channel,0xB7,HS_B7_Data);
    buffer[4] = Uart_Error_None;
    buffer[5] = 0;
    STM2PC_RM671xx(pdev,CDC_IN_EP,buffer,buffer[2]+3);           //���� д�Ĵ���״̬  ��ok  
}
void CH13721_Write_51Register(USB_OTG_CORE_HANDLE *pdev,uint8_t  ep_addr,u8 SigMode ,u8 channel,u8* buffer,u16 LP_B7_Data,u16 HS_B7_Data)
{
    SSD2828_W_Reg(SigMode,channel,0xB7,0x0019);
	  //SSD2828_W_Reg(SigMode,channel,0xB7,LP_B7_Data);
		delay_ms(5);
    SSD2828_W_Array(SigMode,channel,buffer,2);                               

    delay_ms(5);
    SSD2828_W_Reg(SigMode,channel,0xB7,HS_B7_Data);
    buffer[4] = Uart_Error_None;
    buffer[5] = 0;
    STM2PC_RM671xx(pdev,CDC_IN_EP,buffer,buffer[2]+3);           //���� д�Ĵ���״̬  ��ok  
}
void CH13721_Read_Register(USB_OTG_CORE_HANDLE *pdev,uint8_t  ep_addr,u8 SigMode ,u8 channel,u8* buffer,u16 LP_B7_Data,u16 HS_B7_Data)
{
   u16 tmp;
    buffer[1]=0x03;
    SSD2828_W_Reg(SigMode,channel,0xB7,LP_B7_Data|0x0080);                               
    SSD2828_W_Reg(SigMode,channel,0xC1,buffer[2]); //return package size                                
    SSD2828_W_Reg(SigMode,channel,0xC0,0x0001);
    buffer[0] = 0x01;
    buffer[1] = buffer[3];                        
    SSD2828_W_Array(SigMode,channel,buffer,0);
    delay_ms(5);  
    SSD2828_W_Reg(SigMode,channel,0xD4,0x00FA);
    SSD2828_W_Cmd(SigMode,channel,0xFF);
    for(i=0;i<buffer[2]-1;i++)
    {
        SSD2828_W_Cmd(SigMode,channel,0xFA);		
        tmp=SPI3_Read_u16_Data(channel);
        buffer[4+i]=tmp>>8;
        buffer[5+i]=tmp;
        delay_ms(5);
        i++;
    }
		buffer[0]=0x4A;	
		buffer[1]=0x02;
    SSD2828_W_Reg(SigMode,channel,0xB7,HS_B7_Data);
    STM2PC_RM671xx(pdev,CDC_IN_EP,buffer,buffer[2]+3);           //���� ��ȡ�� �Ĵ�������  ��ok 
}

void CH13721_Write_Gamma(USB_OTG_CORE_HANDLE *pdev,uint8_t  ep_addr,u8 SigMode ,u8 channel,u8* buffer,u16 LP_B7_Data,u16 HS_B7_Data)
{
    u8 buffer1[7];
    u8 i,j,m,k=0,temp;
	
	  SSD2828_W_Reg(SigMode,channel,0xB7,0x0019);
		delay_ms(5);
	   
	  
	  
	  if((buffer[3]&0xf0)==0x00)       //GAMMA RED
	  {
	     if((buffer[4]&0x03)==0x00)
		   {
				 SSD2828_W_Reg(SigMode,channel,0xBC,16); 
		     SSD2828_W_Cmd(SigMode,channel,0xBF);	
			   SSD2828_W_Data(SigMode,channel,0xB2); //GAMMA RED GROUP1
		   }
		   else if((buffer[4]&0x03)==0x01)
		   {
				 SSD2828_W_Reg(SigMode,channel,0xBC,16); 
		     SSD2828_W_Cmd(SigMode,channel,0xBF);	
			   SSD2828_W_Data(SigMode,channel,0xB3); //GAMMA RED GROUP2
		   }
		   else if((buffer[4]&0x03)==0x02)
		   {
				 SSD2828_W_Reg(SigMode,channel,0xBC,14); 
		     SSD2828_W_Cmd(SigMode,channel,0xBF);	
			   SSD2828_W_Data(SigMode,channel,0xB4); //GAMMA RED GROUP3
		   }
	  }
		else if((buffer[3]&0xf0)==0x40)  //GAMMA GREEN
		{
			if((buffer[4]&0x03)==0x00)
		   {
				 SSD2828_W_Reg(SigMode,channel,0xBC,16); 
		     SSD2828_W_Cmd(SigMode,channel,0xBF);	
			   SSD2828_W_Data(SigMode,channel,0xB5); //GAMMA GREEN GROUP1
		   }
		   else if((buffer[4]&0x03)==0x01)
		   {
				 SSD2828_W_Reg(SigMode,channel,0xBC,16); 
		     SSD2828_W_Cmd(SigMode,channel,0xBF);	
			   SSD2828_W_Data(SigMode,channel,0xB6); //GAMMA GREEN GROUP2
		   }
		   else if((buffer[4]&0x03)==0x02)
		   {
				 SSD2828_W_Reg(SigMode,channel,0xBC,14); 
		     SSD2828_W_Cmd(SigMode,channel,0xBF);	
			   SSD2828_W_Data(SigMode,channel,0xB7); //GAMMA GREEN GROUP3
		   }
		}
		else if((buffer[3]&0xf0)==0x80)   //GAMMA BLUE
		{
			if((buffer[4]&0x03)==0x00)
		   {
				 SSD2828_W_Reg(SigMode,channel,0xBC,16); 
		     SSD2828_W_Cmd(SigMode,channel,0xBF);	
			   SSD2828_W_Data(SigMode,channel,0xB8); //GAMMA BLUE GROUP1
		   }
		   else if((buffer[4]&0x03)==0x01)
		   {
				 SSD2828_W_Reg(SigMode,channel,0xBC,16); 
		     SSD2828_W_Cmd(SigMode,channel,0xBF);	
			   SSD2828_W_Data(SigMode,channel,0xB9); //GAMMA BLUE GROUP2
		   }
		   else if((buffer[4]&0x03)==0x02)
		   {
				 SSD2828_W_Reg(SigMode,channel,0xBC,14); 
		     SSD2828_W_Cmd(SigMode,channel,0xBF);	
			   SSD2828_W_Data(SigMode,channel,0xBA); //GAMMA BLUE GROUP3
		   }
		} 
    
					
		if(((buffer[4]&0x03)==0x00)||((buffer[4]&0x03)==0x01))
		{
		   for( i = 0;i<15;i++)   //15�����
		   {	                                                                                  
			   SSD2828_W_Data(SigMode,channel,buffer[i+5]);	
			   delay_ms(5);  
		   }
	  }
		else if((buffer[4]&0x03)==0x02)
		{
			for( i = 0;i<13;i++)   //13�����
		   {	                                                                                  
			   SSD2828_W_Data(SigMode,channel,buffer[i+5]);	
			   delay_ms(5);  
		   }
		}
		
		buffer1[0] = 0x02;        
    buffer1[1] = 0xBE;
    buffer1[2] = 0x01;    
    SSD2828_W_Array(SigMode,channel,buffer1,0);		 
	  delay_ms(5); 
				
    buffer[4] = Uart_Error_None;
    buffer[5] = 0;
    STM2PC_RM671xx(pdev,CDC_IN_EP,buffer,buffer[2]+3);            //���� д�Ĵ���״̬  ��ok  
	  SSD2828_W_Reg(SigMode,channel,0xB7,HS_B7_Data);
}

void CH13721_Read_Gamma(USB_OTG_CORE_HANDLE *pdev,uint8_t  ep_addr,u8 SigMode ,u8 channel,u8* buffer,u16 LP_B7_Data,u16 HS_B7_Data)
{
    u8 buffer1[7];
    u16 tmp;
    u8 i,j,k=0;
		
		 if((buffer[3]&0xf0)==0x00)       //GAMMA RED
	  {
	     if((buffer[4]&0x03)==0x00)
		   {
				 SSD2828_W_Reg(SigMode,channel,0xB7,LP_B7_Data|0x0080);
	       SSD2828_W_Reg(SigMode,channel,0xC1,0x000F); //���ظ�������
		     SSD2828_W_Reg(SigMode,channel,0xC0,0x0001); 
		     delay_ms(10);
				 
			   buffer1[0] = 0x02;        
         buffer1[1] = 0xB2;          
         SSD2828_W_Array(SigMode,channel,buffer1,0);		 
	       delay_ms(5);                 //GAMMA RED GROUP1
		   }
		   else if((buffer[4]&0x03)==0x01)
		   {
				 SSD2828_W_Reg(SigMode,channel,0xB7,LP_B7_Data|0x0080);
	       SSD2828_W_Reg(SigMode,channel,0xC1,0x000F); //���ظ�������
		     SSD2828_W_Reg(SigMode,channel,0xC0,0x0001); 
		     delay_ms(10);
				 
			   buffer1[0] = 0x02;        
         buffer1[1] = 0xB3;          
         SSD2828_W_Array(SigMode,channel,buffer1,0);		 
	       delay_ms(5);  
		   }
		   else if((buffer[4]&0x03)==0x02)
		   {
				 SSD2828_W_Reg(SigMode,channel,0xB7,LP_B7_Data|0x0080);
	       SSD2828_W_Reg(SigMode,channel,0xC1,0x000D); //���ظ�������
		     SSD2828_W_Reg(SigMode,channel,0xC0,0x0001); 
		     delay_ms(10);
				 
			   buffer1[0] = 0x02;        
         buffer1[1] = 0xB4;          
         SSD2828_W_Array(SigMode,channel,buffer1,0);		 
	       delay_ms(5);  
		   }
	  }
		else if((buffer[3]&0xf0)==0x40)  //GAMMA GREEN
		{
			if((buffer[4]&0x03)==0x00)
		   {
			  SSD2828_W_Reg(SigMode,channel,0xB7,LP_B7_Data|0x0080);
	       SSD2828_W_Reg(SigMode,channel,0xC1,0x000F); //���ظ�������
		     SSD2828_W_Reg(SigMode,channel,0xC0,0x0001); 
		     delay_ms(10);
				 
			   buffer1[0] = 0x02;        
         buffer1[1] = 0xB5;          
         SSD2828_W_Array(SigMode,channel,buffer1,0);		 
	       delay_ms(5);                 //GAMMA RED GROUP1
		   }
		   else if((buffer[4]&0x03)==0x01)
		   {
			   SSD2828_W_Reg(SigMode,channel,0xB7,LP_B7_Data|0x0080);
	       SSD2828_W_Reg(SigMode,channel,0xC1,0x000F); //���ظ�������
		     SSD2828_W_Reg(SigMode,channel,0xC0,0x0001); 
		     delay_ms(10);
				 
			   buffer1[0] = 0x02;        
         buffer1[1] = 0xB6;          
         SSD2828_W_Array(SigMode,channel,buffer1,0);		 
	       delay_ms(5);                 //GAMMA RED GROUP1
		   }
		   else if((buffer[4]&0x03)==0x02)
		   {
			   SSD2828_W_Reg(SigMode,channel,0xB7,LP_B7_Data|0x0080);
	       SSD2828_W_Reg(SigMode,channel,0xC1,0x000D); //���ظ�������
		     SSD2828_W_Reg(SigMode,channel,0xC0,0x0001); 
		     delay_ms(10);
				 
			   buffer1[0] = 0x02;        
         buffer1[1] = 0xB7;          
         SSD2828_W_Array(SigMode,channel,buffer1,0);		 
	       delay_ms(5);  
		   }
		}
		else if((buffer[3]&0xf0)==0x80)   //GAMMA BLUE
		{
			if((buffer[4]&0x03)==0x00)
		   {
			  SSD2828_W_Reg(SigMode,channel,0xB7,LP_B7_Data|0x0080);
	       SSD2828_W_Reg(SigMode,channel,0xC1,0x000F); //���ظ�������
		     SSD2828_W_Reg(SigMode,channel,0xC0,0x0001); 
		     delay_ms(10);
				 
			   buffer1[0] = 0x02;        
         buffer1[1] = 0xB8;          
         SSD2828_W_Array(SigMode,channel,buffer1,0);		 
	       delay_ms(5);                 //GAMMA RED GROUP1 
		   }
		   else if((buffer[4]&0x03)==0x01)
		   {
			 SSD2828_W_Reg(SigMode,channel,0xB7,LP_B7_Data|0x0080);
	       SSD2828_W_Reg(SigMode,channel,0xC1,0x000F); //���ظ�������
		     SSD2828_W_Reg(SigMode,channel,0xC0,0x0001); 
		     delay_ms(10);
				 
			   buffer1[0] = 0x02;        
         buffer1[1] = 0xB9;          
         SSD2828_W_Array(SigMode,channel,buffer1,0);		 
	       delay_ms(5);                 //GAMMA RED GROUP1
		   }
		   else if((buffer[4]&0x03)==0x02)
		   {
			   SSD2828_W_Reg(SigMode,channel,0xB7,LP_B7_Data|0x0080);
	       SSD2828_W_Reg(SigMode,channel,0xC1,0x000D); //���ظ�������
		     SSD2828_W_Reg(SigMode,channel,0xC0,0x0001); 
		     delay_ms(10);
				 
			   buffer1[0] = 0x02;        
         buffer1[1] = 0xBA;          
         SSD2828_W_Array(SigMode,channel,buffer1,0);		 
	       delay_ms(5);  
		   }
		} 
		
		SSD2828_W_Reg(SigMode,channel,0xD4,0x00FA);
		SSD2828_W_Cmd(SigMode,channel,0xFF);
		delay_ms(5);
		
		if(((buffer[4]&0x03)==0x00)||((buffer[4]&0x03)==0x01))
		{
			    for(i=0;i<15;i++)
		  {
				  SSD2828_W_Cmd(SigMode,channel,0xFA);		
				  tmp=SPI3_Read_u16_Data(channel);
				  buffer[4+i]=tmp>>8;
				  buffer[5+i]=tmp;
			    delay_ms(5);
				  i++;			
		  }
		}
		else if((buffer[4]&0x03)==0x02)
		{
			for(i=0;i<13;i++)
		  {
				  SSD2828_W_Cmd(SigMode,channel,0xFA);		
				  tmp=SPI3_Read_u16_Data(channel);
				  buffer[4+i]=tmp>>8;
				  buffer[5+i]=tmp;
			    delay_ms(5);
				  i++;			
		  }
		}
		
    buffer[0]=0x4A;	//CH13721
    buffer[1]=0x09;
		delay_ms(15);
    STM2PC_RM671xx(pdev,CDC_IN_EP,buffer,buffer[2]+3);          //���� ��ȡ�� Gamma����  ��ok  		

    SSD2828_W_Reg(SigMode,channel,0xB7,HS_B7_Data);
}

void CH13721_Gamma_OTP_Start(USB_OTG_CORE_HANDLE *pdev,uint8_t  ep_addr,u8 SigMode ,u8 channel,u8* buffer,u16 LP_B7_Data,u16 HS_B7_Data)
{
    u8 buffer1[10];
	  u8 mtp_flag;
	  SSD2828_W_Reg(SigMode,channel,0xB7,LP_B7_Data);
		delay_ms(15); 
    buffer1[0] = 0x02;
    buffer1[1] = 0xEB;
    buffer1[2] = 0x10;         //OTP_Register_Access_Enable                      
    SSD2828_W_Array(SigMode,channel,buffer1,0); 
		delay_ms(100); 
	
	 if(buffer[1]==0x0A)  //otp Gamma
	 {
		 buffer1[0] = 0x02;
     buffer1[1] = 0xED;
     buffer1[2] = 0x40;  //OTP��¼��
		 SSD2828_W_Array(SigMode,channel,buffer1,0); 
		delay_ms(10); 
	 }
	 else if(buffer[1]==0x0D)  //otp HBM
	 {
		 buffer1[0] = 0x02;
     buffer1[1] = 0xED;
     buffer1[2] = 0x20;  //OTP��¼��
		 SSD2828_W_Array(SigMode,channel,buffer1,0); 
		delay_ms(10); 
	 }
    else if(buffer[1]==0x0E)  //otp GOA
	 {
		 buffer1[0] = 0x02;
     buffer1[1] = 0xED;
     buffer1[2] = 0x02;  //OTP��¼��
		 SSD2828_W_Array(SigMode,channel,buffer1,0); 
		delay_ms(10); 
	 }
   else if(buffer[1]==0x0E)  //otp ID
	 {
		 buffer1[0] = 0x02;
     buffer1[1] = 0xED;
     buffer1[2] = 0x80;  //OTP��¼��
		 SSD2828_W_Array(SigMode,channel,buffer1,0); 
		delay_ms(10); 
	 }
	 
    buffer1[0] = 0x04;
    buffer1[1] = 0xEE;
    buffer1[2] = 0xA5;    //OTP_MODE_ACTIVE
	  buffer1[3] = 0x5A;    //OTP_MODE_ACTIVE
	  buffer1[4] = 0x3C;    //OTP_MODE_ACTIVE
		SSD2828_W_Array(SigMode,channel,buffer1,0);	
		delay_ms(500); 	
	
     buffer1[0] = 0x02;
     buffer1[1] = 0xED;
     buffer1[2] = 0x00;  //
		 SSD2828_W_Array(SigMode,channel,buffer1,0); 
		delay_ms(10);     	
		
		buffer[1] = 0x0A;
		buffer[2] = 0x02;
		buffer[3] = Uart_Error_None;   //���� OTP Gamma����  ��OK
		buffer[4] = 0;
		STM2PC_RM671xx(pdev,CDC_IN_EP,buffer,buffer[2]+3);         
}


void CH13721_Read_OTP_Times(USB_OTG_CORE_HANDLE *pdev,uint8_t  ep_addr,u8 SigMode ,u8 channel,u8* buffer,u16 LP_B7_Data,u16 HS_B7_Data)
{
    u16 tmp;
    buffer[1]=0x03;
    SSD2828_W_Reg(SigMode,channel,0xB7,LP_B7_Data|0x0080);                               
    SSD2828_W_Reg(SigMode,channel,0xC1,0x0001); //return package size                                
    SSD2828_W_Reg(SigMode,channel,0xC0,0x0001);
    buffer[0] = 0x01;
    buffer[1] = 0xD8;                        
    SSD2828_W_Array(SigMode,channel,buffer,0);
    delay_ms(5);  
		SSD2828_W_Reg(SigMode,channel,0xD4,0x00FA);
    SSD2828_W_Cmd(SigMode,channel,0xFF);

		SSD2828_W_Cmd(SigMode,channel,0xFA);		
		buffer[4]=SPI3_Read_u8_Data(channel);
		delay_ms(5);

		buffer[0]=0x2D;	
		buffer[1]=0x0B;
    SSD2828_W_Reg(SigMode,channel,0xB7,HS_B7_Data);
	
    STM2PC_RM671xx(pdev,CDC_IN_EP,buffer,buffer[2]+3);           //���� ��ȡ�� �Ĵ�������  ��ok 
}


/********************************************************************************************
********************************************************************************************
    UD61720  д�Ĵ��� ���Ĵ���  дGamma ��Gamma OTP��¼
    UD61720  д�Ĵ��� ���Ĵ���  дGamma ��Gamma OTP��¼
    UD61720  д�Ĵ��� ���Ĵ���  дGamma ��Gamma OTP��¼
    UD61720  д�Ĵ��� ���Ĵ���  дGamma ��Gamma OTP��¼
    UD61720  д�Ĵ��� ���Ĵ���  дGamma ��Gamma OTP��¼
********************************************************************************************
********************************************************************************************/
void UD61720_Write_Register(USB_OTG_CORE_HANDLE *pdev,uint8_t  ep_addr,u8 SigMode ,u8 channel,u8* buffer,u16 LP_B7_Data,u16 HS_B7_Data)
{
	  u8 buffer1[7];
		if(USB_Rx_Buffer[1]&0x80)
			SSD2828_W_Reg(SigMode,channel,0xB7,(HS_B7_Data|0x0010)&0XFFFD);
		else
			SSD2828_W_Reg(SigMode,channel,0xB7,LP_B7_Data);	
    SSD2828_W_Array(SigMode,channel,buffer,2);  
    delay_ms(5);		

    buffer1[0] = 0x02;        
    buffer1[1] = 0x80;
    buffer1[2] = 0x10;    
    SSD2828_W_Array(SigMode,channel,buffer1,0);		 
	  delay_ms(5); 		

    SSD2828_W_Reg(SigMode,channel,0xB7,HS_B7_Data);
    buffer[4] = Uart_Error_None;
    buffer[5] = 0;
    STM2PC_RM671xx(pdev,CDC_IN_EP,buffer,buffer[2]+3);           //���� д�Ĵ���״̬  ��ok  
}
void UD61720_Write_51Register(USB_OTG_CORE_HANDLE *pdev,uint8_t  ep_addr,u8 SigMode ,u8 channel,u8* buffer,u16 LP_B7_Data,u16 HS_B7_Data)
{
    SSD2828_W_Reg(SigMode,channel,0xB7,0x0019);
	  //SSD2828_W_Reg(SigMode,channel,0xB7,LP_B7_Data);
		delay_ms(5);
    SSD2828_W_Array(SigMode,channel,buffer,2);                               

    delay_ms(5);
    SSD2828_W_Reg(SigMode,channel,0xB7,HS_B7_Data);
    buffer[4] = Uart_Error_None;
    buffer[5] = 0;
    STM2PC_RM671xx(pdev,CDC_IN_EP,buffer,buffer[2]+3);           //���� д�Ĵ���״̬  ��ok  
}
void UD61720_Read_Register(USB_OTG_CORE_HANDLE *pdev,uint8_t  ep_addr,u8 SigMode ,u8 channel,u8* buffer,u16 LP_B7_Data,u16 HS_B7_Data)
{
    u16 tmp;
    buffer[1]=0x03;
    SSD2828_W_Reg(SigMode,channel,0xB7,LP_B7_Data|0x0080);                               
    SSD2828_W_Reg(SigMode,channel,0xC1,buffer[2]); //return package size                                
    SSD2828_W_Reg(SigMode,channel,0xC0,0x0001);
    buffer[0] = 0x01;
    buffer[1] = buffer[3];                        
    SSD2828_W_Array(SigMode,channel,buffer,0);
    delay_ms(5);  
    SSD2828_W_Reg(SigMode,channel,0xD4,0x00FA);
    SSD2828_W_Cmd(SigMode,channel,0xFF);
    for(i=0;i<buffer[2]-1;i++)
    {
        SSD2828_W_Cmd(SigMode,channel,0xFA);		
        tmp=SPI3_Read_u16_Data(channel);
        buffer[4+i]=tmp>>8;
        buffer[5+i]=tmp;
        delay_ms(5);
        i++;
    }
		buffer[0]=0x4C;	
		buffer[1]=0x02;
    SSD2828_W_Reg(SigMode,channel,0xB7,HS_B7_Data);
    STM2PC_RM671xx(pdev,CDC_IN_EP,buffer,buffer[2]+3);           //���� ��ȡ�� �Ĵ�������  ��ok 
}

void UD61720_Write_Gamma(USB_OTG_CORE_HANDLE *pdev,uint8_t  ep_addr,u8 SigMode ,u8 channel,u8* buffer,u16 LP_B7_Data,u16 HS_B7_Data)
{
    u8 buffer1[7];
    u8 i,j,m,k=0,temp;
	
	  SSD2828_W_Reg(SigMode,channel,0xB7,0x0019);
		delay_ms(5);
	
	  buffer1[0] = 0x02;        
    buffer1[1] = 0x80;
    buffer1[2] = 0x10;    
    SSD2828_W_Array(SigMode,channel,buffer1,0);		 
	  delay_ms(5); 
	
	  if((buffer[3]&0xFF)==0x00)       //GAMMA RED
	  {
				 SSD2828_W_Reg(SigMode,channel,0xBC,22); 
		     SSD2828_W_Cmd(SigMode,channel,0xBF);	
			   SSD2828_W_Data(SigMode,channel,0xC5);    //GAMMA RED   
	  }
		else if((buffer[3]&0xFF)==0x42)  //GAMMA GREEN
		{	
				 SSD2828_W_Reg(SigMode,channel,0xBC,22); 
		     SSD2828_W_Cmd(SigMode,channel,0xBF);	
			   SSD2828_W_Data(SigMode,channel,0xC6); //GAMMA GREEN 
		}
		else if((buffer[3]&0xFF)==0x81)   //GAMMA BLUE
		{		
				 SSD2828_W_Reg(SigMode,channel,0xBC,22); 
		     SSD2828_W_Cmd(SigMode,channel,0xBF);	
			   SSD2828_W_Data(SigMode,channel,0xC7); //GAMMA BLUE 
		} 
    	
		   for( i = 0;i<21;i++)   //21�����
		   {	                                                                                  
			   SSD2828_W_Data(SigMode,channel,buffer[i+4]);	
			   delay_ms(5);  
		   }
		
		buffer1[0] = 0x02;        
    buffer1[1] = 0xBE;
    buffer1[2] = 0x01;    
    SSD2828_W_Array(SigMode,channel,buffer1,0);		 
	  delay_ms(5); 
				
    buffer[4] = Uart_Error_None;
    buffer[5] = 0;
    STM2PC_RM671xx(pdev,CDC_IN_EP,buffer,buffer[2]+3);            //���� д�Ĵ���״̬  ��ok  
	  SSD2828_W_Reg(SigMode,channel,0xB7,HS_B7_Data);
}

void UD61720_Read_Gamma(USB_OTG_CORE_HANDLE *pdev,uint8_t  ep_addr,u8 SigMode ,u8 channel,u8* buffer,u16 LP_B7_Data,u16 HS_B7_Data)
{
    u8 buffer1[7];
    u16 tmp;
    u8 i,j,k=0;
		
		 if((buffer[3]&0x0F)==0x00)       //GAMMA RED
	  {   
				 SSD2828_W_Reg(SigMode,channel,0xB7,LP_B7_Data|0x0080);
	       SSD2828_W_Reg(SigMode,channel,0xC1,0x0016); //���ظ�������
		     SSD2828_W_Reg(SigMode,channel,0xC0,0x0001); 
		     delay_ms(10);
				 
			   buffer1[0] = 0x02;        
         buffer1[1] = 0xC5;          
         SSD2828_W_Array(SigMode,channel,buffer1,0);		 
	       delay_ms(5);                 //GAMMA RED GROUP1
		  
	  }
		else if((buffer[3]&0x0F)==0x04)  //GAMMA GREEN
		{
			  SSD2828_W_Reg(SigMode,channel,0xB7,LP_B7_Data|0x0080);
	       SSD2828_W_Reg(SigMode,channel,0xC1,0x0016); //���ظ�������
		     SSD2828_W_Reg(SigMode,channel,0xC0,0x0001); 
		     delay_ms(10);
				 
			   buffer1[0] = 0x02;        
         buffer1[1] = 0xC6;          
         SSD2828_W_Array(SigMode,channel,buffer1,0);		 
	       delay_ms(5);                 //GAMMA RED GROUP1   
		}
		else if((buffer[3]&0x0F)==0x08)   //GAMMA BLUE
		{
			  SSD2828_W_Reg(SigMode,channel,0xB7,LP_B7_Data|0x0080);
	       SSD2828_W_Reg(SigMode,channel,0xC1,0x0016); //���ظ�������
		     SSD2828_W_Reg(SigMode,channel,0xC0,0x0001); 
		     delay_ms(10);
				 
			   buffer1[0] = 0x02;        
         buffer1[1] = 0xC7;          
         SSD2828_W_Array(SigMode,channel,buffer1,0);		 
	       delay_ms(5);                 //GAMMA RED GROUP1 
		   
		} 
		
		SSD2828_W_Reg(SigMode,channel,0xD4,0x00FA);
		SSD2828_W_Cmd(SigMode,channel,0xFF);
		delay_ms(5);
		
			    for(i=0;i<21;i++)
		  {
				  SSD2828_W_Cmd(SigMode,channel,0xFA);		
				  tmp=SPI3_Read_u16_Data(channel);
				  buffer[4+i]=tmp>>8;
				  buffer[5+i]=tmp;
			    delay_ms(5);
				  i++;			
		  }
			
    buffer[0]=0x4C;	//UD61720
    buffer[1]=0x09;
		delay_ms(15);
    STM2PC_RM671xx(pdev,CDC_IN_EP,buffer,buffer[2]+3);          //���� ��ȡ�� Gamma����  ��ok  		

    SSD2828_W_Reg(SigMode,channel,0xB7,HS_B7_Data);
}

void UD61720_Gamma_OTP_Start(USB_OTG_CORE_HANDLE *pdev,uint8_t  ep_addr,u8 SigMode ,u8 channel,u8* buffer,u16 LP_B7_Data,u16 HS_B7_Data)
{
    u8 buffer1[10];
	  u8 mtp_flag;
	  SSD2828_W_Reg(SigMode,channel,0xB7,LP_B7_Data);
		delay_ms(15); 
	
	  buffer1[0] = 0x02;        
    buffer1[1] = 0x80;
    buffer1[2] = 0x10;    
    SSD2828_W_Array(SigMode,channel,buffer1,0);		 
	  delay_ms(5); 				
	  
//	  buffer1[0] = 0x02;        
//    buffer1[1] = 0x10;
//    buffer1[2] = 0x00;    
//    SSD2828_W_Array(SigMode,channel,buffer1,0);		 
//	  delay_ms(100); 	

//    buffer1[0] = 0x02;        
//    buffer1[1] = 0x80;
//    buffer1[2] = 0x10;    
//    SSD2828_W_Array(SigMode,channel,buffer1,0);		 
//	  delay_ms(5); 		
	
	 if(buffer[1]==0x0A)  //otp Gamma
	 {
		 buffer1[0] = 0x06;
     buffer1[1] = 0x82;
		 buffer1[2] = 0x00;
     buffer1[3] = 0x00; 
     buffer1[4] = 0x00;
		 buffer1[5] = 0x04;
     buffer1[6] = 0x01;		
		 SSD2828_W_Array(SigMode,channel,buffer1,0); 
		delay_ms(500); 
	 }
	 else if(buffer[1]==0x0D)  //otp GOA
	 {
		 buffer1[0] = 0x06;
     buffer1[1] = 0x82;
		 buffer1[2] = 0x00;
     buffer1[3] = 0x00; 
     buffer1[4] = 0x00;
		 buffer1[5] = 0x05;
     buffer1[6] = 0x01;	  
		 SSD2828_W_Array(SigMode,channel,buffer1,0); 
		delay_ms(500); 
	 }
    else if(buffer[1]==0x0E)  //otp POWER
	 {
		 buffer1[0] = 0x06;
     buffer1[1] = 0x82;
		 buffer1[2] = 0x00;
     buffer1[3] = 0x00; 
     buffer1[4] = 0x00;
		 buffer1[5] = 0x06;
     buffer1[6] = 0x01; 
		 SSD2828_W_Array(SigMode,channel,buffer1,0); 
		delay_ms(500); 
	 }
   else if(buffer[1]==0x0F)  //otp OTHER
	 {
		 buffer1[0] = 0x06;
     buffer1[1] = 0x82;
		 buffer1[2] = 0x00;
     buffer1[3] = 0x00; 
     buffer1[4] = 0x00;
		 buffer1[5] = 0x07;
     buffer1[6] = 0x01; 
		 SSD2828_W_Array(SigMode,channel,buffer1,0); 
		delay_ms(500); 
	 }
		
		buffer[1] = 0x0A;
		buffer[2] = 0x02;
		buffer[3] = Uart_Error_None;   //���� OTP Gamma����  ��OK
		buffer[4] = 0;
		STM2PC_RM671xx(pdev,CDC_IN_EP,buffer,buffer[2]+3);         
}


void UD61720_Read_OTP_Times(USB_OTG_CORE_HANDLE *pdev,uint8_t  ep_addr,u8 SigMode ,u8 channel,u8* buffer,u16 LP_B7_Data,u16 HS_B7_Data)
{
}

/********************************************************************************************
********************************************************************************************
    NT37710  д�Ĵ��� ���Ĵ���  дGamma ��Gamma OTP��¼
    NT37710  д�Ĵ��� ���Ĵ���  дGamma ��Gamma OTP��¼
    NT37710  д�Ĵ��� ���Ĵ���  дGamma ��Gamma OTP��¼
    NT37710  д�Ĵ��� ���Ĵ���  дGamma ��Gamma OTP��¼
    NT37710  д�Ĵ��� ���Ĵ���  дGamma ��Gamma OTP��¼
********************************************************************************************
********************************************************************************************/
void NT37710_Write_Register(USB_OTG_CORE_HANDLE *pdev,uint8_t  ep_addr,u8 SigMode ,u8 channel,u8* buffer,u16 LP_B7_Data,u16 HS_B7_Data)
{
		if(SigMode != CMD_Mode)
			SSD2828_W_Reg(SigMode,channel,0xB7,(HS_B7_Data|0x0010)&0XFFFD);
		else
			SSD2828_W_Reg(SigMode,channel,0xB7,LP_B7_Data);
		delay_ms(15);
    SSD2828_W_Array(SigMode,channel,buffer,2);                               

    delay_ms(5);
    SSD2828_W_Reg(SigMode,channel,0xB7,HS_B7_Data);
    buffer[4] = Uart_Error_None;
    buffer[5] = 0;
    STM2PC_RM671xx(pdev,CDC_IN_EP,buffer,buffer[2]+3);           //���� д�Ĵ���״̬  ��ok  
}
void NT37710_Write_51Register(USB_OTG_CORE_HANDLE *pdev,uint8_t  ep_addr,u8 SigMode ,u8 channel,u8* buffer,u16 LP_B7_Data,u16 HS_B7_Data)
{
    if(SigMode != CMD_Mode)
        SSD2828_W_Reg(SigMode,channel,0xB7,(HS_B7_Data|0x0010)&0XFFFD);
		else
				SSD2828_W_Reg(SigMode,channel,0xB7,LP_B7_Data);
    delay_ms(15);
    SSD2828_W_Array(SigMode,channel,buffer,2);                               

    delay_ms(5);
    SSD2828_W_Reg(SigMode,channel,0xB7,HS_B7_Data);
    buffer[4] = Uart_Error_None;
    buffer[5] = 0;
    STM2PC_RM671xx(pdev,CDC_IN_EP,buffer,buffer[2]+3);           //���� д�Ĵ���״̬  ��ok  
}
void NT37710_Read_Register(USB_OTG_CORE_HANDLE *pdev,uint8_t  ep_addr,u8 SigMode ,u8 channel,u8* buffer,u16 LP_B7_Data,u16 HS_B7_Data)
{
    u16 tmp;
    buffer[1]=0x03; 
    SSD2828_W_Reg(SigMode,channel,0xB7,LP_B7_Data|0x0080);                               
    SSD2828_W_Reg(SigMode,channel,0xC1,buffer[2]-1); //return package size                                
    SSD2828_W_Reg(SigMode,channel,0xC0,0x0001);
    buffer[0] = 0x01;
    buffer[1] = buffer[3];                        
    SSD2828_W_Array(SigMode,channel,buffer,0);
    delay_ms(5);  
    SSD2828_W_Reg(SigMode,channel,0xD4,0x00FA);
    SSD2828_W_Cmd(SigMode,channel,0xFF);
    for(i=0;i<buffer[2]-1;i++)
    {
        SSD2828_W_Cmd(SigMode,channel,0xFA);		
        tmp=SPI3_Read_u16_Data(channel);
        buffer[4+i]=tmp>>8;
        buffer[5+i]=tmp;
        delay_ms(5);
        i++;
    }
		buffer[0]=0x2A;	
		buffer[1]=0x02;
    SSD2828_W_Reg(SigMode,channel,0xB7,HS_B7_Data);
    STM2PC_RM671xx(pdev,CDC_IN_EP,buffer,buffer[2]+3);           //���� ��ȡ�� �Ĵ�������  ��ok 
}

void NT37710_Write_Gamma(USB_OTG_CORE_HANDLE *pdev,uint8_t  ep_addr,u8 SigMode ,u8 channel,u8* buffer,u16 LP_B7_Data,u16 HS_B7_Data)
{
    u8 buffer1[7];
    u8 i,j,m,k=0,temp;
	
    if(SigMode != CMD_Mode)
        SSD2828_W_Reg(SigMode,channel,0xB7,(HS_B7_Data|0x0010)&0XFFFD);
		else
				SSD2828_W_Reg(SigMode,channel,0xB7,LP_B7_Data);
		delay_ms(15);

    buffer1[0] = 0x06;       
    buffer1[1] = 0xF0;
    buffer1[2] = 0x55;
    buffer1[3] = 0xAA;
    buffer1[4] = 0x52;
    buffer1[5] = 0x08;
    buffer1[6] = 0x00;                         // Page 2
    SSD2828_W_Array(SigMode,channel,buffer1,0);		
	  delay_ms(5);  

    buffer1[0] = 0x03;       
    buffer1[1] = 0xd9;
	  buffer1[2] = 0x00;
    buffer1[3] = 0x10;	
	  SSD2828_W_Array(SigMode,channel,buffer1,0);		// 
	 // delay_ms(5);  	
	
    buffer1[0] = 0x06;       
    buffer1[1] = 0xF0;
    buffer1[2] = 0x55;
    buffer1[3] = 0xAA;
    buffer1[4] = 0x52;
    buffer1[5] = 0x08;
    buffer1[6] = 0x02;                         // Page 2
    SSD2828_W_Array(SigMode,channel,buffer1,0);		
	  delay_ms(5);  

    buffer1[0] = 0x02;       
    buffer1[1] = 0xbf;
    buffer1[2] = (buffer[3]>>4)&0x0f;  	
	  SSD2828_W_Array(SigMode,channel,buffer1,0);		// 

			for(j=0;j<=2;j++)
			{
				if((buffer[3]&0x0f)==0x00) //re
				{
					switch(j)
					{
							case 0: temp = 0xB0;break; //RED         
							case 1: temp = 0xB1;break;
							case 2: temp = 0xB2;break;
							default :break;			
					}
				}
				else if((buffer[3]&0x0f)==0x01)
				{
					switch(j)
					{
							case 0: temp = 0xB3;break; //GREEN      
							case 1: temp = 0xB4;break;
							case 2: temp = 0xB5;break;
							default :break;			
					}
				}
				else if((buffer[3]&0x0f)==0x02)
				{
					switch(j)
					{
							case 0: temp = 0xB6;break; //BLUE         
							case 1: temp = 0xB7;break;
							case 2: temp = 0xB8;break;
							default :break;			
					}
				}	
				SSD2828_W_Reg(SigMode,channel,0xBC,17); 
				SSD2828_W_Cmd(SigMode,channel,0xBF);	
				SSD2828_W_Data(SigMode,channel,temp); //red-> b0,b1,b2
				for( i = 0;i<16;i++)   //23�����
				{	                                                                                  
					SSD2828_W_Data(SigMode,channel,buffer[k+4]);	
					k++;  
				}
			}	
			buffer[4] = Uart_Error_None;
			buffer[1] =  0x08;
			STM2PC_RM671xx(pdev,CDC_IN_EP,buffer,buffer[2]+3);           //���� дGamma״̬  ��ok 		

			buffer1[0] = 0x01;       
			buffer1[1] = 0x11;
			SSD2828_W_Array(SigMode,channel,buffer1,0);		// 
			delay_ms(2);
			buffer1[0] = 0x06;       
			buffer1[1] = 0xF0;
			buffer1[2] = 0x55;
			buffer1[3] = 0xAA;
			buffer1[4] = 0x52;
			buffer1[5] = 0x08;
			buffer1[6] = 0x00;                         // Page 2
			SSD2828_W_Array(SigMode,channel,buffer1,0);		

			buffer1[0] = 0x03;       
			buffer1[1] = 0xd9;
			buffer1[2] = 0x00;
			buffer1[3] = 0x00;	
			SSD2828_W_Array(SigMode,channel,buffer1,0);		// 
			delay_ms(5); 
		  
			SSD2828_W_Reg(SigMode,channel,0xB7,HS_B7_Data);
}

void NT37710_Write_Gamma_Short_Package(USB_OTG_CORE_HANDLE *pdev,uint8_t  ep_addr,u8 SigMode ,u8 channel,u8* buffer,u16 LP_B7_Data,u16 HS_B7_Data)
{
    u8 buffer1[7];
    u8 i,j,m,k=0,x;
		u32 temp_cx=0;
		//SSD2828_W_Reg(SigMode,channel,0xB7,LP_B7_Data);
	  SSD2828_W_Reg(SigMode,channel,0xB7,0x005b);
		delay_ms(10);

    buffer1[0] = 0x06;       
    buffer1[1] = 0xF0;
    buffer1[2] = 0x55;
    buffer1[3] = 0xAA;
    buffer1[4] = 0x52;
    buffer1[5] = 0x08;
    buffer1[6] = 0x02;                         // Page 2
    SSD2828_W_Array(SigMode,channel,buffer1,0);		
	  delay_ms(5);  

		for(x=0;x<3;x++)
		{
				switch(x)
				{
					case 0: buffer[3] = 0x00;break; //RED//GREEN//BLUE         
					case 1: buffer[3] = 0x42;break;
					case 2: buffer[3] = 0x81;break;
					default :break;			
				}	
				for(j=0;j<3;j++)
				{
					if(buffer[3]==0x00)
					{
						switch(j)
						{
								case 0: buffer1[1] = 0xc3;break; //RED//GREEN//BLUE         
								case 1: buffer1[1] = 0xc4;break;
								case 2: buffer1[1] = 0xc5;break;
								default :break;			
						}
					}
					else if(buffer[3]==0x42)
					{
						switch(j)
						{
								case 0: buffer1[1] = 0xc6;break; //RED//GREEN//BLUE         
								case 1: buffer1[1] = 0xc7;break;
								case 2: buffer1[1] = 0xc8;break;
								default :break;			
						}
					}
					else if(buffer[3]==0x81)
					{
						switch(j)
						{
								case 0: buffer1[1] = 0xc9;break; //RED//GREEN//BLUE         
								case 1: buffer1[1] = 0xca;break;
								case 2: buffer1[1] = 0xcb;break;
								default :break;			
						}
					}			
					SSD2828_W_Reg(SigMode,channel,0xBC,17); 
					SSD2828_W_Cmd(SigMode,channel,0xBF);	
					SSD2828_W_Data(SigMode,channel,buffer1[1]); //red-> b0,b1,b2
					for( i = 0;i<8;i++)   //23�����
					{	                 
						temp_cx=((buffer[k+4]<<8)+buffer[k+5])*4;
						
						SSD2828_W_Data(SigMode,channel,temp_cx>>8);
						SSD2828_W_Data(SigMode,channel,temp_cx&0x00ff);
						delay_ms(2);//��ʱʵ���Ҫ
						k+=2;  
					}
				//	
				}	
			//	delay_ms(5);
			}
			buffer[4] = Uart_Error_None;
			buffer[3] =	 0x0C;
			buffer[1] =  0x08;
			STM2PC_RM671xx(pdev,CDC_IN_EP,buffer,buffer[2]+3);           //���� дGamma״̬  ��ok 		

			SSD2828_W_Reg(SigMode,channel,0xB7,HS_B7_Data);
}

void NT37710_Read_Gamma(USB_OTG_CORE_HANDLE *pdev,uint8_t  ep_addr,u8 SigMode ,u8 channel,u8* buffer,u16 LP_B7_Data,u16 HS_B7_Data)
{
    u8 buffer1[7];
    u16 tmp;
    u8 i,j,k=0;
    //SSD2828_W_Reg(SigMode,channel,0xB7,(HS_B7_Data|0x0010)&0XFFFD);
		SSD2828_W_Reg(SigMode,channel,0xB7,LP_B7_Data);
		delay_ms(15);
    buffer1[0] = 0x06;
    buffer1[1] = 0xF0;
    buffer1[2] = 0x55;
    buffer1[3] = 0xAA;
    buffer1[4] = 0x52;
    buffer1[5] = 0x08;
    buffer1[6] = 0x00;                         // Page 1
    SSD2828_W_Array(SigMode,channel,buffer1,0); 		
    buffer1[0] = 0x03;
    buffer1[1] = 0xd6;
    buffer1[2] = 0x00;
    buffer1[3] = 0x10;                      // Page 1
    SSD2828_W_Array(SigMode,channel,buffer1,0); 		

    buffer1[0] = 0x06;
    buffer1[1] = 0xF0;
    buffer1[2] = 0x55;
    buffer1[3] = 0xAA;
    buffer1[4] = 0x52;
    buffer1[5] = 0x08;
    buffer1[6] = 0x02;                         // Page 1
    SSD2828_W_Array(SigMode,channel,buffer1,0); 	
    delay_ms(5);   
	
    buffer1[0] = 0x02;       
    buffer1[1] = 0xBF;
    buffer1[2] = (buffer[3]>>4)&0x0f;                      // Page 2
    SSD2828_W_Array(SigMode,channel,buffer1,0);		// Page 2
	  delay_ms(5); 
  
    //SSD2828_W_Reg(SigMode,channel,0xB7,((HS_B7_Data|0x0010)&0XFFFD)|0x0080);
		SSD2828_W_Reg(SigMode,channel,0xB7,(LP_B7_Data|0x0080));
    delay_ms(5);
		for(j=0;j<3;j++)	
		{				
			SSD2828_W_Reg(SigMode,channel,0xC1,0x0010); //���ظ�������
			buffer1[0] = 0x01;
			if((buffer[3]&0x0f)==0x00)
			{
				switch(j)
				{
					case 0: buffer1[1] = 0xB0;break; //RED//GREEN//BLUE         
					case 1: buffer1[1] = 0xB1;break;
					case 2: buffer1[1] = 0xB2;break;
					default :break;
				}
			}
			else if((buffer[3]&0x0f)==0x01)
			{
				switch(j)
				{
					case 0: buffer1[1] = 0xB3;break; //RED//GREEN//BLUE         
					case 1: buffer1[1] = 0xB4;break;
					case 2: buffer1[1] = 0xB5;break;
					default :break;
				}
			}	
			else if((buffer[3]&0x0f)==0x02)
			{
				switch(j)
				{
					case 0: buffer1[1] = 0xB6;break; //RED//GREEN//BLUE         
					case 1: buffer1[1] = 0xB7;break;
					case 2: buffer1[1] = 0xB8;break;
					default :break;
				}
			}					
			SSD2828_W_Array(SigMode,channel,buffer1,0);              
			delay_ms(5);  
			SSD2828_W_Cmd(SigMode,channel,0xFF);
			for(i=0;i<8;i++)
			{
					SSD2828_W_Cmd(SigMode,channel,0xFA);		
					tmp=SPI3_Read_u16_Data(channel);
					buffer[4+k]=tmp>>8;
					buffer[5+k]=tmp;
					delay_ms(3);
					k+=2;
			}
		}	
    buffer1[0] = 0x06;
    buffer1[1] = 0xF0;
    buffer1[2] = 0x55;
    buffer1[3] = 0xAA;
    buffer1[4] = 0x52;
    buffer1[5] = 0x08;
    buffer1[6] = 0x00;                         // Page 1
    SSD2828_W_Array(SigMode,channel,buffer1,0); 		
    buffer1[0] = 0x03;
    buffer1[1] = 0xd6;
    buffer1[2] = 0x00;
    buffer1[3] = 0x00;                      // Page 1
    SSD2828_W_Array(SigMode,channel,buffer1,0); 		
		
    buffer[0]=0x2A;	//NT37710
    buffer[1]=0x09;
    STM2PC_RM671xx(pdev,CDC_IN_EP,buffer,buffer[2]+3);          //���� ��ȡ�� Gamma����  ��ok  		

    SSD2828_W_Reg(SigMode,channel,0xB7,HS_B7_Data);

}

void NT37710_Gamma_OTP_Start(USB_OTG_CORE_HANDLE *pdev,uint8_t  ep_addr,u8 SigMode ,u8 channel,u8* buffer,u16 LP_B7_Data,u16 HS_B7_Data)
{
    u8 buffer1[10];
	  u8 mtp_flag;
	  SSD2828_W_Reg(SigMode,channel,0xB7,LP_B7_Data);
		delay_ms(50); 
    buffer1[0] = 0x01;
    buffer1[1] = 0x10;
		SSD2828_W_Array(SigMode,channel,buffer1,0);
		delay_ms(50); 
    buffer1[0] = 0x01;
    buffer1[1] = 0x28;
		SSD2828_W_Array(SigMode,channel,buffer1,0); 
		delay_ms(15); 

    buffer1[0] = 0x01;
    buffer1[1] = 0x11;
		SSD2828_W_Array(SigMode,channel,buffer1,0);	
		delay_ms(150); 	
	
    buffer1[0] = 0x06;
    buffer1[1] = 0xF0;
    buffer1[2] = 0x55;
    buffer1[3] = 0xAA;
    buffer1[4] = 0x52;
    buffer1[5] = 0x08;
    buffer1[6] = 0x01;                         // Page 1
    SSD2828_W_Array(SigMode,channel,buffer1,0);  
//Page1 EBh, MTP_POSEL_REG=1
//    buffer1[0] = 0x04;
//    buffer1[1] = 0xEA;
//    buffer1[2] = 0x87;
//    buffer1[3] = 0x78;
//    buffer1[4] = 0x01;    
//		SSD2828_W_Array(SigMode,channel,buffer1,0); 
//    delay_ms(30);  
//internal power
    buffer1[0] = 0xB7;
    buffer1[1] = 0x18;    
		SSD2828_W_Array(SigMode,channel,buffer1,0); 		

    buffer1[0] = 0x04;
    buffer1[1] = 0xEB;
    buffer1[2] = 0x87;
    buffer1[3] = 0x78;
    buffer1[4] = 0x01;    
		SSD2828_W_Array(SigMode,channel,buffer1,0); 		
		delay_ms(30);  		
    buffer1[0] = 0x02;
    buffer1[1] = 0xCA;
    buffer1[2] = 0x01;   
		SSD2828_W_Array(SigMode,channel,buffer1,0); 		
		delay_ms(10); 
    buffer1[0] = 0x03;
    buffer1[1] = 0xCA;
    buffer1[2] = 0x01;   
		buffer1[3] = 0x02;  
		SSD2828_W_Array(SigMode,channel,buffer1,0); 			
		delay_ms(2); 
	//--------
		buffer1[0] = 0x09;
		buffer1[1] = 0xED;
		buffer1[2] = buffer[4];
		buffer1[3] = buffer[5];
		buffer1[4] = buffer[6];
		buffer1[5] = buffer[7];
		buffer1[6] = buffer[8];                
		buffer1[7] = buffer[9];
		buffer1[8] = buffer[10];
		buffer1[9] = 0x00;  
		SSD2828_W_Array(SigMode,channel,buffer1,0);  
	//---------
		delay_ms(80); 
    buffer1[0] = 0x04;
		buffer1[1] = 0xEE;
    buffer1[2] = 0xA5;
    buffer1[3] = 0x5A;   
		buffer1[4] = 0x3C;  
		SSD2828_W_Array(SigMode,channel,buffer1,0); 
		delay_ms(3000); 

    buffer1[0] = 0x02;
    buffer1[1] = 0xEC;
		buffer1[1] = 0x00;
    SSD2828_W_Array(SigMode,channel,buffer1,0); 
	//--------
		buffer1[0] = 0x09;
		buffer1[1] = 0xED;
		buffer1[2] = 0x00;
		buffer1[3] = 0x00;
		buffer1[4] = 0x00;
		buffer1[5] = 0x00;
		buffer1[6] = 0x00;                
		buffer1[7] = 0x00;
		buffer1[8] = 0x00;
		buffer1[9] = 0x00;  
		SSD2828_W_Array(SigMode,channel,buffer1,0);  
	//---------		
		buffer[2] = 0x02;
		buffer[3] = Uart_Error_None;   //���� OTP Gamma����  ��OK
		buffer[4] = 0;
		STM2PC_RM671xx(pdev,CDC_IN_EP,buffer,buffer[2]+3);         
}



/********************************************************************************************	
	
********************************************************************************************/
/********************************************************************************************
    RM67120  д�Ĵ��� ���Ĵ���  дGamma ��Gamma OTP��¼
    RM67120  д�Ĵ��� ���Ĵ���  дGamma ��Gamma OTP��¼
    RM67120  д�Ĵ��� ���Ĵ���  дGamma ��Gamma OTP��¼
    RM67120  д�Ĵ��� ���Ĵ���  дGamma ��Gamma OTP��¼
    RM67120  д�Ĵ��� ���Ĵ���  дGamma ��Gamma OTP��¼
********************************************************************************************
********************************************************************************************/
void RM67120_Write_Register(USB_OTG_CORE_HANDLE *pdev,uint8_t  ep_addr,u8 SigMode ,u8 channel,u8* buffer,u16 LP_B7_Data,u16 HS_B7_Data)
{
    SSD2828_W_Reg(SigMode,channel,0xB7,(HS_B7_Data|0x0010)&0XFFFD);
    SSD2828_W_Array(SigMode,channel,buffer,2);                               

    delay_ms(5);
    SSD2828_W_Reg(SigMode,channel,0xB7,HS_B7_Data);
    buffer[4] = Uart_Error_None;
    buffer[5] = 0;
    STM2PC_RM671xx(pdev,CDC_IN_EP,buffer,buffer[2]+3);           //���� д�Ĵ���״̬  ��ok  
}

void RM67120_Read_Register(USB_OTG_CORE_HANDLE *pdev,uint8_t  ep_addr,u8 SigMode ,u8 channel,u8* buffer,u16 LP_B7_Data,u16 HS_B7_Data)
{
    u16 tmp;
    buffer[1]=0x03;
    SSD2828_W_Reg(SigMode,channel,0xB7,((HS_B7_Data|0x0010)&0XFFFD)|0x0080);                               
    SSD2828_W_Reg(SigMode,channel,0xC1,buffer[2]-1); //return package size                                
    SSD2828_W_Reg(SigMode,channel,0xC0,0x0001);
    buffer[0] = 0x01;
    buffer[1] = buffer[3];                        
    SSD2828_W_Array(SigMode,channel,buffer,0);
    delay_ms(5);  
    SSD2828_W_Reg(SigMode,channel,0xD4,0x00FA);
    SSD2828_W_Cmd(SigMode,channel,0xFF);
    for(i=0;i<buffer[2]-1;i++)
    {
        SSD2828_W_Cmd(SigMode,channel,0xFA);		
        tmp=SPI3_Read_u16_Data(channel);
        buffer[4+i]=tmp>>8;
        buffer[5+i]=tmp;
        delay_ms(5);
        i++;
    }
		buffer[0]=0x08;	
		buffer[1]=0x02;
    SSD2828_W_Reg(SigMode,channel,0xB7,HS_B7_Data);
    STM2PC_RM671xx(pdev,CDC_IN_EP,buffer,buffer[2]+3);           //���� ��ȡ�� �Ĵ�������  ��ok 

}

void RM67120_Write_Gamma(USB_OTG_CORE_HANDLE *pdev,uint8_t  ep_addr,u8 SigMode ,u8 channel,u8* buffer,u16 LP_B7_Data,u16 HS_B7_Data)
{
    u8 buffer1[7];
    SSD2828_W_Reg(SigMode,channel,0xB7,(HS_B7_Data|0x0010)&0XFFFD);
    buffer1[0] = 0x06;       
    buffer1[1] = 0xF0;
    buffer1[2] = 0x55;
    buffer1[3] = 0xAA;
    buffer1[4] = 0x52;
    buffer1[5] = 0x08;
    buffer1[6] = 0x01;                         // Page 1
    SSD2828_W_Array(SigMode,channel,buffer1,0);		// Page 1	
    SSD2828_W_Reg(SigMode,channel,0xBC,0x0035); //+1 CMD
    SSD2828_W_Reg(SigMode,channel,0xBD,0x0000);	
    SSD2828_W_Cmd(SigMode,channel,0xBF);	
    SSD2828_W_Data(SigMode,channel,((buffer[3]==0x00) ? 0xD1 : ((buffer[3]==0x42) ? 0xD2 : 0xD3))); //RED//GREEN//BLUE
    for( i = 0;i<52;i++)   //26�׻Ҷȵ���
    {	                                                                                  
        SSD2828_W_Data(SigMode,channel,buffer[i+4]);
    }
    delay_ms(5);

    SSD2828_W_Reg(SigMode,channel,0xB7,HS_B7_Data);
    buffer[4] = Uart_Error_None;
    buffer[5] = 0;
    STM2PC_RM671xx(pdev,CDC_IN_EP,buffer,buffer[2]+3);           //���� дGamma״̬  ��ok 
}

void RM67120_Read_Gamma(USB_OTG_CORE_HANDLE *pdev,uint8_t  ep_addr,u8 SigMode ,u8 channel,u8* buffer,u16 LP_B7_Data,u16 HS_B7_Data)
{
    u8 buffer1[7];
    u16 tmp;
    u8 i;
    SSD2828_W_Reg(SigMode,channel,0xB7,(HS_B7_Data|0x0010)&0XFFFD);
    delay_ms(5);
    buffer1[0] = 0x06;
    buffer1[1] = 0xF0;
    buffer1[2] = 0x55;
    buffer1[3] = 0xAA;
    buffer1[4] = 0x52;
    buffer1[5] = 0x08;
    buffer1[6] = 0x01;                         // Page 1
    SSD2828_W_Array(SigMode,channel,buffer1,0); 	

    delay_ms(5);     

    SSD2828_W_Reg(SigMode,channel,0xB7,((HS_B7_Data|0x0010)&0XFFFD)|0x0080);
    delay_ms(5);
    SSD2828_W_Reg(SigMode,channel,0xC1,0x0034);
    buffer[0] = 0x01;
    buffer[1] = (buffer[3]==0x00) ? 0xD1 : ((buffer[3]==0x42) ? 0xD2 : 0xD3);         //RED//GREEN//BLUE                  
    SSD2828_W_Array(SigMode,channel,buffer,0);              
    delay_ms(5);  
    SSD2828_W_Cmd(SigMode,channel,0xFF);
    for(i=0;i<51;i++)
    {
        SSD2828_W_Cmd(SigMode,channel,0xFA);		
        tmp=SPI3_Read_u16_Data(channel);
        buffer[4+i]=tmp>>8;
        buffer[5+i]=tmp;
        delay_ms(5);
        i++;
    }       
    buffer[0]=0x08;	
    buffer[1]=0x09;
    SSD2828_W_Reg(SigMode,channel,0xB7,HS_B7_Data);
    STM2PC_RM671xx(pdev,CDC_IN_EP,buffer,buffer[2]+3);          //���� ��ȡ�� Gamma����  ��ok  
}


void RM67120_Gamma_OTP_Start(USB_OTG_CORE_HANDLE *pdev,uint8_t  ep_addr,u8 SigMode ,u8 channel,u8* buffer,u16 LP_B7_Data,u16 HS_B7_Data)
{
    u8 buffer1[7];
   // SSD2828_W_Reg(SigMode,channel,0xB7,(HS_B7_Data|0x0010)&0XFFFD);
		SSD2828_W_Reg(SigMode,channel,0xB7,LP_B7_Data);
    buffer1[0] = 0x06;
    buffer1[1] = 0xF0;
    buffer1[2] = 0x55;
    buffer1[3] = 0xAA;
    buffer1[4] = 0x52;
    buffer1[5] = 0x08;
    buffer1[6] = 0x01;                         // Page 1
    SSD2828_W_Array(SigMode,channel,buffer1,0);                                

    delay_ms(5);  

    buffer1[0] = 0x03;                       //gamma��¼���� 
    buffer1[1] = 0xED;
    buffer1[2] = 0x40;
    buffer1[3] = 0x00;
    SSD2828_W_Array(SigMode,channel,buffer1,0);                                

    delay_ms(5);  

    buffer1[0] = 0x04;                       
    buffer1[1] = 0xEE;
    buffer1[2] = 0xA5;
    buffer1[3] = 0x5A;
    buffer1[4] = 0x3C;
    SSD2828_W_Array(SigMode,channel,buffer1,0);  
       
    delay_ms(2000);  //more than 500 ms
    buffer1[0] = 0x03;                       //�����¼�������趨
    buffer1[1] = 0xED;
    buffer1[2] = 0x40;
    buffer1[3] = 0x00;
    SSD2828_W_Array(SigMode,channel,buffer1,0); 
    buffer1[3] = Uart_Error_None;
    buffer1[4] = 0;
    STM2PC_RM671xx(pdev,CDC_IN_EP,buffer,buffer[2]+3);           //���� OTP Gamma����  ��ok 
}

void RM67120_ID_OTP_Start(USB_OTG_CORE_HANDLE *pdev,uint8_t  ep_addr,u8 SigMode ,u8 channel,u8* buffer,u16 LP_B7_Data,u16 HS_B7_Data)
{
    u8 buffer1[7];
    //SSD2828_W_Reg(SigMode,channel,0xB7,(HS_B7_Data|0x0010)&0XFFFD);
		SSD2828_W_Reg(SigMode,channel,0xB7,LP_B7_Data);
    buffer1[0] = 0x06;
    buffer1[1] = 0xF0;
    buffer1[2] = 0x55;
    buffer1[3] = 0xAA;
    buffer1[4] = 0x52;
    buffer1[5] = 0x08;
    buffer1[6] = 0x01;                         // Page 1
    SSD2828_W_Array(SigMode,channel,buffer1,0);                                

    delay_ms(5);  

    buffer1[0] = 0x03;                       //ID��¼���� 
    buffer1[1] = 0xED;
    buffer1[2] = 0x80;
    buffer1[3] = 0x00;
    SSD2828_W_Array(SigMode,channel,buffer1,0);                                

    delay_ms(5);  

    buffer1[0] = 0x04;                       
    buffer1[1] = 0xEE;
    buffer1[2] = 0xA5;
    buffer1[3] = 0x5A;
    buffer1[4] = 0x3C;
    SSD2828_W_Array(SigMode,channel,buffer1,0);  
       
    delay_ms(2000);  //more than 500 ms
    buffer1[0] = 0x03;                       //�����¼�������趨
    buffer1[1] = 0xED;
    buffer1[2] = 0x00;
    buffer1[3] = 0x00;
    SSD2828_W_Array(SigMode,channel,buffer1,0); 
    buffer1[3] = Uart_Error_None;
    buffer1[4] = 0;
    STM2PC_RM671xx(pdev,CDC_IN_EP,buffer,buffer[2]+3);           //���� OTP ID����  ��ok 
}


void RM67120_Read_OTP_Time(USB_OTG_CORE_HANDLE *pdev,uint8_t  ep_addr,u8 SigMode ,u8 channel,u8* buffer,u16 LP_B7_Data,u16 HS_B7_Data)
{       
    u8 buffer1[7];    
    SSD2828_W_Reg(SigMode,channel,0xB7,(HS_B7_Data|0x0010)&0XFFFD);
    delay_ms(5);            
    buffer1[0] = 0x06;
    buffer1[1] = 0xF0;
    buffer1[2] = 0x55;
    buffer1[3] = 0xAA;
    buffer1[4] = 0x52;
    buffer1[5] = 0x08;
    buffer1[6] = 0x01;                         // Page 1
    SSD2828_W_Array(SigMode,channel,buffer1,0); 
    delay_ms(5);       
    SSD2828_W_Reg(SigMode,channel,0xB7,((HS_B7_Data|0x0010)&0XFFFD)|0x0080);
    SSD2828_W_Reg(SigMode,channel,0xC1,0x0002);
    buffer1[0] = 0x01;
    buffer1[1] = 0xEF;
    SSD2828_W_Array(SigMode,channel,buffer1,0); 
    SSD2828_W_Reg(SigMode,channel,0xD4,0x00FA);
    delay_ms(90); 
    SSD2828_W_Cmd(SigMode,channel,0xFF);
    SSD2828_W_Cmd(SigMode,channel,0xFA);
    buffer[4]=SPI3_Read_u8_Data(channel);
    SPI3_Read_u8_Data(channel);	
    delay_ms(5);

    SSD2828_W_Reg(SigMode,channel,0xB7,HS_B7_Data);
    buffer[3] = Uart_Error_None;
    STM2PC_RM671xx(pdev,CDC_IN_EP,buffer,buffer[2]+3);           //���� ��ȡ�� OTP����  ��ok 

}


/********************************************************************************************
********************************************************************************************
    RM67160��RM67162  д�Ĵ��� ���Ĵ���  дGamma ��Gamma OTP��¼
    RM67160��RM67162  д�Ĵ��� ���Ĵ���  дGamma ��Gamma OTP��¼
    RM67160��RM67162  д�Ĵ��� ���Ĵ���  дGamma ��Gamma OTP��¼
    RM67160��RM67162  д�Ĵ��� ���Ĵ���  дGamma ��Gamma OTP��¼
    RM67160��RM67162  д�Ĵ��� ���Ĵ���  дGamma ��Gamma OTP��¼
********************************************************************************************
********************************************************************************************/
void RM6716X_Write_Register(USB_OTG_CORE_HANDLE *pdev,uint8_t  ep_addr,u8 SigMode ,u8 channel,u8* buffer,u16 LP_B7_Data,u16 HS_B7_Data)
{
    if((SigMode == SPI3_Mode)||(SigMode == SPI4_Mode))          //3�� 4��8bit SPI ģʽ
    {
        SPI_Write_code(SigMode,channel,buffer[3],buffer[4]);  
    }
    else if((SigMode == Mipi_Mode)||(SigMode == CMD_Mode))
    {
        if(SigMode != CMD_Mode)
            SSD2828_W_Reg(SigMode,channel,0xB7,(HS_B7_Data|0x0010)&0XFFFD);
        delay_ms(5);
        SSD2828_W_Array(SigMode,channel,buffer,2);
        delay_ms(50);
        SSD2828_W_Reg(SigMode,channel,0xB7,HS_B7_Data);
    }
    buffer[4] = Uart_Error_None;
    STM2PC_RM671xx(pdev,CDC_IN_EP,buffer,buffer[2]+3);           //���� д�Ĵ���״̬  ��ok  
}

void RM6716X_Read_Register(USB_OTG_CORE_HANDLE *pdev,uint8_t  ep_addr,u8 SigMode ,u8 channel,u8* buffer,u16 LP_B7_Data,u16 HS_B7_Data)
{
    if(SigMode == SPI3_Mode)          //3��8bit SPI ģʽ
    {

    }
    else if(SigMode == SPI4_Mode)          //4��8bit SPI ģʽ
    {
    
    }
    else if((SigMode == Mipi_Mode)||(SigMode == CMD_Mode))
    {
        if(SigMode == Mipi_Mode)  //RGB+SPI= MIPI
            SSD2828_W_Reg(SigMode,channel,0xB7,((HS_B7_Data|0x0010)&0XFFFD)|0x0080);      
        else
            SSD2828_W_Reg(SigMode,channel,0xB7,LP_B7_Data|0x0080);
        delay_ms(5);
        SSD2828_W_Reg(SigMode,channel,0xC1,0x0002); 
        SSD2828_W_Reg(SigMode,channel,0xC0,0x0001); 
        SSD2828_W_Reg(SigMode,channel,0xBC,0x0001);
        SSD2828_W_Reg(SigMode,channel,0xBD,0x0000);                                    
        for(i=0;i<3;i++)
        {
            SSD2828_W_Cmd(SigMode,channel,0xBF);
            SSD2828_W_Data(SigMode,channel,buffer[3]);
            if(SigMode == Mipi_Mode)  //RGB+SPI= MIPI   
                SSD2828_W_Reg(SigMode,channel,0xD4,0x00FA);  
            delay_ms(25);
            buffer[4]=SSD2828_R_Reg(SigMode,channel,0xFF);
        }                              
        SSD2828_W_Reg(SigMode,channel,0xB7,HS_B7_Data);
        delay_ms(5);
        STM2PC_RM671xx(pdev,CDC_IN_EP,buffer,buffer[2]+3);           //���� ��ȡ�� �Ĵ�������  ��ok  	
    }
}

void RM6716X_Write_Gamma(USB_OTG_CORE_HANDLE *pdev,uint8_t  ep_addr,u8 SigMode ,u8 channel,u8* buffer,u16 LP_B7_Data,u16 HS_B7_Data)
{
    u8 buffer1[3];
    u8 gamma_write_num;
    if(buffer[0]==RM67160)
    {
        gamma_write_num=22;
    }
    else if(buffer[0]==RM67162)
    {
        gamma_write_num=50;
    }
    if((SigMode == SPI3_Mode)||(SigMode == SPI4_Mode))          //3�� 4��8bit SPI ģʽ
    {                                   
        if(buffer[1] == 0x08)
            SPI_Write_code(SigMode,channel,0xFE,0x02);
        else if(buffer[1] == 0x18)
            SPI_Write_code(SigMode,channel,0xFE,0x03);                                 
        for( i = 0;i<gamma_write_num;i++)
        {	                                                                   
            SPI_Write_code(SigMode,channel,((buffer[3]==0x00) ? ((buffer[0]==RM67160) ? RM160_Gamma_r[i] : RM162_Gamma_r[i]) : \
                                           ((buffer[3]==0x34) ? ((buffer[0]==RM67160) ? RM160_Gamma_g[i] : RM162_Gamma_g[i]) : \
                                                                ((buffer[0]==RM67160) ? RM160_Gamma_b[i] : RM162_Gamma_b[i]))),buffer[i+4]);     
            delay_ms(1);
        }                                    
    }
    else if((SigMode == Mipi_Mode)||(SigMode == CMD_Mode))
    {
        if(SigMode != CMD_Mode)
            SSD2828_W_Reg(SigMode,channel,0xB7,(HS_B7_Data|0x0010)&0XFFFD);
        delay_ms(5);
        buffer1[0] = 0x02;
        buffer1[1] = 0xFE;
        buffer1[2] = (buffer[1] == 0x08) ? 0x02 : 0x03;
        SSD2828_W_Array(SigMode,channel,buffer1,0);   
        for( i = 0;i<gamma_write_num;i++)
        {	                                                                   
            SSD2828_W_Reg(SigMode,channel,0xBF,  ((buffer[3]==0x00) ? ((buffer[i+4]<<8)+((buffer[0]==RM67160) ? RM160_Gamma_r[i] : RM162_Gamma_r[i])) : \
                                                        ((buffer[3]==0x34) ? ((buffer[i+4]<<8)+((buffer[0]==RM67160) ? RM160_Gamma_g[i] : RM162_Gamma_g[i])) : \
                                                                                    ((buffer[i+4]<<8)+((buffer[0]==RM67160) ? RM160_Gamma_b[i] : RM162_Gamma_b[i])))));      //RM67160
            delay_ms(1);
        }	
        SSD2828_W_Reg(SigMode,channel,0xB7,HS_B7_Data);                              
    }
    buffer[4] = Uart_Error_None;
    buffer[1] =  0x08;
    STM2PC_RM671xx(pdev,CDC_IN_EP,buffer,buffer[2]+3);           //���� дGamma״̬  ��ok 
}

void RM6716X_auto_Gamma(USB_OTG_CORE_HANDLE *pdev,uint8_t  ep_addr,u8 SigMode ,u8 channel,u8* buffer,u16 LP_B7_Data,u16 HS_B7_Data)
{
    if((SigMode == SPI3_Mode)||(SigMode == SPI4_Mode))          //3�� 4��8bit SPI ģʽ
    {                                                                  
        for( i = 0;i<buffer[2];i++)
        {	                                                                   
            SPI_Write_code(SigMode,channel,buffer[i+3],buffer[i+4]);     
            i++;
        }                                    
    }
    else if((SigMode == Mipi_Mode)||(SigMode == CMD_Mode))
    {
        if(SigMode != CMD_Mode)
            SSD2828_W_Reg(SigMode,channel,0xB7,(HS_B7_Data|0x0010)&0XFFFD);  
        for( i = 0;i<buffer[2];i++)
        {	                                                                   
            SSD2828_W_Reg(SigMode,channel,0xBF,(buffer[i+4]<<8)+buffer[i+3]);      //RM67160
            i++;
        }	
        SSD2828_W_Reg(SigMode,channel,0xB7,HS_B7_Data);                              
    }
}

void RM6716X_Read_Gamma(USB_OTG_CORE_HANDLE *pdev,uint8_t  ep_addr,u8 SigMode ,u8 channel,u8* buffer,u16 LP_B7_Data,u16 HS_B7_Data)
{
    u8 gamma_read_num;
    if(buffer[0]==RM67160)
    {
        gamma_read_num=22;
    }
    else if(buffer[0]==RM67162)
    {
        gamma_read_num=50;
    }
    if(SigMode == SPI3_Mode)          //3��8bit SPI ģʽ
    {
 
    }
    else if(SigMode == SPI4_Mode)          //4��8bit SPI ģʽ
    {
    
    }
    else if((SigMode == Mipi_Mode)||(SigMode == CMD_Mode))
    {
        if(SigMode != CMD_Mode)
            SSD2828_W_Reg(SigMode,channel,0xB7,(HS_B7_Data|0x0010)&0XFFFD);
        SSD2828_W_Reg(SigMode,channel,0xBC,0x0002);
        SSD2828_W_Reg(SigMode,channel,0xBD,0x0000);   

        if(buffer[0]==RM67160)
        {
            if(buffer[1] == 0x09)
                SSD2828_W_Reg(SigMode,channel,0xBF,0x02FE);
            else if(buffer[1] == 0x19)
                SSD2828_W_Reg(SigMode,channel,0xBF,0x03FE);  
        }
        else if(buffer[0]==RM67162)
        {
            if(buffer[1] == 0x09)
                SSD2828_W_Reg(SigMode,channel,0xBF,0xA2FE);
            else if(buffer[1] == 0x19)
                SSD2828_W_Reg(SigMode,channel,0xBF,0xA3FE);  
        }
        if(SigMode == Mipi_Mode)  //RGB+SPI= MIPI
            SSD2828_W_Reg(SigMode,channel,0xB7,((HS_B7_Data|0x0010)&0XFFFD)|0x0080);      
        else
            SSD2828_W_Reg(SigMode,channel,0xB7,LP_B7_Data|0x0080);
//        SSD2828_W_Reg(SigMode,channel,0xC1,0x0001); 
//        SSD2828_W_Reg(SigMode,channel,0xC0,0x0001); 
        SSD2828_W_Reg(SigMode,channel,0xBC,0x0001);
                            
        for( i = 0;i<gamma_read_num;i++)							
        {   
                SSD2828_W_Cmd(SigMode,channel,0xBF);
                SSD2828_W_Data(SigMode,channel,(buffer[3]==0x00) ? ((buffer[0]==RM67160) ? RM160_Gamma_r[i] : RM162_Gamma_r[i]) : \
                                               (buffer[3]==0x34) ? ((buffer[0]==RM67160) ? RM160_Gamma_g[i] : RM162_Gamma_g[i]) : \
                                                                   ((buffer[0]==RM67160) ? RM160_Gamma_b[i] : RM162_Gamma_b[i]));
                delay_ms(25);
                if(SigMode == Mipi_Mode)  //RGB+SPI= MIPI
                    SSD2828_W_Reg(SigMode,channel,0xD4,0x00FA); 
                buffer[i+4]=SSD2828_R_Reg(SigMode,channel,0xFF);
        }                   
        SSD2828_W_Reg(SigMode,channel,0xB7,HS_B7_Data);	                              
    }
    buffer[1] = (buffer[0]==RM67160) ? 0x09 : 0x0C;
    STM2PC_RM671xx(pdev,CDC_IN_EP,buffer,buffer[2]+3);           //���� ��ȡ�� Gamma����  ��ok 
}
void RM6716X_Gamma_OTP_Start(USB_OTG_CORE_HANDLE *pdev,uint8_t  ep_addr,u8 SigMode ,u8 channel,u8* buffer,u16 LP_B7_Data,u16 HS_B7_Data)
{
    u8 tmp;
	  u8 buffer1[3];
    if((SigMode == SPI3_Mode)||(SigMode == SPI4_Mode))          //3�� 4��8bit SPI ģʽ
    {
        delay_ms(120); 
        SPI_Write_code(SigMode,channel,0xfe,0x00);
        SPI_Write_u8_Cmd(SigMode,channel,0x28); 
        delay_ms(100);  //otp
        SPI_Write_code(SigMode,channel,0xfe,0x01);
        SPI_Write_code(SigMode,channel,0xf0,0x10);
        SPI_Write_code(SigMode,channel,0xf2,0x03);
        SPI_Write_code(SigMode,channel,0xf3,0xA5); 
        SPI_Write_code(SigMode,channel,0xf4,0x5a);
        SPI_Write_code(SigMode,channel,0xf5,0x3c);
        delay_ms(500);         // wait for >400 ms 
        
        
        buffer[4] = Uart_Error_None;
        STM2PC_RM671xx(pdev,CDC_IN_EP,buffer,buffer[2]+3);           //���� OTP״̬  ��ok
    }
    else if((SigMode == Mipi_Mode)||(SigMode == CMD_Mode))
    {
        delay_ms(120); 
        if(SigMode != CMD_Mode)
            SSD2828_W_Reg(SigMode,channel,0xB7,(HS_B7_Data|0x0010)&0XFFFD);
        delay_ms(5);
        buffer1[0] = 0x02;
        buffer1[1] = 0xFE;
        buffer1[2] = 0x00;
        SSD2828_W_Array(SigMode,channel,buffer1,0);                                                                      

        buffer1[0] = 0x01;
        buffer1[1] = 0x28;
        SSD2828_W_Array(SigMode,channel,buffer1,0);   
        delay_ms(200);  //otp
        buffer1[0] = 0x02;
        buffer1[1] = 0xFE;
        buffer1[2] = 0x01;
        SSD2828_W_Array(SigMode,channel,buffer1,0);  
        buffer1[0] = 0x02;
        buffer1[1] = 0xF0;       
				buffer1[2] = buffer[3];				
        SSD2828_W_Array(SigMode,channel,buffer1,0);                                        
				
        delay_ms(200); 
        buffer1[1] = 0xF2; buffer1[2] = 0x03;
        SSD2828_W_Array(SigMode,channel,buffer1,0); 
        
        buffer1[1] = 0xF3; buffer1[2] = 0xA5;
        SSD2828_W_Array(SigMode,channel,buffer1,0);
        
        buffer1[1] = 0xF4; buffer1[2] = 0x5A;
        SSD2828_W_Array(SigMode,channel,buffer1,0);
        
        buffer1[1] = 0xF5; buffer1[2] = 0x3C;
        SSD2828_W_Array(SigMode,channel,buffer1,0);
        SSD2828_W_Reg(SigMode,channel,0xB7,HS_B7_Data);
        delay_ms(800);         // wait for >400 ms   
        if(SigMode == Mipi_Mode)  //RGB+SPI= MIPI
            SSD2828_W_Reg(SigMode,channel,0xB7,((HS_B7_Data|0x0010)&0XFFFD)|0x0080);      
        else
            SSD2828_W_Reg(SigMode,channel,0xB7,LP_B7_Data|0x0080);
        SSD2828_W_Reg(SigMode,channel,0xC1,0x0002);
        SSD2828_W_Reg(SigMode,channel,0xC0,0x0001);
        buffer1[0] = 0x01;
        buffer1[1] = 0xEF;
        SSD2828_W_Array(SigMode,channel,buffer1,0);
        if(SigMode == Mipi_Mode)  //RGB+SPI= MIPI   ��¼OTP
            SSD2828_W_Reg(SigMode,channel,0xD4,0x00FA);
        delay_ms(100);															   								
        tmp=SSD2828_R_Reg(SigMode,channel,0xFF); 
        delay_ms(5);
        SSD2828_W_Reg(SigMode,channel,0xB7,HS_B7_Data);
        if (tmp&0x02)       //����
        {
            buffer[4] = Uart_Error_Oth;
        }
        else                //��ȷ
            buffer[4] = Uart_Error_None; 
        STM2PC_RM671xx(pdev,CDC_IN_EP,buffer,buffer[2]+3);           //���� OTP״̬  ��ok                                       
    }
}

void RM6716X_Read_OTP_Time(USB_OTG_CORE_HANDLE *pdev,uint8_t  ep_addr,u8 SigMode ,u8 channel,u8* buffer,u16 LP_B7_Data,u16 HS_B7_Data)
{     
    u8 tmp;
    if(SigMode == SPI3_Mode)          //3��8bit SPI ģʽ
    {
 
    }
    else if(SigMode == SPI4_Mode)          //4��8bit SPI ģʽ
    {
    
    }
    else if((SigMode == Mipi_Mode)||(SigMode == CMD_Mode))
    {
        if(SigMode != CMD_Mode)
            SSD2828_W_Reg(SigMode,channel,0xB7,(HS_B7_Data|0x0010)&0XFFFD);
        delay_ms(5);
        SSD2828_W_Reg(SigMode,channel,0xBC,0x0002);
        SSD2828_W_Reg(SigMode,channel,0xBD,0x0000);
        
        if(buffer[0]==RM67160)
        {
            SSD2828_W_Reg(SigMode,channel,0xBF,0x01FE); 
        }
        else if(buffer[0]==RM67162)
        {
            SSD2828_W_Reg(SigMode,channel,0xBF,0xA1FE);   
        } 
        if(SigMode == Mipi_Mode)  //RGB+SPI= MIPI
            SSD2828_W_Reg(SigMode,channel,0xB7,((HS_B7_Data|0x0010)&0XFFFD)|0x0080);      
        else
            SSD2828_W_Reg(SigMode,channel,0xB7,LP_B7_Data|0x0080);        
        delay_ms(5);
        SSD2828_W_Reg(SigMode,channel,0xBC,0x0001);
        SSD2828_W_Cmd(SigMode,channel,0xBF);
        SSD2828_W_Data(SigMode,channel,0xEE);
        if(SigMode == Mipi_Mode)  //RGB+SPI= MIPI  
            SSD2828_W_Reg(SigMode,channel,0xD4,0x00FA);							
        delay_ms(50);                                 
        tmp=SSD2828_R_Reg(SigMode,channel,0xFF); 
        
        if (tmp&0x10)
            buffer[4] = 1;
        else if (tmp&0x08)
            buffer[4] = 2;
        else
            buffer[4] = 0;
        delay_ms(5);
        SSD2828_W_Reg(SigMode,channel,0xB7,HS_B7_Data);
        buffer[3] = Uart_Error_None;
        STM2PC_RM671xx(pdev,CDC_IN_EP,buffer,buffer[2]+3);           //���� ��ȡ��OTP����  ��ok                                  
    }
    buffer[4] = Uart_Error_None;
    STM2PC_RM671xx(pdev,CDC_IN_EP,buffer,buffer[2]+3);           //���� OTP״̬  ��ok

}

/********************************************************************************************
********************************************************************************************
    RM6D010  д�Ĵ��� ���Ĵ���  дGamma ��Gamma OTP��¼
    RM6D010  д�Ĵ��� ���Ĵ���  дGamma ��Gamma OTP��¼
    RM6D010  д�Ĵ��� ���Ĵ���  дGamma ��Gamma OTP��¼
    RM6D010  д�Ĵ��� ���Ĵ���  дGamma ��Gamma OTP��¼
    RM6D010  д�Ĵ��� ���Ĵ���  дGamma ��Gamma OTP��¼
********************************************************************************************
********************************************************************************************/
void RM6D01X_Write_Register(USB_OTG_CORE_HANDLE *pdev,uint8_t  ep_addr,u8 SigMode ,u8 channel,u8* buffer,u16 LP_B7_Data,u16 HS_B7_Data)
{
	if((SigMode == SPI3_Mode)||(SigMode == SPI4_Mode))          //3�� 4��8bit SPI ģʽ
    {
        SPI_Write_code(SigMode,channel,buffer[3],buffer[4]);  
    }
    else if((SigMode == Mipi_Mode)||(SigMode == CMD_Mode))
    {
        if(SigMode != CMD_Mode)
          SSD2828_W_Reg(SigMode,channel,0xB7,(HS_B7_Data|0x0010)&0XFFFD);
				else
				  SSD2828_W_Reg(SigMode,channel,0xB7,LP_B7_Data);
        delay_ms(2);
        SSD2828_W_Array(SigMode,channel,buffer,2);
        delay_ms(5);
        SSD2828_W_Reg(SigMode,channel,0xB7,HS_B7_Data);
    }
    buffer[4] = Uart_Error_None;
    STM2PC_RM671xx(pdev,CDC_IN_EP,buffer,buffer[2]+3);           //���� д�Ĵ���״̬  ��ok  
}

void RM6D01X_Read_Register(USB_OTG_CORE_HANDLE *pdev,uint8_t  ep_addr,u8 SigMode ,u8 channel,u8* buffer,u16 LP_B7_Data,u16 HS_B7_Data)
{
    if(SigMode == SPI3_Mode)          //3��8bit SPI ģʽ
    {

    }
    else if(SigMode == SPI4_Mode)          //4��8bit SPI ģʽ
    {
    
    }
    else if((SigMode == Mipi_Mode)||(SigMode == CMD_Mode))
    {
        SSD2828_W_Reg(SigMode,channel,0xC1,0x0002); 
			  delay_ms(5);
			  SSD2828_W_Reg(SigMode,channel,0xB7,LP_B7_Data|0x00D0);
        delay_ms(5);
        SSD2828_W_Reg(SigMode,channel,0xBC,0x0001);
        SSD2828_W_Reg(SigMode,channel,0xBD,0x0000);                                    
        for(i=0;i<3;i++)
        {
            SSD2828_W_Cmd(SigMode,channel,0xBF);
            SSD2828_W_Data(SigMode,channel,buffer[3]);
            if(SigMode == Mipi_Mode)  //RGB+SPI= MIPI   
                SSD2828_W_Reg(SigMode,channel,0xD4,0x00FA);  
            delay_ms(5);
            buffer[4]=SSD2828_R_Reg(SigMode,channel,0xFF);

        }                              
        SSD2828_W_Reg(SigMode,channel,0xB7,HS_B7_Data);
        delay_ms(15);
        STM2PC_RM671xx(pdev,CDC_IN_EP,buffer,buffer[2]+3);           //���� ��ȡ�� �Ĵ�������  ��ok  	
    }
}

void RM6D01X_Write_Gamma(USB_OTG_CORE_HANDLE *pdev,uint8_t  ep_addr,u8 SigMode ,u8 channel,u8* buffer,u16 LP_B7_Data,u16 HS_B7_Data)
{
    u8 buffer1[3];
    u8 gamma_write_num=50;
    
    if((SigMode == SPI3_Mode)||(SigMode == SPI4_Mode))          //3�� 4��8bit SPI ģʽ
    {   			
        if((buffer[3]&0x03) == 0x00)
            SPI_Write_code(SigMode,channel,0xFE,0x02);
        else if((buffer[3]&0x03) == 0x01)
            SPI_Write_code(SigMode,channel,0xFE,0x03);
            buffer1[0] = 0x02;
            buffer1[1] = 0xEF;
            buffer1[2] = 0x01;   
		        SSD2828_W_Array(SigMode,channel,buffer1,0);
		        delay_ms(15);		
        for( i = 0;i<gamma_write_num;i++)
        {	                                                                   
            SPI_Write_code(SigMode,channel,((buffer[3]==0x00) ? ((buffer[0]==RM6D010) ? RM010_Gamma_r[i] : RM010_Gamma_r[i]) : \
                                           ((buffer[3]==0x34) ? ((buffer[0]==RM6D010) ? RM010_Gamma_g[i] : RM010_Gamma_g[i]) : \
                                                                ((buffer[0]==RM6D010) ? RM010_Gamma_b[i] : RM010_Gamma_b[i]))),buffer[i+4]);     
            delay_ms(1);
        }                                    
    }
    else if((SigMode == Mipi_Mode)||(SigMode == CMD_Mode))
    {
			   if(SigMode != CMD_Mode)
            SSD2828_W_Reg(SigMode,channel,0xB7,(HS_B7_Data|0x0010)&0XFFFD);
				 else
			      SSD2828_W_Reg(SigMode,channel,0xB7,LP_B7_Data);
         delay_ms(20);
////				buffer1[0] = 0x02;
////        buffer1[1] = 0xFE;
////        buffer1[2] = 0x01;   
////		    SSD2828_W_Array(SigMode,channel,buffer1,0);
////				 delay_ms(2);
////				buffer1[0] = 0x02;
////        buffer1[1] = 0xA7;
////        buffer1[2] = 0x00;   
////		    SSD2828_W_Array(SigMode,channel,buffer1,0);
////				 delay_ms(2);
//        buffer1[0] = 0x02;
//        buffer1[1] = 0xFE;
//			  buffer1[2] = 0x02;
//        SSD2828_W_Array(SigMode,channel,buffer1,0); 
//				delay_ms(2); 
        buffer1[0] = 0x02;
        buffer1[1] = 0xEF;
        buffer1[2] = 0x01;   
		    SSD2828_W_Array(SigMode,channel,buffer1,0);
		    delay_ms(10);			
        for( i = 0;i<gamma_write_num;i++)
        {	                                                                   			
					if((buffer[3]&0xf0)==0x00)
							SSD2828_W_Reg(SigMode,channel,0xBF,(u16)(buffer[i+4]<<8)|RM010_Gamma_r[i]);  
					else if((buffer[3]&0xf0)==0x40)
						  SSD2828_W_Reg(SigMode,channel,0xBF,(u16)(buffer[i+4]<<8)|RM010_Gamma_g[i]); 
					else if((buffer[3]&0xf0)==0x80)
							SSD2828_W_Reg(SigMode,channel,0xBF,(u16)(buffer[i+4]<<8)|RM010_Gamma_b[i]); 																		
					delay_ms(5);
        }	
		buffer1[0] = 0x02;
    buffer1[1] = 0xF0;
    buffer1[2] = 0x01;    
	  SSD2828_W_Array(SigMode,channel,buffer1,0);
    delay_ms(2);
		buffer1[0] = 0x02;
    buffer1[1] = 0xEF;
    buffer1[2] = 0x00;    
		SSD2828_W_Array(SigMode,channel,buffer1,0); 
		delay_ms(2);		
		buffer1[0] = 0x02;
    buffer1[1] = 0xF0;
    buffer1[2] = 0x00;    
	  SSD2828_W_Array(SigMode,channel,buffer1,0);		
    delay_ms(2);
        SSD2828_W_Reg(SigMode,channel,0xB7,HS_B7_Data);                              
    }
    buffer[4] = Uart_Error_None;
//    buffer[1] =  0x08;
    STM2PC_RM671xx(pdev,CDC_IN_EP,buffer,buffer[2]+3);           //���� дGamma״̬  ��ok 
}

void RM6D01X_auto_Gamma(USB_OTG_CORE_HANDLE *pdev,uint8_t  ep_addr,u8 SigMode ,u8 channel,u8* buffer,u16 LP_B7_Data,u16 HS_B7_Data)
{
	 u8 buffer1[3];
    if((SigMode == SPI3_Mode)||(SigMode == SPI4_Mode))          //3�� 4��8bit SPI ģʽ
    {                                                                  
        for( i = 0;i<buffer[2];i++)
        {	                                                                   
            SPI_Write_code(SigMode,channel,buffer[i+3],buffer[i+4]);     
            i++;
        }                                    
    }
    else if((SigMode == Mipi_Mode)||(SigMode == CMD_Mode))
    {
        if(SigMode != CMD_Mode)
            SSD2828_W_Reg(SigMode,channel,0xB7,(HS_B7_Data|0x0010)&0XFFFD);  
				
//		buffer1[0] = 0x02;
//		buffer1[1] = 0xFE;
//		buffer1[2] = 0x02;              // GAMMA1  page1
//		SSD2828_W_Array(SigMode,channel,buffer1,0);	
//		SSD2828_W_Reg(SigMode,channel,0xBC,0x02);
				buffer1[0] = 0x02;
        buffer1[1] = 0xEF;
        buffer1[2] = 0x01;   
		    SSD2828_W_Array(SigMode,channel,buffer1,0);
		    delay_ms(10);			
        for( i = 0;i<buffer[2];i++)
        {	                                                                   
            SSD2828_W_Reg(SigMode,channel,0xBF,(buffer[i+4]<<8)+buffer[i+3]);      //RM6D010
            i++;
					 //delay_ms(10);
        }	
		buffer1[0] = 0x02;
    buffer1[1] = 0xF0;
    buffer1[2] = 0x01;    
	  SSD2828_W_Array(SigMode,channel,buffer1,0);
    delay_ms(2);
		buffer1[0] = 0x02;
    buffer1[1] = 0xEF;
    buffer1[2] = 0x00;    
		SSD2828_W_Array(SigMode,channel,buffer1,0); 
		delay_ms(2);		
		buffer1[0] = 0x02;
    buffer1[1] = 0xF0;
    buffer1[2] = 0x00;    
	  SSD2828_W_Array(SigMode,channel,buffer1,0);		
    delay_ms(2);
        SSD2828_W_Reg(SigMode,channel,0xB7,HS_B7_Data);                              
    }
}

void RM6D01X_Read_Gamma(USB_OTG_CORE_HANDLE *pdev,uint8_t  ep_addr,u8 SigMode ,u8 channel,u8* buffer,u16 LP_B7_Data,u16 HS_B7_Data)
{
	  u8 buffer1[3];
	  u8 gamma_read_num=50;

			if((SigMode == Mipi_Mode)||(SigMode == CMD_Mode))
    {
        if(SigMode != CMD_Mode)
        SSD2828_W_Reg(SigMode,channel,0xB7,(HS_B7_Data|0x0010)&0XFFFD);
        SSD2828_W_Reg(SigMode,channel,0xBC,0x0002);
        SSD2828_W_Reg(SigMode,channel,0xBD,0x0000);   

//					if(buffer[0]==RM6D010)
//        {
////            if(buffer[1] == 0x09)
////                SSD2828_W_Reg(SigMode,channel,0xBF,0x02FE);
////            else if(buffer[1] == 0x19)
////                SSD2828_W_Reg(SigMode,channel,0xBF,0x03FE); 
//					if(buffer[4] == 0x00)
//    {
////        buffer1[0] = 0x02;
////        buffer1[1] = 0xFE;
////        buffer1[2] = 0x00;
////        SSD2828_W_Array(SigMode,channel,buffer1,0);
////        buffer1[0] = 0x02;
////        buffer1[1] = 0x38;
////        buffer1[2] = 0x00;
////        SSD2828_W_Array(SigMode,channel,buffer1,0); 

////        buffer1[0] = 0x02;
////        buffer1[1] = 0x53;
////        buffer1[2] = 0x20;
////        SSD2828_W_Array(SigMode,channel,buffer1,0);          

//        buffer1[0] = 0x02;
//        buffer1[1] = 0xFE;
//        buffer1[2] = 0x02;      // GAMMA1  page1
//        SSD2828_W_Array(SigMode,channel,buffer1,0); 
//    }
//    else if(buffer[4] == 0x01)
//    {
////        buffer1[0] = 0x02;
////        buffer1[1] = 0xFE;
////        buffer1[2] = 0x00;
////        SSD2828_W_Array(SigMode,channel,buffer1,0);
////        buffer1[0] = 0x02;
////        buffer1[1] = 0x53;
////        buffer1[2] = 0x20;
////        SSD2828_W_Array(SigMode,channel,buffer1,0);  

////        buffer1[0] = 0x02;
////        buffer1[1] = 0x39;
////        buffer1[2] = 0x00;
////        SSD2828_W_Array(SigMode,channel,buffer1,0);   

//        buffer1[0] = 0x02;
//        buffer1[1] = 0xFE;       
//        buffer1[2] = 0x03;      // GAMMA2  page7
//        SSD2828_W_Array(SigMode,channel,buffer1,0); 
//    }
//    else if(buffer[4] == 0x02)
//    {
////        buffer1[0] = 0x02;
////        buffer1[1] = 0xFE;
////        buffer1[2] = 0x00;
////        SSD2828_W_Array(SigMode,channel,buffer1,0);	
////        buffer1[0] = 0x02;
////        buffer1[1] = 0x38;
////        buffer1[2] = 0x00;
////        SSD2828_W_Array(SigMode,channel,buffer1,0);  
////        buffer1[0] = 0x02;
////        buffer1[1] = 0x53;
////        buffer1[2] = 0x60;
////        SSD2828_W_Array(SigMode,channel,buffer1,0);         
//        buffer1[0] = 0x02;
//        buffer1[1] = 0xFE;        
//        buffer1[2] = 0x03;      // GAMMA3  page8
//        SSD2828_W_Array(SigMode,channel,buffer1,0); 
//    }
//		else
//		{
//				buffer[4] = Uart_Error_Oth;
//				STM2PC_RM671xx(pdev,CDC_IN_EP,buffer,buffer[2]+3);           //���� дGamma״̬  ��ok  				
//				//goto RM69300_r_end;
//		}		
//        }
        if(SigMode == Mipi_Mode)  //RGB+SPI= MIPI
            SSD2828_W_Reg(SigMode,channel,0xB7,((HS_B7_Data|0x0010)&0XFFFD)|0x0080); 
              //SSD2828_W_Reg(SigMode,channel,0xB7,LP_B7_Data|0x0080);				
        else
        SSD2828_W_Reg(SigMode,channel,0xB7,LP_B7_Data|0x0080);
        SSD2828_W_Reg(SigMode,channel,0xC1,0x0001); 
        SSD2828_W_Reg(SigMode,channel,0xC0,0x0001); 
        SSD2828_W_Reg(SigMode,channel,0xBC,0x0001);
                            
        for( i = 0;i<gamma_read_num;i++)							
        {   
                SSD2828_W_Cmd(SigMode,channel,0xBF);
                SSD2828_W_Data(SigMode,channel,(buffer[3]==0x00) ? ((buffer[0]==RM6D010) ? RM010_Gamma_r[i] : RM010_Gamma_r[i]) : \
                                               (buffer[3]==0x42) ? ((buffer[0]==RM6D010) ? RM010_Gamma_g[i] : RM010_Gamma_g[i]) : \
                                                                   ((buffer[0]==RM6D010) ? RM010_Gamma_b[i] : RM010_Gamma_b[i]));
                delay_ms(25);
                if(SigMode == Mipi_Mode)  //RGB+SPI= MIPI
                    SSD2828_W_Reg(SigMode,channel,0xD4,0x00FA); 
                buffer[i+4]=SSD2828_R_Reg(SigMode,channel,0xFF);
        }                   
        SSD2828_W_Reg(SigMode,channel,0xB7,HS_B7_Data);	                              
    }
    buffer[1] = 0x0C; //(buffer[0]==RM67160) ? 0x09 : 0x0C;
    STM2PC_RM671xx(pdev,CDC_IN_EP,buffer,buffer[2]+3);           //���� ��ȡ�� Gamma����  ��ok 
}


void RM6D01X_Gamma_OTP_Start(USB_OTG_CORE_HANDLE *pdev,uint8_t  ep_addr,u8 SigMode ,u8 channel,u8* buffer,u16 LP_B7_Data,u16 HS_B7_Data)
{
    u8 tmp;
	  u8 buffer1[3];
    if((SigMode == SPI3_Mode)||(SigMode == SPI4_Mode))          //3�� 4��8bit SPI ģʽ
    {
        delay_ms(120); 
        SPI_Write_code(SigMode,channel,0xfe,0x00);
        SPI_Write_u8_Cmd(SigMode,channel,0x28); 
        delay_ms(100);  //otp
        SPI_Write_code(SigMode,channel,0xfe,0x01);
        SPI_Write_code(SigMode,channel,0xf0,0x10);
        SPI_Write_code(SigMode,channel,0xf2,0x03);
        SPI_Write_code(SigMode,channel,0xf3,0xA5); 
        SPI_Write_code(SigMode,channel,0xf4,0x5a);
        SPI_Write_code(SigMode,channel,0xf5,0x3c);
        delay_ms(500);         // wait for >400 ms 
        
        
        buffer[4] = Uart_Error_None;
        STM2PC_RM671xx(pdev,CDC_IN_EP,buffer,buffer[2]+3);           //���� OTP״̬  ��ok
    }
    else if((SigMode == Mipi_Mode)||(SigMode == CMD_Mode))
    {
        delay_ms(120); 
        if(SigMode != CMD_Mode)
            SSD2828_W_Reg(SigMode,channel,0xB7,(HS_B7_Data|0x0010)&0XFFFD);
        delay_ms(5);
        buffer1[0] = 0x02;
        buffer1[1] = 0xFE;
        buffer1[2] = 0x00;
        SSD2828_W_Array(SigMode,channel,buffer1,0);                                                                      

        buffer1[0] = 0x01;
        buffer1[1] = 0x28;
        SSD2828_W_Array(SigMode,channel,buffer1,0); 
        delay_ms(200);
//        buffer1[0] = 0x01;
//        buffer1[1] = 0x10;
//        SSD2828_W_Array(SigMode,channel,buffer1,0);     // ����ʾ  
//				
//        delay_ms(100);  //otp
        buffer1[0] = 0x02;
        buffer1[1] = 0xFE;
        buffer1[2] = 0x01;
        SSD2828_W_Array(SigMode,channel,buffer1,0);  
        if(buffer[1]==0x0A)  //otp GammaSet1
    {
        buffer1[0] = 0x02;
        buffer1[1] = 0xF0;
        buffer1[2] = 0x20;        
     
        SSD2828_W_Array(SigMode,channel,buffer1,0);     //OTP_CMD2_P1_PGM_EN          
    }
    if(buffer[1]==0x0D)  //otp GammaSet2
    {
        buffer1[0] = 0x02;
   
   
            buffer1[1] = 0xF0;
            buffer1[2] = 0x10;        
     
        SSD2828_W_Array(SigMode,channel,buffer1,0);     //OTP_CMD2_P6_PGM_EN  
    }
    if(buffer[1]==0x0E) //otp GammaSet3
    {		
        buffer1[0] = 0x02;
 
            buffer1[1] = 0xF6;
            buffer1[2] = 0x80;
    
        
        SSD2828_W_Array(SigMode,channel,buffer1,0);     //OTP_CMD2_P8_PGM_EN  
    }                                       

        delay_ms(200); 
        buffer1[1] = 0xF2; 
				buffer1[2] = 0x03;
        SSD2828_W_Array(SigMode,channel,buffer1,0); 
        
        buffer1[1] = 0xF3; 
				buffer1[2] = 0xA5;
        SSD2828_W_Array(SigMode,channel,buffer1,0);
        
        buffer1[1] = 0xF4;
				buffer1[2] = 0x5A;
        SSD2828_W_Array(SigMode,channel,buffer1,0);
        
        buffer1[1] = 0xF5; 
				buffer1[2] = 0x3C;
        SSD2828_W_Array(SigMode,channel,buffer1,0);
        SSD2828_W_Reg(SigMode,channel,0xB7,HS_B7_Data);
        delay_ms(800);         // wait for >400 ms   
        if(SigMode == Mipi_Mode)  //RGB+SPI= MIPI
            SSD2828_W_Reg(SigMode,channel,0xB7,((HS_B7_Data|0x0010)&0XFFFD)|0x0080);      
        else
            SSD2828_W_Reg(SigMode,channel,0xB7,LP_B7_Data|0x0080);
        SSD2828_W_Reg(SigMode,channel,0xC1,0x0002);
        SSD2828_W_Reg(SigMode,channel,0xC0,0x0001);
        buffer1[0] = 0x01;
        buffer1[1] = 0xEF;
        SSD2828_W_Array(SigMode,channel,buffer1,0);
        if(SigMode == Mipi_Mode)  //RGB+SPI= MIPI   ��¼OTP
            SSD2828_W_Reg(SigMode,channel,0xD4,0x00FA);
        delay_ms(100);															   								
        tmp=SSD2828_R_Reg(SigMode,channel,0xFF); 
        delay_ms(5);
        SSD2828_W_Reg(SigMode,channel,0xB7,HS_B7_Data);
        if (tmp&0x02)       //����
        {
            buffer[4] = Uart_Error_Oth;
        }
        else                //��ȷ
            buffer[4] = Uart_Error_None; 
        STM2PC_RM671xx(pdev,CDC_IN_EP,buffer,buffer[2]+3);           //���� OTP״̬  ��ok                                       
    }
}

void RM6D01X_Read_OTP_Time(USB_OTG_CORE_HANDLE *pdev,uint8_t  ep_addr,u8 SigMode ,u8 channel,u8* buffer,u16 LP_B7_Data,u16 HS_B7_Data)
{     
    u8 tmp;
    if(SigMode == SPI3_Mode)          //3��8bit SPI ģʽ
    {
 
    }
    else if(SigMode == SPI4_Mode)          //4��8bit SPI ģʽ
    {
    
    }
    else if((SigMode == Mipi_Mode)||(SigMode == CMD_Mode))
    {
        if(SigMode != CMD_Mode)
            SSD2828_W_Reg(SigMode,channel,0xB7,(HS_B7_Data|0x0010)&0XFFFD);
        delay_ms(5);
        SSD2828_W_Reg(SigMode,channel,0xBC,0x0002);
        SSD2828_W_Reg(SigMode,channel,0xBD,0x0000);
        
        if(buffer[0]==RM6D010)
        {
            SSD2828_W_Reg(SigMode,channel,0xBF,0x02FE); 
        }
        
        if(SigMode == Mipi_Mode)  //RGB+SPI= MIPI
            SSD2828_W_Reg(SigMode,channel,0xB7,((HS_B7_Data|0x0010)&0XFFFD)|0x0080);      
        else
            SSD2828_W_Reg(SigMode,channel,0xB7,LP_B7_Data|0x0080);        
        delay_ms(5);
        SSD2828_W_Reg(SigMode,channel,0xBC,0x0001);
        SSD2828_W_Cmd(SigMode,channel,0xBF);
        SSD2828_W_Data(SigMode,channel,0xEE);
        if(SigMode == Mipi_Mode)  //RGB+SPI= MIPI  
            SSD2828_W_Reg(SigMode,channel,0xD4,0x00FA);							
        delay_ms(50);                                 
        tmp=SSD2828_R_Reg(SigMode,channel,0xFF); 
        
        if (tmp&0x08)
            buffer[4] = 1;
        else if (tmp&0x10)
            buffer[4] = 2;
        else
            buffer[4] = 0;
        delay_ms(5);
        SSD2828_W_Reg(SigMode,channel,0xB7,HS_B7_Data);
        buffer[3] = Uart_Error_None;
        STM2PC_RM671xx(pdev,CDC_IN_EP,buffer,buffer[2]+3);           //���� ��ȡ��OTP����  ��ok                                  
    }
    buffer[4] = Uart_Error_None;
    STM2PC_RM671xx(pdev,CDC_IN_EP,buffer,buffer[2]+3);           //���� OTP״̬  ��ok

}

void RM6D01x_Gamma_switch(USB_OTG_CORE_HANDLE *pdev,uint8_t  ep_addr,u8 SigMode ,u8 channel,u8* buffer,u16 LP_B7_Data,u16 HS_B7_Data)
{
    u8 buffer1[5];
    static u8 RM6D010_state = 0;    // 0=normal   1=idle  2=HBM
    SSD2828_W_Reg(SigMode,channel,0xB7,(HS_B7_Data|0x0010)&0XFFFD);
    delay_ms(10);
    if(buffer[3] == 0x01)
    {
//        buffer1[0] = 0x02;
//        buffer1[1] = 0xFE;
//        buffer1[2] = 0x00;
//        SSD2828_W_Array(SigMode,channel,buffer1,0);
//        if(RM6D010_state == 2)
//        {
//            buffer1[0] = 0x02;
//            buffer1[1] = 0x53;
//            buffer1[2] = 0x20;         //Brightness control is on
//            SSD2828_W_Array(SigMode,channel,buffer1,0);
//        }
//        else if(RM6D010_state == 1)
//        {
//            buffer1[0] = 0x02;
//            buffer1[1] = 0x38;
//            buffer1[2] = 0x00;         //Exit idle 
//            SSD2828_W_Array(SigMode,channel,buffer1,0);  
//        }  
        
        buffer1[0] = 0x02;
        buffer1[1] = 0xFE;
        buffer1[2] = 0x02;
        SSD2828_W_Array(SigMode,channel,buffer1,0);
        RM6D010_state = 0;
    }
    else if(buffer[3] == 0x02)
    {
//        buffer1[0] = 0x02;
//        buffer1[1] = 0xFE;
//        buffer1[2] = 0x00;
//        SSD2828_W_Array(SigMode,channel,buffer1,0);
//        if(RM6D010_state == 2)
//        {
//            buffer1[0] = 0x02;
//            buffer1[1] = 0x53;
//            buffer1[2] = 0x20;
//            SSD2828_W_Array(SigMode,channel,buffer1,0);
//        }
//        buffer1[0] = 0x02;
//        buffer1[1] = 0x39;
//        buffer1[2] = 0x00;            //Enter idle
//        SSD2828_W_Array(SigMode,channel,buffer1,0);
        buffer1[0] = 0x02;
        buffer1[1] = 0xFE;
        buffer1[2] = 0x03;
        SSD2828_W_Array(SigMode,channel,buffer1,0);
        RM6D010_state = 1;
       
    }   
    else if(buffer[3] == 0x03)
    {
//        buffer1[0] = 0x02;
//        buffer1[1] = 0xFE;
//        buffer1[2] = 0x00;
//        SSD2828_W_Array(SigMode,channel,buffer1,0);
//        if(RM6D010_state == 1)
//        {
//            buffer1[0] = 0x02;
//            buffer1[1] = 0x38;
//            buffer1[2] = 0x00;
//            SSD2828_W_Array(SigMode,channel,buffer1,0);
//        }
//        buffer1[0] = 0x02;
//        buffer1[1] = 0x53;
//        buffer1[2] = 0x60;
//        SSD2828_W_Array(SigMode,channel,buffer1,0);
        buffer1[0] = 0x02;
        buffer1[1] = 0xFE;
        buffer1[2] = 0x03;
        SSD2828_W_Array(SigMode,channel,buffer1,0);
        RM6D010_state = 2;
    }
    
    delay_ms(5);
    buffer1[0] = 0x02;
    buffer1[1] = 0xEF;
    buffer1[2] = 0x01;
    SSD2828_W_Array(SigMode,channel,buffer1,0);
    delay_ms(50);    
    
		buffer1[0] = 0x02;
		buffer1[1] = 0xF0;
		buffer1[2] = 0x01;    
		SSD2828_W_Array(SigMode,channel,buffer1,0);
		buffer1[0] = 0x02;
		buffer1[1] = 0xEF;
		buffer1[2] = 0x00;    
		SSD2828_W_Array(SigMode,channel,buffer1,0);     
		delay_ms(5);
		SSD2828_W_Reg(SigMode,channel,0xB7,HS_B7_Data);
		buffer[4] = Uart_Error_None;
		STM2PC_RM671xx(pdev,CDC_IN_EP,buffer,buffer[2]+3);           //���� дGamma״̬  ��ok  
}



/********************************************************************************************
********************************************************************************************
    HX83200A  д�Ĵ��� ���Ĵ���  дGamma ��Gamma OTP��¼
    HX83200A  д�Ĵ��� ���Ĵ���  дGamma ��Gamma OTP��¼
    HX83200A  д�Ĵ��� ���Ĵ���  дGamma ��Gamma OTP��¼
    HX83200A  д�Ĵ��� ���Ĵ���  дGamma ��Gamma OTP��¼
    HX83200A  д�Ĵ��� ���Ĵ���  дGamma ��Gamma OTP��¼
********************************************************************************************
********************************************************************************************/
void HX83200_Write_Register(USB_OTG_CORE_HANDLE *pdev,uint8_t  ep_addr,u8 SigMode ,u8 channel,u8* buffer,u16 LP_B7_Data,u16 HS_B7_Data)
{
    SSD2828_W_Reg(SigMode,channel,0xB7,(HS_B7_Data|0x0010)&0XFFFD);
		//SSD2828_W_Reg(SigMode,channel,0xB7,LP_B7_Data);
    delay_ms(5);
    SSD2828_W_Array(SigMode,channel,buffer,2);

    SSD2828_W_Reg(SigMode,channel,0xB7,HS_B7_Data);
    delay_ms(5);
    buffer[4] = Uart_Error_None;
    STM2PC_RM671xx(pdev,CDC_IN_EP,buffer,buffer[2]+3);           //���� д�Ĵ���״̬  ��ok
}

void HX83200_Write51_Register(u8 SigMode ,u8 channel,u8* buffer,u16 LP_B7_Data,u16 HS_B7_Data)
{
    if(SigMode != CMD_Mode)
        SSD2828_W_Reg(SigMode,channel,0xB7,(HS_B7_Data|0x0010)&0XFFFD);
    delay_ms(5);
    SSD2828_W_Array(SigMode,channel,buffer,2);
    SSD2828_W_Reg(SigMode,channel,0xB7,HS_B7_Data);
    delay_ms(5);
    if(buffer[0]==RM67195)
    {
        SSD2828_W_Reg(SigMode,channel,0xBF,0xAAFB); 
    }
}

void HX83200_Read_Register(USB_OTG_CORE_HANDLE *pdev,uint8_t  ep_addr,u8 SigMode ,u8 channel,u8* buffer,u16 LP_B7_Data,u16 HS_B7_Data)
{
    u8 buffer1[2];
		u16 tmp;
    if(SigMode == Mipi_Mode)  //RGB+SPI= MIPI
        SSD2828_W_Reg(SigMode,channel,0xB7,((HS_B7_Data|0x0010)&0XFFFD)|0x0080);      
    else
        SSD2828_W_Reg(SigMode,channel,0xB7,LP_B7_Data|0x0080); 
    delay_ms(5);
    SSD2828_W_Reg(SigMode,channel,0xC1,buffer[2]-1); //return package size                                
    SSD2828_W_Reg(SigMode,channel,0xC0,0x0001);                           
    buffer1[0] = 0x01;
    buffer1[1] = buffer[3];
    SSD2828_W_Array(SigMode,channel,buffer1,0); 
    delay_ms(20);

    buffer[0] = 0x01;
    buffer[1] = buffer[3];                        
    SSD2828_W_Array(SigMode,channel,buffer,0);
    delay_ms(5);  
    SSD2828_W_Reg(SigMode,channel,0xD4,0x00FA);
    SSD2828_W_Cmd(SigMode,channel,0xFF);
    for(i=0;i<buffer[2]-1;i++)
    {
  //      SSD2828_W_Cmd(SigMode,channel,0xFA);		
        tmp=SPI3_Read_u16_Data(channel);
        buffer[4+i]=tmp>>8;
        buffer[5+i]=tmp;
        delay_ms(5);
        i++;
    }
		buffer[0] = 0x2b;  
		buffer[1] = 0x02;  
    SSD2828_W_Reg(SigMode,channel,0xB7,HS_B7_Data);
    STM2PC_RM671xx(pdev,CDC_IN_EP,buffer,buffer[2]+3);           //���� ��ȡ�� �Ĵ�������  ��ok 	
}

void HX83200_Write_Gamma(USB_OTG_CORE_HANDLE *pdev,uint8_t  ep_addr,u8 SigMode ,u8 channel,u8* buffer,u16 LP_B7_Data,u16 HS_B7_Data)
{
    u8 buffer1[3];
		u8 i=0,j=0,k=0,l=0,temp=0;
	//	SSD2828_W_Reg(SigMode,channel,0xB7,LP_B7_Data);
    SSD2828_W_Reg(SigMode,channel,0xB7,(HS_B7_Data|0x0010)&0XFFFD);
		delay_ms(5);
    buffer1[0] = 0x02;
    buffer1[1] = 0xB0;
		if(buffer[3]==0x81)
			buffer1[2] = 0x06;  //PAGE 6
		else
			buffer1[2] = 0x05;  //PAGE 5
		SSD2828_W_Array(SigMode,channel,buffer1,0);
		
		for(i=0;i<29;i++)
		{
				if((buffer[3]==0x00)||(buffer[3]==0x81))	
				{
						temp|=(buffer[k+5]<<(l*2));
						if((++l==4)||(i==28))
						{
							SSD2828_W_Reg(SigMode,channel,0xBF,(u16)((temp<<8)|(0xB8-j++))); //����λ 
							temp=0;l=0;
						}
						SSD2828_W_Reg(SigMode,channel,0xBF,(u16)(buffer[k+4]<<8)|(0xD5-i));  //�Ͱ�λ						
				}
				else if(buffer[3]==0x42)
				{
						temp|=(buffer[k+5]<<(l*2));
						if((++l==4)||(i==28))
						{
							SSD2828_W_Reg(SigMode,channel,0xBF,(u16)((temp<<8)|(0xDD-j++))); //����λ 
							temp=0;l=0;
						}
						SSD2828_W_Reg(SigMode,channel,0xBF,(u16)(buffer[k+4]<<8)|(0xFA-i));  //�Ͱ�λ			
				}
				k+=2;
				delay_ms(1);
		}
    SSD2828_W_Reg(SigMode,channel,0xB7,HS_B7_Data);
    buffer[4] = Uart_Error_None;
    STM2PC_RM671xx(pdev,CDC_IN_EP,buffer,buffer[2]+3);           //���� дGamma״̬  ��ok  
}

void HX83200_auto_Gamma(USB_OTG_CORE_HANDLE *pdev,uint8_t  ep_addr,u8 SigMode ,u8 channel,u8* buffer,u16 LP_B7_Data,u16 HS_B7_Data)
{
  
}

void HX83200_Read_Gamma(USB_OTG_CORE_HANDLE *pdev,uint8_t  ep_addr,u8 SigMode ,u8 channel,u8* buffer,u16 LP_B7_Data,u16 HS_B7_Data)
{
    u8 i,j=0,k=0,MSB=0,LSB=0,temp;
		u8 buffer1[3];
		SSD2828_W_Reg(SigMode,channel,0xB7,LP_B7_Data);
    //SSD2828_W_Reg(SigMode,channel,0xB7,(HS_B7_Data|0x0010)&0XFFFD);
		delay_ms(15);
	
    buffer1[0] = 0x02;
    buffer1[1] = 0xB0;
		if(buffer[3]==0x81) //��ɫ
		{
			buffer1[2] = 0x06;  //PAGE 6
		}
		else
		{
			buffer1[2] = 0x05;  //PAGE 5
		}
		SSD2828_W_Array(SigMode,channel,buffer1,0);
		SSD2828_W_Reg(SigMode,channel,0xB7,LP_B7_Data|0x0080);
//  SSD2828_W_Reg(SigMode,channel,0xB7,((HS_B7_Data|0x0010)&0XFFFD)|0x0080);      
		SSD2828_W_Reg(SigMode,channel,0xC1,0x0001); 
		SSD2828_W_Reg(SigMode,channel,0xC0,0x0001); 
		SSD2828_W_Reg(SigMode,channel,0xBC,0x0001);                     
		for( i = 0; i<58; i++)							
		{ 
			if(i%2==0)//ż�����߰�λ
			{
					SSD2828_W_Cmd(SigMode,channel,0xBF);
					if(buffer[3]==0x00)
					{
							SSD2828_W_Data(SigMode,channel,0xB8-MSB);
					}
					else if(buffer[3]==0x42)
					{
							SSD2828_W_Data(SigMode,channel,0xDD-MSB);				
					}
					else if(buffer[3]==0x81)
					{
							SSD2828_W_Data(SigMode,channel,0xB8-MSB);				
					}
					delay_ms(5);
					SSD2828_W_Reg(SigMode,channel,0xD4,0x00FA); 
					temp=SSD2828_R_Reg(SigMode,channel,0xFF);
					buffer[i+4]=(temp>>(k*2))&0x03;
					k++;		
					if(++j==4)
					{
						MSB++;
						j=0;
						k=0;
					}					
			}	
			else
			{
					SSD2828_W_Cmd(SigMode,channel,0xBF);
					if(buffer[3]==0x00)
						SSD2828_W_Data(SigMode,channel,0xD5-LSB);
					else if(buffer[3]==0x42)
						SSD2828_W_Data(SigMode,channel,0xFA-LSB);
					else if(buffer[3]==0x81)
						SSD2828_W_Data(SigMode,channel,0xD5-LSB);
					delay_ms(5);
					SSD2828_W_Reg(SigMode,channel,0xD4,0x00FA); 
					buffer[i+4]=SSD2828_R_Reg(SigMode,channel,0xFF);
					LSB++;
			}
			delay_ms(1);
		}
		SSD2828_W_Reg(SigMode,channel,0xB7,HS_B7_Data);	                              
    STM2PC_RM671xx(pdev,CDC_IN_EP,buffer,buffer[2]+3);           //���� ��ȡ�� Gamma����  ��ok 
}

void HX83200_Gamma_OTP_Start(USB_OTG_CORE_HANDLE *pdev,uint8_t  ep_addr,u8 SigMode ,u8 channel,u8* buffer,u16 LP_B7_Data,u16 HS_B7_Data)
{
    u16 tmp,i;
    u8 buffer1[3];

    SSD2828_W_Reg(SigMode,channel,0xB7,LP_B7_Data);
    delay_ms(15);
    buffer1[0] = 0x02;  //select page =E
    buffer1[1] = 0xB0;
    buffer1[2] = 0x0E;  
    SSD2828_W_Array(SigMode,channel,buffer1,0); 
	//	delay_ms(5); 
    buffer1[0] = 0x02;  //program password
    buffer1[1] = 0xB2;
    buffer1[2] = 0xA5;  
    SSD2828_W_Array(SigMode,channel,buffer1,0); 
//		delay_ms(5);
		for(i=0;i<3;i++)	
		{	
				buffer1[0] = 0x02;  //select group
				buffer1[1] = 0xB1;
				buffer1[2] = 0x0c+i;  
				SSD2828_W_Array(SigMode,channel,buffer1,0); 
			//	delay_ms(5); 
				buffer1[0] = 0x02;  //start to program
				buffer1[1] = 0xB3;
				buffer1[2] = 0x01;  
				SSD2828_W_Array(SigMode,channel,buffer1,0); 	
				delay_ms(50);         // wait for >25ms
		}
  //  SSD2828_W_Reg(SigMode,channel,0xB7,HS_B7_Data);

    buffer[4] = Uart_Error_None; 
    STM2PC_RM671xx(pdev,CDC_IN_EP,buffer,buffer[2]+3);           //���� дOTP״̬  ��ok  
}

void HX83200_Read_OTP_Time(USB_OTG_CORE_HANDLE *pdev,uint8_t  ep_addr,u8 SigMode ,u8 channel,u8* buffer,u16 LP_B7_Data,u16 HS_B7_Data)
{
    u8 buffer1[3];
		u8 tmp=0,NX83200_OTP_Times=0;
    SSD2828_W_Reg(SigMode,channel,0xB7,LP_B7_Data);
    delay_ms(5);
	
		for(i=0;i<3;i++)	
		{	
				buffer1[0] = 0x02;  //select page =E
				buffer1[1] = 0xB0;
				buffer1[2] = 0x0E;  
				SSD2828_W_Array(SigMode,channel,buffer1,0); 
			
				buffer1[0] = 0x02;  //program password
				buffer1[1] = 0xB2;
				buffer1[2] = 0xA5;  
				SSD2828_W_Array(SigMode,channel,buffer1,0); 
		
				//------------------------------------
				buffer1[0] = 0x02;  //��ȡĿ���ַ�ĸ�4λ
				buffer1[1] = 0xB4;
				buffer1[2] = 0x04;  //05
				SSD2828_W_Array(SigMode,channel,buffer1,0); 	

				buffer1[0] = 0x02;  //��ȡĿ���ַ�ĵ�8λ
				buffer1[1] = 0xB5;
				buffer1[2] = 0x35+37*i;   //13
				SSD2828_W_Array(SigMode,channel,buffer1,0); 	
				delay_ms(5);
				//-----------------------------------
				buffer1[0] = 0x02;  //to set OTP read
				buffer1[1] = 0xB3;
				buffer1[2] = 0x04;  
				SSD2828_W_Array(SigMode,channel,buffer1,0); 	

				SSD2828_W_Reg(SigMode,channel,0xB7,LP_B7_Data|0x0080); 
				delay_ms(5);
				SSD2828_W_Reg(SigMode,channel,0xC1,0x0001);
				SSD2828_W_Reg(SigMode,channel,0xC0,0x0001);
				buffer1[0] = 0x01;
				buffer1[1] = 0xb6; //read reg.B6 OTP value
				SSD2828_W_Array(SigMode,channel,buffer1,0);
				delay_ms(5);
				SSD2828_W_Reg(SigMode,channel,0xD4,0x00FA);														   								
				tmp=SSD2828_R_Reg(SigMode,channel,0xFF); 
				if(tmp&0x80)
				{
					NX83200_OTP_Times++;
				}
				delay_ms(5);	
		}
		SSD2828_W_Reg(SigMode,channel,0xB7,HS_B7_Data);
    buffer[4] = NX83200_OTP_Times; 
    STM2PC_RM671xx(pdev,CDC_IN_EP,buffer,buffer[2]+3);           //���� дOTP״̬  ��ok	
}


/********************************************************************************************
********************************************************************************************
    RM67195��RM67295��RM67198  д�Ĵ��� ���Ĵ���  дGamma ��Gamma OTP��¼
    RM67195��RM67295��RM67198  д�Ĵ��� ���Ĵ���  дGamma ��Gamma OTP��¼
    RM67195��RM67295��RM67198  д�Ĵ��� ���Ĵ���  дGamma ��Gamma OTP��¼
    RM67195��RM67295��RM67198  д�Ĵ��� ���Ĵ���  дGamma ��Gamma OTP��¼
    RM67195��RM67295��RM67198  д�Ĵ��� ���Ĵ���  дGamma ��Gamma OTP��¼
********************************************************************************************
********************************************************************************************/
void RM6719X_Write_Register(USB_OTG_CORE_HANDLE *pdev,uint8_t  ep_addr,u8 SigMode ,u8 channel,u8* buffer,u16 LP_B7_Data,u16 HS_B7_Data)
{
    if(SigMode != CMD_Mode)
        SSD2828_W_Reg(SigMode,channel,0xB7,(HS_B7_Data|0x0010)&0XFFFD);
		else
				SSD2828_W_Reg(SigMode,channel,0xB7,LP_B7_Data);
    delay_ms(5);
    SSD2828_W_Array(SigMode,channel,buffer,2);
    SSD2828_W_Reg(SigMode,channel,0xB7,HS_B7_Data);
    delay_ms(5);
    if(buffer[0]==RM67195)
    {
        SSD2828_W_Reg(SigMode,channel,0xBF,0xAAFB); 
    }
    buffer[4] = Uart_Error_None;
    STM2PC_RM671xx(pdev,CDC_IN_EP,buffer,buffer[2]+3);           //���� д�Ĵ���״̬  ��ok
}

void RM6719X_Write51_Register(u8 SigMode ,u8 channel,u8* buffer,u16 LP_B7_Data,u16 HS_B7_Data)
{
    if(SigMode != CMD_Mode)
        SSD2828_W_Reg(SigMode,channel,0xB7,(HS_B7_Data|0x0010)&0XFFFD);
    delay_ms(5);
    SSD2828_W_Array(SigMode,channel,buffer,2);
    SSD2828_W_Reg(SigMode,channel,0xB7,HS_B7_Data);
    delay_ms(5);
    if(buffer[0]==RM67195)
    {
        SSD2828_W_Reg(SigMode,channel,0xBF,0xAAFB); 
    }
}

void RM6719X_Read_Register(USB_OTG_CORE_HANDLE *pdev,uint8_t  ep_addr,u8 SigMode ,u8 channel,u8* buffer,u16 LP_B7_Data,u16 HS_B7_Data)
{
    u8 buffer1[2];
    if(SigMode == Mipi_Mode)  //RGB+SPI= MIPI
        SSD2828_W_Reg(SigMode,channel,0xB7,((HS_B7_Data|0x0010)&0XFFFD)|0x0080);      
    else
        SSD2828_W_Reg(SigMode,channel,0xB7,LP_B7_Data|0x0080); 
    delay_ms(5);
    SSD2828_W_Reg(SigMode,channel,0xC1,0x0002);                                
    buffer1[0] = 0x01;
    buffer1[1] = buffer[3];
    SSD2828_W_Array(SigMode,channel,buffer1,0); 
    delay_ms(20);
    buffer[4]=SSD2828_R_Reg(SigMode,channel,0xFF);
    SSD2828_W_Reg(SigMode,channel,0xB7,HS_B7_Data);
    delay_ms(5);
    STM2PC_RM671xx(pdev,CDC_IN_EP,buffer,buffer[2]+3);           //���� ��ȡ�� �Ĵ�������  ��ok  
}

void RM6719X_Write_Gamma(USB_OTG_CORE_HANDLE *pdev,uint8_t  ep_addr,u8 SigMode ,u8 channel,u8* buffer,u16 LP_B7_Data,u16 HS_B7_Data)
{
    u8 buffer1[3];
    if(SigMode != CMD_Mode)
        SSD2828_W_Reg(SigMode,channel,0xB7,(HS_B7_Data|0x0010)&0XFFFD);
    buffer1[0] = 0x02;
    buffer1[1] = 0xFE;
    buffer1[2] = ((buffer[0]==RM67198)||(buffer[0]==RM67298)) ? 0x50 : 0x05;
		SSD2828_W_Array(SigMode,channel,buffer1,0);
		
		if(buffer[0]==RM67198) //67198����gamma ������fe,51
		{
				if(buffer[3]&0x08)
				{
					buffer1[2]=0x51;
					SSD2828_W_Array(SigMode,channel,buffer1,0);
					buffer[3]-=0x08;
				}
		}
		if(buffer[0]==RM67298) //69298����gamma ������fe,51
		{
				if(buffer[3]&0x08)
				{
					buffer1[2]=0x52;
					SSD2828_W_Array(SigMode,channel,buffer1,0);
					buffer[3]-=0x08;
				}
		}
		  
    if((buffer[0]==RM67195)||(buffer[0]==RM67295)||(buffer[0]==RM67198)||(buffer[0]==RM67298))
    {
        for( i = 0;i<buffer[2]-1;i++) 
        {	 
            if(buffer[0]==RM67195)
            {
                if(buffer[3]==0x00)	
                    SSD2828_W_Reg(SigMode,channel,0xBF,(u16)(buffer[i+4]<<8)|RM195_Gamma_r[i]);  
                else if(buffer[3]==0x42)
                    SSD2828_W_Reg(SigMode,channel,0xBF,(u16)(buffer[i+4]<<8)|RM195_Gamma_g[i]); 
                else if(buffer[3]==0x81)
                    SSD2828_W_Reg(SigMode,channel,0xBF,(u16)(buffer[i+4]<<8)|RM195_Gamma_b[i]); 
                SSD2828_W_Reg(SigMode,channel,0xBF,0xAAFB); 
            }
            else if(buffer[0]==RM67295)
            {
                if(buffer[3]==0x00)	
                    SSD2828_W_Reg(SigMode,channel,0xBF,(u16)(buffer[i+4]<<8)|RM295_Gamma_r[i]);  
                else if(buffer[3]==0x42)
                    SSD2828_W_Reg(SigMode,channel,0xBF,(u16)(buffer[i+4]<<8)|RM295_Gamma_g[i]); 
                else if(buffer[3]==0x81)
                    SSD2828_W_Reg(SigMode,channel,0xBF,(u16)(buffer[i+4]<<8)|RM295_Gamma_b[i]); 																		
            }
            else if(buffer[0]==RM67198||(buffer[0]==RM67298))
            {
                if(buffer[2]==67)
                {
                    if(buffer[3]==0x00)	
                        SSD2828_W_Reg(SigMode,channel,0xBF,(u16)(buffer[i+4]<<8)|RM198_Gamma33pt_r[i]);  
                    else if(buffer[3]==0x42)
                        SSD2828_W_Reg(SigMode,channel,0xBF,(u16)(buffer[i+4]<<8)|RM198_Gamma33pt_g[i]); 
                    else if(buffer[3]==0x81)
                        SSD2828_W_Reg(SigMode,channel,0xBF,(u16)(buffer[i+4]<<8)|RM198_Gamma33pt_b[i]); 																				
                }
                else if(buffer[2]==39)
                {
                    if(buffer[3]==0x00)	
                        SSD2828_W_Reg(SigMode,channel,0xBF,(u16)(buffer[i+4]<<8)|RM198_Gamma19pt_r[i]);  
                    else if(buffer[3]==0x42)
                        SSD2828_W_Reg(SigMode,channel,0xBF,(u16)(buffer[i+4]<<8)|RM198_Gamma19pt_g[i]); 
                    else if(buffer[3]==0x81)
                        SSD2828_W_Reg(SigMode,channel,0xBF,(u16)(buffer[i+4]<<8)|RM198_Gamma19pt_b[i]); 																				
                }
                else
                {
                    if(buffer[3]==0x00)	
                        SSD2828_W_Reg(SigMode,channel,0xBF,(u16)(buffer[i+4]<<8)|RM295_Gamma_r[i]);  
                    else if(buffer[3]==0x42)
                        SSD2828_W_Reg(SigMode,channel,0xBF,(u16)(buffer[i+4]<<8)|RM295_Gamma_g[i]); 
                    else if(buffer[3]==0x81)
                        SSD2828_W_Reg(SigMode,channel,0xBF,(u16)(buffer[i+4]<<8)|RM295_Gamma_b[i]); 																							
                }
                    
            }
            delay_ms(5);
        }
    }
    if(buffer[0]==RM67198||(buffer[0]==RM67298))
    {
        SSD2828_W_Reg(SigMode,channel,0xBF,0x00FE); 
    }				
    delay_ms(5);
    SSD2828_W_Reg(SigMode,channel,0xB7,HS_B7_Data);
    buffer[4] = Uart_Error_None;
    STM2PC_RM671xx(pdev,CDC_IN_EP,buffer,buffer[2]+3);           //���� дGamma״̬  ��ok  
}

void RM6719X_auto_Gamma(USB_OTG_CORE_HANDLE *pdev,uint8_t  ep_addr,u8 SigMode ,u8 channel,u8* buffer,u16 LP_B7_Data,u16 HS_B7_Data)
{
    if(SigMode != CMD_Mode)
        SSD2828_W_Reg(SigMode,channel,0xB7,(HS_B7_Data|0x0010)&0XFFFD);
    //SSD2828_W_Reg(SigMode,channel,0xBC,0x02);
    for( i = 0;i<buffer[2];i++) 
    {	 
            SSD2828_W_Reg(SigMode,channel,0xBF,(u16)(buffer[i+4]<<8)|buffer[i+3]);  
            if(buffer[0]==RM67195)
            {
                    SSD2828_W_Reg(SigMode,channel,0xBF,0xAAFB);
            }
            i++;
            delay_ms(2);
    }				
    SSD2828_W_Reg(SigMode,channel,0xB7,HS_B7_Data);
}

void RM6719X_Read_Gamma(USB_OTG_CORE_HANDLE *pdev,uint8_t  ep_addr,u8 SigMode ,u8 channel,u8* buffer,u16 LP_B7_Data,u16 HS_B7_Data)
{
    u8 buffer1[3];
    if(SigMode != CMD_Mode)
        SSD2828_W_Reg(SigMode,channel,0xB7,(HS_B7_Data|0x0010)&0XFFFD);
    delay_ms(2);
    buffer1[0] = 0x02;
    buffer1[1] = 0xFE;
    buffer1[2] = ((buffer[0]==RM67198)||(buffer[0]==RM67298)) ? 0x50 : 0x05;
    SSD2828_W_Array(SigMode,channel,buffer1,0); 
	
    if(buffer[0]==RM67198) //67198����gamma ������fe,51
    {
        if((buffer[3]&0x08)!=0)
        {
            buffer1[2]=0x51;
            SSD2828_W_Array(SigMode,channel,buffer1,0);
            buffer[3]-=0x08;
        }
    }
		if(buffer[0]==RM67298) //67198����gamma ������fe,52
    {
        if((buffer[3]&0x08)!=0)
        {
            buffer1[2]=0x52;
            SSD2828_W_Array(SigMode,channel,buffer1,0);
            buffer[3]-=0x08;
        }
    }
    if(SigMode == Mipi_Mode)  //RGB+SPI= MIPI
        SSD2828_W_Reg(SigMode,channel,0xB7,((HS_B7_Data|0x0010)&0XFFFD)|0x0080);      
    else
        SSD2828_W_Reg(SigMode,channel,0xB7,LP_B7_Data|0x0080);      
    SSD2828_W_Reg(SigMode,channel,0xBC,0x0001);	
    for( i = 0;i<buffer[2]-1;i++)			 	
    {  	
        
        SSD2828_W_Cmd(SigMode,channel,0xBF);
        if(buffer[0]==RM67195)
        {
            SSD2828_W_Data(SigMode,channel,(buffer[3]==0x00) ? RM195_Gamma_r[i] : ((buffer[3]==0x42) ? RM195_Gamma_g[i] : RM195_Gamma_b[i]));
            delay_ms(15); 
        }
        else if(buffer[0]==RM67295)
        {
            SSD2828_W_Data(SigMode,channel,(buffer[3]==0x00) ? RM295_Gamma_r[i] : ((buffer[3]==0x42) ? RM295_Gamma_g[i] : RM295_Gamma_b[i]));
            delay_ms(15); 
        }
        else if(buffer[0]==RM67198||(buffer[0]==RM67298))
        {
            if(buffer[2]==39)
            {
                SSD2828_W_Data(SigMode,channel,(buffer[3]==0x00) ? RM198_Gamma19pt_r[i] : ((buffer[3]==0x42) ? RM198_Gamma19pt_g[i] : RM198_Gamma19pt_b[i]));
            }
            else if(buffer[2]==67)
            {
                SSD2828_W_Data(SigMode,channel,(buffer[3]==0x00) ? RM198_Gamma33pt_r[i] : ((buffer[3]==0x42) ? RM198_Gamma33pt_g[i] : RM198_Gamma33pt_b[i]));
            }
            else
            {
                SSD2828_W_Data(SigMode,channel,(buffer[3]==0x00) ? RM295_Gamma_r[i] : ((buffer[3]==0x42) ? RM295_Gamma_g[i] : RM295_Gamma_b[i]));
            }
            delay_ms(8); 
        }
        if(SigMode == Mipi_Mode)  //RGB+SPI= MIPI
            SSD2828_W_Reg(SigMode,channel,0xD4,0x00FA);                                              
        delay_ms(12);                                                          
        buffer[4+i]=SSD2828_R_Reg(SigMode,channel,0xFF); 
    }
    delay_ms(1);
    SSD2828_W_Reg(SigMode,channel,0xB7,HS_B7_Data);
    STM2PC_RM671xx(pdev,CDC_IN_EP,buffer,buffer[2]+3);           //���� ��ȡ�� Gamma����  ��ok  
}


void RM67298_Demura_Ram_EN(USB_OTG_CORE_HANDLE *pdev,uint8_t  ep_addr,u8 SigMode ,u8 channel,u8* buffer,u16 LP_B7_Data,u16 HS_B7_Data,u8 RM298_EN)
{
		u8 Demura_EN_code_table[]=
		{
				0xfe,0x40,
				0x0e,0x80,
				0xfe,0x20,
				0xb1,0x00,
				0xfe,0xd0,
				0x55,0x30,
				0x07,0x01,
				0xfe,0x40,
				0x0e,0x50,
				0xfe,0xd0,
				0x42,0x81,
				0x27,0x55
		};
		u8 Demura_OFF_code_table[]=
		{
				0xfe,0x40,
				0x0e,0x80,
				0xfe,0x20,
				0xb1,0x00,
				0xfe,0xd0,
				0x55,0x00,
				0x07,0x01,
				0xfe,0x40,
				0x0e,0x50,
				0xfe,0xd0,
				0x42,0x01,
				0x27,0x55
		};		
		
		u8 buffer1[3];
	  u8 i=0;
		SSD2828_W_Reg(SigMode,channel,0xB7,LP_B7_Data);
		delay_ms(5);
		for(i=0;i<24;i++)
		{
			buffer1[0] = 0x02;
			if(RM298_EN==1)
			{
					buffer1[1] = Demura_EN_code_table[i];
					buffer1[2] = Demura_EN_code_table[i+1];
			}
			else
			{
					buffer1[1] = Demura_OFF_code_table[i];
					buffer1[2] = Demura_OFF_code_table[i+1];		
			}
			SSD2828_W_Array(SigMode,channel,buffer1,0);
			i+=2;
		}
					SSD2828_W_Reg(SigMode,channel,0xB7,HS_B7_Data);
}

void RM6719X_Gamma_OTP_Start(USB_OTG_CORE_HANDLE *pdev,uint8_t  ep_addr,u8 SigMode ,u8 channel,u8* buffer,u16 LP_B7_Data,u16 HS_B7_Data,u8 RM195_otptime)
{
    u16 tmp;
    u8 buffer1[3];
    delay_ms(120); 
    if(SigMode != CMD_Mode)
        SSD2828_W_Reg(SigMode,channel,0xB7,(HS_B7_Data|0x0010)&0XFFFD);
    delay_ms(5);
    buffer1[0] = 0x02;
    buffer1[1] = 0xFE;
    buffer1[2] = 0x00;
    SSD2828_W_Array(SigMode,channel,buffer1,0); 
    delay_ms(5);
    buffer1[0] = 0x01;
    buffer1[1] = 0x28;
    SSD2828_W_Array(SigMode,channel,buffer1,0); 
    delay_ms(200);  //otp
    buffer1[0] = 0x02;
    buffer1[1] = 0xFE;
    buffer1[2] = 0x04;
    SSD2828_W_Array(SigMode,channel,buffer1,0);
    if((buffer[0]==RM67198)||(buffer[0]==RM67298))
    {															
        SSD2828_W_Reg(SigMode,channel,0xBF,0x40FE);	
    }
    delay_ms(200);  

    if(buffer[1]==0x0D)  //page0 RM67198&RM67195һ��
		{
			if(buffer[0]==RM67298)
			 {
				 SSD2828_W_Reg(SigMode,channel,0xBF,(RM195_otptime==0x01) ? 0x01F6 : ((RM195_otptime==0x02) ? 0x01F7 : 0x80F9));//OTP_cmd2_P0_PGM_EN
			 }			 
			 else
			 {
        SSD2828_W_Reg(SigMode,channel,0xBF,(RM195_otptime==0x01) ? 0x01F7 : ((RM195_otptime==0x02) ? 0x01F8 : 0x02F8));//OTP_cmd2_P0_PGM_EN
       }
		 }			 
		if(buffer[1]==0x0A)  //page1
		 {
        if(buffer[0]==RM67198)
        {
            SSD2828_W_Reg(SigMode,channel,0xBF, 0x02F7);//OTP_cmd2_P1_PGM_EN
        }
				else if(buffer[0]==RM67298)
        {
            SSD2828_W_Reg(SigMode,channel,0xBF, 0x02F6);//OTP_cmd2_P1_PGM_EN
        }
        else
        {
            SSD2828_W_Reg(SigMode,channel,0xBF,(RM195_otptime==0x01) ? 0x02F7 : ((RM195_otptime==0x02) ? 0x08F8 : 0x10F8) );//OTP_cmd2_P1_PGM_EN
        }
     }
    if(buffer[1]==0x0E) //page 2 
		{		
				if(buffer[0]==RM67198)
				{
						SSD2828_W_Reg(SigMode,channel,0xBF,(RM195_otptime==0x01) ? 0x04F7 : 0x04F8);//OTP_cmd2_P2_PGM_EN
				}	
				else if(buffer[0]==RM67298)
				{
						SSD2828_W_Reg(SigMode,channel,0xBF,(RM195_otptime==0x01) ? 0x04F6 : 0x04F7);//OTP_cmd2_P2_PGM_EN
				}	
				else 
        {
						SSD2828_W_Reg(SigMode,channel,0xBF,(RM195_otptime==0x01) ? 0x04F7 : ((RM195_otptime==0x02) ? 0x20F8 : 0x40F8));//OTP_cmd2_P2_PGM_EN
				}
		}
		//zangqiang 20171120
    if(buffer[1]==0x0F) //Page 3
		{
				if(buffer[0]==RM67198)
				{
						SSD2828_W_Reg(SigMode,channel,0xBF,(RM195_otptime==0x01) ? 0x80F7 : 0x08F8);//CGM_PGM_EN
				}	
				
				//zangqiang 20171120
				else if(buffer[0]==RM67298)
				{
						SSD2828_W_Reg(SigMode,channel,0xBF,(RM195_otptime==0x01) ? 0x08F6 : 0x08F7);//OTP_cmd2_P3_PGM_EN
				}	
				//zangqiang 20171120
				
				else
				{
						SSD2828_W_Reg(SigMode,channel,0xBF,(RM195_otptime==0x01) ? 0x38F7 : 0x0000);//OTP_cmd2_P3-5_PGM_EN
				}
		}
		
		 if(buffer[1]==0x07)
		{
				if(buffer[0]==RM67298)
				{
						SSD2828_W_Reg(SigMode,channel,0xBF,(RM195_otptime==0x01) ? 0x20F6 : 0x20F7);//SPR_PGM_EN
				}	
		}
		
		//zangqiang 20171120
			if(buffer[1]==0x17)//OTP_cmd3_ANA_PGM_EN
		{
				SSD2828_W_Reg(SigMode,channel,0xBF,0xD0FE);	//�л�ҳ
				if(buffer[0]==RM67298)
				{
						SSD2828_W_Reg(SigMode,channel,0xBF,(RM195_otptime==0x01) ? 0x01E5 : 0x01E4);//OTP_cmd3_ANA_PGM_EN
				}	
				SSD2828_W_Reg(SigMode,channel,0xBF,0x40FE);	//�л�ҳ
		}
		
		//zangqiang 20171120
		//zangqiang 20171121
			if(buffer[1]==0x18)//OTP_Page_D0
		{
				SSD2828_W_Reg(SigMode,channel,0xBF,0xD0FE);	//�л�ҳ
				if(buffer[0]==RM67298)
				{
						SSD2828_W_Reg(SigMode,channel,0xBF,(RM195_otptime==0x01) ? 0x02E5 : 0x02E4);//OTP_Page_D0
				}	
				SSD2828_W_Reg(SigMode,channel,0xBF,0x40FE);	//�л�ҳ
		}
			if(buffer[1]==0x19)//OTP_Page_E0
		{

				if(buffer[0]==RM67298)
				{
						SSD2828_W_Reg(SigMode,channel,0xBF,(RM195_otptime==0x01) ? 0x40F6 : 0x40F7);//OTP_Page_E0
				}	

		}
		
		//zangqiang 20171121
		//zangqiang 20171125
			if(buffer[1]==0x1A)//OTP_Page_10
		{
				if(buffer[0]==RM67298)
				{
						SSD2828_W_Reg(SigMode,channel,0xBF,(RM195_otptime==0x01) ? 0x80F6 : 0x80F7);//OTP_Page_10
				}	
		}
			if(buffer[1]==0x1B)//OTP_Page_12
		{
				if(buffer[0]==RM67298)
				{
						SSD2828_W_Reg(SigMode,channel,0xBF,(RM195_otptime==0x01) ? 0x01F8 : 0x01F9);//OTP_Page_12
				}	
		}
		
		//zangqiang 20171125		
		
		
		
    delay_ms(200); 
    SSD2828_W_Reg(SigMode,channel,0xBF,0x03F2);
    SSD2828_W_Reg(SigMode,channel,0xBF,0xA5F3);
    SSD2828_W_Reg(SigMode,channel,0xBF,0x5AF4);
    SSD2828_W_Reg(SigMode,channel,0xBF,0x3CF5);                        
    delay_ms(800);         // wait for >400 ms   
    if(SigMode == Mipi_Mode)  //RGB+SPI= MIPI
        SSD2828_W_Reg(SigMode,channel,0xB7,((HS_B7_Data|0x0010)&0XFFFD)|0x0080);      
    else
        SSD2828_W_Reg(SigMode,channel,0xB7,LP_B7_Data|0x0080); 
    SSD2828_W_Reg(SigMode,channel,0xC1,0x0002);
    SSD2828_W_Reg(SigMode,channel,0xC0,0x0001);
    buffer1[0] = 0x01;
    buffer1[1] = 0xEF;
    SSD2828_W_Array(SigMode,channel,buffer,0);
    if(SigMode == Mipi_Mode)  //RGB+SPI= MIPI   ��¼OTP
        SSD2828_W_Reg(SigMode,channel,0xD4,0x00FA);
    delay_ms(100);															   								
    tmp=SSD2828_R_Reg(SigMode,channel,0xFF); 
    delay_ms(5);
    SSD2828_W_Reg(SigMode,channel,0xB7,HS_B7_Data);
    if (tmp&0x02)       //����
    {
        buffer[4] = Uart_Error_Oth;
    }
    else                //��ȷ
        buffer[4] = Uart_Error_None; 
    STM2PC_RM671xx(pdev,CDC_IN_EP,buffer,buffer[2]+3);           //���� дOTP״̬  ��ok
}

void RM6719X_Read_OTP_Time(USB_OTG_CORE_HANDLE *pdev,uint8_t  ep_addr,u8 SigMode ,u8 channel,u8* buffer,u16 LP_B7_Data,u16 HS_B7_Data)
{
    u8 buffer1[3];
    u16 tmp;
    u8 RM195_otptime;
    if(SigMode != CMD_Mode)
        SSD2828_W_Reg(SigMode,channel,0xB7,(HS_B7_Data|0x0010)&0XFFFD);
    delay_ms(5);
    buffer1[0] = 0x02;
    buffer1[1] = 0xFE;
    buffer1[2] = 0x04;
    SSD2828_W_Array(SigMode,channel,buffer1,0);
    if(buffer[0]==RM67198)
    {															
        SSD2828_W_Reg(SigMode,channel,0xBF,0x40FE);
        SSD2828_W_Reg(SigMode,channel,0xBF,0x80F2); //otp_load_en	
    }															
    SSD2828_W_Reg(SigMode,channel,0xBF,0x03F6);
    
    if(SigMode == Mipi_Mode)  //RGB+SPI= MIPI
        SSD2828_W_Reg(SigMode,channel,0xB7,((HS_B7_Data|0x0010)&0XFFFD)|0x0080);      
    else
        SSD2828_W_Reg(SigMode,channel,0xB7,LP_B7_Data|0x0080); 

    delay_ms(5);
    buffer1[0] = 0x01;
    buffer1[1] = 0xEE;
    SSD2828_W_Array(SigMode,channel,buffer1,0);
    delay_ms(20);
    if(SigMode == Mipi_Mode)  //RGB+SPI= MIPI  
        SSD2828_W_Reg(SigMode,channel,0xD4,0x00FA);							
    delay_ms(5);
                                        
    tmp=SSD2828_R_Reg(SigMode,channel,0xFF); 

    if(buffer[0]==RM67198)	
    {
        if (tmp&0x10)
                RM195_otptime=4;
        else if (tmp&0x08)
                RM195_otptime=3;
        else if (tmp&0x04)
                RM195_otptime=2;
        else
        {
            buffer[0] = 0x01;
            buffer[1] = 0xED;
            SSD2828_W_Array(SigMode,channel,buffer,0);
            delay_ms(20);
            if(SigMode == Mipi_Mode)  //RGB+SPI= MIPI   
                SSD2828_W_Reg(SigMode,channel,0xD4,0x00FA);							
            delay_ms(5);
            tmp=SSD2828_R_Reg(SigMode,channel,0xFF); 												
            if (tmp&0x02)
                RM195_otptime=1;																	
        }
    }
    else
    {
        if (tmp&0x10)
            RM195_otptime=3;
        else if (tmp&0x08)
            RM195_otptime=2;
        else
        {
            SSD2828_W_Reg(SigMode,channel,0xBC,0x0001);
            SSD2828_W_Cmd(SigMode,channel,0xBF);
            SSD2828_W_Data(SigMode,channel,0xED);
            if(SigMode == Mipi_Mode)  //RGB+SPI= MIPI   
                SSD2828_W_Reg(SigMode,channel,0xD4,0x00FA);							
            delay_ms(50);
            tmp=SSD2828_R_Reg(SigMode,channel,0xFF); 												
            if (tmp&0x02)
                RM195_otptime=1;
        }		
    }
                                    
    buffer[4]=RM195_otptime;
    RM195_otptime=0x00;
    delay_ms(5);
    SSD2828_W_Reg(SigMode,channel,0xB7,HS_B7_Data);
    buffer[3] = Uart_Error_None;
    STM2PC_RM671xx(pdev,CDC_IN_EP,buffer,buffer[2]+3);           //���� ��ȡ��OTP����  ��ok  
}



/********************************************************************************************
********************************************************************************************
    RM69300  д�Ĵ��� ���Ĵ���  дGamma ��Gamma OTP��¼
    RM69300  д�Ĵ��� ���Ĵ���  дGamma ��Gamma OTP��¼
    RM69300  д�Ĵ��� ���Ĵ���  дGamma ��Gamma OTP��¼
    RM69300  д�Ĵ��� ���Ĵ���  дGamma ��Gamma OTP��¼
    RM69300  д�Ĵ��� ���Ĵ���  дGamma ��Gamma OTP��¼
********************************************************************************************
********************************************************************************************/
void RM693xx_Write_Register(USB_OTG_CORE_HANDLE *pdev,uint8_t  ep_addr,u8 SigMode ,u8 channel,u8* buffer,u16 LP_B7_Data,u16 HS_B7_Data)
{
    SSD2828_W_Reg(SigMode,channel,0xB7,0x035b);
		//SSD2828_W_Reg(SigMode,channel,0xB7,LP_B7_Data);
    delay_ms(5); 
    SSD2828_W_Array(SigMode,channel,buffer,2);
    SSD2828_W_Reg(SigMode,channel,0xB7,HS_B7_Data);
    delay_ms(5);
    buffer[4] = Uart_Error_None;
    STM2PC_RM671xx(pdev,CDC_IN_EP,buffer,buffer[2]+3);           //���� д�Ĵ���״̬  ��ok
}

void RM693xx_Read_Register(USB_OTG_CORE_HANDLE *pdev,uint8_t  ep_addr,u8 SigMode ,u8 channel,u8* buffer,u16 LP_B7_Data,u16 HS_B7_Data)
{
    u8 buffer1[2];
    SSD2828_W_Reg(SigMode,channel,0xB7,((HS_B7_Data|0x0010)&0xFFFD)|0x0080);  
    delay_ms(5);
    SSD2828_W_Reg(SigMode,channel,0xC1,0x0002);                                
    buffer1[0] = 0x01;
    buffer1[1] = buffer[3];
    SSD2828_W_Array(SigMode,channel,buffer1,0); 
    delay_ms(20);
    buffer[4]=SSD2828_R_Reg(SigMode,channel,0xFF);
    SSD2828_W_Reg(SigMode,channel,0xB7,HS_B7_Data);
    delay_ms(5);
    STM2PC_RM671xx(pdev,CDC_IN_EP,buffer,buffer[2]+3);           //���� ��ȡ�� �Ĵ�������  ��ok  
}

void RM693xx_Write_Gamma(USB_OTG_CORE_HANDLE *pdev,uint8_t  ep_addr,u8 SigMode ,u8 channel,u8* buffer,u16 LP_B7_Data,u16 HS_B7_Data)
{
    u8 buffer1[5];
    //SSD2828_W_Reg(SigMode,channel,0xB7,(HS_B7_Data|0x0010)&0XFFfd);
		SSD2828_W_Reg(SigMode,channel,0xB7,0x035b);  //09->0b ,�������������
		delay_ms(5);

    if((buffer[3]&0x03) == 0x00)
    {
        buffer1[0] = 0x02;
        buffer1[1] = 0xFE;
        buffer1[2] = 0x00;
        SSD2828_W_Array(SigMode,channel,buffer1,0);
				delay_ms(5);
        buffer1[0] = 0x02;
        buffer1[1] = 0x38;
        buffer1[2] = 0x00;
        SSD2828_W_Array(SigMode,channel,buffer1,0); 
				delay_ms(5);
        buffer1[0] = 0x02;
        buffer1[1] = 0x53;
        buffer1[2] = 0x20;
        SSD2828_W_Array(SigMode,channel,buffer1,0); 
				delay_ms(5);
        buffer1[0] = 0x02;
        buffer1[1] = 0xFE;
        buffer1[2] = 0x05;              // GAMMA1  page1
        SSD2828_W_Array(SigMode,channel,buffer1,0);
    }
    else if((buffer[3]&0x03) == 0x01)
    {
        buffer1[0] = 0x02;
        buffer1[1] = 0xFE;
        buffer1[2] = 0x00;
        SSD2828_W_Array(SigMode,channel,buffer1,0);
				delay_ms(5);
        buffer1[0] = 0x02;
        buffer1[1] = 0x53;
        buffer1[2] = 0x20;
        SSD2828_W_Array(SigMode,channel,buffer1,0);  
				delay_ms(5);
        buffer1[0] = 0x02;
        buffer1[1] = 0x39;
        buffer1[2] = 0x00;
        SSD2828_W_Array(SigMode,channel,buffer1,0);   
				delay_ms(5);
        buffer1[0] = 0x02;
        buffer1[1] = 0xFE;
        buffer1[2] = 0x02;              // GAMMA2  page7
        SSD2828_W_Array(SigMode,channel,buffer1,0);
    }   
    else if((buffer[3]&0x03) == 0x02)
    {
        buffer1[0] = 0x02;
        buffer1[1] = 0xFE;
        buffer1[2] = 0x00;
        SSD2828_W_Array(SigMode,channel,buffer1,0);
				delay_ms(5);
        buffer1[0] = 0x02;
        buffer1[1] = 0x38;
        buffer1[2] = 0x00;
        SSD2828_W_Array(SigMode,channel,buffer1,0); 
				delay_ms(5);
        buffer1[0] = 0x02;
        buffer1[1] = 0x53;
        buffer1[2] = 0x60;
        SSD2828_W_Array(SigMode,channel,buffer1,0); 
				delay_ms(5);
        buffer1[0] = 0x02;
        buffer1[1] = 0xFE;
        buffer1[2] = 0x03;              // GAMMA3  page8
        SSD2828_W_Array(SigMode,channel,buffer1,0);
    }
		else
		{
				buffer[4] = Uart_Error_Oth;
				STM2PC_RM671xx(pdev,CDC_IN_EP,buffer,buffer[2]+3);           //���� дGamma״̬  ��ok  				
				goto RM69300_w_end;
		}
    buffer1[0] = 0x02;
    buffer1[1] = 0xEF;
    buffer1[2] = 0x01;   
		SSD2828_W_Array(SigMode,channel,buffer1,0);
		delay_ms(10);
    for( i = 0;i<buffer[2]-1;i++) 
    {	 
        if((buffer[3]&0xf0)==0x00)	
            SSD2828_W_Reg(SigMode,channel,0xBF,(u16)(buffer[i+4]<<8)|RM300_Gamma_r[i]);  
        else if((buffer[3]&0xf0)==0x40)
            SSD2828_W_Reg(SigMode,channel,0xBF,(u16)(buffer[i+4]<<8)|RM300_Gamma_g[i]); 
        else if((buffer[3]&0xf0)==0x80)
            SSD2828_W_Reg(SigMode,channel,0xBF,(u16)(buffer[i+4]<<8)|RM300_Gamma_b[i]); 																		

        delay_ms(3);
    }
    buffer1[0] = 0x02;
    buffer1[1] = 0xF0;
    buffer1[2] = 0x01;    
	  SSD2828_W_Array(SigMode,channel,buffer1,0);
    delay_ms(2);
		buffer1[0] = 0x02;
    buffer1[1] = 0xEF;
    buffer1[2] = 0x00;    
		SSD2828_W_Array(SigMode,channel,buffer1,0);    

    delay_ms(2);
    SSD2828_W_Reg(SigMode,channel,0xB7,HS_B7_Data);
    buffer[4] = Uart_Error_None;
    STM2PC_RM671xx(pdev,CDC_IN_EP,buffer,buffer[2]+3);           //���� дGamma״̬  ��ok  
		RM69300_w_end: ;
}


void RM693xx_Read_Gamma(USB_OTG_CORE_HANDLE *pdev,uint8_t  ep_addr,u8 SigMode ,u8 channel,u8* buffer,u16 LP_B7_Data,u16 HS_B7_Data)
{
    u8 buffer1[3];
		SSD2828_W_Reg(SigMode,channel,0xB7,LP_B7_Data);
		delay_ms(15);
    if(buffer[4] == 0x00)
    {
        buffer1[0] = 0x02;
        buffer1[1] = 0xFE;
        buffer1[2] = 0x00;
        SSD2828_W_Array(SigMode,channel,buffer1,0);
        buffer1[0] = 0x02;
        buffer1[1] = 0x38;
        buffer1[2] = 0x00;
        SSD2828_W_Array(SigMode,channel,buffer1,0); 

        buffer1[0] = 0x02;
        buffer1[1] = 0x53;
        buffer1[2] = 0x20;
        SSD2828_W_Array(SigMode,channel,buffer1,0);          

        buffer1[0] = 0x02;
        buffer1[1] = 0xFE;
        buffer1[2] = 0x05;      // GAMMA1  page1
        SSD2828_W_Array(SigMode,channel,buffer1,0); 
    }
    else if(buffer[4] == 0x01)
    {
        buffer1[0] = 0x02;
        buffer1[1] = 0xFE;
        buffer1[2] = 0x00;
        SSD2828_W_Array(SigMode,channel,buffer1,0);
        buffer1[0] = 0x02;
        buffer1[1] = 0x53;
        buffer1[2] = 0x20;
        SSD2828_W_Array(SigMode,channel,buffer1,0);  

        buffer1[0] = 0x02;
        buffer1[1] = 0x39;
        buffer1[2] = 0x00;
        SSD2828_W_Array(SigMode,channel,buffer1,0);   

        buffer1[0] = 0x02;
        buffer1[1] = 0xFE;       
        buffer1[2] = 0x02;      // GAMMA2  page7
        SSD2828_W_Array(SigMode,channel,buffer1,0); 
    }
    else if(buffer[4] == 0x02)
    {
        buffer1[0] = 0x02;
        buffer1[1] = 0xFE;
        buffer1[2] = 0x00;
        SSD2828_W_Array(SigMode,channel,buffer1,0);	
        buffer1[0] = 0x02;
        buffer1[1] = 0x38;
        buffer1[2] = 0x00;
        SSD2828_W_Array(SigMode,channel,buffer1,0);  
        buffer1[0] = 0x02;
        buffer1[1] = 0x53;
        buffer1[2] = 0x60;
        SSD2828_W_Array(SigMode,channel,buffer1,0);         
        buffer1[0] = 0x02;
        buffer1[1] = 0xFE;        
        buffer1[2] = 0x03;      // GAMMA3  page8
        SSD2828_W_Array(SigMode,channel,buffer1,0); 
    }
		else
		{
				buffer[4] = Uart_Error_Oth;
				STM2PC_RM671xx(pdev,CDC_IN_EP,buffer,buffer[2]+3);           //���� дGamma״̬  ��ok  				
				goto RM69300_r_end;
		}		
		delay_ms(15);	
    buffer1[0] = 0x02;
    buffer1[1] = 0xEF;
    buffer1[2] = 0x01;   
		SSD2828_W_Array(SigMode,channel,buffer1,0);		
    delay_ms(5);		
    SSD2828_W_Reg(SigMode,channel,0xB7,LP_B7_Data|0x0080); 
    SSD2828_W_Reg(SigMode,channel,0xBC,0x0001);	
    for( i = 0;i<buffer[2]-1;i++)			 	
    {  	
        
        SSD2828_W_Reg(SigMode,channel,0xC1,0x0002);                                
        buffer1[0] = 0x01;
        buffer1[1] = (buffer[3]==0x00) ? RM300_Gamma_r[i] : ((buffer[3]==0x42) ? RM300_Gamma_g[i] : RM300_Gamma_b[i]);
        SSD2828_W_Array(SigMode,channel,buffer1,0); 
				delay_ms(6);
        buffer[4+i]=SSD2828_R_Reg(SigMode,channel,0xFF);        
    }
    buffer1[0] = 0x02;
    buffer1[1] = 0xEF;
    buffer1[2] = 0x00;   
		SSD2828_W_Array(SigMode,channel,buffer1,0);		

    delay_ms(5);
    SSD2828_W_Reg(SigMode,channel,0xB7,HS_B7_Data);
    STM2PC_RM671xx(pdev,CDC_IN_EP,buffer,buffer[2]+3);           //���� ��ȡ�� Gamma����  ��ok  
		RM69300_r_end: ;
}

void RM693xx_Gamma_OTP_Start(USB_OTG_CORE_HANDLE *pdev,uint8_t  ep_addr,u8 SigMode ,u8 channel,u8* buffer,u16 LP_B7_Data,u16 HS_B7_Data,u8 RM3xx_otptime)
{
    u16 tmp;
    u8 buffer1[3];  
    SSD2828_W_Reg(SigMode,channel,0xB7,(HS_B7_Data|0x0010)&0XFFFD);   
    delay_ms(5);
    buffer1[0] = 0x02;
    buffer1[1] = 0xFE;
    buffer1[2] = 0x00;
    SSD2828_W_Array(SigMode,channel,buffer1,0);     // UCS
    delay_ms(5);
    buffer1[0] = 0x01;
    buffer1[1] = 0x28;
    SSD2828_W_Array(SigMode,channel,buffer1,0);     // ����ʾ
    delay_ms(100);
    buffer1[0] = 0x01;
    buffer1[1] = 0x10;
    SSD2828_W_Array(SigMode,channel,buffer1,0);     // ����ʾ     
    delay_ms(100);  //otp
    buffer1[0] = 0x02;
    buffer1[1] = 0xFE;
    buffer1[2] = 0x04;
    SSD2828_W_Array(SigMode,channel,buffer1,0);     //Page0
    
    if(buffer[1]==0x0A)  //otp GammaSet1
    {
        buffer1[0] = 0x02;
        if(RM3xx_otptime == 1)      // 1st
        {
            buffer1[1] = 0xF7;
            buffer1[2] = 0x02;
        }
        else if(RM3xx_otptime == 2) // 2nd
        {
            buffer1[1] = 0xF9;
            buffer1[2] = 0x02;        
        }
        SSD2828_W_Array(SigMode,channel,buffer1,0);     //OTP_CMD2_P1_PGM_EN          
    }
		if(buffer[1]==0x07)
		{
				if(buffer[0]==RM69300)
				{
						SSD2828_W_Reg(SigMode,channel,0xBF,(RM3xx_otptime==0x01) ? 0x20F6 : 0x20F7);//spr
				}	
		}
    if(buffer[1]==0x0D)  //otp GammaSet2
    {
        buffer1[0] = 0x02;
        if(RM3xx_otptime == 1)      // 1st
        {
            buffer1[1] = 0xF7;
            buffer1[2] = 0x80;
        }
        else if(RM3xx_otptime == 2) // 2nd
        {
            buffer1[1] = 0xF9;
            buffer1[2] = 0x08;        
        }
        SSD2828_W_Array(SigMode,channel,buffer1,0);     //OTP_CMD2_P7_PGM_EN  
    }
    if(buffer[1]==0x0E) //otp GammaSet3
    {		
        buffer1[0] = 0x02;
        if(RM3xx_otptime == 1)      // 1st
        {
            buffer1[1] = 0xF8;
            buffer1[2] = 0x01;
        }
        else if(RM3xx_otptime == 2) // 2nd
        {
            buffer1[1] = 0xF9;
            buffer1[2] = 0x10;        
        }        
        SSD2828_W_Array(SigMode,channel,buffer1,0);     //OTP_CMD2_P8_PGM_EN  
    }
    if(buffer[1]==0x0F) // otp goa
    {
        buffer1[0] = 0x02;
        if(RM3xx_otptime == 1)      // 1st
        {
            buffer1[1] = 0xF7;
            buffer1[2] = 0x04;;
        }
        else if(RM3xx_otptime == 2) // 2nd
        {
            buffer1[1] = 0xF9;
            buffer1[2] = 0x04;        
        }
        else if(RM3xx_otptime == 3) // 3rd
        {
            buffer1[1] = 0xF9;
            buffer1[2] = 0x40;        
        }
        SSD2828_W_Array(SigMode,channel,buffer1,0);     //OTP_CMD2_P2_PGM_EN  
    }
    if(buffer[1]==0x10) // otp power
    {
        buffer1[0] = 0x02;        
        if(RM3xx_otptime == 1)      // 1st
        {
            buffer1[1] = 0xF7;
            buffer1[2] = 0x01;
        }
        else if(RM3xx_otptime == 2) // 2nd
        {
            buffer1[1] = 0xF9;
            buffer1[2] = 0x01;        
        }
        else if(RM3xx_otptime == 3) // 3rd
        {
            buffer1[1] = 0xF9;
            buffer1[2] = 0x80;        
        }        
        SSD2828_W_Array(SigMode,channel,buffer1,0);     //OTP_CMD2_P0_PGM_EN  
    }    

    buffer1[0] = 0x02;
    buffer1[1] = 0xF2;
    buffer1[2] = 0x03;
    SSD2828_W_Array(SigMode,channel,buffer1,0);    
	
    buffer1[0] = 0x02;
    buffer1[1] = 0xF3;
    buffer1[2] = 0xA5;
    SSD2828_W_Array(SigMode,channel,buffer1,0);  

    buffer1[0] = 0x02;
    buffer1[1] = 0xF4;
    buffer1[2] = 0x5A;
    SSD2828_W_Array(SigMode,channel,buffer1,0);  

    buffer1[0] = 0x02;
    buffer1[1] = 0xF5;
    buffer1[2] = 0x3C;
    SSD2828_W_Array(SigMode,channel,buffer1,0);  		
                      
    delay_ms(1500);         // wait for >400 ms   

    buffer[4] = Uart_Error_Oth;
    STM2PC_RM671xx(pdev,CDC_IN_EP,buffer,buffer[2]+3);           //���� дOTP״̬  ��ok
}

void RM693xx_Read_OTP_Time(USB_OTG_CORE_HANDLE *pdev,uint8_t  ep_addr,u8 SigMode ,u8 channel,u8* buffer,u16 LP_B7_Data,u16 HS_B7_Data)
{
    u8 buffer1[3];
    u16 ED_D,EE_D,EF_D;
    u8 RM300_otptime = 0;
    if(SigMode != CMD_Mode)
        SSD2828_W_Reg(SigMode,channel,0xB7,(HS_B7_Data|0x0010)&0XFFFD);
    delay_ms(5);
    buffer1[0] = 0x02;
    buffer1[1] = 0xFE;
    buffer1[2] = 0x04;
    SSD2828_W_Array(SigMode,channel,buffer1,0);     // Page 0
		
    buffer1[0] = 0x02;
    buffer1[1] = 0xF2;
    buffer1[2] = 0x80;
    SSD2828_W_Array(SigMode,channel,buffer1,0);     // otp_load_en 
		
    buffer1[0] = 0x02;
    buffer1[1] = 0xF6;
    buffer1[2] = 0x03;
    SSD2828_W_Array(SigMode,channel,buffer1,0);     // Margin Read Check      
    SSD2828_W_Reg(SigMode,channel,0xB7,((HS_B7_Data|0x0010)&0XFFFD)|0x0080);      

    delay_ms(5);
    buffer1[0] = 0x01;
    buffer1[1] = 0xED;
    SSD2828_W_Array(SigMode,channel,buffer1,0);
    delay_ms(20);
    if(SigMode == Mipi_Mode)  //RGB+SPI= MIPI  
        SSD2828_W_Reg(SigMode,channel,0xD4,0x00FA);							
    delay_ms(5);                                  
    ED_D=SSD2828_R_Reg(SigMode,channel,0xFF); 
    
    
    buffer1[0] = 0x01;
    buffer1[1] = 0xEE;
    SSD2828_W_Array(SigMode,channel,buffer1,0);
    delay_ms(20);
    if(SigMode == Mipi_Mode)  //RGB+SPI= MIPI  
        SSD2828_W_Reg(SigMode,channel,0xD4,0x00FA);							
    delay_ms(5);                             
    EE_D=SSD2828_R_Reg(SigMode,channel,0xFF); 
    
    buffer1[0] = 0x01;
    buffer1[1] = 0xEF;
    SSD2828_W_Array(SigMode,channel,buffer1,0);
    delay_ms(20);
    if(SigMode == Mipi_Mode)  //RGB+SPI= MIPI  
        SSD2828_W_Reg(SigMode,channel,0xD4,0x00FA);							
    delay_ms(5);                               
    EF_D=SSD2828_R_Reg(SigMode,channel,0xFF);     
    if(buffer[3] == 0x00)       //��ȡgamma1����
    {
        if(ED_D&0X02)
            RM300_otptime = 1;
        if(EE_D&0X10)
            RM300_otptime = 2;
    }
    else if(buffer[3] == 0x01)  //��ȡgamma2����
    {
        if(ED_D&0X02)
            RM300_otptime = 1; 
        if(EE_D&0X40)
            RM300_otptime = 2;        
    }
    else if(buffer[3] == 0x02)  //��ȡgamma3����
    {
        if(EE_D&0X01)
            RM300_otptime = 1;
        if(EE_D&0X80)
            RM300_otptime = 2;        
    }
    else if(buffer[3] == 0x03)  //��ȡGOA����
    {
        if(ED_D&0X04)
            RM300_otptime = 1;
        if(EE_D&0X20)
            RM300_otptime = 2;  
        if(EF_D&0X80)
            RM300_otptime = 3;
        
    }
    else if(buffer[3] == 0x04)  //��ȡPOWER����
    {
        if(ED_D&0X01)
            RM300_otptime = 1;
        if(EE_D&0X08)
            RM300_otptime = 2;  
        if(EF_D&0X40)
            RM300_otptime = 3;   
    }

                                    
    buffer[4]=RM300_otptime;
    RM300_otptime=0x00;
    delay_ms(5);
    SSD2828_W_Reg(SigMode,channel,0xB7,HS_B7_Data);
    buffer[3] = Uart_Error_None;
    STM2PC_RM671xx(pdev,CDC_IN_EP,buffer,buffer[2]+3);           //���� ��ȡ��OTP����  ��ok  
}

void RM693xx_Gamma_switch(USB_OTG_CORE_HANDLE *pdev,uint8_t  ep_addr,u8 SigMode ,u8 channel,u8* buffer,u16 LP_B7_Data,u16 HS_B7_Data)
{
    u8 buffer1[5];
    static u8 RM69300_state = 0;    // 0=normal   1=idle  2=HBM
    SSD2828_W_Reg(SigMode,channel,0xB7,(HS_B7_Data|0x0010)&0XFFFD);
    delay_ms(10);
    if(buffer[3] == 0x01)
    {
        buffer1[0] = 0x02;
        buffer1[1] = 0xFE;
        buffer1[2] = 0x00;
        SSD2828_W_Array(SigMode,channel,buffer1,0);
        if(RM69300_state == 2)
        {
            buffer1[0] = 0x02;
            buffer1[1] = 0x53;
            buffer1[2] = 0x20;         //Brightness control is on
            SSD2828_W_Array(SigMode,channel,buffer1,0);
        }
        else if(RM69300_state == 1)
        {
            buffer1[0] = 0x02;
            buffer1[1] = 0x38;
            buffer1[2] = 0x00;         //Exit idle 
            SSD2828_W_Array(SigMode,channel,buffer1,0);  
        }  
        
        buffer1[0] = 0x02;
        buffer1[1] = 0xFE;
        buffer1[2] = 0x05;
        SSD2828_W_Array(SigMode,channel,buffer1,0);
        RM69300_state = 0;
    }
    else if(buffer[3] == 0x02)
    {
        buffer1[0] = 0x02;
        buffer1[1] = 0xFE;
        buffer1[2] = 0x00;
        SSD2828_W_Array(SigMode,channel,buffer1,0);
        if(RM69300_state == 2)
        {
            buffer1[0] = 0x02;
            buffer1[1] = 0x53;
            buffer1[2] = 0x20;
            SSD2828_W_Array(SigMode,channel,buffer1,0);
        }
        buffer1[0] = 0x02;
        buffer1[1] = 0x39;
        buffer1[2] = 0x00;            //Enter idle
        SSD2828_W_Array(SigMode,channel,buffer1,0);
        buffer1[0] = 0x02;
        buffer1[1] = 0xFE;
        buffer1[2] = 0x02;
        SSD2828_W_Array(SigMode,channel,buffer1,0);
        RM69300_state = 1;
       
    }   
    else if(buffer[3] == 0x03)
    {
        buffer1[0] = 0x02;
        buffer1[1] = 0xFE;
        buffer1[2] = 0x00;
        SSD2828_W_Array(SigMode,channel,buffer1,0);
        if(RM69300_state == 1)
        {
            buffer1[0] = 0x02;
            buffer1[1] = 0x38;
            buffer1[2] = 0x00;
            SSD2828_W_Array(SigMode,channel,buffer1,0);
        }
        buffer1[0] = 0x02;
        buffer1[1] = 0x53;
        buffer1[2] = 0x60;
        SSD2828_W_Array(SigMode,channel,buffer1,0);
        buffer1[0] = 0x02;
        buffer1[1] = 0xFE;
        buffer1[2] = 0x03;
        SSD2828_W_Array(SigMode,channel,buffer1,0);
        RM69300_state = 2;
    }
    
    delay_ms(50);
    buffer1[0] = 0x02;
    buffer1[1] = 0xEF;
    buffer1[2] = 0x01;
    SSD2828_W_Array(SigMode,channel,buffer1,0);
    delay_ms(50);    
    if(buffer[4] == 0x01) 
    {
        buffer1[0] = 0x02;
        buffer1[1] = 0xBC;
        buffer1[2] = 0x00;                  // 28���
        SSD2828_W_Array(SigMode,channel,buffer1,0);
    }
    else if(buffer[4] == 0x02)
    {
    
        buffer1[0] = 0x02;
        buffer1[1] = 0xBC;
        buffer1[2] = 0x01;                  // 15���
        SSD2828_W_Array(SigMode,channel,buffer1,0);
    }
		delay_ms(50);
		buffer1[0] = 0x02;
		buffer1[1] = 0xF0;
		buffer1[2] = 0x01;    
		SSD2828_W_Array(SigMode,channel,buffer1,0);
		buffer1[0] = 0x02;
		buffer1[1] = 0xEF;
		buffer1[2] = 0x00;    
		SSD2828_W_Array(SigMode,channel,buffer1,0);     
		delay_ms(5);
		SSD2828_W_Reg(SigMode,channel,0xB7,HS_B7_Data);
		buffer[4] = Uart_Error_None;
		STM2PC_RM671xx(pdev,CDC_IN_EP,buffer,buffer[2]+3);           //���� дGamma״̬  ��ok  
}

void RM693xx_Write_51Register(USB_OTG_CORE_HANDLE *pdev,uint8_t  ep_addr,u8 SigMode ,u8 channel,u8* buffer,u16 LP_B7_Data,u16 HS_B7_Data)
{
    SSD2828_W_Reg(SigMode,channel,0xB7,(HS_B7_Data|0x0010)&0XFFFD);
    SSD2828_W_Array(SigMode,channel,buffer,2);
    SSD2828_W_Reg(SigMode,channel,0xB7,HS_B7_Data);
}

/********************************************************************************************
********************************************************************************************
    RM69350  д�Ĵ��� ���Ĵ���  дGamma ��Gamma OTP��¼
    RM69350  д�Ĵ��� ���Ĵ���  дGamma ��Gamma OTP��¼
    RM69350  д�Ĵ��� ���Ĵ���  дGamma ��Gamma OTP��¼
    RM69350  д�Ĵ��� ���Ĵ���  дGamma ��Gamma OTP��¼
    RM69350  д�Ĵ��� ���Ĵ���  дGamma ��Gamma OTP��¼
********************************************************************************************
********************************************************************************************/
void RM6935x_Write_Register(USB_OTG_CORE_HANDLE *pdev,uint8_t  ep_addr,u8 SigMode ,u8 channel,u8* buffer,u16 LP_B7_Data,u16 HS_B7_Data)
{
    SSD2828_W_Reg(SigMode,channel,0xB7,(HS_B7_Data|0x0010)&0XFFfd);
		//SSD2828_W_Reg(SigMode,channel,0xB7,LP_B7_Data);
    delay_ms(15); 
    SSD2828_W_Array(SigMode,channel,buffer,2);
    SSD2828_W_Reg(SigMode,channel,0xB7,HS_B7_Data);
    delay_ms(5);
    buffer[4] = Uart_Error_None;
    STM2PC_RM671xx(pdev,CDC_IN_EP,buffer,buffer[2]+3);           //���� д�Ĵ���״̬  ��ok
}

void RM6935x_Read_Register(USB_OTG_CORE_HANDLE *pdev,uint8_t  ep_addr,u8 SigMode ,u8 channel,u8* buffer,u16 LP_B7_Data,u16 HS_B7_Data)
{
    u8 buffer1[2];
		SSD2828_W_Reg(SigMode,channel,0xB7,((HS_B7_Data|0x0010)&0XFFFD)|0x0080);  //bit7 1:������ bit4 1��HS CLOCK Disable
    delay_ms(5);
    SSD2828_W_Reg(SigMode,channel,0xC1,0x0002);                                
    buffer1[0] = 0x01;
    buffer1[1] = buffer[3];
    SSD2828_W_Array(SigMode,channel,buffer1,0); 
    delay_ms(20);
    buffer[4]=SSD2828_R_Reg(SigMode,channel,0xFF);
    SSD2828_W_Reg(SigMode,channel,0xB7,HS_B7_Data);
    delay_ms(5);
    STM2PC_RM671xx(pdev,CDC_IN_EP,buffer,buffer[2]+3);           //���� ��ȡ�� �Ĵ�������  ��ok  
}

void RM6935x_Write_Gamma(USB_OTG_CORE_HANDLE *pdev,uint8_t  ep_addr,u8 SigMode ,u8 channel,u8* buffer,u16 LP_B7_Data,u16 HS_B7_Data)
{
    u8 buffer1[5];
    SSD2828_W_Reg(SigMode,channel,0xB7,(HS_B7_Data|0x0010)&0XFFfd);
		delay_ms(5);

    if((buffer[3]&0x03) == 0x00)
    {
        buffer1[0] = 0x02;
        buffer1[1] = 0xFE;
        buffer1[2] = 0x00;
        SSD2828_W_Array(SigMode,channel,buffer1,0);     //UCS 
        buffer1[0] = 0x02;
        buffer1[1] = 0x38;
        buffer1[2] = 0x00;                             //exit Idle Mode
        SSD2828_W_Array(SigMode,channel,buffer1,0); 
        buffer1[0] = 0x02;
        buffer1[1] = 0x53;
        buffer1[2] = 0x20;                              //select HBM and Enable BC Control
        SSD2828_W_Array(SigMode,channel,buffer1,0); 
        buffer1[0] = 0x02;
        buffer1[1] = 0xFE;
        buffer1[2] = 0x50;                // GAMMA1  page1
        SSD2828_W_Array(SigMode,channel,buffer1,0);
    }
	
    else if((buffer[3]&0x03) == 0x01)
    {
        buffer1[0] = 0x02;
        buffer1[1] = 0xFE;
        buffer1[2] = 0x00;
        SSD2828_W_Array(SigMode,channel,buffer1,0);
        buffer1[0] = 0x02;
        buffer1[1] = 0x53;
        buffer1[2] = 0x20;                           //Select HBM and Enable BC Control
        SSD2828_W_Array(SigMode,channel,buffer1,0);  
        buffer1[0] = 0x02;
        buffer1[1] = 0x39;
        buffer1[2] = 0x00;                          //enter Idle Mode
        SSD2828_W_Array(SigMode,channel,buffer1,0);   
        buffer1[0] = 0x02;
        buffer1[1] = 0xFE;
        buffer1[2] = 0x52;                // GAMMA2  page7
        SSD2828_W_Array(SigMode,channel,buffer1,0);
    }   
    else if((buffer[3]&0x03) == 0x02)
    {
        buffer1[0] = 0x02;
        buffer1[1] = 0xFE;
        buffer1[2] = 0x00;
        SSD2828_W_Array(SigMode,channel,buffer1,0);
        buffer1[0] = 0x02;
        buffer1[1] = 0x38;
        buffer1[2] = 0x00;
        SSD2828_W_Array(SigMode,channel,buffer1,0); 
        buffer1[0] = 0x02;
        buffer1[1] = 0x53;
        buffer1[2] = 0x60;
        SSD2828_W_Array(SigMode,channel,buffer1,0); 
			  buffer1[0] = 0x02;
        buffer1[1] = 0xFE;
        buffer1[2] = 0x40;
        SSD2828_W_Array(SigMode,channel,buffer1,0); 
			  buffer1[0] = 0x02;
        buffer1[1] = 0x6B;
        buffer1[2] = 0x00;
        SSD2828_W_Array(SigMode,channel,buffer1,0); 
        buffer1[0] = 0x02;
        buffer1[1] = 0xFE;
        buffer1[2] = 0x54;                // GAMMA3  page8
        SSD2828_W_Array(SigMode,channel,buffer1,0);
    }
		else
		{
				buffer[4] = Uart_Error_Oth;
				STM2PC_RM671xx(pdev,CDC_IN_EP,buffer,buffer[2]+3);           //���� дGamma״̬  ��ok  				
				goto RM69350_w_end;
		}

    for( i = 0;i<buffer[2]-1;i++) 
    {	 
        if((buffer[3]&0xf0)==0x00)	
            SSD2828_W_Reg(SigMode,channel,0xBF,(u16)(buffer[i+4]<<8)|RM350_Gamma_r[i]);  
        else if((buffer[3]&0xf0)==0x40)
            SSD2828_W_Reg(SigMode,channel,0xBF,(u16)(buffer[i+4]<<8)|RM350_Gamma_g[i]); 
        else if((buffer[3]&0xf0)==0x80)
            SSD2828_W_Reg(SigMode,channel,0xBF,(u16)(buffer[i+4]<<8)|RM350_Gamma_b[i]); 																		

        delay_ms(3);
    }

    SSD2828_W_Reg(SigMode,channel,0xB7,HS_B7_Data);
    buffer[4] = Uart_Error_None;
    STM2PC_RM671xx(pdev,CDC_IN_EP,buffer,buffer[2]+3);           //���� дGamma״̬  ��ok  
		RM69350_w_end: ;
}

void RM6935x_Read_Gamma(USB_OTG_CORE_HANDLE *pdev,uint8_t  ep_addr,u8 SigMode ,u8 channel,u8* buffer,u16 LP_B7_Data,u16 HS_B7_Data)
{
    u8 buffer1[3];
		SSD2828_W_Reg(SigMode,channel,0xB7,LP_B7_Data);
		delay_ms(15);
		buffer1[0] = 0x02;
        
    if(buffer[4] == 0x00)      //Gamma1
    {
        buffer1[0] = 0x02;
        buffer1[1] = 0xFE;
        buffer1[2] = 0x00;
        SSD2828_W_Array(SigMode,channel,buffer1,0);
        buffer1[0] = 0x02;
        buffer1[1] = 0x38;
        buffer1[2] = 0x00;
        SSD2828_W_Array(SigMode,channel,buffer1,0); 

        buffer1[0] = 0x02;
        buffer1[1] = 0x53;
        buffer1[2] = 0x20;
        SSD2828_W_Array(SigMode,channel,buffer1,0);          

        buffer1[0] = 0x02;
        buffer1[1] = 0xFE;
			  buffer1[2] = 0x50;      // GAMMA1  page1
        SSD2828_W_Array(SigMode,channel,buffer1,0); 
    }
    else if(buffer[4] == 0x01) //Gamma2
    {
        buffer1[0] = 0x02;
        buffer1[1] = 0xFE;
        buffer1[2] = 0x00;
        SSD2828_W_Array(SigMode,channel,buffer1,0);
        buffer1[0] = 0x02;
        buffer1[1] = 0x53;
        buffer1[2] = 0x20;
        SSD2828_W_Array(SigMode,channel,buffer1,0);  

        buffer1[0] = 0x02;
        buffer1[1] = 0x39;
        buffer1[2] = 0x00;
        SSD2828_W_Array(SigMode,channel,buffer1,0);  

        buffer1[0] = 0x02;
        buffer1[1] = 0xFE;       
        buffer1[2] = 0x52;      // GAMMA2  page7
        SSD2828_W_Array(SigMode,channel,buffer1,0); 
    }
    else if(buffer[4] == 0x02) //Gamma3
    {
        buffer1[0] = 0x02;
        buffer1[1] = 0xFE;
        buffer1[2] = 0x00;
        SSD2828_W_Array(SigMode,channel,buffer1,0);	
        buffer1[0] = 0x02;
        buffer1[1] = 0x38;
        buffer1[2] = 0x00;
        SSD2828_W_Array(SigMode,channel,buffer1,0);  
        buffer1[0] = 0x02;
        buffer1[1] = 0x53;
        buffer1[2] = 0x60;
        SSD2828_W_Array(SigMode,channel,buffer1,0);         
        buffer1[0] = 0x02;
        buffer1[1] = 0xFE;        
        buffer1[2] = 0x54;      // GAMMA3  page8
        SSD2828_W_Array(SigMode,channel,buffer1,0); 
    }
		else
		{
				buffer[4] = Uart_Error_Oth;
				STM2PC_RM671xx(pdev,CDC_IN_EP,buffer,buffer[2]+3);           //���� дGamma״̬  ��ok  				
    		goto RM69350_r_end;
		}		
		delay_ms(15);		
    SSD2828_W_Reg(SigMode,channel,0xB7,LP_B7_Data|0x0080); 
    SSD2828_W_Reg(SigMode,channel,0xBC,0x0001);	
    for( i = 0;i<buffer[2]-1;i++)			 	
    {  	
        
        SSD2828_W_Reg(SigMode,channel,0xC1,0x0002);                                
        buffer1[0] = 0x01;
        buffer1[1] = (buffer[3]==0x00) ? RM350_Gamma_r[i] : ((buffer[3]==0x42) ? RM350_Gamma_g[i] : RM350_Gamma_b[i]);
        SSD2828_W_Array(SigMode,channel,buffer1,0); 
				delay_ms(6);
        buffer[4+i]=SSD2828_R_Reg(SigMode,channel,0xFF);        
    }	
    delay_ms(5);
    SSD2828_W_Reg(SigMode,channel,0xB7,HS_B7_Data);
    STM2PC_RM671xx(pdev,CDC_IN_EP,buffer,buffer[2]+3);           //���� ��ȡ�� Gamma����  ��ok  
		RM69350_r_end: ;
}

void RM6935x_Gamma_OTP_Start(USB_OTG_CORE_HANDLE *pdev,uint8_t  ep_addr,u8 SigMode ,u8 channel,u8* buffer,u16 LP_B7_Data,u16 HS_B7_Data,u8 RM35x_otptime)
{
    u16 tmp;
    u8 buffer1[3];  
    SSD2828_W_Reg(SigMode,channel,0xB7,(HS_B7_Data|0x0010)&0XFFFD);   
    delay_ms(5);
    buffer1[0] = 0x02;
    buffer1[1] = 0xFE;
    buffer1[2] = 0x00;
    SSD2828_W_Array(SigMode,channel,buffer1,0);     // UCS�û�ѡ��
    delay_ms(5);
    buffer1[0] = 0x01;
    buffer1[1] = 0x28;
    SSD2828_W_Array(SigMode,channel,buffer1,0);     // ����ʾ
    delay_ms(100);
    buffer1[0] = 0x01;
    buffer1[1] = 0x10;
    SSD2828_W_Array(SigMode,channel,buffer1,0);     // ����ʾ     
    delay_ms(100);  //otp
    buffer1[0] = 0x02;
    buffer1[1] = 0xFE;
    buffer1[2] = 0x40;
	  //buffer1[2] = 0x04;
    SSD2828_W_Array(SigMode,channel,buffer1,0);     //Page0
    
    if(buffer[1]==0x0A)  //otp GammaSet1
    {
        buffer1[0] = 0x02;
        if(RM35x_otptime == 1)      // 1st
        {
            buffer1[1] = 0xF6;
            buffer1[2] = 0x02;
        }
        else if(RM35x_otptime == 2) // 2nd
        {
            buffer1[1] = 0xF7;
            buffer1[2] = 0x02;        
        }
        SSD2828_W_Array(SigMode,channel,buffer1,0);     //OTP_CMD2_P1_PGM_EN          
    }
    if(buffer[1]==0x0D)  //otp GammaSet2
    {
        buffer1[0] = 0x02;
        if(RM35x_otptime == 1)      // 1st
        {
            buffer1[1] = 0xF6;
            buffer1[2] = 0x40;
        }
        else if(RM35x_otptime == 2) // 2nd
        {
            buffer1[1] = 0xF7;
            buffer1[2] = 0x40;        
        }
        SSD2828_W_Array(SigMode,channel,buffer1,0);     //OTP_CMD2_P6_PGM_EN  
    }
    if(buffer[1]==0x0E) //otp GammaSet3
    {		
        buffer1[0] = 0x02;
        if(RM35x_otptime == 1)      // 1st
        {
            buffer1[1] = 0xF6;
            buffer1[2] = 0x80;
        }
        else if(RM35x_otptime == 2) // 2nd
        {
            buffer1[1] = 0xF7;
            buffer1[2] = 0x80;        
        }        
        SSD2828_W_Array(SigMode,channel,buffer1,0);     //OTP_CMD2_P8_PGM_EN  
    }
    if(buffer[1]==0x0F) // otp goa
    {
        buffer1[0] = 0x02;
        if(RM35x_otptime == 1)      // 1st
        {
            buffer1[1] = 0xF6;
            buffer1[2] = 0x04;;
        }
        else if(RM35x_otptime == 2) // 2nd
        {
            buffer1[1] = 0xF7;
            buffer1[2] = 0x04;        
        }
        else if(RM35x_otptime == 3) // 3rd
        {
            buffer1[1] = 0xF9;
            buffer1[2] = 0x80;        
        }
        SSD2828_W_Array(SigMode,channel,buffer1,0);     //OTP_CMD2_P2_PGM_EN  
    }
    if(buffer[1]==0x10) // otp power
    {
        buffer1[0] = 0x02;        
        if(RM35x_otptime == 1)      // 1st
        {
            buffer1[1] = 0xF6;
            buffer1[2] = 0x01;
        }
        else if(RM35x_otptime == 2) // 2nd
        {
            buffer1[1] = 0xF7;
            buffer1[2] = 0x01;        
        }
        else if(RM35x_otptime == 3) // 3rd
        {
            buffer1[1] = 0xF9;
            buffer1[2] = 0x40;        
        }        
        SSD2828_W_Array(SigMode,channel,buffer1,0);     //OTP_CMD2_P0_PGM_EN  
    }    

    buffer1[0] = 0x02;//MTPCTR
    buffer1[1] = 0xF2;
    buffer1[2] = 0x03;
    SSD2828_W_Array(SigMode,channel,buffer1,0);    
	
    buffer1[0] = 0x02;
    buffer1[1] = 0xF3;
    buffer1[2] = 0xA5;
    SSD2828_W_Array(SigMode,channel,buffer1,0);  

    buffer1[0] = 0x02;
    buffer1[1] = 0xF4;
    buffer1[2] = 0x5A;
    SSD2828_W_Array(SigMode,channel,buffer1,0);  

    buffer1[0] = 0x02;
    buffer1[1] = 0xF5;
    buffer1[2] = 0x3C;
    SSD2828_W_Array(SigMode,channel,buffer1,0);  		
                      
    delay_ms(1500);         // wait for >400 ms   

    buffer[4] = Uart_Error_Oth;
    STM2PC_RM671xx(pdev,CDC_IN_EP,buffer,buffer[2]+3);           //���� дOTP״̬  ��ok
}

void RM6935x_Read_OTP_Time(USB_OTG_CORE_HANDLE *pdev,uint8_t  ep_addr,u8 SigMode ,u8 channel,u8* buffer,u16 LP_B7_Data,u16 HS_B7_Data)
{
    u8 buffer1[3];
    u16 ED_D,EE_D,EB_D;
    u8 RM350_otptime = 0;
    if(SigMode != CMD_Mode)
        SSD2828_W_Reg(SigMode,channel,0xB7,(HS_B7_Data|0x0010)&0XFFFD);
    delay_ms(5);
    buffer1[0] = 0x02;
    buffer1[1] = 0xFE;
    buffer1[2] = 0x40;
    SSD2828_W_Array(SigMode,channel,buffer1,0);     // Page 0  ��69300��һ��
    buffer1[0] = 0x02;
    buffer1[1] = 0xF2;
    buffer1[2] = 0x80;
    SSD2828_W_Array(SigMode,channel,buffer1,0);     // otp_load_en   ��69300һ�� 														
    buffer1[0] = 0x02;
    buffer1[1] = 0xF6;
    buffer1[2] = 0x03;
    SSD2828_W_Array(SigMode,channel,buffer1,0);     // Margin Read Check   ��69300һ�� 	  
    
    SSD2828_W_Reg(SigMode,channel,0xB7,((HS_B7_Data|0x0010)&0XFFFD)|0x0080);      

    delay_ms(5);
    buffer1[0] = 0x01;
    buffer1[1] = 0xEE;         //��69300������
    SSD2828_W_Array(SigMode,channel,buffer1,0);
    delay_ms(20);
    if(SigMode == Mipi_Mode)  //RGB+SPI= MIPI  
        SSD2828_W_Reg(SigMode,channel,0xD4,0x00FA);							
    delay_ms(5);                                  
    EE_D=SSD2828_R_Reg(SigMode,channel,0xFF); 
    
    
    buffer1[0] = 0x01;
    buffer1[1] = 0xED;
    SSD2828_W_Array(SigMode,channel,buffer1,0);
    delay_ms(20);
    if(SigMode == Mipi_Mode)  //RGB+SPI= MIPI  
        SSD2828_W_Reg(SigMode,channel,0xD4,0x00FA);							
    delay_ms(5);                             
    ED_D=SSD2828_R_Reg(SigMode,channel,0xFF); 
    
    buffer1[0] = 0x01;
    buffer1[1] = 0xEB;
    SSD2828_W_Array(SigMode,channel,buffer1,0);
    delay_ms(20);
    if(SigMode == Mipi_Mode)  //RGB+SPI= MIPI  
        SSD2828_W_Reg(SigMode,channel,0xD4,0x00FA);							
    delay_ms(5);                               
    EB_D=SSD2828_R_Reg(SigMode,channel,0xFF);     
    if(buffer[3] == 0x00)       //��ȡgamma1����
    {
        if(EE_D&0X02)
            RM350_otptime = 1;
        if(ED_D&0X02)
            RM350_otptime = 2;
    }
    else if(buffer[3] == 0x01)  //��ȡgamma2����
    {
        if(EE_D&0X02)
            RM350_otptime = 1; 
        if(ED_D&0X40)
            RM350_otptime = 2;        
    }
    else if(buffer[3] == 0x02)  //��ȡgamma3����
    {
        if(EE_D&0X80)
            RM350_otptime = 1;
        if(ED_D&0X80)
            RM350_otptime = 2;        
    }
    else if(buffer[3] == 0x03)  //��ȡGOA����
    {
        if(EE_D&0X04)
            RM350_otptime = 1;
        if(ED_D&0X20)
            RM350_otptime = 2;  
        if(EB_D&0X80)
            RM350_otptime = 3;
        
    }
    else if(buffer[3] == 0x04)  //��ȡPOWER����
    {
        if(EE_D&0X01)
            RM350_otptime = 1;
        if(ED_D&0X01)
            RM350_otptime = 2;  
        if(EB_D&0X40)
            RM350_otptime = 3;   
    }                                
    buffer[4]=RM350_otptime;
    RM350_otptime=0x00;
    delay_ms(5);
    SSD2828_W_Reg(SigMode,channel,0xB7,HS_B7_Data);
    buffer[3] = Uart_Error_None;
    STM2PC_RM671xx(pdev,CDC_IN_EP,buffer,buffer[2]+3);           //���� ��ȡ��OTP����  ��ok  
}

void RM6935x_Gamma_switch(USB_OTG_CORE_HANDLE *pdev,uint8_t  ep_addr,u8 SigMode ,u8 channel,u8* buffer,u16 LP_B7_Data,u16 HS_B7_Data)
{
    u8 buffer1[5];
    static u8 RM69350_state = 0;    // 0=normal   1=idle  2=HBM
    SSD2828_W_Reg(SigMode,channel,0xB7,(HS_B7_Data|0x0010)&0XFFFD);
    delay_ms(10);
    if(buffer[3] == 0x01)
    {
        buffer1[0] = 0x02;
        buffer1[1] = 0xFE;
        buffer1[2] = 0x00;
        SSD2828_W_Array(SigMode,channel,buffer1,0);   //UCS�û�ѡ��69300һ��
        if(RM69350_state == 2)
        {
            buffer1[0] = 0x02;
            buffer1[1] = 0x53;
            buffer1[2] = 0x20;
            SSD2828_W_Array(SigMode,channel,buffer1,0);
        }
        else if(RM69350_state == 1)
        {
            buffer1[0] = 0x02;
            buffer1[1] = 0x38;
            buffer1[2] = 0x00;
            SSD2828_W_Array(SigMode,channel,buffer1,0);  
        }  
        
        buffer1[0] = 0x02;
        buffer1[1] = 0xFE;
				buffer1[2] = 0x50;   //MCS Page1 Gamma set-1
        SSD2828_W_Array(SigMode,channel,buffer1,0);
        RM69350_state = 0;
    }
    else if(buffer[3] == 0x02)                       //UCS�û�ѡ��69300һ��
    {
        buffer1[0] = 0x02;
        buffer1[1] = 0xFE;
        buffer1[2] = 0x00;
        SSD2828_W_Array(SigMode,channel,buffer1,0);
        if(RM69350_state == 2)
        {
            buffer1[0] = 0x02;
            buffer1[1] = 0x53;
            buffer1[2] = 0x20;
            SSD2828_W_Array(SigMode,channel,buffer1,0);
        }
        buffer1[0] = 0x02;
        buffer1[1] = 0x39;
        buffer1[2] = 0x00;
        SSD2828_W_Array(SigMode,channel,buffer1,0);    
        buffer1[0] = 0x02;
        buffer1[1] = 0xFE;
				buffer1[2] = 0x52;                             //MCS Page7 Gamma set-2
        SSD2828_W_Array(SigMode,channel,buffer1,0);
        RM69350_state = 1;
       
    }   
    else if(buffer[3] == 0x03)
    {
        buffer1[0] = 0x02;
        buffer1[1] = 0xFE;
        buffer1[2] = 0x00;
        SSD2828_W_Array(SigMode,channel,buffer1,0);
        if(RM69350_state == 1)
        {
            buffer1[0] = 0x02;
            buffer1[1] = 0x38;
            buffer1[2] = 0x00;
            SSD2828_W_Array(SigMode,channel,buffer1,0);
        }
        buffer1[0] = 0x02;
        buffer1[1] = 0x53;
        buffer1[2] = 0x60;
        SSD2828_W_Array(SigMode,channel,buffer1,0);
        buffer1[0] = 0x02;
        buffer1[1] = 0xFE;
				buffer1[2] = 0x54;     //GAMMA3
        SSD2828_W_Array(SigMode,channel,buffer1,0);
        RM69350_state = 2;
    }
    
//    delay_ms(50);
//    buffer1[0] = 0x02;
//    buffer1[1] = 0xEF;       //��69300��һ��
//    buffer1[2] = 0x01;
//    SSD2828_W_Array(SigMode,channel,buffer1,0);
//    delay_ms(50);    
    if(buffer[4] == 0x01) 
    {
        buffer1[0] = 0x02;
        buffer1[1] = 0xBC;
        buffer1[2] = 0x00;                  // 28�����69300һ��
        SSD2828_W_Array(SigMode,channel,buffer1,0);
    }
    else if(buffer[4] == 0x02)
    {
    
        buffer1[0] = 0x02;
        buffer1[1] = 0xBC;
        buffer1[2] = 0x01;                  // 15�����69300һ��
        SSD2828_W_Array(SigMode,channel,buffer1,0);
    }
//		delay_ms(50);
//		buffer1[0] = 0x02;
//		buffer1[1] = 0xF0;
//		buffer1[2] = 0x01;    
//		SSD2828_W_Array(SigMode,channel,buffer1,0);   //����Gamma��RAM���Ĵ���
//		buffer1[0] = 0x02;
//		buffer1[1] = 0xEF;
//		buffer1[2] = 0x00;    
//		SSD2828_W_Array(SigMode,channel,buffer1,0);   //��ֹ��ȡGamma  
//		delay_ms(5);
		SSD2828_W_Reg(SigMode,channel,0xB7,HS_B7_Data);
		buffer[4] = Uart_Error_None;
		STM2PC_RM671xx(pdev,CDC_IN_EP,buffer,buffer[2]+3);           //���� дGamma״̬  ��ok  
}

void RM6935X_auto_Gamma(USB_OTG_CORE_HANDLE *pdev,uint8_t  ep_addr,u8 SigMode ,u8 channel,u8* buffer,u16 LP_B7_Data,u16 HS_B7_Data)
{
	u8 buffer1[3];
	if(SigMode != CMD_Mode)
        SSD2828_W_Reg(SigMode,channel,0xB7,(HS_B7_Data|0x0010)&0XFFFD);

	
		buffer1[0] = 0x02;
		buffer1[1] = 0xFE;
		buffer1[2] = 0x50;              // GAMMA1  page1
		SSD2828_W_Array(SigMode,channel,buffer1,0);	
		SSD2828_W_Reg(SigMode,channel,0xBC,0x02);
	
    for( i = 0;i<buffer[2];i++) 
    {	 
            SSD2828_W_Reg(SigMode,channel,0xBF,(u16)(buffer[i+4]<<8)|buffer[i+3]);  

            i++;
            delay_ms(2);
    }				
    SSD2828_W_Reg(SigMode,channel,0xB7,HS_B7_Data);
}

void RM6935x_Write_51Register(USB_OTG_CORE_HANDLE *pdev,uint8_t  ep_addr,u8 SigMode ,u8 channel,u8* buffer,u16 LP_B7_Data,u16 HS_B7_Data)
{
    SSD2828_W_Reg(SigMode,channel,0xB7,(HS_B7_Data|0x0010)&0XFFFD);
    SSD2828_W_Array(SigMode,channel,buffer,2);
    SSD2828_W_Reg(SigMode,channel,0xB7,HS_B7_Data);
}

/********************************************************************************************
********************************************************************************************
    TC1100  д�Ĵ��� ���Ĵ���  дGamma ��Gamma OTP��¼
    TC1100  д�Ĵ��� ���Ĵ���  дGamma ��Gamma OTP��¼
    TC1100  д�Ĵ��� ���Ĵ���  дGamma ��Gamma OTP��¼
    TC1100  д�Ĵ��� ���Ĵ���  дGamma ��Gamma OTP��¼
    TC1100  д�Ĵ��� ���Ĵ���  дGamma ��Gamma OTP��¼
********************************************************************************************
********************************************************************************************/
void TC1100_Write_Register(USB_OTG_CORE_HANDLE *pdev,uint8_t  ep_addr,u8 SigMode ,u8 channel,u8* buffer,u16 LP_B7_Data,u16 HS_B7_Data)
{
    //SSD2828_W_Reg(SigMode,channel,0xB7,(LP_B7_Data&0xFBFF)|0x0400); 
		SSD2828_W_Reg(SigMode,channel,0xB7,(HS_B7_Data|0x0410)&0XFFFD);//HS mode&DCS long package	write	
    delay_ms(5);
    SSD2828_W_Array(SigMode,channel,buffer,2);

    SSD2828_W_Reg(SigMode,channel,0xB7,HS_B7_Data);
    delay_ms(5);
    buffer[4] = Uart_Error_None;
    STM2PC_RM671xx(pdev,CDC_IN_EP,buffer,buffer[2]+3);           //���� д�Ĵ���״̬  ��ok
}
void TC1100_Read_Register(USB_OTG_CORE_HANDLE *pdev,uint8_t  ep_addr,u8 SigMode ,u8 channel,u8* buffer,u16 LP_B7_Data,u16 HS_B7_Data)
{
    u16 tmp;
		u8 buffer1[5];
    SSD2828_W_Reg(SigMode,channel,0xB7,LP_B7_Data|0x0080);   //general mode read                            
    SSD2828_W_Reg(SigMode,channel,0xC1,buffer[2]-2); //return package size                                
    SSD2828_W_Reg(SigMode,channel,0xC0,0x0001); //reset
	
    buffer1[0] = 0x02;
    buffer1[2] = buffer[3]; 	//Page	
    buffer1[3] = buffer[4]; 	//offset	
    SSD2828_W_Array(SigMode,channel,buffer1,0);
	
    delay_ms(5);  
    SSD2828_W_Reg(SigMode,channel,0xD4,0x00FA);
    SSD2828_W_Cmd(SigMode,channel,0xFF);
    for(i=0;i<buffer[2]-2;i++)  //data read out length
    {
        SSD2828_W_Cmd(SigMode,channel,0xFA);		
        tmp=SPI3_Read_u16_Data(channel);
        buffer[5+i]=tmp>>8;
        buffer[6+i]=tmp;
        delay_ms(5);
        i++;
    }
    SSD2828_W_Reg(SigMode,channel,0xB7,HS_B7_Data);
    STM2PC_RM671xx(pdev,CDC_IN_EP,buffer,buffer[2]+3);           //���� ��ȡ�� �Ĵ�������  ��ok 
}
void TC1100_Write_Gamma(USB_OTG_CORE_HANDLE *pdev,uint8_t ep_addr,u8 SigMode ,u8 channel,u8* buffer,u16 LP_B7_Data,u16 HS_B7_Data)
{
    u8 buffer1[5];
		u8 i=0,m=0;
    SSD2828_W_Reg(SigMode,channel,0xB7,(HS_B7_Data|0x0410)&0XFFFD);
    delay_ms(5);
		for(i=4;i<buffer[2]+4;i++) //gamma���ݴ�buffer4 ��ʼ
		{
			if(buffer[3]==0x01)//red
			{
				buffer1[0] = 0x04;
				buffer1[1] = 0xDF; 	//Page	
				buffer1[2] = 0xE0+TC1100_Gamma_r[m]; 	//Page	
				buffer1[3] = TC1100_Gamma_r[m+1]; 	//offset	
				buffer1[4] = buffer[i]; 	
				SSD2828_W_Array(SigMode,channel,buffer1,0);
			}
			else if(buffer[3]==0x02)//green
			{
				buffer1[0] = 0x04;
				buffer1[1] = 0xDF; 	//Page	
				buffer1[2] = 0xE0+TC1100_Gamma_g[m]; 	//Page	
				buffer1[3] = TC1100_Gamma_g[m+1]; 	//offset	
				buffer1[4] = buffer[i]; 	
				SSD2828_W_Array(SigMode,channel,buffer1,0);			
			}
			else if(buffer[3]==0x03)//blue
			{
				buffer1[0] = 0x04;
				buffer1[1] = 0xDF; 	//Page	
				buffer1[2] = 0xE0+TC1100_Gamma_b[m];  	//Page	
				buffer1[3] = TC1100_Gamma_b[m+1]; 	//offset	
				buffer1[4] = buffer[i]; 	
				SSD2828_W_Array(SigMode,channel,buffer1,0);			
			}
			m+=2;
		}

    delay_ms(5);
    SSD2828_W_Reg(SigMode,channel,0xB7,HS_B7_Data);
    buffer[4] = Uart_Error_None;
    STM2PC_RM671xx(pdev,CDC_IN_EP,buffer,buffer[2]+3);           //���� дGamma״̬  ��ok  	

}
void TC1100_Gamma_OTP_Start(USB_OTG_CORE_HANDLE *pdev,uint8_t  ep_addr,u8 SigMode ,u8 channel,u8* buffer,u16 LP_B7_Data,u16 HS_B7_Data)
{
		u8 buffer1[6];
		SSD2828_W_Reg(SigMode,channel,0xB7,(LP_B7_Data&0xFBFF)|0x0400); 	
		delay_ms(15);

    buffer1[0] = 0x02;
    buffer1[1] = 0xE0; 
    buffer1[2] = 0x11;
    SSD2828_W_Array(SigMode,channel,buffer1,0); 	
		delay_ms(1234);	
    SSD2828_W_Reg(SigMode,channel,0xB7,HS_B7_Data);
    delay_ms(5);
    buffer[4] = Uart_Error_None;
    STM2PC_RM671xx(pdev,CDC_IN_EP,buffer,buffer[2]+3);           //���� дGamma״̬  ��ok 		
}


/********************************************************************************************
********************************************************************************************
    Demura  Flashʹ��
		Demura	Flash����
		Demura	Flashд��
		Demura	FlashУ��
		Demura	Flash OTP
		Demura	���ܿ���&�ر�
********************************************************************************************
********************************************************************************************/
void Inter_Demura_Ram(USB_OTG_CORE_HANDLE *pdev,uint8_t  ep_addr,u8 SigMode ,u8 channel,u8* buffer,u16 LP_B7_Data,u16 HS_B7_Data)
{
	u8 buffer1[3];
	u8 RM67298_Inter_Demura_Ram_Cmd_Table[26]={0xfe,0x40,0x0e,0x80,0xfe,0x20,0xb1,0x00,0xfe,0xd0,0x55,0x30,0x07,0x01,0xfe,0x40,0x0e,0x50,0xfe,0xd0,0x42,0x81,0x27,0x55,0xfe,0x00};
	u8 RM69350_Inter_Demura_Ram_Cmd_Table[22]={0xfe,0x40,0x0e,0x80,0xfe,0xd2,0x18,0x55,0xfe,0xd0,0x03,0x09,0x4d,0x01,0x78,0x01,0xfe,0x40,0x10,0x82,0xfe,0x00};	
	
	SSD2828_W_Reg(SigMode,channel,0xB7,LP_B7_Data);
	delay_ms(15);
	
	if(buffer[7] == RM67298)	//297 298 299
	{	
		for(i=0;i<sizeof(RM67298_Inter_Demura_Ram_Cmd_Table);i++)
		{
			buffer1[0] = 0x02;
			buffer1[1] = RM67298_Inter_Demura_Ram_Cmd_Table[i];
			buffer1[2] = RM67298_Inter_Demura_Ram_Cmd_Table[i+1];
			SSD2828_W_Array(SigMode,channel,buffer1,0);
			delay_ms(15);
			i+=1;
		}	
	}
	else if(buffer[7] == RM69350)	//350
	{
		for(i=0;i<sizeof(RM69350_Inter_Demura_Ram_Cmd_Table);i++)
		{
			buffer1[0] = 0x02;
			buffer1[1] = RM69350_Inter_Demura_Ram_Cmd_Table[i];
			buffer1[2] = RM69350_Inter_Demura_Ram_Cmd_Table[i+1];
			SSD2828_W_Array(SigMode,channel,buffer1,0);
			delay_ms(15);
			i+=1;
		}		
	}
  buffer[0] = 0x2F;
	buffer[1] = 0x01;		

	SSD2828_W_Reg(SigMode,channel,0xB7,HS_B7_Data);
	STM2PC_RM671xx(pdev,CDC_IN_EP,buffer,buffer[2]+3);        																					
}
                                    
void Exit_Demura_Ram(USB_OTG_CORE_HANDLE *pdev,uint8_t  ep_addr,u8 SigMode ,u8 channel,u8* buffer,u16 LP_B7_Data,u16 HS_B7_Data)
{
	u8 buffer1[3];
	u8 RM67298_Exit_Demura_Ram_Cmd_Table[24]={0xfe,0x40,0x0e,0x80,0xfe,0x20,0xb1,0x00,0xfe,0xd0,0x55,0x00,0x07,0x01,0xfe,0x40,0x0e,0x50,0xfe,0xd0,0x42,0x01,0x27,0x55};
	SSD2828_W_Reg(SigMode,channel,0xB7,LP_B7_Data);
	delay_ms(15);
	for(i=0;i<sizeof(RM67298_Exit_Demura_Ram_Cmd_Table);i++)
	{
		buffer1[0] = 0x02;
		buffer1[1] = RM67298_Exit_Demura_Ram_Cmd_Table[i];
		buffer1[2] = RM67298_Exit_Demura_Ram_Cmd_Table[i+1];
		SSD2828_W_Array(SigMode,channel,buffer1,0);
		delay_ms(5);
		i+=1;
	}	
  buffer[0] = 0x2F;
	buffer[1] = 0x02;		

	SSD2828_W_Reg(SigMode,channel,0xB7,HS_B7_Data);
	STM2PC_RM671xx(pdev,CDC_IN_EP,buffer,buffer[2]+3);           //���� ��ȡ�� Gamma����  ��ok  
}
                               
void Flash_Write_Demura(USB_OTG_CORE_HANDLE *pdev,uint8_t  ep_addr,u8 SigMode ,u8 channel,u8* buffer,u16 LP_B7_Data,u16 HS_B7_Data)
{
		u32 error_times=0;
		u8 buffer1[3];
		u8 RM67298_Flash_Write_Demura_data_Table[20]={0xfe,0x20,0xf2,0x04,0xb1,0x00,0xab,0x00,0xac,0x00,0xad,0x00,0xae,0x1a,0xaf,0xb3,0xb0,0x62,0xf2,0x10};
		u8 RM69350_Flash_Write_Demura_data_Table[24]={0xfe,0x20,0xf2,0x04,0xfe,0xd2,0x18,0x55,0x12,0x00,0x13,0x00,0x14,0x00,0x15,0x17,0x16,0xff,0x17,0xff,0xfe,0x20,0xf2,0x10};		
		SSD2828_W_Reg(SigMode,channel,0xB7,LP_B7_Data);
		delay_ms(15);	
		if(OLED.H_pixel == 1080 && OLED.V_pixel == 2160)	//599 601 inch
		{
			RM67298_Flash_Write_Demura_data_Table[13] = 0x1a;
			RM67298_Flash_Write_Demura_data_Table[15] = 0xb3;
			RM67298_Flash_Write_Demura_data_Table[17] = 0x62;
			//u8 RM67298_Flash_Write_Demura_data_Table[20]={0xfe,0x20,0xf2,0x04,0xb1,0x00,0xab,0x00,0xac,0x00,0xad,0x00,0xae,0x1a,0xaf,0xb3,0xb0,0x62,0xf2,0x10};
		}
		else if(OLED.H_pixel == 1080 && OLED.V_pixel == 2248)	//621 inch
		{
			RM67298_Flash_Write_Demura_data_Table[13] = 0x1b;
			RM67298_Flash_Write_Demura_data_Table[15] = 0xc9;
			RM67298_Flash_Write_Demura_data_Table[17] = 0xd2;
			//u8 RM67298_Flash_Write_Demura_data_Table[20]={0xfe,0x20,0xf2,0x04,0xb1,0x00,0xab,0x00,0xac,0x00,0xad,0x00,0xae,0x1b,0xaf,0xc9,0xb0,0xd2,0xf2,0x10};
		}
//----------------------------------------------------------------------RM67298			
		if(buffer[7] == RM67298)	//297 298 299
		{			
			for(i=0;i<sizeof(RM67298_Flash_Write_Demura_data_Table);i++)
			{
				buffer1[0] = 0x02;
				buffer1[1] = RM67298_Flash_Write_Demura_data_Table[i];
				buffer1[2] = RM67298_Flash_Write_Demura_data_Table[i+1];
				SSD2828_W_Array(SigMode,channel,buffer1,0);
				delay_ms(5);
				i+=1;
			}	
			SSD2828_W_Reg(SigMode,channel,0xB7,LP_B7_Data|0x0080); 
			SSD2828_W_Reg(SigMode,channel,0xBC,0x0001);	
			SSD2828_W_Reg(SigMode,channel,0xC1,0x0002); //return package  
			do
			{                        
					buffer1[0] = 0x01;
					buffer1[1] = 0xE8;
					SSD2828_W_Array(SigMode,channel,buffer1,0); 
					delay_ms(6);
					buffer[4]=SSD2828_R_Reg(SigMode,channel,0xFF);   
					delay_ms(100);
			}while((buffer[4]&0x80)!=0x80);
			delay_ms(5);
			buffer[0] = 0x2F;
			buffer[1] = 0x03;		
			buffer[4] = 0x80;				
		}
//----------------------------------------------------------------------RM69350			
		else if(buffer[7] == RM69350) //350
		{
			buffer[0] = 0x2F;
			buffer[1] = 0x03;		
			buffer[4] = 0x08;	
			
			for(i=0;i<sizeof(RM69350_Flash_Write_Demura_data_Table);i++)
			{
				buffer1[0] = 0x02;
				buffer1[1] = RM69350_Flash_Write_Demura_data_Table[i];
				buffer1[2] = RM69350_Flash_Write_Demura_data_Table[i+1];
				SSD2828_W_Array(SigMode,channel,buffer1,0);
				delay_ms(100);
				i+=1;
			}	
			delay_ms(500);
			
			buffer1[0] = 0x02;
			buffer1[1] = 0xfe;
			buffer1[2] = 0xe0;
			SSD2828_W_Array(SigMode,channel,buffer1,0);			
			delay_ms(5);
		//	SSD2828_W_Reg(SigMode,channel,0xB7,((HS_B7_Data|0x0010)&0XFFFD)|0x0080);  //bit7 1:������ bit4 1��HS CLOCK Disable
			SSD2828_W_Reg(SigMode,channel,0xB7,LP_B7_Data|0x0080); 
			delay_ms(15);
			SSD2828_W_Reg(SigMode,channel,0xC1,0x0002); //return package  
			do
			{                        
					buffer1[0] = 0x01;
					buffer1[1] = 0x6B;
					SSD2828_W_Array(SigMode,channel,buffer1,0); 
					delay_ms(20);
					buffer[4]=SSD2828_R_Reg(SigMode,channel,0xFF);   
					delay_ms(50);
					if((++error_times)>300)//15sec timeout error
					{
						buffer[4] = 0x00;	
						goto error_tips0;
					}				
			}while(buffer[4]!=0x08);	
		}
	error_tips0:
		SSD2828_W_Reg(SigMode,channel,0xB7,HS_B7_Data);
		STM2PC_RM671xx(pdev,CDC_IN_EP,buffer,buffer[2]+3);		
}                       

void Flash_Write_Demura2(USB_OTG_CORE_HANDLE *pdev,uint8_t  ep_addr,u8 SigMode ,u8 channel,u8* buffer,u16 LP_B7_Data,u16 HS_B7_Data)
{
	u32 error_time=0;
	u8 buffer1[3];
	USB_Rx_Demura_Total_Num+=0x180000;
	u8 RM69350_Flash_Write_Demura_data_Table2[24]={0xfe,0x20,0xf2,0x04,0xfe,0xd2,0x18,0x55,0x12,0x18,0x13,0x00,0x14,0x00,0x15,(USB_Rx_Demura_Total_Num>>16),0x16,(USB_Rx_Demura_Total_Num>>8),0x17,(USB_Rx_Demura_Total_Num),0xfe,0x20,0xf2,0x10}; //7d		
		
	SSD2828_W_Reg(SigMode,channel,0xB7,LP_B7_Data);
	//SSD2828_W_Reg(SigMode,channel,0xB7,(HS_B7_Data|0x0010)&0XFFfd);		
	delay_ms(15);		
	if(buffer[7] == RM69350)	//350
	{		
		buffer[0] = 0x2F;
		buffer[1] = 0x0A;		
		buffer[4] = 0x08;	
		for(i=0;i<sizeof(RM69350_Flash_Write_Demura_data_Table2);i++)
		{
			buffer1[0] = 0x02;
			buffer1[1] = RM69350_Flash_Write_Demura_data_Table2[i];
			buffer1[2] = RM69350_Flash_Write_Demura_data_Table2[i+1];
			SSD2828_W_Array(SigMode,channel,buffer1,0);
			delay_ms(100);
			i+=1;
		}	
		//------------------------------------------------------------- 
		delay_ms(500);
		
		buffer1[0] = 0x02;
		buffer1[1] = 0xfe;
		buffer1[2] = 0xe0;
		SSD2828_W_Array(SigMode,channel,buffer1,0);			
		delay_ms(50);		
		SSD2828_W_Reg(SigMode,channel,0xB7,LP_B7_Data|0x0080);
			delay_ms(15);		
		//SSD2828_W_Reg(SigMode,channel,0xB7,((HS_B7_Data|0x0010)&0XFFFD)|0x0080);  //bit7 1:������ bit4 1��HS CLOCK Disable
		SSD2828_W_Reg(SigMode,channel,0xBC,0x0001); 
		SSD2828_W_Reg(SigMode,channel,0xC1,0x0002); //return package  
		
		do
		{						 
			buffer1[0] = 0x01;
			buffer1[1] = 0x6B;
			SSD2828_W_Array(SigMode,channel,buffer1,0); 
			delay_ms(20);
			buffer[4]=SSD2828_R_Reg(SigMode,channel,0xFF);	 
			delay_ms(50);
			if((++error_time)>300)//15sec timeout error
			{
				buffer[4] = 0x00;	
				goto error_tips1;
			}
		}while((buffer[4]&0x08)!=0x08);	
		
	error_tips1:
		SSD2828_W_Reg(SigMode,channel,0xB7,HS_B7_Data);
		STM2PC_RM671xx(pdev,CDC_IN_EP,buffer,buffer[2]+3);
	}
}


void Flash_Check_Demura(USB_OTG_CORE_HANDLE *pdev,uint8_t  ep_addr,u8 SigMode ,u8 channel,u8* buffer,u16 LP_B7_Data,u16 HS_B7_Data)
{
	u8 buffer1[3];
	u8 RM67298_Flash_Check_Read_REG[2]={0xE6,0xE7};
	u8 RM67298_Flash_Check_Demura_data_Table[18]={0xfe,0x20,0xf2,0x04,0xab,0x00,0xac,0x00,0xad,0x00,0xae,0x1a,0xaf,0xb3,0xb0,0x62,0xf2,0x01};	
	u8 RM69350_Flash_Check_Read_REG[2]={0x69,0x6a};
	u8 RM69350_Flash_Check_Demura_data_Table[36]={0xfe,0xd2,0x06,0x03,0xfe,0x20,0xf3,0x08,0xf3,0x00,0xf3,0x04,0xf3,0x02,0xfe,0x20,0xf2,0x04,0xfe,0xd2,0x12,0x00,0x13,0x00,0x14,0x00,0x15,(USB_Rx_Demura_Total_Num>>16),0x16,(USB_Rx_Demura_Total_Num>>8),0x17,(USB_Rx_Demura_Total_Num-0x0b),0xfe,0x20,0xf2,0x01};	
	
	SSD2828_W_Reg(SigMode,channel,0xB7,LP_B7_Data);
	delay_ms(15);
	if(OLED.H_pixel == 1080 && OLED.V_pixel == 2160)	//599 601 inch
	{
		RM67298_Flash_Check_Demura_data_Table[11] =0x1a;
		RM67298_Flash_Check_Demura_data_Table[13] =0xb3;
		RM67298_Flash_Check_Demura_data_Table[15] =0x62;
		//u8 RM67298_Flash_Check_Demura_data_Table[18]={0xfe,0x20,0xf2,0x04,0xab,0x00,0xac,0x00,0xad,0x00,0xae,0x1a,0xaf,0xb3,0xb0,0x62,0xf2,0x01};
	}
	else if(OLED.H_pixel == 1080 && OLED.V_pixel == 2248)	//621 inch
	{
		RM67298_Flash_Check_Demura_data_Table[11] =0x1b;
		RM67298_Flash_Check_Demura_data_Table[13] =0xc9;
		RM67298_Flash_Check_Demura_data_Table[15] =0xd2;
		//u8 RM67298_Flash_Check_Demura_data_Table[18]={0xfe,0x20,0xf2,0x04,0xab,0x00,0xac,0x00,0xad,0x00,0xae,0x1b,0xaf,0xc9,0xb0,0xd2,0xf2,0x01};
	}
		
	if(buffer[7] == 0x2E)	//297 298 299
	{
		for(i=0;i<sizeof(RM67298_Flash_Check_Demura_data_Table);i++)
		{
			buffer1[0] = 0x02;
			buffer1[1] = RM67298_Flash_Check_Demura_data_Table[i];
			buffer1[2] = RM67298_Flash_Check_Demura_data_Table[i+1];
			SSD2828_W_Array(SigMode,channel,buffer1,0);
			delay_ms(5);
			i+=1;
		}	
		delay_ms(200);
	
    SSD2828_W_Reg(SigMode,channel,0xB7,LP_B7_Data|0x0080); 
    SSD2828_W_Reg(SigMode,channel,0xBC,0x0001);		
    for( i = 0;i<sizeof(RM67298_Flash_Check_Read_REG);i++)			 	
    {  	
        SSD2828_W_Reg(SigMode,channel,0xC1,0x0001); //return package                           
        buffer1[0] = 0x01;
        buffer1[1] = RM67298_Flash_Check_Read_REG[i];
        SSD2828_W_Array(SigMode,channel,buffer1,0); 
				delay_ms(6);
        buffer[4+i]=SSD2828_R_Reg(SigMode,channel,0xFF);        
    }	

    delay_ms(5);
		buffer[0] = 0x2F;
		buffer[1] = 0x04;				
		
    SSD2828_W_Reg(SigMode,channel,0xB7,HS_B7_Data);
    STM2PC_RM671xx(pdev,CDC_IN_EP,buffer,buffer[2]+3);           //���� ��ȡ�� Gamma����  ��ok  		
	}
//----------------------------------------------------------------	
	else if(buffer[7] == RM69350)	//350
	{
		for(i=0;i<sizeof(RM69350_Flash_Check_Demura_data_Table);i++)
		{
			buffer1[0] = 0x02;
			buffer1[1] = RM69350_Flash_Check_Demura_data_Table[i];
			buffer1[2] = RM69350_Flash_Check_Demura_data_Table[i+1];
			SSD2828_W_Array(SigMode,channel,buffer1,0);
			delay_ms(6);
			i+=1;
		}	
		delay_ms(1200);
		
		buffer1[0] = 0x02;
		buffer1[1] = 0xfe;
		buffer1[2] = 0xe0;
		SSD2828_W_Array(SigMode,channel,buffer1,0);			
		delay_ms(15);		
		
		SSD2828_W_Reg(SigMode,channel,0xB7,LP_B7_Data|0x0080); 
		SSD2828_W_Reg(SigMode,channel,0xBC,0x0001); 	
		for(i=0;i<sizeof(RM69350_Flash_Check_Read_REG);i++) 			
		{	
			SSD2828_W_Reg(SigMode,channel,0xC1,0x0001); //return package						   
			buffer1[0] = 0x01;
			buffer1[1] = RM69350_Flash_Check_Read_REG[i];
			SSD2828_W_Array(SigMode,channel,buffer1,0); 
			delay_ms(6);
			buffer[4+i]=SSD2828_R_Reg(SigMode,channel,0xFF);		
		}	

		delay_ms(5);
		buffer[0] = 0x2F;
		buffer[1] = 0x04;				

		SSD2828_W_Reg(SigMode,channel,0xB7,HS_B7_Data);
		STM2PC_RM671xx(pdev,CDC_IN_EP,buffer,buffer[2]+3);			 //???? ????? Gamma???  ??ok			
	}
		
}
                                  
void Flash_Erase_Demura(USB_OTG_CORE_HANDLE *pdev,uint8_t  ep_addr,u8 SigMode ,u8 channel,u8* buffer,u16 LP_B7_Data,u16 HS_B7_Data)
{
	u8 buffer1[3];
	u8 RM67298_Flash_Erase_Demura_data_Table[8]={0xfe,0x20,0xf2,0x04,0xb1,0x00,0xf3,0x01};
	u8 RM69350_Flash_Erase_Demura_data_Table[10]={0xfe,0xd2,0x18,0x55,0xfe,0x20,0xf2,0x04,0xf3,0x01};
	u8 RM67298_Read_REG=0xE8;
	u8 RM69350_Read_REG=0x6B;	
	
	SSD2828_W_Reg(SigMode,channel,0xB7,LP_B7_Data);
	delay_ms(15);
		
	if(buffer[7] == RM67298)	//297 298 299
	{
		for(i=0;i<sizeof(RM67298_Flash_Erase_Demura_data_Table);i++)
		{
			buffer1[0] = 0x02;
			buffer1[1] = RM67298_Flash_Erase_Demura_data_Table[i];
			buffer1[2] = RM67298_Flash_Erase_Demura_data_Table[i+1];
			SSD2828_W_Array(SigMode,channel,buffer1,0);
			delay_ms(5);
			i+=1;
		}
    SSD2828_W_Reg(SigMode,channel,0xB7,LP_B7_Data|0x0080); 
    SSD2828_W_Reg(SigMode,channel,0xBC,0x0001);	
		SSD2828_W_Reg(SigMode,channel,0xC1,0x0002); //return package  
    do
		{                        
        buffer1[0] = 0x01;
        buffer1[1] = RM67298_Read_REG;
        SSD2828_W_Array(SigMode,channel,buffer1,0); 
				delay_ms(6);
        buffer[4]=SSD2828_R_Reg(SigMode,channel,0xFF);      
    }while((buffer[4]&0x80)!=0x80);
    delay_ms(5);
		buffer[0] = 0x2F;
		buffer[1] = 0x05;		
		buffer[4] = 0x80;	

    SSD2828_W_Reg(SigMode,channel,0xB7,HS_B7_Data);
    STM2PC_RM671xx(pdev,CDC_IN_EP,buffer,buffer[2]+3);           //���� ��ȡ�� Gamma����  ��ok  
	}
	else if(buffer[7] == RM69350)	//350
	{	
		for(i=0;i<sizeof(RM69350_Flash_Erase_Demura_data_Table);i++)
		{
			buffer1[0] = 0x02;
			buffer1[1] = RM69350_Flash_Erase_Demura_data_Table[i];
			buffer1[2] = RM69350_Flash_Erase_Demura_data_Table[i+1];
			SSD2828_W_Array(SigMode,channel,buffer1,0);
			delay_ms(6);
			i+=1;
		}
		delay_ms(5000);	
		buffer1[0] = 0x02;
		buffer1[1] = 0xfe;
		buffer1[2] = 0xe0;
		SSD2828_W_Array(SigMode,channel,buffer1,0);			
		delay_ms(15);		
		SSD2828_W_Reg(SigMode,channel,0xB7,LP_B7_Data|0x0080); 
		SSD2828_W_Reg(SigMode,channel,0xBC,0x0001); 
		SSD2828_W_Reg(SigMode,channel,0xC1,0x0002); //return package  
		do
		{						 
			buffer1[0] = 0x01;
			buffer1[1] = RM69350_Read_REG;
			SSD2828_W_Array(SigMode,channel,buffer1,0); 
			delay_ms(6);
			buffer[4]=SSD2828_R_Reg(SigMode,channel,0xFF);		
		}while((buffer[4]&0x08)!=0x08);
		delay_ms(5);
		buffer[0] = 0x2F;
		buffer[1] = 0x05;		
		buffer[4] = 0x08;	
		
		SSD2828_W_Reg(SigMode,channel,0xB7,HS_B7_Data);
		STM2PC_RM671xx(pdev,CDC_IN_EP,buffer,buffer[2]+3);
	}
}

void Flash_Demura_OTP(USB_OTG_CORE_HANDLE *pdev,uint8_t  ep_addr,u8 SigMode ,u8 channel,u8* buffer,u16 LP_B7_Data,u16 HS_B7_Data)
{
#if 1
	  u8 buffer1[3];
		u8 RM67298_Flash_Demura_OTP_Table[18]={0xfe,0x00,0x28,0x00,0x10,0x00,0xfe,0x40,0xf8,0x02,0xf2,0x01,0xf3,0xa5,0xf4,0x5a,0xf5,0x3c};
		u8 Read_REG=0xE8;
		SSD2828_W_Reg(SigMode,channel,0xB7,LP_B7_Data);
		delay_ms(15);

		for(i=0;i<sizeof(RM67298_Flash_Demura_OTP_Table);i++)
		{
			buffer1[0] = 0x02;
			buffer1[1] = RM67298_Flash_Demura_OTP_Table[i];
			buffer1[2] = RM67298_Flash_Demura_OTP_Table[i+1];
			SSD2828_W_Array(SigMode,channel,buffer1,0);
			delay_ms(5);
			i+=1;
		}
    delay_ms(5);
		buffer[0] = 0x2F;
		buffer[1] = 0x09;

    SSD2828_W_Reg(SigMode,channel,0xB7,HS_B7_Data);
    STM2PC_RM671xx(pdev,CDC_IN_EP,buffer,buffer[2]+3);           //���� ��ȡ�� Gamma����  ��ok  
#endif
}

void Enable_Flash_Control(USB_OTG_CORE_HANDLE *pdev,uint8_t  ep_addr,u8 SigMode ,u8 channel,u8* buffer,u16 LP_B7_Data,u16 HS_B7_Data)
{
	u8 buffer1[3];	
	u8 RM67298_Enable_Flash_Control[4] = {0xfe, 0x20, 0xf3, 0x02};
	u8 RM67298_Enable_Flash_Read_REG[3] = {0xEA,0xEB,0xEC};
	u8 RM69350_Enable_Flash_Control[24] = {0xfe,0xd2,0x01,0xcf,0xfe,0x40,0x4d,0x20,0xfe,0xd2,0x05,0x18,0xfe,0x20,0xf3,0x08,0xf3,0x00,0xf3,0x04,0xf3,0x02,0xfe,0xe0};
	u8 RM69350_Enable_Flash_Read_REG[3] = {0x6d,0x6e,0x6f};	
	
	if(buffer[7] == RM67298)	//297 298 299
	{
		SSD2828_W_Reg(SigMode,channel,0xB7,LP_B7_Data);
		delay_ms(15);
		for(i=0;i<sizeof(RM67298_Enable_Flash_Control);i++)
		{
			buffer1[0] = 0x02;
			buffer1[1] = RM67298_Enable_Flash_Control[i];
			buffer1[2] = RM67298_Enable_Flash_Control[i+1];
			SSD2828_W_Array(SigMode,channel,buffer1,0);
			delay_ms(5);
			i+=1;
		}
    SSD2828_W_Reg(SigMode,channel,0xB7,LP_B7_Data|0x0080); 
    SSD2828_W_Reg(SigMode,channel,0xBC,0x0001);	
    for( i = 0; i< 3;i++)			 	
    {  	
        SSD2828_W_Reg(SigMode,channel,0xC1,0x0001); //return package                           
        buffer1[0] = 0x01;
        buffer1[1] = RM67298_Enable_Flash_Read_REG[i];
        SSD2828_W_Array(SigMode,channel,buffer1,0); 
				delay_ms(6);
        buffer[4+i]=SSD2828_R_Reg(SigMode,channel,0xFF);        
    }	
		//buffer[2]=5;
    delay_ms(5);
    SSD2828_W_Reg(SigMode,channel,0xB7,HS_B7_Data);
    STM2PC_RM671xx(pdev,CDC_IN_EP,buffer,buffer[2]+3);           //���� ��ȡ�� Gamma����  ��ok  
	}
	else if(buffer[7] == RM69350)	//350
	{
		SSD2828_W_Reg(SigMode,channel,0xB7,LP_B7_Data);
		delay_ms(15);
		for(i=0;i<sizeof(RM69350_Enable_Flash_Control);i++)
		{
		buffer1[0] = 0x02;
		buffer1[1] = RM69350_Enable_Flash_Control[i];
		buffer1[2] = RM69350_Enable_Flash_Control[i+1];
		SSD2828_W_Array(SigMode,channel,buffer1,0);
		delay_ms(15);
		i+=1;
		}
		SSD2828_W_Reg(SigMode,channel,0xB7,LP_B7_Data|0x0080); 
		SSD2828_W_Reg(SigMode,channel,0xBC,0x0001); 
		for( i = 0; i< 3;i++)				
		{	
			SSD2828_W_Reg(SigMode,channel,0xC1,0x0001); //return package						   
			buffer1[0] = 0x01;
			buffer1[1] = RM69350_Enable_Flash_Read_REG[i];
			SSD2828_W_Array(SigMode,channel,buffer1,0); 
			delay_ms(6);
			buffer[4+i]=SSD2828_R_Reg(SigMode,channel,0xFF);		
		}
		delay_ms(5);
		SSD2828_W_Reg(SigMode,channel,0xB7,HS_B7_Data);
		STM2PC_RM671xx(pdev,CDC_IN_EP,buffer,buffer[2]+3);			 //???? ????? Gamma???  ??ok	
	}
}

void Demura_Function_ON(USB_OTG_CORE_HANDLE *pdev,uint8_t  ep_addr,u8 SigMode ,u8 channel,u8* buffer,u16 LP_B7_Data,u16 HS_B7_Data)
{
	u8 buffer1[3];
	u8 RM67298_Demura_Function_ON_Table[10]={0xfe,0x20,0x09,0x10,0xA6,0xC0,0xA7,0xFF,0xBC,0x01};
	u8 RM69350_Demura_Function_ON_Table[8] = {0xfe, 0xd2, 0x0e, 0xc0, 0x0f, 0x7f, 0xAA, 0x01};
	SSD2828_W_Reg(SigMode,channel,0xB7,LP_B7_Data);
	delay_ms(15);	
	
	if(buffer[7] == RM67298)	//297 298 299
	{
		for(i=0;i<sizeof(RM67298_Demura_Function_ON_Table);i++)
		{
			buffer1[0] = 0x02;
			buffer1[1] = RM67298_Demura_Function_ON_Table[i];
			buffer1[2] = RM67298_Demura_Function_ON_Table[i+1];
			SSD2828_W_Array(SigMode,channel,buffer1,0);
			delay_ms(5);
			i+=1;
		}
    delay_ms(5);
		buffer[0] = 0x2F;
		buffer[1] = 0x07;		
    SSD2828_W_Reg(SigMode,channel,0xB7,HS_B7_Data);
    STM2PC_RM671xx(pdev,CDC_IN_EP,buffer,buffer[2]+3);           //���ݳ���-crc���ֽ�
	}
	else if(buffer[7] == RM69350)	//350
	{
		for(i=0;i<sizeof(RM69350_Demura_Function_ON_Table);i++)
		{
		buffer1[0] = 0x02;
		buffer1[1] = RM69350_Demura_Function_ON_Table[i];
		buffer1[2] = RM69350_Demura_Function_ON_Table[i+1];
		SSD2828_W_Array(SigMode,channel,buffer1,0);
		delay_ms(50);
		i+=1;
		}
		delay_ms(5);
		buffer[0] = 0x2F;
		buffer[1] = 0x07;		
		SSD2828_W_Reg(SigMode,channel,0xB7,HS_B7_Data);
		STM2PC_RM671xx(pdev,CDC_IN_EP,buffer,buffer[2]+3);			 //??????-crc}???
	}
}

void Demura_Function_OFF(USB_OTG_CORE_HANDLE *pdev,uint8_t  ep_addr,u8 SigMode ,u8 channel,u8* buffer,u16 LP_B7_Data,u16 HS_B7_Data)
{
	u8 buffer1[3];
	u8 RM67298_Demura_Function_OFF_Table[10]={0xfe,0x20,0x09,0x10,0xA6,0xC0,0xA7,0xFF,0xBC,0x81};
	u8 RM69350_Demura_Function_OFF_Table[8] = {0xfe,0xd2,0x0e,0xc0,0x0f,0x7f,0xAA,0x81};
	
	SSD2828_W_Reg(SigMode,channel,0xB7,LP_B7_Data);
	delay_ms(15);

	if(buffer[7] == RM67298)	//297 298 299
	{
		for(i=0;i<sizeof(RM67298_Demura_Function_OFF_Table);i++)
		{
			buffer1[0] = 0x02;
			buffer1[1] = RM67298_Demura_Function_OFF_Table[i];
			buffer1[2] = RM67298_Demura_Function_OFF_Table[i+1];
			SSD2828_W_Array(SigMode,channel,buffer1,0);
			delay_ms(5);
			i+=1;
		}
    delay_ms(5);
		buffer[0] = 0x2F;
		buffer[1] = 0x08;				
    SSD2828_W_Reg(SigMode,channel,0xB7,HS_B7_Data);
    STM2PC_RM671xx(pdev,CDC_IN_EP,buffer,buffer[2]+3);           //���� ��ȡ�� Gamma����  ��ok  
	}
	else if(buffer[7] == RM69350)	//350
	{
		for(i=0;i<sizeof(RM69350_Demura_Function_OFF_Table);i++)
		{
			buffer1[0] = 0x02;
			buffer1[1] = RM69350_Demura_Function_OFF_Table[i];
			buffer1[2] = RM69350_Demura_Function_OFF_Table[i+1];
			SSD2828_W_Array(SigMode,channel,buffer1,0);
			delay_ms(50);
			i+=1;
		}
		delay_ms(5);
		buffer[0] = 0x2F;
		buffer[1] = 0x08;				
		SSD2828_W_Reg(SigMode,channel,0xB7,HS_B7_Data);
		STM2PC_RM671xx(pdev,CDC_IN_EP,buffer,buffer[2]+3);			 //???? ????? Gamma???  ??ok	
	}
}

/********************************************************************************************
********************************************************************************************
    Generate  д�Ĵ��� 
    Generate  ���Ĵ��� 
********************************************************************************************
********************************************************************************************/
void Generate_Write_Function(USB_OTG_CORE_HANDLE *pdev,uint8_t  ep_addr,u8 SigMode ,u8 channel,u8* buffer,u16 LP_B7_Data,u16 HS_B7_Data)
{
		if((buffer[1]&0x80)==0x00)
		{	
			if((buffer[1]&0x0f)==0x08)//LP_Gerneric_Short_Write
			{SSD2828_W_Reg(SigMode,channel,0xB7,LP_Gerneric_Short_Write);}
			else if((buffer[1]&0x0f)==0x09)//HS_Gerneric_Short_Write
			{SSD2828_W_Reg(SigMode,channel,0xB7,HS_Gerneric_Short_Write);}
			else if((buffer[1]&0x0f)==0x0a)//LP_DCS_Short_Write
			{SSD2828_W_Reg(SigMode,channel,0xB7,LP_DCS_Short_Write);}
			else if((buffer[1]&0x0f)==0x0b)//HS_DCS_Short_Write	
			{SSD2828_W_Reg(SigMode,channel,0xB7,HS_DCS_Short_Write);}	
			else if((buffer[1]&0x0f)==0x0c)//LP_Gerneric_Long_Write
			{SSD2828_W_Reg(SigMode,channel,0xB7,LP_Gerneric_Long_Write);}
			else if((buffer[1]&0x0f)==0x0d)//HS_Gerneric_Long_Write
			{SSD2828_W_Reg(SigMode,channel,0xB7,HS_Gerneric_Long_Write);}
			else if((buffer[1]&0x0f)==0x0e)//LP_DCS_Long_Write
			{SSD2828_W_Reg(SigMode,channel,0xB7,LP_DCS_Long_Write);}
			else if((buffer[1]&0x0f)==0x0f)//HS_DCS_Long_Write	
			{SSD2828_W_Reg(SigMode,channel,0xB7,HS_DCS_Long_Write);}	
			delay_ms(15);			
		}
//------------------------------------------------------																	
		SSD2828_W_Array(SigMode,channel,buffer,2);
		delay_ms(5);
		if((buffer[1]&0x80)==0x80)//�������һ������
		{
			SSD2828_W_Reg(SigMode,channel,0xB7,HS_B7_Data);
		}
		buffer[1]=0x01;
    buffer[4] = Uart_Error_None;
    STM2PC_RM671xx(pdev,CDC_IN_EP,buffer,buffer[2]+3);           //���� дGamma״̬  ��ok 
}

void Generate_Read_Function(USB_OTG_CORE_HANDLE *pdev,uint8_t  ep_addr,u8 SigMode ,u8 channel,u8* buffer,u16 LP_B7_Data,u16 HS_B7_Data)
{
		u16 tmp;
		u8 buffer1[3];
		u8 read_back=buffer[4];
		if((buffer[1]&0x80)==0x00)
		{
			if((buffer[1]&0x0f)==0x00)//LP_Gerneric_Short_Read
			{SSD2828_W_Reg(SigMode,channel,0xB7,LP_Gerneric_Short_Read);}
			else if((buffer[1]&0x0f)==0x01)//HS_Gerneric_Short_Read
			{SSD2828_W_Reg(SigMode,channel,0xB7,HS_Gerneric_Short_Read);}
			else if((buffer[1]&0x0f)==0x02)//LP_DCS_Short_Read
			{SSD2828_W_Reg(SigMode,channel,0xB7,LP_DCS_Short_Read);}
			else if((buffer[1]&0x0f)==0x03)//HS_DCS_Short_Read	
			{SSD2828_W_Reg(SigMode,channel,0xB7,HS_DCS_Short_Read);}	
			else if((buffer[1]&0x0f)==0x04)//LP_Gerneric_Long_Read
			{SSD2828_W_Reg(SigMode,channel,0xB7,LP_Gerneric_Long_Read);}
			else if((buffer[1]&0x0f)==0x05)//HS_Gerneric_Long_Read
			{SSD2828_W_Reg(SigMode,channel,0xB7,HS_Gerneric_Long_Read);}
			else if((buffer[1]&0x0f)==0x06)//LP_DCS_Long_Read
			{SSD2828_W_Reg(SigMode,channel,0xB7,LP_DCS_Long_Read);}
			else if((buffer[1]&0x0f)==0x07)//HS_DCS_Long_Read	
			{SSD2828_W_Reg(SigMode,channel,0xB7,HS_DCS_Long_Read);}			
			delay_ms(15);
		}		
//------------------------------------------------------															
    SSD2828_W_Reg(SigMode,channel,0xC1,buffer[2]-1); //return package size                                
    SSD2828_W_Reg(SigMode,channel,0xC0,0x0001); //reset
	
    buffer1[0] = 0x01;
    buffer1[1] = buffer[3]; 	//Download register
    SSD2828_W_Array(SigMode,channel,buffer1,0);
    delay_ms(5);  
    SSD2828_W_Reg(SigMode,channel,0xD4,0x00FA);
    SSD2828_W_Cmd(SigMode,channel,0xFF);
    for(i=0;i<read_back;i++)  //readback num
    {
        SSD2828_W_Cmd(SigMode,channel,0xFA);
        tmp=SPI3_Read_u16_Data(channel);
        buffer[4+i]=tmp>>8;
        buffer[5+i]=tmp;
        delay_ms(5);
        i++;
    }
		if((buffer[1]&0x80)==0x80)//�������һ������
		{
			SSD2828_W_Reg(SigMode,channel,0xB7,HS_B7_Data);
		}
		buffer[1]=0x02;
    STM2PC_RM671xx(pdev,CDC_IN_EP,buffer,buffer[2]+3);           //���� ��ȡ�� �Ĵ�������  ��ok 
}