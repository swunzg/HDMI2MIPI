
#include "usb_conf.h"
#include "diskio.h"
#include "usbh_msc_core.h"
#include "sdio_sd.h"
#include "main.h"
#include <stdio.h>
/*-----------------------------------------------------------------------*/
/* Correspondence between physical drive number and physical drive.      */

#define BlockSize            512 /* Block Size in Bytes */
#define SD_Mode								1		//0 dma,1 interrupt

static SD_CardInfo SDCardInfo;
/*--------------------------------------------------------------------------

Module Private Functions and Variables

---------------------------------------------------------------------------*/

static volatile DSTATUS Stat = STA_NOINIT;	/* Disk status */

extern USB_OTG_CORE_HANDLE          USB_OTG_Core;
extern USBH_HOST                     USB_Host;

/*-----------------------------------------------------------------------*/
/* Initialize Disk Drive                                                 */
/*-----------------------------------------------------------------------*/

DSTATUS disk_initialize (
                         BYTE drv		/* Physical drive number (0) */
                           )
{
  switch(drv)
	{
		case SD_Disc:
			Stat = SD_Init();
    	
    	if(Stat != SD_OK)
    	{
        	return STA_NOINIT; //其他错误：初始化失败
    	}
    	else
    	{
	  		Stat = SD_GetCardInfo(&SDCardInfo); //读sd卡信息

	   		if (Stat != SD_OK)
	    	{
	     		return  STA_NOINIT;//RES_NOTRDY;  //报NOT READY错误
	      }
	              // Select Card 
	     	Stat = SD_SelectDeselect((u32) (SDCardInfo.RCA << 16));

	     	if (Stat != SD_OK)
	     	{
	        	return  STA_NOINIT;//RES_NOTRDY;  //报NOT READY错误
	     	}

	   		switch(SD_Mode)
	   		{
	    		case 0:  //dma方式
		    		Stat = SD_EnableWideBusOperation(SDIO_BusWide_4b);
		       		if (Stat != SD_OK)
		        	{  
		         		return RES_NOTRDY;  //报NOT READY错误
		        	}

		       		Stat = SD_SetDeviceMode(SD_DMA_MODE);
		      		if (Stat != SD_OK)
		      		{
		         		return RES_NOTRDY;  //报NOT READY错误
		      		}
		      		break;

	    		case 1:  //中断方式
		       		Stat = SD_EnableWideBusOperation(SDIO_BusWide_4b);
		       		if (Stat != SD_OK)
		        	{  
		         		return RES_NOTRDY;  //报NOT READY错误
		        	}
		        	
		       		Stat = SD_SetDeviceMode(SD_INTERRUPT_MODE);  
		      		if (Stat != SD_OK)
		      		{
		         		return RES_NOTRDY;  //报NOT READY错误
		      		}
		       		break;
	       		
	     		default :
	     			return RES_NOTRDY;
	   		}
			}

			return 0;           //初始化成功
		case USB_Disc:
			if(HCD_IsDeviceConnected(&USB_OTG_Core))
			{  
				Stat &= ~STA_NOINIT;
			}
			
			return Stat;
		default:
			return STA_NOINIT;
	}

  
  
}



/*-----------------------------------------------------------------------*/
/* Get Disk Status                                                       */
/*-----------------------------------------------------------------------*/

DSTATUS disk_status (
                     BYTE drv		/* Physical drive number (0) */
                       )
{
//  if (drv) return STA_NOINIT;		/* Supports only single drive */
//  return Stat;
	switch (drv)
	{
		case SD_Disc :
			return RES_OK;
		case USB_Disc :
			return RES_OK;
		default:
			return STA_NOINIT;
	}
}



/*-----------------------------------------------------------------------*/
/* Read Sector(s)                                                        */
/*-----------------------------------------------------------------------*/

DRESULT disk_read (
                   BYTE drv,			/* Physical drive number (0) */
                   BYTE *buff,			/* Pointer to the data buffer to store read data */
                   DWORD sector,		/* Start sector number (LBA) */
                   BYTE count			/* Sector count (1..255) */
                     )
{
	SD_Error Status;
  BYTE status = USBH_MSC_OK;
  
  if (!count) return RES_PARERR;
//  if (Status & STA_NOINIT) return RES_NOTRDY;
  
	switch (drv)
	{  
		case SD_Disc:
		
				switch(SD_Mode)
				{
						case 0:  //dma方式
								if(count==1)// 1个sector的读操作      
								{      
								Status = SD_ReadBlock(sector << 9,(u32 *)(&buff[0]),BlockSize);//sector<<9 扇区地址转为字节地址 一个扇区512字节                                    
								}                                                
								else                    //多个sector的读操作     
								{    
									Status = SD_ReadMultiBlocks(sector << 9,(u32 *)(&buff[0]),BlockSize,count);                                      
								}
							break;
							
						case 1:  //中断方式
								if(count==1)            // 1个sector的读操作      
								{      
								Status = SD_ReadBlock(sector << 9,(u32 *)(&buff[0]),BlockSize);                                              
								}                                                
								else                    //多个sector的读操作     
								{    
									Status = SD_ReadMultiBlocks(sector << 9 ,(u32 *)(&buff[0]),BlockSize,count);                                     
								}  	  
								break;
								
						default:
							Status=SD_ERROR;
					}			
					//处理返回值，将sdcard.c的返回值转成ff.c的返回值
					if(Status == SD_OK)
							return RES_OK;
					else
							return RES_ERROR;
		case USB_Disc:
			if(HCD_IsDeviceConnected(&USB_OTG_Core))
			{  				
				do
				{
					status = USBH_MSC_Read10(&USB_OTG_Core, buff,sector,512*count);
					USBH_MSC_HandleBOTXfer(&USB_OTG_Core ,&USB_Host);
					
					if(!HCD_IsDeviceConnected(&USB_OTG_Core))
					{ 
						return RES_ERROR;
					}      
				}
				while(status == USBH_MSC_BUSY );
			}
			
			if(status == USBH_MSC_OK)
				return RES_OK;
			return RES_ERROR;		
	}
  return RES_OK;
}



/*-----------------------------------------------------------------------*/
/* Write Sector(s)                                                       */
/*-----------------------------------------------------------------------*/

#if _READONLY == 0
DRESULT disk_write (
                    BYTE drv,			/* Physical drive number (0) */
                    const BYTE *buff,	/* Pointer to the data to be written */
                    DWORD sector,		/* Start sector number (LBA) */
                    BYTE count			/* Sector count (1..255) */
                      )
{
	SD_Error Status ;
  BYTE status = USBH_MSC_OK;
  if (!count) return RES_PARERR;
//  if (Status & STA_NOINIT) return RES_NOTRDY;
 // if (Status & STA_PROTECT) return RES_WRPRT;
  
	switch (drv)
	{
		case SD_Disc:  
				switch(SD_Mode)
				{
						case 0:  //dma方式
								if(count==1)            // 1个sector的写操作      
								{      
										Status = SD_WriteBlock(sector << 9,(u32 *)(&buff[0]),BlockSize);//sector<<9 扇区地址转为字节地址 一个扇区512字节                                             
								}                                                
							else                    //多个sector的写操作     
								{    
										Status =SD_WriteMultiBlocks(sector << 9,(u32 *)(&buff[0]),BlockSize,count);                                          
								}  	  
						break;

						case 1:  //中断方式
								if(count==1)           // 1个sector的写操作      
								{      
										Status = SD_WriteBlock(sector << 9 ,(u32 *)(&buff[0]),BlockSize);                                            
								}                                                
								else                    //多个sector的写操作     
								{    
										Status = SD_WriteMultiBlocks(sector << 9 ,(u32 *)(&buff[0]),BlockSize,count);                                     
								}  
						break;

					default :
						Status=SD_ERROR;
				}
																						
				//处理返回值，将sdcard.c的返回值转成ff.c的返回值
				if(Status == SD_OK)
						return RES_OK;
				else
						return RES_ERROR;
		case USB_Disc:
			if(HCD_IsDeviceConnected(&USB_OTG_Core))
			{  
				do
				{
					status = USBH_MSC_Write10(&USB_OTG_Core,(BYTE*)buff,sector,512*count);
					USBH_MSC_HandleBOTXfer(&USB_OTG_Core, &USB_Host);
					
					if(!HCD_IsDeviceConnected(&USB_OTG_Core))
					{ 
						return RES_ERROR;
					}
				}
				
				while(status == USBH_MSC_BUSY );
				
			}
			
			if(status == USBH_MSC_OK)
				return RES_OK;
			return RES_ERROR;	
	}
	return RES_OK;
}
#endif /* _READONLY == 0 */



/*-----------------------------------------------------------------------*/
/* Miscellaneous Functions                                               */
/*-----------------------------------------------------------------------*/

#if _USE_IOCTL != 0
DRESULT disk_ioctl (
                    BYTE drv,		/* Physical drive number (0) */
                    BYTE ctrl,		/* Control code */
                    void *buff		/* Buffer to send/receive control data */
                      )
{
  DRESULT res = RES_OK;
  u32 x, y, z;
  res = RES_ERROR;
  
//  if (Stat & STA_NOINIT) return RES_NOTRDY;
	
	if (drv==SD_Disc)
	{   
    	//FATFS目前版本仅需处理CTRL_SYNC，GET_SECTOR_COUNT，GET_BLOCK_SIZ三个命令
    	switch(ctrl)
    	{
     		case CTRL_SYNC:
				if(SD_GetTransferState()==SD_NO_TRANSFER)
         		{
             		res = RES_OK;
         		}
         		else
         		{
             		res = RES_ERROR;
         		}
         		break;
        
     		case GET_BLOCK_SIZE:
				*(WORD*)buff = BlockSize;
         		res = RES_OK;
         		break;

     		case GET_SECTOR_COUNT:     //读卡容量
		        ////formula of the capacity///////////////
		        //
		        //  memory capacity = BLOCKNR * BLOCK_LEN
		        // 
		        // BLOCKNR = (C_SIZE + 1)* MULT
		        //
		        //           C_SIZE_MULT+2
		        // MULT = 2
		        //
		        //               READ_BL_LEN
		        // BLOCK_LEN = 2
		    	//////////////////////////////////////////
     			if (SD_GetCardInfo(&SDCardInfo)==SD_OK)//读sd卡信息
         		{
          			x=SDCardInfo.SD_csd.DeviceSize+1; //C_SIZE + 1
    				y=SDCardInfo.SD_csd.DeviceSizeMul+2; //C_SIZE_MULT+2
    				z=SDCardInfo.SD_csd.RdBlockLen+y;
       				*(DWORD*)buff =x<<z; 
					res = RES_OK;
				}
				else
				{
					res = RES_ERROR ;
				}
				break;

     		default:
				res = RES_PARERR;
    	}
    	return res;
  }
	else if(drv==USB_Disc)
	{
		
		switch (ctrl) 
		{
			case CTRL_SYNC :		/* Make sure that no pending write process */
				
				res = RES_OK;
				break;
				
			case GET_SECTOR_COUNT :	/* Get number of sectors on the disk (DWORD) */
				
				*(DWORD*)buff = (DWORD) USBH_MSC_Param.MSCapacity;
				res = RES_OK;
				break;
				
			case GET_SECTOR_SIZE :	/* Get R/W sector size (WORD) */
				*(WORD*)buff = 512;
				res = RES_OK;
				break;
				
			case GET_BLOCK_SIZE :	/* Get erase block size in unit of sector (DWORD) */
				
				*(DWORD*)buff = 512;
				
				break;
				
				
			default:
				res = RES_PARERR;
		}

	} 
  return res;
}
#endif /* _USE_IOCTL != 0 */
