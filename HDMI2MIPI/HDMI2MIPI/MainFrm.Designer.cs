﻿namespace WindowsFormsApp1
{
    partial class MainFrm
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.tabCtl_Vesa = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.Pixel_Clock = new System.Windows.Forms.NumericUpDown();
            this.label9 = new System.Windows.Forms.Label();
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.txtLCD_CFG = new System.Windows.Forms.TextBox();
            this.chkDEPos = new System.Windows.Forms.CheckBox();
            this.chkclkpos = new System.Windows.Forms.CheckBox();
            this.chkVsync = new System.Windows.Forms.CheckBox();
            this.chkHsync = new System.Windows.Forms.CheckBox();
            this.VFP = new System.Windows.Forms.NumericUpDown();
            this.VSW = new System.Windows.Forms.NumericUpDown();
            this.HSW = new System.Windows.Forms.NumericUpDown();
            this.VBP = new System.Windows.Forms.NumericUpDown();
            this.HBP = new System.Windows.Forms.NumericUpDown();
            this.HFP = new System.Windows.Forms.NumericUpDown();
            this.VACT = new System.Windows.Forms.NumericUpDown();
            this.label6 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.HACT = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.ID_driver_group = new System.Windows.Forms.GroupBox();
            this.btnRun = new System.Windows.Forms.Button();
            this.label664 = new System.Windows.Forms.Label();
            this.SSD28xx_Download = new System.Windows.Forms.Button();
            this.btnMIPIDriverW = new System.Windows.Forms.Button();
            this.txtMIPIDriver = new System.Windows.Forms.TextBox();
            this.label_code_count = new System.Windows.Forms.Label();
            this.lstMIPIDriver = new System.Windows.Forms.ListBox();
            this.button6 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.btnOpenCom = new System.Windows.Forms.Button();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.button3 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this.label33 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.txtINITIAL_FULLNESS_OFFSET = new System.Windows.Forms.MaskedTextBox();
            this.label35 = new System.Windows.Forms.Label();
            this.txtINITIAL_DELAY = new System.Windows.Forms.MaskedTextBox();
            this.label36 = new System.Windows.Forms.Label();
            this.txtFIRST_LINE_BPG_OFFSET = new System.Windows.Forms.MaskedTextBox();
            this.txtFLATNESS_MAX_QP = new System.Windows.Forms.MaskedTextBox();
            this.txtFLATNESS_DET_THRESH = new System.Windows.Forms.MaskedTextBox();
            this.label37 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.txtFLATNESS_MIN_QP = new System.Windows.Forms.MaskedTextBox();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.label24 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.txtRC_MODEL_SIZE = new System.Windows.Forms.MaskedTextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.txtRC_TGT_OFFSET_HI = new System.Windows.Forms.MaskedTextBox();
            this.txtRC_MINQP = new System.Windows.Forms.MaskedTextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.txtRC_MAXQP = new System.Windows.Forms.MaskedTextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.txtRC_BUF_THRESH = new System.Windows.Forms.MaskedTextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtRC_QUANT_INCR_LIMIT1 = new System.Windows.Forms.MaskedTextBox();
            this.txtRC_OFFSET = new System.Windows.Forms.MaskedTextBox();
            this.label29 = new System.Windows.Forms.Label();
            this.txtRC_QUANT_INCR_LIMIT0 = new System.Windows.Forms.MaskedTextBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.cmbxBITS_PER_PIXEL = new System.Windows.Forms.ComboBox();
            this.label30 = new System.Windows.Forms.Label();
            this.cmbxBITS_PER_COMPONENT = new System.Windows.Forms.ComboBox();
            this.label31 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.checkedListBox1 = new System.Windows.Forms.CheckedListBox();
            this.txtLINE_BUFFER_BPC = new System.Windows.Forms.MaskedTextBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.radioButton4 = new System.Windows.Forms.RadioButton();
            this.radioButton3 = new System.Windows.Forms.RadioButton();
            this.txtSliceWidth = new System.Windows.Forms.MaskedTextBox();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.txtDisplayWidth = new System.Windows.Forms.MaskedTextBox();
            this.txtDisplayHeight = new System.Windows.Forms.MaskedTextBox();
            this.txtSliceHeight = new System.Windows.Forms.MaskedTextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.listBox3_BMPFile = new System.Windows.Forms.ListBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.btnReload = new System.Windows.Forms.Button();
            this.groupBox15 = new System.Windows.Forms.GroupBox();
            this.label21 = new System.Windows.Forms.Label();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.btnPatternW = new System.Windows.Forms.Button();
            this.label20 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.numPatternB = new System.Windows.Forms.NumericUpDown();
            this.numPatternG = new System.Windows.Forms.NumericUpDown();
            this.numPatternR = new System.Windows.Forms.NumericUpDown();
            this.btnbleak = new System.Windows.Forms.Button();
            this.btnred = new System.Windows.Forms.Button();
            this.btnwhite = new System.Windows.Forms.Button();
            this.btngreen = new System.Windows.Forms.Button();
            this.btnblue = new System.Windows.Forms.Button();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.Y_VALUE = new System.Windows.Forms.Label();
            this.X_VALUE = new System.Windows.Forms.Label();
            this.btnLCD_Set_Disp_Window_cfg = new System.Windows.Forms.Button();
            this.label416 = new System.Windows.Forms.Label();
            this.label412 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.X0 = new System.Windows.Forms.Label();
            this.YY1 = new System.Windows.Forms.NumericUpDown();
            this.XX0 = new System.Windows.Forms.NumericUpDown();
            this.YY0 = new System.Windows.Forms.NumericUpDown();
            this.XX1 = new System.Windows.Forms.NumericUpDown();
            this.serialPort1 = new System.IO.Ports.SerialPort(this.components);
            this.LabMOdelSelect = new System.Windows.Forms.Label();
            this.button7 = new System.Windows.Forms.Button();
            this.txtRC_TGT_OFFSET_LO = new System.Windows.Forms.MaskedTextBox();
            this.txtRC_EDGE_FACTOR = new System.Windows.Forms.MaskedTextBox();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.tabCtl_Vesa.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Pixel_Clock)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.VFP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.VSW)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.HSW)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.VBP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.HBP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.HFP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.VACT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.HACT)).BeginInit();
            this.ID_driver_group.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.groupBox11.SuspendLayout();
            this.groupBox9.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.groupBox15.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numPatternB)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numPatternG)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numPatternR)).BeginInit();
            this.groupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.YY1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.XX0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.YY0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.XX1)).BeginInit();
            this.SuspendLayout();
            // 
            // tabCtl_Vesa
            // 
            this.tabCtl_Vesa.Controls.Add(this.tabPage1);
            this.tabCtl_Vesa.Controls.Add(this.tabPage2);
            this.tabCtl_Vesa.Location = new System.Drawing.Point(12, 12);
            this.tabCtl_Vesa.Name = "tabCtl_Vesa";
            this.tabCtl_Vesa.SelectedIndex = 0;
            this.tabCtl_Vesa.Size = new System.Drawing.Size(697, 556);
            this.tabCtl_Vesa.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.button7);
            this.tabPage1.Controls.Add(this.groupBox3);
            this.tabPage1.Controls.Add(this.ID_driver_group);
            this.tabPage1.Controls.Add(this.button5);
            this.tabPage1.Controls.Add(this.button4);
            this.tabPage1.Controls.Add(this.btnOpenCom);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(689, 530);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Timing";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.LabMOdelSelect);
            this.groupBox3.Controls.Add(this.Pixel_Clock);
            this.groupBox3.Controls.Add(this.label9);
            this.groupBox3.Controls.Add(this.checkBox2);
            this.groupBox3.Controls.Add(this.txtLCD_CFG);
            this.groupBox3.Controls.Add(this.chkDEPos);
            this.groupBox3.Controls.Add(this.chkclkpos);
            this.groupBox3.Controls.Add(this.chkVsync);
            this.groupBox3.Controls.Add(this.chkHsync);
            this.groupBox3.Controls.Add(this.VFP);
            this.groupBox3.Controls.Add(this.VSW);
            this.groupBox3.Controls.Add(this.HSW);
            this.groupBox3.Controls.Add(this.VBP);
            this.groupBox3.Controls.Add(this.HBP);
            this.groupBox3.Controls.Add(this.HFP);
            this.groupBox3.Controls.Add(this.VACT);
            this.groupBox3.Controls.Add(this.label6);
            this.groupBox3.Controls.Add(this.label2);
            this.groupBox3.Controls.Add(this.label11);
            this.groupBox3.Controls.Add(this.label5);
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.Controls.Add(this.label8);
            this.groupBox3.Controls.Add(this.HACT);
            this.groupBox3.Controls.Add(this.label1);
            this.groupBox3.Controls.Add(this.label3);
            this.groupBox3.Controls.Add(this.label4);
            this.groupBox3.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.groupBox3.Location = new System.Drawing.Point(19, 6);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(557, 157);
            this.groupBox3.TabIndex = 94;
            this.groupBox3.TabStop = false;
            // 
            // Pixel_Clock
            // 
            this.Pixel_Clock.DecimalPlaces = 1;
            this.Pixel_Clock.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.Pixel_Clock.Location = new System.Drawing.Point(437, 123);
            this.Pixel_Clock.Maximum = new decimal(new int[] {
            200,
            0,
            0,
            0});
            this.Pixel_Clock.Name = "Pixel_Clock";
            this.Pixel_Clock.Size = new System.Drawing.Size(60, 26);
            this.Pixel_Clock.TabIndex = 23;
            this.Pixel_Clock.Value = new decimal(new int[] {
            25,
            0,
            0,
            0});
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(387, 131);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(46, 20);
            this.label9.TabIndex = 0;
            this.label9.Text = "PCLK:";
            // 
            // checkBox2
            // 
            this.checkBox2.AutoSize = true;
            this.checkBox2.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.checkBox2.Location = new System.Drawing.Point(6, 125);
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new System.Drawing.Size(113, 24);
            this.checkBox2.TabIndex = 29;
            this.checkBox2.Text = "VESA Enable";
            this.checkBox2.UseVisualStyleBackColor = true;
            // 
            // txtLCD_CFG
            // 
            this.txtLCD_CFG.Location = new System.Drawing.Point(196, 94);
            this.txtLCD_CFG.Name = "txtLCD_CFG";
            this.txtLCD_CFG.ReadOnly = true;
            this.txtLCD_CFG.Size = new System.Drawing.Size(47, 26);
            this.txtLCD_CFG.TabIndex = 97;
            this.txtLCD_CFG.Text = "0";
            // 
            // chkDEPos
            // 
            this.chkDEPos.AutoSize = true;
            this.chkDEPos.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.chkDEPos.Location = new System.Drawing.Point(333, 96);
            this.chkDEPos.Name = "chkDEPos";
            this.chkDEPos.Size = new System.Drawing.Size(63, 24);
            this.chkDEPos.TabIndex = 32;
            this.chkDEPos.Text = "DETE";
            this.chkDEPos.UseVisualStyleBackColor = true;
            // 
            // chkclkpos
            // 
            this.chkclkpos.AutoSize = true;
            this.chkclkpos.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.chkclkpos.Location = new System.Drawing.Point(249, 96);
            this.chkclkpos.Name = "chkclkpos";
            this.chkclkpos.Size = new System.Drawing.Size(78, 24);
            this.chkclkpos.TabIndex = 31;
            this.chkclkpos.Text = "VCLKTE";
            this.chkclkpos.UseVisualStyleBackColor = true;
            // 
            // chkVsync
            // 
            this.chkVsync.AutoSize = true;
            this.chkVsync.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.chkVsync.Location = new System.Drawing.Point(471, 96);
            this.chkVsync.Name = "chkVsync";
            this.chkVsync.Size = new System.Drawing.Size(61, 24);
            this.chkVsync.TabIndex = 29;
            this.chkVsync.Text = "VSTE";
            this.chkVsync.UseVisualStyleBackColor = true;
            // 
            // chkHsync
            // 
            this.chkHsync.AutoSize = true;
            this.chkHsync.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.chkHsync.Location = new System.Drawing.Point(402, 96);
            this.chkHsync.Name = "chkHsync";
            this.chkHsync.Size = new System.Drawing.Size(63, 24);
            this.chkHsync.TabIndex = 30;
            this.chkHsync.Text = "HSTE";
            this.chkHsync.UseVisualStyleBackColor = true;
            // 
            // VFP
            // 
            this.VFP.Location = new System.Drawing.Point(437, 60);
            this.VFP.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.VFP.Name = "VFP";
            this.VFP.Size = new System.Drawing.Size(60, 26);
            this.VFP.TabIndex = 25;
            // 
            // VSW
            // 
            this.VSW.Location = new System.Drawing.Point(213, 55);
            this.VSW.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.VSW.Name = "VSW";
            this.VSW.Size = new System.Drawing.Size(60, 26);
            this.VSW.TabIndex = 28;
            // 
            // HSW
            // 
            this.HSW.Location = new System.Drawing.Point(213, 18);
            this.HSW.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.HSW.Name = "HSW";
            this.HSW.Size = new System.Drawing.Size(60, 26);
            this.HSW.TabIndex = 27;
            // 
            // VBP
            // 
            this.VBP.Location = new System.Drawing.Point(326, 55);
            this.VBP.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.VBP.Name = "VBP";
            this.VBP.Size = new System.Drawing.Size(60, 26);
            this.VBP.TabIndex = 26;
            // 
            // HBP
            // 
            this.HBP.Location = new System.Drawing.Point(326, 18);
            this.HBP.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.HBP.Name = "HBP";
            this.HBP.Size = new System.Drawing.Size(60, 26);
            this.HBP.TabIndex = 25;
            // 
            // HFP
            // 
            this.HFP.Location = new System.Drawing.Point(437, 18);
            this.HFP.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.HFP.Name = "HFP";
            this.HFP.Size = new System.Drawing.Size(60, 26);
            this.HFP.TabIndex = 23;
            // 
            // VACT
            // 
            this.VACT.Location = new System.Drawing.Point(52, 55);
            this.VACT.Maximum = new decimal(new int[] {
            4096,
            0,
            0,
            0});
            this.VACT.Name = "VACT";
            this.VACT.Size = new System.Drawing.Size(76, 26);
            this.VACT.TabIndex = 22;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(392, 60);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(41, 20);
            this.label6.TabIndex = 18;
            this.label6.Text = "V FP:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(392, 20);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(39, 20);
            this.label2.TabIndex = 0;
            this.label2.Text = "HFP:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(2, 97);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(187, 20);
            this.label11.TabIndex = 21;
            this.label11.Text = "触发沿默认(未勾选)为上升沿";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(2, 57);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(48, 20);
            this.label5.TabIndex = 21;
            this.label5.Text = "VACT:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(279, 57);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(43, 20);
            this.label7.TabIndex = 19;
            this.label7.Text = "V BP:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(160, 60);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(47, 20);
            this.label8.TabIndex = 20;
            this.label8.Text = "V SW:";
            // 
            // HACT
            // 
            this.HACT.Location = new System.Drawing.Point(52, 18);
            this.HACT.Maximum = new decimal(new int[] {
            4096,
            0,
            0,
            0});
            this.HACT.Name = "HACT";
            this.HACT.Size = new System.Drawing.Size(76, 26);
            this.HACT.TabIndex = 17;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(2, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(50, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "HACT:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(279, 20);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(41, 20);
            this.label3.TabIndex = 0;
            this.label3.Text = "HBP:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(162, 20);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(45, 20);
            this.label4.TabIndex = 0;
            this.label4.Text = "HSW:";
            // 
            // ID_driver_group
            // 
            this.ID_driver_group.Controls.Add(this.btnRun);
            this.ID_driver_group.Controls.Add(this.label664);
            this.ID_driver_group.Controls.Add(this.SSD28xx_Download);
            this.ID_driver_group.Controls.Add(this.button6);
            this.ID_driver_group.Controls.Add(this.btnMIPIDriverW);
            this.ID_driver_group.Controls.Add(this.txtMIPIDriver);
            this.ID_driver_group.Controls.Add(this.label_code_count);
            this.ID_driver_group.Controls.Add(this.lstMIPIDriver);
            this.ID_driver_group.Enabled = false;
            this.ID_driver_group.Font = new System.Drawing.Font("微软雅黑", 8F);
            this.ID_driver_group.Location = new System.Drawing.Point(19, 209);
            this.ID_driver_group.Name = "ID_driver_group";
            this.ID_driver_group.Size = new System.Drawing.Size(591, 311);
            this.ID_driver_group.TabIndex = 92;
            this.ID_driver_group.TabStop = false;
            this.ID_driver_group.Text = "Driver IC";
            // 
            // btnRun
            // 
            this.btnRun.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.btnRun.Font = new System.Drawing.Font("Book Antiqua", 10.5F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRun.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.btnRun.Location = new System.Drawing.Point(317, 266);
            this.btnRun.Name = "btnRun";
            this.btnRun.Size = new System.Drawing.Size(180, 39);
            this.btnRun.TabIndex = 10;
            this.btnRun.Text = "RUN";
            this.btnRun.UseVisualStyleBackColor = false;
            // 
            // label664
            // 
            this.label664.AutoSize = true;
            this.label664.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label664.Location = new System.Drawing.Point(7, 515);
            this.label664.Name = "label664";
            this.label664.Size = new System.Drawing.Size(0, 20);
            this.label664.TabIndex = 13;
            // 
            // SSD28xx_Download
            // 
            this.SSD28xx_Download.BackColor = System.Drawing.Color.White;
            this.SSD28xx_Download.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.SSD28xx_Download.ForeColor = System.Drawing.Color.Red;
            this.SSD28xx_Download.Location = new System.Drawing.Point(81, 271);
            this.SSD28xx_Download.Name = "SSD28xx_Download";
            this.SSD28xx_Download.Size = new System.Drawing.Size(50, 23);
            this.SSD28xx_Download.TabIndex = 12;
            this.SSD28xx_Download.Text = "一键";
            this.SSD28xx_Download.UseVisualStyleBackColor = false;
            // 
            // btnMIPIDriverW
            // 
            this.btnMIPIDriverW.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btnMIPIDriverW.Location = new System.Drawing.Point(11, 272);
            this.btnMIPIDriverW.Name = "btnMIPIDriverW";
            this.btnMIPIDriverW.Size = new System.Drawing.Size(51, 22);
            this.btnMIPIDriverW.TabIndex = 10;
            this.btnMIPIDriverW.Text = "写入";
            this.btnMIPIDriverW.UseVisualStyleBackColor = true;
            // 
            // txtMIPIDriver
            // 
            this.txtMIPIDriver.Location = new System.Drawing.Point(6, 205);
            this.txtMIPIDriver.Name = "txtMIPIDriver";
            this.txtMIPIDriver.Size = new System.Drawing.Size(501, 22);
            this.txtMIPIDriver.TabIndex = 11;
            // 
            // label_code_count
            // 
            this.label_code_count.AutoSize = true;
            this.label_code_count.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label_code_count.Location = new System.Drawing.Point(60, 274);
            this.label_code_count.Name = "label_code_count";
            this.label_code_count.Size = new System.Drawing.Size(18, 20);
            this.label_code_count.TabIndex = 13;
            this.label_code_count.Text = "n";
            // 
            // lstMIPIDriver
            // 
            this.lstMIPIDriver.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lstMIPIDriver.FormattingEnabled = true;
            this.lstMIPIDriver.HorizontalScrollbar = true;
            this.lstMIPIDriver.ItemHeight = 20;
            this.lstMIPIDriver.Location = new System.Drawing.Point(5, 20);
            this.lstMIPIDriver.Name = "lstMIPIDriver";
            this.lstMIPIDriver.Size = new System.Drawing.Size(501, 164);
            this.lstMIPIDriver.TabIndex = 9;
            // 
            // button6
            // 
            this.button6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button6.BackColor = System.Drawing.SystemColors.Window;
            this.button6.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.button6.Location = new System.Drawing.Point(196, 275);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(88, 30);
            this.button6.TabIndex = 95;
            this.button6.Text = "Download";
            this.button6.UseVisualStyleBackColor = false;
            // 
            // button5
            // 
            this.button5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button5.BackColor = System.Drawing.SystemColors.Window;
            this.button5.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.button5.Location = new System.Drawing.Point(582, 93);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(88, 30);
            this.button5.TabIndex = 95;
            this.button5.Text = "Load";
            this.button5.UseVisualStyleBackColor = false;
            // 
            // button4
            // 
            this.button4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button4.BackColor = System.Drawing.SystemColors.Window;
            this.button4.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.button4.Location = new System.Drawing.Point(582, 45);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(88, 30);
            this.button4.TabIndex = 95;
            this.button4.Text = "reference CFG";
            this.button4.UseVisualStyleBackColor = false;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // btnOpenCom
            // 
            this.btnOpenCom.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOpenCom.BackColor = System.Drawing.SystemColors.Window;
            this.btnOpenCom.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btnOpenCom.Location = new System.Drawing.Point(582, 9);
            this.btnOpenCom.Name = "btnOpenCom";
            this.btnOpenCom.Size = new System.Drawing.Size(88, 30);
            this.btnOpenCom.TabIndex = 95;
            this.btnOpenCom.Text = "打开串口";
            this.btnOpenCom.UseVisualStyleBackColor = false;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.button3);
            this.tabPage2.Controls.Add(this.button2);
            this.tabPage2.Controls.Add(this.button1);
            this.tabPage2.Controls.Add(this.groupBox11);
            this.tabPage2.Controls.Add(this.groupBox9);
            this.tabPage2.Controls.Add(this.groupBox5);
            this.tabPage2.Controls.Add(this.groupBox4);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(689, 530);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "VESA PPS Setting";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(481, 386);
            this.button3.Margin = new System.Windows.Forms.Padding(2);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(77, 24);
            this.button3.TabIndex = 11;
            this.button3.Text = "Save as";
            this.button3.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(392, 386);
            this.button2.Margin = new System.Windows.Forms.Padding(2);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(77, 24);
            this.button2.TabIndex = 11;
            this.button2.Text = "Save";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(303, 386);
            this.button1.Margin = new System.Windows.Forms.Padding(2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(77, 24);
            this.button1.TabIndex = 11;
            this.button1.Text = "Load PPS";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // groupBox11
            // 
            this.groupBox11.Controls.Add(this.label33);
            this.groupBox11.Controls.Add(this.label34);
            this.groupBox11.Controls.Add(this.txtINITIAL_FULLNESS_OFFSET);
            this.groupBox11.Controls.Add(this.label35);
            this.groupBox11.Controls.Add(this.txtINITIAL_DELAY);
            this.groupBox11.Controls.Add(this.label36);
            this.groupBox11.Controls.Add(this.txtFIRST_LINE_BPG_OFFSET);
            this.groupBox11.Controls.Add(this.txtFLATNESS_MAX_QP);
            this.groupBox11.Controls.Add(this.txtFLATNESS_DET_THRESH);
            this.groupBox11.Controls.Add(this.label37);
            this.groupBox11.Controls.Add(this.label38);
            this.groupBox11.Controls.Add(this.txtFLATNESS_MIN_QP);
            this.groupBox11.Location = new System.Drawing.Point(384, 11);
            this.groupBox11.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox11.Size = new System.Drawing.Size(303, 180);
            this.groupBox11.TabIndex = 10;
            this.groupBox11.TabStop = false;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(4, 10);
            this.label33.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(143, 12);
            this.label33.TabIndex = 0;
            this.label33.Text = "INITIAL_FULLNESS_OFFSET";
            this.label33.Click += new System.EventHandler(this.label10_Click);
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(4, 35);
            this.label34.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(83, 12);
            this.label34.TabIndex = 0;
            this.label34.Text = "INITIAL_DELAY";
            this.label34.Click += new System.EventHandler(this.label10_Click);
            // 
            // txtINITIAL_FULLNESS_OFFSET
            // 
            this.txtINITIAL_FULLNESS_OFFSET.Location = new System.Drawing.Point(149, 8);
            this.txtINITIAL_FULLNESS_OFFSET.Margin = new System.Windows.Forms.Padding(2);
            this.txtINITIAL_FULLNESS_OFFSET.Name = "txtINITIAL_FULLNESS_OFFSET";
            this.txtINITIAL_FULLNESS_OFFSET.Size = new System.Drawing.Size(68, 21);
            this.txtINITIAL_FULLNESS_OFFSET.TabIndex = 4;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label35.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.label35.Location = new System.Drawing.Point(3, 59);
            this.label35.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(149, 17);
            this.label35.TabIndex = 0;
            this.label35.Text = "FIRST_LINE_BPG_OFFSET";
            this.toolTip1.SetToolTip(this.label35, "Code determines recommended value based on slice height");
            this.label35.Click += new System.EventHandler(this.label10_Click);
            // 
            // txtINITIAL_DELAY
            // 
            this.txtINITIAL_DELAY.Location = new System.Drawing.Point(149, 33);
            this.txtINITIAL_DELAY.Margin = new System.Windows.Forms.Padding(2);
            this.txtINITIAL_DELAY.Name = "txtINITIAL_DELAY";
            this.txtINITIAL_DELAY.Size = new System.Drawing.Size(68, 21);
            this.txtINITIAL_DELAY.TabIndex = 4;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(4, 109);
            this.label36.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(95, 12);
            this.label36.TabIndex = 0;
            this.label36.Text = "FLATNESS_MIN_QP";
            this.label36.Click += new System.EventHandler(this.label10_Click);
            // 
            // txtFIRST_LINE_BPG_OFFSET
            // 
            this.txtFIRST_LINE_BPG_OFFSET.Location = new System.Drawing.Point(156, 59);
            this.txtFIRST_LINE_BPG_OFFSET.Margin = new System.Windows.Forms.Padding(2);
            this.txtFIRST_LINE_BPG_OFFSET.Name = "txtFIRST_LINE_BPG_OFFSET";
            this.txtFIRST_LINE_BPG_OFFSET.Size = new System.Drawing.Size(61, 21);
            this.txtFIRST_LINE_BPG_OFFSET.TabIndex = 4;
            this.txtFIRST_LINE_BPG_OFFSET.Text = "15";
            // 
            // txtFLATNESS_MAX_QP
            // 
            this.txtFLATNESS_MAX_QP.Location = new System.Drawing.Point(149, 131);
            this.txtFLATNESS_MAX_QP.Margin = new System.Windows.Forms.Padding(2);
            this.txtFLATNESS_MAX_QP.Name = "txtFLATNESS_MAX_QP";
            this.txtFLATNESS_MAX_QP.Size = new System.Drawing.Size(68, 21);
            this.txtFLATNESS_MAX_QP.TabIndex = 4;
            // 
            // txtFLATNESS_DET_THRESH
            // 
            this.txtFLATNESS_DET_THRESH.Location = new System.Drawing.Point(149, 157);
            this.txtFLATNESS_DET_THRESH.Margin = new System.Windows.Forms.Padding(2);
            this.txtFLATNESS_DET_THRESH.Name = "txtFLATNESS_DET_THRESH";
            this.txtFLATNESS_DET_THRESH.Size = new System.Drawing.Size(68, 21);
            this.txtFLATNESS_DET_THRESH.TabIndex = 4;
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(4, 159);
            this.label37.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(119, 12);
            this.label37.TabIndex = 0;
            this.label37.Text = "FLATNESS_DET_THRESH";
            this.label37.Click += new System.EventHandler(this.label10_Click);
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(4, 133);
            this.label38.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(95, 12);
            this.label38.TabIndex = 0;
            this.label38.Text = "FLATNESS_MAX_QP";
            this.label38.Click += new System.EventHandler(this.label10_Click);
            // 
            // txtFLATNESS_MIN_QP
            // 
            this.txtFLATNESS_MIN_QP.Location = new System.Drawing.Point(149, 107);
            this.txtFLATNESS_MIN_QP.Margin = new System.Windows.Forms.Padding(2);
            this.txtFLATNESS_MIN_QP.Name = "txtFLATNESS_MIN_QP";
            this.txtFLATNESS_MIN_QP.Size = new System.Drawing.Size(68, 21);
            this.txtFLATNESS_MIN_QP.TabIndex = 4;
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.label24);
            this.groupBox9.Controls.Add(this.label23);
            this.groupBox9.Controls.Add(this.label25);
            this.groupBox9.Controls.Add(this.txtRC_MODEL_SIZE);
            this.groupBox9.Controls.Add(this.label26);
            this.groupBox9.Controls.Add(this.label22);
            this.groupBox9.Controls.Add(this.txtRC_EDGE_FACTOR);
            this.groupBox9.Controls.Add(this.txtRC_TGT_OFFSET_LO);
            this.groupBox9.Controls.Add(this.txtRC_TGT_OFFSET_HI);
            this.groupBox9.Controls.Add(this.txtRC_MINQP);
            this.groupBox9.Controls.Add(this.label27);
            this.groupBox9.Controls.Add(this.label13);
            this.groupBox9.Controls.Add(this.txtRC_MAXQP);
            this.groupBox9.Controls.Add(this.label28);
            this.groupBox9.Controls.Add(this.txtRC_BUF_THRESH);
            this.groupBox9.Controls.Add(this.label10);
            this.groupBox9.Controls.Add(this.txtRC_QUANT_INCR_LIMIT1);
            this.groupBox9.Controls.Add(this.txtRC_OFFSET);
            this.groupBox9.Controls.Add(this.label29);
            this.groupBox9.Controls.Add(this.txtRC_QUANT_INCR_LIMIT0);
            this.groupBox9.Location = new System.Drawing.Point(5, 202);
            this.groupBox9.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox9.Size = new System.Drawing.Size(683, 172);
            this.groupBox9.TabIndex = 10;
            this.groupBox9.TabStop = false;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(167, 26);
            this.label24.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(83, 12);
            this.label24.TabIndex = 0;
            this.label24.Text = "RC_MODEL_SIZE";
            this.label24.Click += new System.EventHandler(this.label10_Click);
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(167, 49);
            this.label23.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(53, 12);
            this.label23.TabIndex = 0;
            this.label23.Text = "RC_MINQP";
            this.label23.Click += new System.EventHandler(this.label10_Click);
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(5, 26);
            this.label25.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(101, 12);
            this.label25.TabIndex = 0;
            this.label25.Text = "RC_TGT_OFFSET_HI";
            this.label25.Click += new System.EventHandler(this.label10_Click);
            // 
            // txtRC_MODEL_SIZE
            // 
            this.txtRC_MODEL_SIZE.Location = new System.Drawing.Point(255, 24);
            this.txtRC_MODEL_SIZE.Margin = new System.Windows.Forms.Padding(2);
            this.txtRC_MODEL_SIZE.Name = "txtRC_MODEL_SIZE";
            this.txtRC_MODEL_SIZE.Size = new System.Drawing.Size(188, 21);
            this.txtRC_MODEL_SIZE.TabIndex = 4;
            this.txtRC_MODEL_SIZE.Text = "9999";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(5, 49);
            this.label26.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(101, 12);
            this.label26.TabIndex = 0;
            this.label26.Text = "RC_TGT_OFFSET_LO";
            this.label26.Click += new System.EventHandler(this.label10_Click);
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(167, 75);
            this.label22.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(53, 12);
            this.label22.TabIndex = 0;
            this.label22.Text = "RC_MAXQP";
            this.label22.Click += new System.EventHandler(this.label10_Click);
            // 
            // txtRC_TGT_OFFSET_HI
            // 
            this.txtRC_TGT_OFFSET_HI.Location = new System.Drawing.Point(110, 24);
            this.txtRC_TGT_OFFSET_HI.Margin = new System.Windows.Forms.Padding(2);
            this.txtRC_TGT_OFFSET_HI.Name = "txtRC_TGT_OFFSET_HI";
            this.txtRC_TGT_OFFSET_HI.Size = new System.Drawing.Size(53, 21);
            this.txtRC_TGT_OFFSET_HI.TabIndex = 4;
            this.txtRC_TGT_OFFSET_HI.Text = "3";
            this.txtRC_TGT_OFFSET_HI.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtRC_MINQP
            // 
            this.txtRC_MINQP.Location = new System.Drawing.Point(253, 47);
            this.txtRC_MINQP.Margin = new System.Windows.Forms.Padding(2);
            this.txtRC_MINQP.Name = "txtRC_MINQP";
            this.txtRC_MINQP.Size = new System.Drawing.Size(299, 21);
            this.txtRC_MINQP.TabIndex = 4;
            this.txtRC_MINQP.Text = "0,0,1,1,3,3,3,3,3,3,5,5,5,9,12";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(5, 75);
            this.label27.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(89, 12);
            this.label27.TabIndex = 0;
            this.label27.Text = "RC_EDGE_FACTOR";
            this.label27.Click += new System.EventHandler(this.label10_Click);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(167, 101);
            this.label13.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(59, 12);
            this.label13.TabIndex = 0;
            this.label13.Text = "RC_OFFSET";
            this.label13.Click += new System.EventHandler(this.label10_Click);
            // 
            // txtRC_MAXQP
            // 
            this.txtRC_MAXQP.Location = new System.Drawing.Point(255, 73);
            this.txtRC_MAXQP.Margin = new System.Windows.Forms.Padding(2);
            this.txtRC_MAXQP.Name = "txtRC_MAXQP";
            this.txtRC_MAXQP.Size = new System.Drawing.Size(299, 21);
            this.txtRC_MAXQP.TabIndex = 4;
            this.txtRC_MAXQP.Text = "4,4,5,6,7,7,7,8,9,10,10,11,11,12,13";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(5, 101);
            this.label28.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(125, 12);
            this.label28.TabIndex = 0;
            this.label28.Text = "RC_QUANT_INCR_LIMIT0";
            this.label28.Click += new System.EventHandler(this.label10_Click);
            // 
            // txtRC_BUF_THRESH
            // 
            this.txtRC_BUF_THRESH.Location = new System.Drawing.Point(255, 121);
            this.txtRC_BUF_THRESH.Margin = new System.Windows.Forms.Padding(2);
            this.txtRC_BUF_THRESH.Name = "txtRC_BUF_THRESH";
            this.txtRC_BUF_THRESH.Size = new System.Drawing.Size(425, 21);
            this.txtRC_BUF_THRESH.TabIndex = 4;
            this.txtRC_BUF_THRESH.Text = "896,1792,2688,3584,4480,5376,6272,6720,7168,7616,7744,7872,8000,8064  ";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(167, 123);
            this.label10.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(83, 12);
            this.label10.TabIndex = 0;
            this.label10.Text = "RC_BUF_THRESH";
            this.label10.Click += new System.EventHandler(this.label10_Click);
            // 
            // txtRC_QUANT_INCR_LIMIT1
            // 
            this.txtRC_QUANT_INCR_LIMIT1.Location = new System.Drawing.Point(133, 121);
            this.txtRC_QUANT_INCR_LIMIT1.Margin = new System.Windows.Forms.Padding(2);
            this.txtRC_QUANT_INCR_LIMIT1.Name = "txtRC_QUANT_INCR_LIMIT1";
            this.txtRC_QUANT_INCR_LIMIT1.Size = new System.Drawing.Size(31, 21);
            this.txtRC_QUANT_INCR_LIMIT1.TabIndex = 4;
            this.txtRC_QUANT_INCR_LIMIT1.Text = "11";
            this.txtRC_QUANT_INCR_LIMIT1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtRC_OFFSET
            // 
            this.txtRC_OFFSET.Location = new System.Drawing.Point(253, 99);
            this.txtRC_OFFSET.Margin = new System.Windows.Forms.Padding(2);
            this.txtRC_OFFSET.Name = "txtRC_OFFSET";
            this.txtRC_OFFSET.Size = new System.Drawing.Size(299, 21);
            this.txtRC_OFFSET.TabIndex = 4;
            this.txtRC_OFFSET.Text = "2,0,0,-2,-4,-6,-8,-8,-8,-10,-10,-12,-12,-12,-12";
            this.txtRC_OFFSET.MaskInputRejected += new System.Windows.Forms.MaskInputRejectedEventHandler(this.maskedTextBox1_MaskInputRejected);
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(5, 125);
            this.label29.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(125, 12);
            this.label29.TabIndex = 0;
            this.label29.Text = "RC_QUANT_INCR_LIMIT1";
            this.label29.Click += new System.EventHandler(this.label10_Click);
            // 
            // txtRC_QUANT_INCR_LIMIT0
            // 
            this.txtRC_QUANT_INCR_LIMIT0.Location = new System.Drawing.Point(133, 99);
            this.txtRC_QUANT_INCR_LIMIT0.Margin = new System.Windows.Forms.Padding(2);
            this.txtRC_QUANT_INCR_LIMIT0.Name = "txtRC_QUANT_INCR_LIMIT0";
            this.txtRC_QUANT_INCR_LIMIT0.Size = new System.Drawing.Size(31, 21);
            this.txtRC_QUANT_INCR_LIMIT0.TabIndex = 4;
            this.txtRC_QUANT_INCR_LIMIT0.Text = "11";
            this.txtRC_QUANT_INCR_LIMIT0.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.cmbxBITS_PER_PIXEL);
            this.groupBox5.Controls.Add(this.label30);
            this.groupBox5.Controls.Add(this.cmbxBITS_PER_COMPONENT);
            this.groupBox5.Controls.Add(this.label31);
            this.groupBox5.Controls.Add(this.label32);
            this.groupBox5.Controls.Add(this.checkedListBox1);
            this.groupBox5.Controls.Add(this.txtLINE_BUFFER_BPC);
            this.groupBox5.Location = new System.Drawing.Point(197, 8);
            this.groupBox5.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox5.Size = new System.Drawing.Size(185, 183);
            this.groupBox5.TabIndex = 7;
            this.groupBox5.TabStop = false;
            // 
            // cmbxBITS_PER_PIXEL
            // 
            this.cmbxBITS_PER_PIXEL.FormattingEnabled = true;
            this.cmbxBITS_PER_PIXEL.Items.AddRange(new object[] {
            "4",
            "8",
            "16",
            "24"});
            this.cmbxBITS_PER_PIXEL.Location = new System.Drawing.Point(115, 56);
            this.cmbxBITS_PER_PIXEL.Margin = new System.Windows.Forms.Padding(2);
            this.cmbxBITS_PER_PIXEL.Name = "cmbxBITS_PER_PIXEL";
            this.cmbxBITS_PER_PIXEL.Size = new System.Drawing.Size(68, 20);
            this.cmbxBITS_PER_PIXEL.TabIndex = 10;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(4, 14);
            this.label30.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(95, 12);
            this.label30.TabIndex = 0;
            this.label30.Text = "LINE_BUFFER_BPC";
            this.label30.Click += new System.EventHandler(this.label10_Click);
            // 
            // cmbxBITS_PER_COMPONENT
            // 
            this.cmbxBITS_PER_COMPONENT.FormattingEnabled = true;
            this.cmbxBITS_PER_COMPONENT.Items.AddRange(new object[] {
            "4",
            "8",
            "16",
            "24"});
            this.cmbxBITS_PER_COMPONENT.Location = new System.Drawing.Point(115, 35);
            this.cmbxBITS_PER_COMPONENT.Margin = new System.Windows.Forms.Padding(2);
            this.cmbxBITS_PER_COMPONENT.Name = "cmbxBITS_PER_COMPONENT";
            this.cmbxBITS_PER_COMPONENT.Size = new System.Drawing.Size(68, 20);
            this.cmbxBITS_PER_COMPONENT.TabIndex = 10;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(4, 37);
            this.label31.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(113, 12);
            this.label31.TabIndex = 0;
            this.label31.Text = "BITS_PER_COMPONENT";
            this.label31.Click += new System.EventHandler(this.label10_Click);
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(4, 61);
            this.label32.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(89, 12);
            this.label32.TabIndex = 0;
            this.label32.Text = "BITS_PER_PIXEL";
            this.label32.Click += new System.EventHandler(this.label10_Click);
            // 
            // checkedListBox1
            // 
            this.checkedListBox1.FormattingEnabled = true;
            this.checkedListBox1.Items.AddRange(new object[] {
            "BLOCK_PRED_ENABLE",
            "ENABLE_422",
            "USE_YUV_INPUT",
            "VBR_ENABLE"});
            this.checkedListBox1.Location = new System.Drawing.Point(6, 98);
            this.checkedListBox1.Margin = new System.Windows.Forms.Padding(2);
            this.checkedListBox1.Name = "checkedListBox1";
            this.checkedListBox1.Size = new System.Drawing.Size(177, 68);
            this.checkedListBox1.TabIndex = 8;
            // 
            // txtLINE_BUFFER_BPC
            // 
            this.txtLINE_BUFFER_BPC.Location = new System.Drawing.Point(115, 12);
            this.txtLINE_BUFFER_BPC.Margin = new System.Windows.Forms.Padding(2);
            this.txtLINE_BUFFER_BPC.Mask = "9";
            this.txtLINE_BUFFER_BPC.Name = "txtLINE_BUFFER_BPC";
            this.txtLINE_BUFFER_BPC.PromptChar = ' ';
            this.txtLINE_BUFFER_BPC.Size = new System.Drawing.Size(68, 21);
            this.txtLINE_BUFFER_BPC.TabIndex = 4;
            this.txtLINE_BUFFER_BPC.Text = "9";
            this.txtLINE_BUFFER_BPC.ValidatingType = typeof(int);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.groupBox2);
            this.groupBox4.Controls.Add(this.txtDisplayWidth);
            this.groupBox4.Controls.Add(this.txtDisplayHeight);
            this.groupBox4.Controls.Add(this.txtSliceHeight);
            this.groupBox4.Controls.Add(this.label19);
            this.groupBox4.Controls.Add(this.label17);
            this.groupBox4.Controls.Add(this.label16);
            this.groupBox4.Location = new System.Drawing.Point(5, 8);
            this.groupBox4.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox4.Size = new System.Drawing.Size(175, 183);
            this.groupBox4.TabIndex = 5;
            this.groupBox4.TabStop = false;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.radioButton4);
            this.groupBox2.Controls.Add(this.radioButton3);
            this.groupBox2.Controls.Add(this.txtSliceWidth);
            this.groupBox2.Controls.Add(this.radioButton2);
            this.groupBox2.Controls.Add(this.radioButton1);
            this.groupBox2.Location = new System.Drawing.Point(19, 57);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox2.Size = new System.Drawing.Size(145, 93);
            this.groupBox2.TabIndex = 3;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Slice Width";
            // 
            // radioButton4
            // 
            this.radioButton4.AutoSize = true;
            this.radioButton4.Location = new System.Drawing.Point(0, 70);
            this.radioButton4.Margin = new System.Windows.Forms.Padding(2);
            this.radioButton4.Name = "radioButton4";
            this.radioButton4.Size = new System.Drawing.Size(59, 16);
            this.radioButton4.TabIndex = 3;
            this.radioButton4.TabStop = true;
            this.radioButton4.Text = "Custom";
            this.radioButton4.UseVisualStyleBackColor = true;
            // 
            // radioButton3
            // 
            this.radioButton3.AutoSize = true;
            this.radioButton3.Location = new System.Drawing.Point(0, 53);
            this.radioButton3.Margin = new System.Windows.Forms.Padding(2);
            this.radioButton3.Name = "radioButton3";
            this.radioButton3.Size = new System.Drawing.Size(113, 16);
            this.radioButton3.TabIndex = 4;
            this.radioButton3.TabStop = true;
            this.radioButton3.Text = "1/4 of original";
            this.radioButton3.UseVisualStyleBackColor = true;
            // 
            // txtSliceWidth
            // 
            this.txtSliceWidth.Location = new System.Drawing.Point(62, 69);
            this.txtSliceWidth.Margin = new System.Windows.Forms.Padding(2);
            this.txtSliceWidth.Name = "txtSliceWidth";
            this.txtSliceWidth.Size = new System.Drawing.Size(68, 21);
            this.txtSliceWidth.TabIndex = 4;
            this.txtSliceWidth.Text = "1920";
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Location = new System.Drawing.Point(0, 37);
            this.radioButton2.Margin = new System.Windows.Forms.Padding(2);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(113, 16);
            this.radioButton2.TabIndex = 5;
            this.radioButton2.TabStop = true;
            this.radioButton2.Text = "1/2 of original";
            this.radioButton2.UseVisualStyleBackColor = true;
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Location = new System.Drawing.Point(0, 19);
            this.radioButton1.Margin = new System.Windows.Forms.Padding(2);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(113, 16);
            this.radioButton1.TabIndex = 6;
            this.radioButton1.TabStop = true;
            this.radioButton1.Text = "1/1 of original";
            this.radioButton1.UseVisualStyleBackColor = true;
            // 
            // txtDisplayWidth
            // 
            this.txtDisplayWidth.Location = new System.Drawing.Point(123, 15);
            this.txtDisplayWidth.Margin = new System.Windows.Forms.Padding(2);
            this.txtDisplayWidth.Mask = "9999";
            this.txtDisplayWidth.Name = "txtDisplayWidth";
            this.txtDisplayWidth.PromptChar = ' ';
            this.txtDisplayWidth.Size = new System.Drawing.Size(41, 21);
            this.txtDisplayWidth.TabIndex = 4;
            this.txtDisplayWidth.Text = "1440";
            this.txtDisplayWidth.ValidatingType = typeof(int);
            // 
            // txtDisplayHeight
            // 
            this.txtDisplayHeight.Location = new System.Drawing.Point(123, 35);
            this.txtDisplayHeight.Margin = new System.Windows.Forms.Padding(2);
            this.txtDisplayHeight.Name = "txtDisplayHeight";
            this.txtDisplayHeight.Size = new System.Drawing.Size(41, 21);
            this.txtDisplayHeight.TabIndex = 4;
            this.txtDisplayHeight.Text = "3160";
            // 
            // txtSliceHeight
            // 
            this.txtSliceHeight.Location = new System.Drawing.Point(112, 152);
            this.txtSliceHeight.Margin = new System.Windows.Forms.Padding(2);
            this.txtSliceHeight.Name = "txtSliceHeight";
            this.txtSliceHeight.Size = new System.Drawing.Size(53, 21);
            this.txtSliceHeight.TabIndex = 4;
            this.txtSliceHeight.Text = "32";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(17, 17);
            this.label19.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(83, 12);
            this.label19.TabIndex = 0;
            this.label19.Text = "Display Width";
            this.label19.Click += new System.EventHandler(this.label10_Click);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(17, 37);
            this.label17.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(89, 12);
            this.label17.TabIndex = 0;
            this.label17.Text = "Display Height";
            this.label17.Click += new System.EventHandler(this.label10_Click);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(17, 157);
            this.label16.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(77, 12);
            this.label16.TabIndex = 0;
            this.label16.Text = "Slice Height";
            this.label16.Click += new System.EventHandler(this.label10_Click);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(875, 345);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(17, 12);
            this.label14.TabIndex = 27;
            this.label14.Text = "0%";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(829, 199);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(108, 134);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 25;
            this.pictureBox1.TabStop = false;
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(829, 338);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(108, 20);
            this.progressBar1.TabIndex = 26;
            // 
            // listBox3_BMPFile
            // 
            this.listBox3_BMPFile.FormattingEnabled = true;
            this.listBox3_BMPFile.ItemHeight = 12;
            this.listBox3_BMPFile.Location = new System.Drawing.Point(725, 198);
            this.listBox3_BMPFile.Name = "listBox3_BMPFile";
            this.listBox3_BMPFile.Size = new System.Drawing.Size(99, 160);
            this.listBox3_BMPFile.TabIndex = 24;
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(725, 171);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(167, 21);
            this.textBox2.TabIndex = 23;
            // 
            // textBox1
            // 
            this.textBox1.AllowDrop = true;
            this.textBox1.BackColor = System.Drawing.SystemColors.WindowText;
            this.textBox1.Enabled = false;
            this.textBox1.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.textBox1.Location = new System.Drawing.Point(725, 363);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBox1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBox1.Size = new System.Drawing.Size(213, 77);
            this.textBox1.TabIndex = 21;
            // 
            // btnReload
            // 
            this.btnReload.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btnReload.Location = new System.Drawing.Point(901, 170);
            this.btnReload.Name = "btnReload";
            this.btnReload.Size = new System.Drawing.Size(32, 23);
            this.btnReload.TabIndex = 20;
            this.btnReload.Text = "......";
            this.btnReload.UseVisualStyleBackColor = true;
            // 
            // groupBox15
            // 
            this.groupBox15.Controls.Add(this.label21);
            this.groupBox15.Controls.Add(this.checkBox1);
            this.groupBox15.Controls.Add(this.btnPatternW);
            this.groupBox15.Controls.Add(this.label20);
            this.groupBox15.Controls.Add(this.label18);
            this.groupBox15.Controls.Add(this.numPatternB);
            this.groupBox15.Controls.Add(this.numPatternG);
            this.groupBox15.Controls.Add(this.numPatternR);
            this.groupBox15.Controls.Add(this.btnbleak);
            this.groupBox15.Controls.Add(this.btnred);
            this.groupBox15.Controls.Add(this.btnwhite);
            this.groupBox15.Controls.Add(this.btngreen);
            this.groupBox15.Controls.Add(this.btnblue);
            this.groupBox15.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.groupBox15.ForeColor = System.Drawing.Color.DarkViolet;
            this.groupBox15.Location = new System.Drawing.Point(725, 41);
            this.groupBox15.Name = "groupBox15";
            this.groupBox15.Size = new System.Drawing.Size(209, 123);
            this.groupBox15.TabIndex = 22;
            this.groupBox15.TabStop = false;
            this.groupBox15.Text = "Pattern";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.ForeColor = System.Drawing.Color.Lime;
            this.label21.Location = new System.Drawing.Point(72, 33);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(20, 17);
            this.label21.TabIndex = 8;
            this.label21.Text = "G:";
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Checked = true;
            this.checkBox1.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox1.Location = new System.Drawing.Point(25, 61);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(41, 21);
            this.checkBox1.TabIndex = 7;
            this.checkBox1.Text = "All";
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // btnPatternW
            // 
            this.btnPatternW.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btnPatternW.Location = new System.Drawing.Point(111, 56);
            this.btnPatternW.Name = "btnPatternW";
            this.btnPatternW.Size = new System.Drawing.Size(91, 26);
            this.btnPatternW.TabIndex = 4;
            this.btnPatternW.Text = "Showing";
            this.btnPatternW.UseVisualStyleBackColor = true;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label20.Location = new System.Drawing.Point(140, 33);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(19, 17);
            this.label20.TabIndex = 3;
            this.label20.Text = "B:";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.ForeColor = System.Drawing.Color.Red;
            this.label18.Location = new System.Drawing.Point(6, 33);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(19, 17);
            this.label18.TabIndex = 3;
            this.label18.Text = "R:";
            // 
            // numPatternB
            // 
            this.numPatternB.Location = new System.Drawing.Point(159, 29);
            this.numPatternB.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.numPatternB.Name = "numPatternB";
            this.numPatternB.Size = new System.Drawing.Size(43, 23);
            this.numPatternB.TabIndex = 2;
            this.numPatternB.Value = new decimal(new int[] {
            32,
            0,
            0,
            0});
            // 
            // numPatternG
            // 
            this.numPatternG.Location = new System.Drawing.Point(92, 29);
            this.numPatternG.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.numPatternG.Name = "numPatternG";
            this.numPatternG.Size = new System.Drawing.Size(43, 23);
            this.numPatternG.TabIndex = 2;
            this.numPatternG.Value = new decimal(new int[] {
            32,
            0,
            0,
            0});
            // 
            // numPatternR
            // 
            this.numPatternR.Location = new System.Drawing.Point(25, 29);
            this.numPatternR.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.numPatternR.Name = "numPatternR";
            this.numPatternR.Size = new System.Drawing.Size(43, 23);
            this.numPatternR.TabIndex = 2;
            this.numPatternR.Value = new decimal(new int[] {
            32,
            0,
            0,
            0});
            // 
            // btnbleak
            // 
            this.btnbleak.BackColor = System.Drawing.Color.Black;
            this.btnbleak.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btnbleak.Location = new System.Drawing.Point(177, 82);
            this.btnbleak.Name = "btnbleak";
            this.btnbleak.Size = new System.Drawing.Size(26, 29);
            this.btnbleak.TabIndex = 14;
            this.btnbleak.Text = "BL";
            this.btnbleak.UseVisualStyleBackColor = false;
            // 
            // btnred
            // 
            this.btnred.BackColor = System.Drawing.Color.Red;
            this.btnred.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btnred.Location = new System.Drawing.Point(25, 82);
            this.btnred.Name = "btnred";
            this.btnred.Size = new System.Drawing.Size(26, 29);
            this.btnred.TabIndex = 14;
            this.btnred.Text = "R";
            this.btnred.UseVisualStyleBackColor = false;
            // 
            // btnwhite
            // 
            this.btnwhite.BackColor = System.Drawing.Color.White;
            this.btnwhite.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btnwhite.Location = new System.Drawing.Point(139, 82);
            this.btnwhite.Name = "btnwhite";
            this.btnwhite.Size = new System.Drawing.Size(26, 29);
            this.btnwhite.TabIndex = 14;
            this.btnwhite.Text = "W";
            this.btnwhite.UseVisualStyleBackColor = false;
            // 
            // btngreen
            // 
            this.btngreen.BackColor = System.Drawing.Color.Green;
            this.btngreen.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btngreen.Location = new System.Drawing.Point(64, 82);
            this.btngreen.Name = "btngreen";
            this.btngreen.Size = new System.Drawing.Size(26, 29);
            this.btngreen.TabIndex = 14;
            this.btngreen.Text = "G";
            this.btngreen.UseVisualStyleBackColor = false;
            // 
            // btnblue
            // 
            this.btnblue.BackColor = System.Drawing.Color.Blue;
            this.btnblue.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btnblue.Location = new System.Drawing.Point(104, 82);
            this.btnblue.Name = "btnblue";
            this.btnblue.Size = new System.Drawing.Size(26, 29);
            this.btnblue.TabIndex = 14;
            this.btnblue.Text = "B";
            this.btnblue.UseVisualStyleBackColor = false;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.Y_VALUE);
            this.groupBox6.Controls.Add(this.X_VALUE);
            this.groupBox6.Controls.Add(this.btnLCD_Set_Disp_Window_cfg);
            this.groupBox6.Controls.Add(this.label416);
            this.groupBox6.Controls.Add(this.label412);
            this.groupBox6.Controls.Add(this.label15);
            this.groupBox6.Controls.Add(this.X0);
            this.groupBox6.Controls.Add(this.YY1);
            this.groupBox6.Controls.Add(this.XX0);
            this.groupBox6.Controls.Add(this.YY0);
            this.groupBox6.Controls.Add(this.XX1);
            this.groupBox6.Font = new System.Drawing.Font("新宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.groupBox6.ForeColor = System.Drawing.Color.DimGray;
            this.groupBox6.Location = new System.Drawing.Point(725, 459);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(212, 72);
            this.groupBox6.TabIndex = 97;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "开窗显示";
            // 
            // Y_VALUE
            // 
            this.Y_VALUE.AutoSize = true;
            this.Y_VALUE.Location = new System.Drawing.Point(279, 56);
            this.Y_VALUE.Name = "Y_VALUE";
            this.Y_VALUE.Size = new System.Drawing.Size(0, 14);
            this.Y_VALUE.TabIndex = 32;
            // 
            // X_VALUE
            // 
            this.X_VALUE.AutoSize = true;
            this.X_VALUE.Location = new System.Drawing.Point(279, 18);
            this.X_VALUE.Name = "X_VALUE";
            this.X_VALUE.Size = new System.Drawing.Size(0, 14);
            this.X_VALUE.TabIndex = 31;
            // 
            // btnLCD_Set_Disp_Window_cfg
            // 
            this.btnLCD_Set_Disp_Window_cfg.ForeColor = System.Drawing.Color.Black;
            this.btnLCD_Set_Disp_Window_cfg.Location = new System.Drawing.Point(183, 19);
            this.btnLCD_Set_Disp_Window_cfg.Name = "btnLCD_Set_Disp_Window_cfg";
            this.btnLCD_Set_Disp_Window_cfg.Size = new System.Drawing.Size(25, 52);
            this.btnLCD_Set_Disp_Window_cfg.TabIndex = 30;
            this.btnLCD_Set_Disp_Window_cfg.Text = "设定";
            this.btnLCD_Set_Disp_Window_cfg.UseVisualStyleBackColor = true;
            // 
            // label416
            // 
            this.label416.AutoSize = true;
            this.label416.Font = new System.Drawing.Font("Arial Narrow", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label416.Location = new System.Drawing.Point(79, 53);
            this.label416.Name = "label416";
            this.label416.Size = new System.Drawing.Size(22, 17);
            this.label416.TabIndex = 29;
            this.label416.Text = "Ye";
            // 
            // label412
            // 
            this.label412.AutoSize = true;
            this.label412.Font = new System.Drawing.Font("Arial Narrow", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label412.Location = new System.Drawing.Point(-4, 54);
            this.label412.Name = "label412";
            this.label412.Size = new System.Drawing.Size(22, 17);
            this.label412.TabIndex = 28;
            this.label412.Text = "Ys";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Arial Narrow", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(79, 23);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(22, 17);
            this.label15.TabIndex = 27;
            this.label15.Text = "Xe";
            // 
            // X0
            // 
            this.X0.AutoSize = true;
            this.X0.Font = new System.Drawing.Font("Arial Narrow", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.X0.Location = new System.Drawing.Point(-4, 24);
            this.X0.Name = "X0";
            this.X0.Size = new System.Drawing.Size(22, 17);
            this.X0.TabIndex = 26;
            this.X0.Text = "Xs";
            // 
            // YY1
            // 
            this.YY1.Location = new System.Drawing.Point(104, 53);
            this.YY1.Maximum = new decimal(new int[] {
            65536,
            0,
            0,
            0});
            this.YY1.Name = "YY1";
            this.YY1.Size = new System.Drawing.Size(53, 23);
            this.YY1.TabIndex = 25;
            // 
            // XX0
            // 
            this.XX0.Location = new System.Drawing.Point(17, 19);
            this.XX0.Maximum = new decimal(new int[] {
            65536,
            0,
            0,
            0});
            this.XX0.Name = "XX0";
            this.XX0.Size = new System.Drawing.Size(51, 23);
            this.XX0.TabIndex = 17;
            // 
            // YY0
            // 
            this.YY0.Font = new System.Drawing.Font("幼圆", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.YY0.Location = new System.Drawing.Point(17, 51);
            this.YY0.Maximum = new decimal(new int[] {
            65536,
            0,
            0,
            0});
            this.YY0.Name = "YY0";
            this.YY0.Size = new System.Drawing.Size(51, 23);
            this.YY0.TabIndex = 22;
            // 
            // XX1
            // 
            this.XX1.Location = new System.Drawing.Point(104, 21);
            this.XX1.Maximum = new decimal(new int[] {
            65536,
            0,
            0,
            0});
            this.XX1.Name = "XX1";
            this.XX1.Size = new System.Drawing.Size(53, 23);
            this.XX1.TabIndex = 23;
            // 
            // LabMOdelSelect
            // 
            this.LabMOdelSelect.AutoSize = true;
            this.LabMOdelSelect.Location = new System.Drawing.Point(209, 130);
            this.LabMOdelSelect.Name = "LabMOdelSelect";
            this.LabMOdelSelect.Size = new System.Drawing.Size(69, 20);
            this.LabMOdelSelect.TabIndex = 98;
            this.LabMOdelSelect.Text = "----------";
            // 
            // button7
            // 
            this.button7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button7.BackColor = System.Drawing.SystemColors.Window;
            this.button7.Font = new System.Drawing.Font("微软雅黑", 10F);
            this.button7.Location = new System.Drawing.Point(582, 136);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(88, 30);
            this.button7.TabIndex = 96;
            this.button7.Text = "Save";
            this.button7.UseVisualStyleBackColor = false;
            // 
            // txtRC_TGT_OFFSET_LO
            // 
            this.txtRC_TGT_OFFSET_LO.Location = new System.Drawing.Point(110, 47);
            this.txtRC_TGT_OFFSET_LO.Margin = new System.Windows.Forms.Padding(2);
            this.txtRC_TGT_OFFSET_LO.Name = "txtRC_TGT_OFFSET_LO";
            this.txtRC_TGT_OFFSET_LO.Size = new System.Drawing.Size(53, 21);
            this.txtRC_TGT_OFFSET_LO.TabIndex = 4;
            this.txtRC_TGT_OFFSET_LO.Text = "3";
            this.txtRC_TGT_OFFSET_LO.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtRC_EDGE_FACTOR
            // 
            this.txtRC_EDGE_FACTOR.Location = new System.Drawing.Point(110, 71);
            this.txtRC_EDGE_FACTOR.Margin = new System.Windows.Forms.Padding(2);
            this.txtRC_EDGE_FACTOR.Name = "txtRC_EDGE_FACTOR";
            this.txtRC_EDGE_FACTOR.Size = new System.Drawing.Size(53, 21);
            this.txtRC_EDGE_FACTOR.TabIndex = 4;
            this.txtRC_EDGE_FACTOR.Text = "3";
            this.txtRC_EDGE_FACTOR.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // toolTip1
            // 
            this.toolTip1.ToolTipTitle = "FIRST_LINE_BPG_OFFSET";
            // 
            // MainFrm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(1370, 579);
            this.Controls.Add(this.groupBox6);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.listBox3_BMPFile);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.btnReload);
            this.Controls.Add(this.tabCtl_Vesa);
            this.Controls.Add(this.groupBox15);
            this.Name = "MainFrm";
            this.Text = "Form1";
            this.tabCtl_Vesa.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Pixel_Clock)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.VFP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.VSW)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.HSW)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.VBP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.HBP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.HFP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.VACT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.HACT)).EndInit();
            this.ID_driver_group.ResumeLayout(false);
            this.ID_driver_group.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.groupBox11.ResumeLayout(false);
            this.groupBox11.PerformLayout();
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.groupBox15.ResumeLayout(false);
            this.groupBox15.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numPatternB)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numPatternG)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numPatternR)).EndInit();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.YY1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.XX0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.YY0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.XX1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl tabCtl_Vesa;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.NumericUpDown VFP;
        private System.Windows.Forms.NumericUpDown VSW;
        private System.Windows.Forms.NumericUpDown HSW;
        private System.Windows.Forms.NumericUpDown VBP;
        private System.Windows.Forms.NumericUpDown HBP;
        private System.Windows.Forms.NumericUpDown HFP;
        public System.Windows.Forms.NumericUpDown VACT;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        public System.Windows.Forms.NumericUpDown HACT;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.NumericUpDown Pixel_Clock;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.GroupBox ID_driver_group;
        private System.Windows.Forms.Button btnRun;
        private System.Windows.Forms.Label label664;
        private System.Windows.Forms.Button SSD28xx_Download;
        private System.Windows.Forms.Button btnMIPIDriverW;
        private System.Windows.Forms.TextBox txtMIPIDriver;
        private System.Windows.Forms.Label label_code_count;
        private System.Windows.Forms.ListBox lstMIPIDriver;
        private System.Windows.Forms.Button btnOpenCom;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.ListBox listBox3_BMPFile;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button btnReload;
        private System.Windows.Forms.GroupBox groupBox15;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.Button btnPatternW;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.NumericUpDown numPatternB;
        private System.Windows.Forms.NumericUpDown numPatternG;
        private System.Windows.Forms.NumericUpDown numPatternR;
        private System.Windows.Forms.Button btnbleak;
        private System.Windows.Forms.Button btnred;
        private System.Windows.Forms.Button btnwhite;
        private System.Windows.Forms.Button btngreen;
        private System.Windows.Forms.Button btnblue;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Label Y_VALUE;
        private System.Windows.Forms.Label X_VALUE;
        private System.Windows.Forms.Button btnLCD_Set_Disp_Window_cfg;
        private System.Windows.Forms.Label label416;
        private System.Windows.Forms.Label label412;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label X0;
        private System.Windows.Forms.NumericUpDown YY1;
        private System.Windows.Forms.NumericUpDown XX0;
        private System.Windows.Forms.NumericUpDown YY0;
        private System.Windows.Forms.NumericUpDown XX1;
        private System.IO.Ports.SerialPort serialPort1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RadioButton radioButton4;
        private System.Windows.Forms.RadioButton radioButton3;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.MaskedTextBox txtDisplayWidth;
        private System.Windows.Forms.MaskedTextBox txtDisplayHeight;
        private System.Windows.Forms.MaskedTextBox txtSliceHeight;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.MaskedTextBox txtSliceWidth;
        private System.Windows.Forms.MaskedTextBox txtRC_QUANT_INCR_LIMIT1;
        private System.Windows.Forms.MaskedTextBox txtRC_QUANT_INCR_LIMIT0;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.MaskedTextBox txtLINE_BUFFER_BPC;
        private System.Windows.Forms.MaskedTextBox txtRC_TGT_OFFSET_HI;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.CheckedListBox checkedListBox1;
        private System.Windows.Forms.ComboBox cmbxBITS_PER_COMPONENT;
        private System.Windows.Forms.ComboBox cmbxBITS_PER_PIXEL;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.GroupBox groupBox11;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.MaskedTextBox txtINITIAL_FULLNESS_OFFSET;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.MaskedTextBox txtINITIAL_DELAY;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.MaskedTextBox txtFIRST_LINE_BPG_OFFSET;
        private System.Windows.Forms.MaskedTextBox txtFLATNESS_MAX_QP;
        private System.Windows.Forms.MaskedTextBox txtFLATNESS_DET_THRESH;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.MaskedTextBox txtFLATNESS_MIN_QP;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.MaskedTextBox txtRC_MODEL_SIZE;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.MaskedTextBox txtRC_MINQP;
        private System.Windows.Forms.MaskedTextBox txtRC_MAXQP;
        private System.Windows.Forms.MaskedTextBox txtRC_BUF_THRESH;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.MaskedTextBox txtRC_OFFSET;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox txtLCD_CFG;
        private System.Windows.Forms.CheckBox chkDEPos;
        private System.Windows.Forms.CheckBox chkclkpos;
        private System.Windows.Forms.CheckBox checkBox2;
        private System.Windows.Forms.CheckBox chkVsync;
        private System.Windows.Forms.CheckBox chkHsync;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label LabMOdelSelect;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.MaskedTextBox txtRC_EDGE_FACTOR;
        private System.Windows.Forms.MaskedTextBox txtRC_TGT_OFFSET_LO;
        private System.Windows.Forms.ToolTip toolTip1;
    }
}

